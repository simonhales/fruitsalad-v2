import { generateEntry } from '../../src/firebase/bananart/game/entry/generator';
import mockPrompt from './mockPrompt';

const entryKey = 'ENTRY_KEY';
const entryOrder = 0;
const prompt = mockPrompt;
const userKey = 'USER_A_KEY';
const groupUserKey = 'USER_B_KEY';

const entry = generateEntry(entryKey, entryOrder, prompt, [userKey], [groupUserKey]);

export default entry;
