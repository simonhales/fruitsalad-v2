export default {
  key: 'PROMPT_KEY',
  disabled: false,
  familyFriendly: true,
  text: 'PROMPT_TEXT',
  tags: ['PROMPT_TAG'],
};
