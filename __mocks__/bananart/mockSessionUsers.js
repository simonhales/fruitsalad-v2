import { generateDummySessionUser } from '../../src/data/dummy/session';

export const MOCK_SESSION_FIVE_USERS = [
  generateDummySessionUser('simon'),
  generateDummySessionUser('chiao', false),
  generateDummySessionUser('aya'),
  generateDummySessionUser('lea'),
  generateDummySessionUser('pusheen'),
];

export const MOCK_SESSION_SEVEN_USERS = [
  generateDummySessionUser('simon'),
  generateDummySessionUser('chiao', false),
  generateDummySessionUser('aya'),
  generateDummySessionUser('lea'),
  generateDummySessionUser('pusheen'),
  generateDummySessionUser('boop'),
  generateDummySessionUser('woop'),
];
