import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
import LoadingView from './views/LoadingView/LoadingView';
import FirebaseController from './components/FirebaseController/FirebaseController';
import ReduxController from './components/ReduxController/ReduxController';
import NotFoundView from './views/NotFoundView/NotFoundView';
import DevCreateView from './views/dev/DevCreateView/DevCreateView';
import LandingView from './views/LandingView/LandingView';
import ControllerJoinView from './views/controller/ControllerJoinView/ControllerJoinView';
import DisplayView from './views/display/DisplayView/DisplayView';
import DevView from './views/dev/DevView/DevView';
import DebugView from './views/dev/DebugView/DebugView';

const LoadableControllerView = Loadable({
  loader: () => import('./components/FirebaseController/FirebaseController'),
  loading: LoadingView,
});

const LoadableDisplayView = Loadable({
  loader: () => import('./components/FirebaseDisplay/FirebaseDisplay'),
  loading: LoadingView,
});

const LoadablePreviewView = Loadable({
  loader: () => import('./components/display/FirebaseDisplayPreview/FirebaseDisplayPreview'),
  loading: LoadingView,
});

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route exact path="/" component={LandingView} />
          <Route exact path="/firebase" component={FirebaseController} />
          <Route exact path="/redux" component={ReduxController} />
          <Route exact path="/loading" component={LoadingView} />
          <Route exact path="/404" component={NotFoundView} />
          <Route exact path="/join" component={ControllerJoinView} />
          <Route path="/dev" component={DevView} />
          <Route exact path="/:id" component={LoadableControllerView} />
          <Route exact path="/:id/display" component={LoadableDisplayView} />
          <Route exact path="/:id/preview" component={LoadablePreviewView} />
          <Route exact path="/:id/display/raw" component={DisplayView} />
          <Route exact path="/:id/debug" component={DebugView} />
          <Route exact path="/:id/dev" component={DevCreateView} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
