// @flow

import type { SessionModel } from '../../firebase/bananart/session/models';
import {
  isSessionStatePending,
  isSessionStateStarting,
} from '../../firebase/bananart/session/state';
import { getCurrentGameFromSession } from '../../firebase/bananart/game/state';
import {
  getGameCurrentRound,
  getRoundFromGame,
  getRoundState,
} from '../../firebase/bananart/game/round/state';
import { roundStates } from '../../firebase/bananart/game/round/models';
import type { EntryModel, EntryStates } from '../../firebase/bananart/game/entry/models';
import { getEntryState, getGameCurrentEntry } from '../../firebase/bananart/game/entry/state';
import { entryStates } from '../../firebase/bananart/game/entry/models';

export function audioCheckSessionStateIsAtHub(session: SessionModel | null): boolean {
  if (!session) return false;
  const sessionPending = isSessionStatePending(session);
  return sessionPending;
}

export function audioCheckSessionStateIsAtStarting(session: SessionModel | null): boolean {
  if (!session) return false;
  const sessionPending = isSessionStateStarting(session);
  return sessionPending;
}

export function audioCheckRoundStateIsAtDrawingSubmitted(session: SessionModel | null): boolean {
  if (!session) return false;
  const game = getCurrentGameFromSession(session);
  if (!game) return false;
  const round = getGameCurrentRound(game);
  if (!round) return false;
  const roundState = getRoundState(round);
  if (roundState === roundStates.drawingSubmitted) return true;
  return false;
}

export function audioCheckRoundStateIsAtDrawingTimesUp(session: SessionModel | null): boolean {
  if (!session) return false;
  const game = getCurrentGameFromSession(session);
  if (!game) return false;
  const round = getGameCurrentRound(game);
  if (!round) return false;
  const roundState = getRoundState(round);
  if (roundState === roundStates.drawingTimesUp) return true;
  return false;
}

export function audioCheckRoundStateIsAtDrawing(session: SessionModel | null): boolean {
  if (!session) return false;
  const game = getCurrentGameFromSession(session);
  if (!game) return false;
  const round = getGameCurrentRound(game);
  if (!round) return false;
  const roundState = getRoundState(round);
  if (roundState === roundStates.drawing) return true;
  return false;
}

function isEntryState(session: SessionModel | null, state: EntryStates): boolean {
  if (!session) return false;
  const game = getCurrentGameFromSession(session);
  if (!game) return false;
  const entry = getGameCurrentEntry(game);
  if (!entry) return false;
  const entryState = getEntryState(entry);
  return entryState === state;
}

export function audioCheckEntryStateIsAtIntro(session: SessionModel | null): boolean {
  return isEntryState(session, entryStates.intro);
}

export function audioCheckEntryStateIsAtGuessingIntro(session: SessionModel | null): boolean {
  return isEntryState(session, entryStates.guessingIntro);
}

export function audioCheckEntryStateIsAtGuessingSubmitted(session: SessionModel | null): boolean {
  return isEntryState(session, entryStates.guessingSubmitted);
}

export function audioCheckEntryStateIsAtGuessingTimesUp(session: SessionModel | null): boolean {
  return isEntryState(session, entryStates.guessingTimesUp);
}

export function audioCheckEntryStateIsAtVotingIntro(session: SessionModel | null): boolean {
  return isEntryState(session, entryStates.votingIntro);
}

export function audioCheckEntryStateIsAtVotingSubmitted(session: SessionModel | null): boolean {
  return isEntryState(session, entryStates.votingSubmitted);
}

export function audioCheckEntryStateIsAtVotingTimesup(session: SessionModel | null): boolean {
  return isEntryState(session, entryStates.votingTimesUp);
}

export function audioCheckEntryStateIsAtResults(session: SessionModel | null): boolean {
  return isEntryState(session, entryStates.results);
}
