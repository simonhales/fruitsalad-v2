import { combineNames } from './messages';

describe('audio -> narrator -> messages', () => {
  test('names are combined correctly', () => {
    const names = ['simon', 'chiao', 'aya'];
    const combinedNames = combineNames(names);
    expect(combinedNames).toEqual('simon, chiao, and aya');
  });
  test('2 names are combined correctly', () => {
    const names = ['simon', 'chiao'];
    const combinedNames = combineNames(names);
    expect(combinedNames).toEqual('simon, and chiao');
  });
  test('1 name is returned correctly', () => {
    const names = ['simon'];
    const combinedNames = combineNames(names);
    expect(combinedNames).toEqual('simon');
  });
});
