// @flow

import assets from '../../utils/assets';

export type NarratorAsset = {
  key: string,
  url: string,
};

export default {
  welcomeToFruitSalad: {
    key: 'welcomeToFruitSalad',
    url: assets.narrator.welcomeToFruitSalad,
  },
  nomnomnom: {
    key: 'nomnomnom',
    url: assets.narrator.nomnomnom,
  },
  needFourPlayers: {
    key: 'needFourPlayers',
    url: assets.narrator.needFourPlayers,
  },
};
