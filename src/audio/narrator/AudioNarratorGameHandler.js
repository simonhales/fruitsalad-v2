// @flow
import { narratorLog } from 'utils/logging';
import { Narrator } from '../../rxjs/narrator';
import narratorAssets from './assets';
import type { SessionModel } from '../../firebase/bananart/session/models';
import {
  getSessionStartGameInitiatedTimestamp,
  getSessionState,
  getSessionUsers,
  isSessionStatePending,
} from '../../firebase/bananart/session/state';
import { sessionStates } from '../../firebase/bananart/session/models';
import type { SessionUserModel } from '../../firebase/bananart/session/user/models';
import {
  generateCorrectAnswerMessage,
  generateEntryIntroMessage,
  generateFakeAnswerMessage,
  generateGameStartingMessage,
  generateGetAllGuessesSubmittedMessage,
  generateGetDrawingMessage,
  generateGetGuessMessage,
  generateGetTimesUp,
  generateGetVotingMessage,
  generateGetVotingSubmittedMessage,
  generateGetVotingTimesUpMessage,
  generateGoodbyeUsersMessage,
  generateRoundDrawingSubmittedMessage,
  generateTimesUpMessage,
  generateWelcomeMultipleUsersToFruitSaladMessage,
  generateWelcomeUsersMessage,
} from './messages';
import { convertSecondsToMilis, getFutureTimestamp } from '../../utils/time';
import {
  audioCheckSessionStateIsAtStarting,
  audioCheckSessionStateIsAtHub,
  audioCheckRoundStateIsAtDrawingSubmitted,
  audioCheckRoundStateIsAtDrawingTimesUp,
  audioCheckRoundStateIsAtDrawing,
  audioCheckEntryStateIsAtIntro,
  audioCheckEntryStateIsAtGuessingIntro,
  audioCheckEntryStateIsAtGuessingSubmitted,
  audioCheckEntryStateIsAtGuessingTimesUp,
  audioCheckEntryStateIsAtVotingIntro,
  audioCheckEntryStateIsAtVotingSubmitted,
  audioCheckEntryStateIsAtVotingTimesup,
  audioCheckEntryStateIsAtResults,
} from './state';
import {
  getCurrentGameFromSession,
  getGameState,
  getGameUserViaKey,
} from '../../firebase/bananart/game/state';
import { gameStates } from '../../firebase/bananart/game/models';
import { getGameCurrentRound, getRoundState } from '../../firebase/bananart/game/round/state';
import type { GameModel } from '../../firebase/bananart/game/models';
import { roundStates } from '../../firebase/bananart/game/round/models';
import type { RoundModel } from '../../firebase/bananart/game/round/models';
import {
  getAnswerRevealDelay,
  getEntryAnswerIndex,
  getEntryAnswerViaAnswerIndex,
  getEntryArtistKeysList,
  getEntryState,
  getGameCurrentEntry,
} from '../../firebase/bananart/game/entry/state';
import { entryStates } from '../../firebase/bananart/game/entry/models';
import type { EntryAnswerModel, EntryModel } from '../../firebase/bananart/game/entry/models';

function loadInitialAssets(narrator: Narrator) {
  narrator.loadAssetTrack(narratorAssets.welcomeToFruitSalad);
  narrator.loadAssetTrack(narratorAssets.nomnomnom);
  narrator.loadAssetTrack(narratorAssets.needFourPlayers);
}

function triggerRoundDrawingNarration(narrator: Narrator, game: GameModel, round: RoundModel) {
  const message = generateGetDrawingMessage();
  const key = message;
  const uniqueKey = `ROUND_DRAWING_${round.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playResolution: audioCheckRoundStateIsAtDrawing,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerRoundDrawingTimesUpNarration(
  narrator: Narrator,
  game: GameModel,
  round: RoundModel
) {
  const message = generateTimesUpMessage();
  const key = message;
  const uniqueKey = `ROUND_DRAWING_TIMES_UP_${round.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playResolution: audioCheckRoundStateIsAtDrawingTimesUp,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerRoundDrawingSubmittedNarration(
  narrator: Narrator,
  game: GameModel,
  round: RoundModel
) {
  const message = generateRoundDrawingSubmittedMessage();
  const key = message;
  const uniqueKey = `ROUND_DRAWING_SUBMITTED_${round.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playResolution: audioCheckRoundStateIsAtDrawingSubmitted,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerEntryIntroNarration(narrator, game: GameModel, entry: EntryModel) {
  const artists = getEntryArtistKeysList(entry);
  const users = artists.map(userKey => getGameUserViaKey(game, userKey));
  const names = users.map(user => user.name);
  const message = generateEntryIntroMessage(names);
  const key = message;
  const uniqueKey = `ENTRY_INTRO_${entry.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playResolution: audioCheckEntryStateIsAtIntro,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerEntryGuessingNarration(narrator, game: GameModel, entry: EntryModel) {
  const message = generateGetGuessMessage();
  const key = message;
  const uniqueKey = `ENTRY_GUESS_${entry.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playResolution: audioCheckEntryStateIsAtGuessingIntro,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerEntryGuessingCompletedNarration(narrator, game: GameModel, entry: EntryModel) {
  const message = generateGetAllGuessesSubmittedMessage();
  const key = message;
  const uniqueKey = `ENTRY_GUESS_COMPLETED_${entry.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playImmediately: true,
      playResolution: audioCheckEntryStateIsAtGuessingSubmitted,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerEntryGuessingTimesUpNarration(narrator, game: GameModel, entry: EntryModel) {
  const message = generateGetTimesUp();
  const key = message;
  const uniqueKey = `ENTRY_GUESS_TIMES_UP_${entry.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playImmediately: true,
      playResolution: audioCheckEntryStateIsAtGuessingTimesUp,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerEntryVotingIntroNarration(narrator, game: GameModel, entry: EntryModel) {
  const message = generateGetVotingMessage();
  const key = message;
  const uniqueKey = `ENTRY_VOTING_INTRO_${entry.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playImmediately: true,
      playResolution: audioCheckEntryStateIsAtVotingIntro,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerEntryVotingSubmittedNarration(narrator, game: GameModel, entry: EntryModel) {
  const message = generateGetVotingSubmittedMessage();
  const key = message;
  const uniqueKey = `ENTRY_VOTING_SUBMITTED_${entry.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playImmediately: true,
      playResolution: audioCheckEntryStateIsAtVotingSubmitted,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerEntryVotingTimesUpNarration(narrator, game: GameModel, entry: EntryModel) {
  const message = generateGetVotingTimesUpMessage();
  const key = message;
  const uniqueKey = `ENTRY_VOTING_TIMES_UP_${entry.key}`;
  narrator.generateAndAddToQueue(
    key,
    {
      playImmediately: true,
      playResolution: audioCheckEntryStateIsAtVotingTimesup,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerEntryAnswerRevealedNarration(
  narrator,
  game: GameModel,
  entry: EntryModel,
  answer: EntryAnswerModel
) {
  const message = answer.prompt ? generateCorrectAnswerMessage() : generateFakeAnswerMessage();
  const key = message;
  const uniqueKey = `ENTRY_ANSWER_REVEALED_${answer.key}_${entry.key}`;
  const timerDelay = getAnswerRevealDelay(entry, answer.key);
  narrator.generateAndAddToQueue(
    key,
    {
      playImmediately: true,
      playResolution: audioCheckEntryStateIsAtResults,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(2)),
      waitToPlayTimestamp: getFutureTimestamp(timerDelay),
    },
    message,
    uniqueKey
  );
}

function triggerEntryResultsNarration(narrator, game: GameModel, entry: EntryModel) {
  const answerIndex = getEntryAnswerIndex(entry);
  const answer = answerIndex > -1 ? getEntryAnswerViaAnswerIndex(entry, answerIndex) : null;
  if (!answer) return;
  triggerEntryAnswerRevealedNarration(narrator, game, entry, answer);
}

function triggerRoundVotingNarration(narrator: Narrator, game: GameModel, round: RoundModel) {
  const entry = getGameCurrentEntry(game);
  if (!entry) return;
  const entryState = getEntryState(entry);

  switch (entryState) {
    case entryStates.intro:
      triggerEntryIntroNarration(narrator, game, entry);
      break;
    case entryStates.guessingIntro:
      triggerEntryGuessingNarration(narrator, game, entry);
      break;
    case entryStates.guessingSubmitted:
      triggerEntryGuessingCompletedNarration(narrator, game, entry);
      break;
    case entryStates.guessingTimesUp:
      triggerEntryGuessingTimesUpNarration(narrator, game, entry);
      break;
    case entryStates.votingIntro:
      triggerEntryVotingIntroNarration(narrator, game, entry);
      break;
    case entryStates.votingSubmitted:
      triggerEntryVotingSubmittedNarration(narrator, game, entry);
      break;
    case entryStates.votingTimesUp:
      triggerEntryVotingTimesUpNarration(narrator, game, entry);
      break;
    case entryStates.results:
      triggerEntryResultsNarration(narrator, game, entry);
      break;
    default:
      break;
  }
}

function triggerRoundNarration(narrator: Narrator, game: GameModel, round: RoundModel) {
  const roundState = getRoundState(round);

  switch (roundState) {
    case roundStates.drawing:
      triggerRoundDrawingNarration(narrator, game, round);
      break;
    case roundStates.drawingSubmitted:
      triggerRoundDrawingSubmittedNarration(narrator, game, round);
      break;
    case roundStates.drawingTimesUp:
      triggerRoundDrawingTimesUpNarration(narrator, game, round);
      break;
    case roundStates.voting:
      triggerRoundVotingNarration(narrator, game, round);
      break;
    default:
      break;
  }
}

function triggerGamePlayingNarration(narrator: Narrator, game: GameModel) {
  const round = getGameCurrentRound(game);

  if (!round) return;

  triggerRoundNarration(narrator, game, round);
}

function triggerGameNarration(narrator: Narrator, session: SessionModel) {
  const game = getCurrentGameFromSession(session);

  if (!game) return;

  const gameState = getGameState(game);

  switch (gameState) {
    case gameStates.playing:
      triggerGamePlayingNarration(narrator, game);
      break;
    default:
      break;
  }
}

function triggerWelcomeToFruitSalad(narrator: Narrator) {
  narrator.loadAndAddToQueue(
    narratorAssets.welcomeToFruitSalad,
    {
      playResolution: audioCheckSessionStateIsAtHub,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    narratorAssets.welcomeToFruitSalad.key
  );
}

function triggerWelcomeMultipleUsersToFruitSalad(
  narrator: Narrator,
  users: Array<SessionUserModel>
) {
  const usersNames = users.map(user => user.name);
  const message = generateWelcomeMultipleUsersToFruitSaladMessage(usersNames);
  const uniqueKey = message;
  narratorLog.log(`Welcome message for multiple users to Fruit Salad: ${message}`);
  narrator.generateAndAddToQueue(
    uniqueKey,
    {
      playResolution: audioCheckSessionStateIsAtHub,
      maxPlayTimestamp: getFutureTimestamp(convertSecondsToMilis(5)),
    },
    message,
    uniqueKey
  );
}

function triggerWelcomeMultipleUsers(narrator: Narrator, users: Array<SessionUserModel>) {
  const usersNames = users.map(user => user.name);
  const usersKeys = users.map(user => user.key);
  const message = generateWelcomeUsersMessage(usersNames);
  const uniqueKey = `triggerWelcomeMultipleUsers_${usersKeys.join(',')}`;
  narratorLog.log(`Welcome message for multiple users: ${message}`);
  narrator.generateAndAddToQueue(
    message,
    {
      playResolution: audioCheckSessionStateIsAtHub,
    },
    message,
    uniqueKey
  );
}

function triggerGoodbyeMultipleUsers(narrator: Narrator, users: Array<SessionUserModel>) {
  const usersNames = users.map(user => user.name);
  const usersKeys = users.map(user => user.key);
  const message = generateGoodbyeUsersMessage(usersNames);
  const uniqueKey = `triggerGoodbyeMultipleUsers${usersKeys.join(',')}`;
  narratorLog.log(`Goodbye message for multiple users: ${message}`);
  narrator.generateAndAddToQueue(
    message,
    {
      playResolution: audioCheckSessionStateIsAtHub,
    },
    message,
    uniqueKey
  );
}

function triggerWelcomeUserMessages(
  narrator: Narrator,
  session: SessionModel,
  previousSession: SessionModel | null
) {
  const currentUsers = getSessionUsers(session);
  const currentUsersKeys = currentUsers.map(user => user.key);
  const previousUsers = previousSession ? getSessionUsers(previousSession) : [];
  const previousUsersKeys = previousUsers.map(user => user.key);
  const newUsers = currentUsers.filter(user => !previousUsersKeys.includes(user.key));
  const oldUsers = previousUsers.filter(user => !currentUsersKeys.includes(user.key));
  if (!previousSession || (previousSession && !isSessionStatePending(previousSession))) {
    // assume this is the first iteration

    if (currentUsers.length > 0) {
      triggerWelcomeMultipleUsersToFruitSalad(narrator, currentUsers);
    } else {
      // play generic welcome message
      triggerWelcomeToFruitSalad(narrator);
    }
  } else {
    if (newUsers.length > 0) {
      triggerWelcomeMultipleUsers(narrator, newUsers);
    }
    if (oldUsers.length > 0) {
      triggerGoodbyeMultipleUsers(narrator, oldUsers);
    }
  }
}

function triggerGameAboutToStartMessage(narrator: Narrator, session: SessionModel) {
  const users = getSessionUsers(session);
  const usersNames = users.map(user => user.name);
  const message = generateGameStartingMessage(usersNames);
  const uniqueKey = getSessionStartGameInitiatedTimestamp(session).toString();
  narratorLog.log(`Game about to start: ${message}`);
  narrator.generateAndAddToQueue(
    message,
    {
      playResolution: audioCheckSessionStateIsAtStarting,
      playImmediately: true,
    },
    message,
    uniqueKey
  );
}

function triggerSessionHubSounds(
  narrator: Narrator,
  session: SessionModel,
  previousSessionState: SessionModel | null
) {
  narratorLog.log(`Trigger Session Hub Sounds`);
  triggerWelcomeUserMessages(narrator, session, previousSessionState);
}

function triggerSessionStartingSounds(narrator: Narrator, session: SessionModel) {
  triggerGameAboutToStartMessage(narrator, session);
}

function triggerGameSounds(
  narrator: Narrator,
  session: SessionModel,
  previousSessionState: SessionModel | null
) {
  const sessionState = getSessionState(session);
  switch (sessionState) {
    case sessionStates.pending:
      triggerSessionHubSounds(narrator, session, previousSessionState);
      break;
    case sessionStates.starting:
      triggerSessionStartingSounds(narrator, session);
      break;
    case sessionStates.playing:
      triggerGameNarration(narrator, session);
      break;
    default:
      break;
  }
}

export class AudioNarratorGameHandler {
  narrator: Narrator;

  constructor(session: SessionModel | null) {
    this.narrator = new Narrator();
    this.narrator.startNarrator();
    this.loadInitialAssets();
    this.triggerGameSounds(session, null);
  }

  loadInitialAssets() {
    loadInitialAssets(this.narrator);
  }

  triggerGameSounds(session: SessionModel | null, previousSessionState: SessionModel | null) {
    if (!session) return;
    triggerGameSounds(this.narrator, session, previousSessionState);
  }
}
