// @flow

export function combineNames(names: Array<string>): string {
  if (names.length === 1) return names[0];
  const lastName = names.pop();
  const commaNames = names.join(', ');
  return `${commaNames}, and ${lastName}`;
}

export function generateWelcomeMultipleUsersToFruitSaladMessage(names: Array<string>): string {
  const combinedNames = combineNames(names);
  return `Konichiwa and welcome ${combinedNames}, to fruit salad party!`;
}

export function generateWelcomeUsersMessage(names: Array<string>): string {
  const combinedNames = combineNames(names);
  return `Welcome ${combinedNames}!`;
}

export function generateGoodbyeUsersMessage(names: Array<string>): string {
  const combinedNames = combineNames(names);
  return `Farewell ${combinedNames}!`;
}

export function generateGameStartingMessage(names: Array<string>): string {
  const combinedNames = combineNames(names);
  return `The game is about to start! Today we have playing with us: ${combinedNames}`;
}

export function generateEntryIntroMessage(names: Array<string>): string {
  const combinedNames = combineNames(names);
  return `Art by ${combinedNames}`;
}

// todo - create static audio asset
export function generateRoundDrawingSubmittedMessage(): string {
  return `All drawings have been submitted!`;
}

// todo - create static audio asset
export function generateTimesUpMessage(): string {
  return `Time's up!`;
}

// todo - create static audio asset
export function generateGetDrawingMessage(): string {
  return `Check your fone and get drawing!`;
}

// todo - create static audio asset
export function generateGetGuessMessage(): string {
  return `Guess what they were drawing.`;
}

// todo - create static audio asset
export function generateGetAllGuessesSubmittedMessage(): string {
  return `All guesses submitted!`;
}

// todo - create static audio asset
export function generateGetTimesUp(): string {
  return `Times up!`;
}

// todo - create static audio asset
export function generateGetVotingMessage(): string {
  return `Vote for which one you think is the correct answer.`;
}

// todo - create static audio asset
export function generateGetVotingSubmittedMessage(): string {
  return `All votes submitted!`;
}

// todo - create static audio asset
export function generateGetVotingTimesUpMessage(): string {
  return `Times up!`;
}

// todo - create static audio asset
export function generateCorrectAnswerMessage(): string {
  return `Correct answer!`;
}

// todo - create static audio asset
export function generateFakeAnswerMessage(): string {
  return `Fake answer!`;
}
