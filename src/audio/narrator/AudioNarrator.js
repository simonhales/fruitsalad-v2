// @flow
import { audioLog } from 'utils/logging';
import type { SessionModel } from '../../firebase/bananart/session/models';
import { getNarratorMuted } from '../../utils/env';
import { timeout } from '../../utils/async';

export type AudioPlayConditions = {
  playImmediately?: boolean,
  playResolution: (state: SessionModel) => boolean,
  onPlay?: () => void,
  onComplete?: () => void,
  maxPlayTimestamp?: number,
  waitToPlayTimestamp?: number,
};
export type AudioTrack = AudioPlayConditions & {
  key: string,
  uniqueKey?: string,
};
export type AudioTrackWithHowler = AudioTrack & {
  audioHowler: any,
};

function getTrackWaitingKey(track: AudioTrackWithHowler) {
  return track.uniqueKey ? track.uniqueKey : track.key;
}

class AudioNarrator {
  audioInProgress = false;

  muted = getNarratorMuted();

  session: SessionModel;

  currentTrack: AudioTrackWithHowler;

  queue: Array<AudioTrackWithHowler> = [];

  waitingToPlay: string = '';

  addTrackToQueue(track: AudioTrack, audioHowler: any) {
    const combinedTrack: AudioTrackWithHowler = {
      ...track,
      audioHowler,
    };
    if (combinedTrack.playImmediately && this.canTrackBePlayed(combinedTrack)) {
      this.playTrackImmediately(combinedTrack);
      return;
    }
    audioLog.log(`Add track to queue: ${combinedTrack.key}`);
    this.queue.push(combinedTrack);
    audioLog.log(`Queue is now`, this.queue);
    this.startPlaying();
  }

  async playTrackImmediately(track: AudioTrackWithHowler) {
    if (track.waitToPlayTimestamp) {
      const waitingKey = getTrackWaitingKey(track);
      this.waitingToPlay = waitingKey;
      const timer = (track.waitToPlayTimestamp ? track.waitToPlayTimestamp : 0) - Date.now();
      if (timer > 0) {
        await timeout(timer);
      }
      if (this.waitingToPlay !== waitingKey) {
        return;
      }
    }
    if (this.currentTrack) {
      this.currentTrack.audioHowler.stop();
    }
    this.playTrack(track);
  }

  startPlaying() {
    if (this.audioInProgress) return;
    audioLog.log(`Start playing`);
    this.playNextQueuedTrack();
  }

  playNextQueuedTrack() {
    if (this.queue.length === 0) {
      audioLog.log(`No more tracks in the queue`);
      this.finishedPlaying();
      return;
    }
    const track = this.queue.shift();
    if (this.canTrackBePlayed(track)) {
      this.playTrack(track);
    } else {
      audioLog.log(`Track ${track.key} can't be played, so skipping it.`);
      this.playNextQueuedTrack();
    }
  }

  playTrack(track: AudioTrackWithHowler) {
    audioLog.log(`Playing track ${track.key}`, track);
    this.waitingToPlay = '';
    this.audioInProgress = true;
    this.currentTrack = track;
    const { audioHowler, onPlay } = track;
    audioHowler.once('end', () => {
      this.completedTrack(track);
    });
    audioHowler.mute(this.muted);
    audioHowler.play();
    if (onPlay) {
      onPlay();
    }
  }

  completedTrack(track: AudioTrackWithHowler) {
    audioLog.log(`Completed track ${track.key}`);
    const { onComplete } = track;
    if (onComplete) {
      onComplete();
    }
    this.playNextQueuedTrack();
  }

  canTrackBePlayed(track: AudioTrackWithHowler): boolean {
    const { maxPlayTimestamp, playResolution } = track;
    const validTime = maxPlayTimestamp ? Date.now() <= maxPlayTimestamp : true;
    const validState = playResolution ? playResolution(this.session) : true;
    return validTime && validState;
  }

  finishedPlaying() {
    audioLog.log(`Finished playing`);
    this.audioInProgress = false;
  }

  setSession(session: SessionModel) {
    this.session = session;
  }

  setMuted(muted: boolean) {
    this.muted = muted;
    if (this.currentTrack) {
      this.currentTrack.audioHowler.mute(muted);
    }
  }
}

export const audioNarrator = new AudioNarrator();
