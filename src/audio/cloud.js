// @flow
import 'isomorphic-fetch';

const CLOUD_API_URL =
  'https://texttospeech.googleapis.com/v1beta1/text:synthesize?key=AIzaSyAXlroI4XdHzpXCDHFW41UviWaCtNrz4-w';

export type GoogleTextCloudResponse = {
  audioContent: string,
};

export function generateCloudMessage(message: string): Promise<GoogleTextCloudResponse> {
  const url = CLOUD_API_URL;

  const options = {
    method: 'POST',
    headers: new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json',
    }),
    body: JSON.stringify({
      audioConfig: {
        audioEncoding: 'MP3',
        effectsProfileId: ['large-home-entertainment-class-device'],
        pitch: '-2.00',
        speakingRate: '1.0',
      },
      input: {
        text: message,
      },
      voice: {
        // languageCode: 'en-GB',
        // name: 'en-GB-Wavenet-A',
        // languageCode: 'tr-TR',
        // name: 'tr-TR-Wavenet-A',
        // languageCode: 'en-GB',
        // name: 'en-GB-Wavenet-C',
        // languageCode: 'de-DE',
        // name: 'de-DE-Wavenet-A',
        languageCode: 'ja-JP',
        name: 'ja-JP-Wavenet-A',
      },
    }),
  };

  return fetch(url, options).then(response => response.json());
}
