// @flow
import { musicLog } from 'utils/logging';
import type { SessionModel } from '../../firebase/bananart/session/models';
import { getSessionState } from '../../firebase/bananart/session/state';
import { sessionStates } from '../../firebase/bananart/session/models';
import {
  drawingBackgroundMusic,
  entryCompletedBackgroundMusic,
  entryResultsBackgroundMusic,
  guessingBackgroundMusic,
  hubBackgroundMusic,
  votingBackgroundMusic,
} from './tracks';
import { audioMusic } from './AudioMusic';
import { getCurrentGameFromSession, getGameState } from '../../firebase/bananart/game/state';
import { gameStates } from '../../firebase/bananart/game/models';
import type { GameModel } from '../../firebase/bananart/game/models';
import { getGameCurrentRound, getRoundState } from '../../firebase/bananart/game/round/state';
import { roundStates } from '../../firebase/bananart/game/round/models';
import type { RoundModel } from '../../firebase/bananart/game/round/models';
import { getEntryState, getGameCurrentEntry } from '../../firebase/bananart/game/entry/state';
import { entryStates } from '../../firebase/bananart/game/entry/models';
import type { EntryModel } from '../../firebase/bananart/game/entry/models';

function triggerSessionPendingMusic(session: SessionModel) {
  audioMusic.addTrackToQueue(hubBackgroundMusic);
}

function triggerRoundDrawingMusic(round: RoundModel) {
  const track = {
    ...drawingBackgroundMusic,
    key: `DRAWING_${round.key}`,
  };
  audioMusic.addTrackToQueue(track);
}

function triggerEntryGuessingMusic(entry: EntryModel) {
  const track = {
    ...guessingBackgroundMusic,
    key: `GUESSING_${entry.key}`,
  };
  audioMusic.addTrackToQueue(track);
}

function triggerEntryVotingMusic(entry: EntryModel) {
  const track = {
    ...votingBackgroundMusic,
    key: `VOTING_${entry.key}`,
  };
  audioMusic.addTrackToQueue(track);
}

function triggerEntryResultsMusic(entry: EntryModel) {
  const track = {
    ...entryResultsBackgroundMusic,
    key: `ENTRY_RESULTS_${entry.key}`,
  };
  audioMusic.addTrackToQueue(track);
}

function triggerEntryCompletedMusic(entry: EntryModel) {
  const track = {
    ...entryCompletedBackgroundMusic,
    key: `ENTRY_COMPLETED_${entry.key}`,
  };
  audioMusic.addTrackToQueue(track);
}

function triggerRoundVotingMusic(round: RoundModel, game: GameModel) {
  const entry = getGameCurrentEntry(game);
  if (!entry) return;
  const entryState = getEntryState(entry);
  switch (entryState) {
    case entryStates.guessingIntro:
    case entryStates.guessing:
      triggerEntryGuessingMusic(entry);
      break;
    case entryStates.votingIntro:
    case entryStates.voting:
      triggerEntryVotingMusic(entry);
      break;
    case entryStates.results:
      triggerEntryResultsMusic(entry);
      break;
    case entryStates.completed:
      triggerEntryCompletedMusic(entry);
      break;
    default:
      break;
  }
}

function triggerGamePlayingMusic(game: GameModel) {
  const round = getGameCurrentRound(game);
  if (!round) return;
  const roundState = getRoundState(round);
  switch (roundState) {
    case roundStates.drawingIntro:
    case roundStates.drawing:
      triggerRoundDrawingMusic(round);
      break;
    case roundStates.voting:
      triggerRoundVotingMusic(round, game);
      break;
    default:
      break;
  }
}

function triggerSessionPlayingMusic(session: SessionModel) {
  const game = getCurrentGameFromSession(session);
  if (!game) return;
  const gameState = getGameState(game);
  switch (gameState) {
    case gameStates.playing:
      triggerGamePlayingMusic(game);
      break;
    default:
      break;
  }
}

function triggerGameMusic(session: SessionModel, previousSessionState: SessionModel | null) {
  const sessionState = getSessionState(session);
  switch (sessionState) {
    case sessionStates.pending:
      triggerSessionPendingMusic(session);
      break;
    case sessionStates.playing:
      triggerSessionPlayingMusic(session);
      break;
    default:
      break;
  }
}

export class AudioMusicGameHandler {
  constructor(session: SessionModel | null) {
    musicLog.log(`Created AudioMusicGameHandler`);
    this.triggerGameMusic(session, null);
  }

  triggerGameMusic(session: SessionModel | null, previousSessionState: SessionModel | null) {
    if (!session) return;
    musicLog.log(`Triggering game music.`);
    triggerGameMusic(session, previousSessionState);
  }
}
