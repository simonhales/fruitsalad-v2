// @flow

import type { AudioMusicTrack } from './tracks';
import { getMusicMuted } from '../../utils/env';
import { musicLog } from '../../utils/logging';

class AudioMusic {
  audioInProgress: boolean = false;

  muted: boolean = getMusicMuted();

  playingTrack: AudioMusicTrack | null = null;

  nextTrack: AudioMusicTrack | null = null;

  addTrackToQueue(track: AudioMusicTrack) {
    if (track.playImmediately) {
      musicLog.log(`Playing music track immediately.`);
      this.playTrackImmediately(track);
    } else {
      musicLog.log(`Adding music track to queue.`);
      this.startPlaying(track);
    }
  }

  clearQueue() {
    musicLog.log(`Clearing queue.`);
    this.nextTrack = null;
  }

  playTrackImmediately(track: AudioMusicTrack) {
    this.clearQueue();
    if (this.playingTrack && this.playingTrack.key === track.key) return;
    this.stopPlayingTrack();
    this.playTrack(track);
  }

  playTrack(track: AudioMusicTrack) {
    this.audioInProgress = true;
    this.playingTrack = track;
    if (this.playingTrack) {
      this.playingTrack.audioHowler.once('end', () => {
        this.playingTrackEnded();
      });
    }
    if (this.playingTrack) {
      this.playingTrack.audioHowler.mute(this.muted);
    }
    if (this.playingTrack) {
      this.playingTrack.audioHowler.play();
    }
  }

  playingTrackEnded() {
    musicLog.log(`Playing track ended.`);
    this.audioInProgress = false;
    if (this.playingTrack && !this.nextTrack) {
      if (this.playingTrack.loop) {
        this.playTrack(this.playingTrack);
      }
    } else if (this.nextTrack) {
      this.playNextTrack();
    }
  }

  playNextTrack() {
    const track = this.nextTrack;
    if (track) {
      this.clearQueue();
      this.playTrack(track);
    }
  }

  stopPlayingTrack() {
    musicLog.log(`Stop playing track.`);
    if (this.playingTrack) {
      const track = this.playingTrack;
      track.audioHowler.off('end');
      if (track.fadeOutOnStop) {
        track.audioHowler.fade(track.volume, 0, 1000);
        track.audioHowler.once('fade', () => {
          track.audioHowler.volume(track.volume);
          track.audioHowler.stop();
        });
      } else {
        track.audioHowler.stop();
      }
    }
  }

  startPlaying(track: AudioMusicTrack) {
    if (this.playingTrack) {
      if (this.playingTrack.key === track.key) return;
      if (this.nextTrack && this.nextTrack.key === track.key) return;
      this.nextTrack = track;
    } else {
      this.playTrack(track);
    }
  }

  setMuted(muted: boolean) {
    this.muted = muted;
    if (this.playingTrack) {
      this.playingTrack.audioHowler.mute(muted);
    }
  }
}

export const audioMusic = new AudioMusic();
