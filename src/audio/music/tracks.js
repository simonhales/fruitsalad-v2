// @flow

import assets from '../../utils/assets';
import { generateHowlerAudio } from '../utils';

const FULLY_CHARGED_VOLUME = 0.3;
const MUSIC_VOLUME = 0.5;
const FLAMINGO_VOLUME = 0.5;

export type AudioMusicTrack = {
  key: string,
  playImmediately: boolean,
  loop: boolean,
  fadeOutOnStop: boolean,
  audioHowler: any,
  volume: number,
};

export const hubBackgroundMusic: AudioMusicTrack = {
  key: 'HUB_BACKGROUND_MUSIC',
  playImmediately: true,
  loop: true,
  fadeOutOnStop: true,
  volume: FLAMINGO_VOLUME,
  audioHowler: generateHowlerAudio(assets.music.flamingoLoucoFull, FLAMINGO_VOLUME),
};

export const drawingBackgroundMusic: AudioMusicTrack = {
  key: 'DRAWING_BACKGROUND_MUSIC',
  playImmediately: true,
  loop: false,
  fadeOutOnStop: true,
  volume: FULLY_CHARGED_VOLUME,
  audioHowler: generateHowlerAudio(assets.music.fullyChargedFull, FULLY_CHARGED_VOLUME),
};

export const guessingBackgroundMusic: AudioMusicTrack = {
  key: 'GUESSING_BACKGROUND_MUSIC',
  playImmediately: true,
  loop: false,
  fadeOutOnStop: true,
  volume: MUSIC_VOLUME,
  audioHowler: generateHowlerAudio(assets.music.shortBandito, MUSIC_VOLUME),
};

export const votingBackgroundMusic: AudioMusicTrack = {
  key: 'VOTING_BACKGROUND_MUSIC',
  playImmediately: true,
  loop: false,
  fadeOutOnStop: true,
  volume: MUSIC_VOLUME,
  audioHowler: generateHowlerAudio(assets.music.shortDjangoRun, MUSIC_VOLUME),
};

export const entryResultsBackgroundMusic: AudioMusicTrack = {
  key: 'ENTRY_RESULTS_BACKGROUND_MUSIC',
  playImmediately: true,
  loop: true,
  fadeOutOnStop: false,
  volume: FULLY_CHARGED_VOLUME,
  audioHowler: generateHowlerAudio(assets.music.fullyChargedLoop6, FULLY_CHARGED_VOLUME),
};

export const entryCompletedBackgroundMusic: AudioMusicTrack = {
  key: 'ENTRY_RESULTS_BACKGROUND_MUSIC',
  playImmediately: false,
  loop: true,
  fadeOutOnStop: false,
  volume: FULLY_CHARGED_VOLUME,
  audioHowler: generateHowlerAudio(assets.music.fullyChargedLoop8, FULLY_CHARGED_VOLUME),
};
