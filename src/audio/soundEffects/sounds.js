// @flow

import { generateHowlerAudio } from '../utils';
import assets from '../../utils/assets';

const SOUND_EFFECTS_VOLUME = 0.85;

export type SoundEffect = {
  key: string,
  audioHowler: any,
  volume: number,
};

export const randomPlayerSound: SoundEffect = {
  key: 'RANDOM_PLAYER_SOUND',
  audioHowler: generateHowlerAudio(assets.sounds.randomPlayerSound, SOUND_EFFECTS_VOLUME),
  volume: SOUND_EFFECTS_VOLUME,
};

export const playerJoinedSound: SoundEffect = {
  key: 'PLAYER_JOINED',
  audioHowler: generateHowlerAudio(assets.sounds.randomPlayerSound, SOUND_EFFECTS_VOLUME),
  volume: SOUND_EFFECTS_VOLUME,
};

export const timesUpSound: SoundEffect = {
  key: 'TIMES_UP',
  audioHowler: generateHowlerAudio(assets.sounds.gongSound, SOUND_EFFECTS_VOLUME),
  volume: SOUND_EFFECTS_VOLUME,
};

export const celebrationSound: SoundEffect = {
  key: 'CELEBRATION',
  audioHowler: generateHowlerAudio(assets.sounds.celebrationSound, SOUND_EFFECTS_VOLUME),
  volume: SOUND_EFFECTS_VOLUME,
};

export const sadSound: SoundEffect = {
  key: 'SAD',
  audioHowler: generateHowlerAudio(assets.sounds.sadTromboneSound, SOUND_EFFECTS_VOLUME),
  volume: SOUND_EFFECTS_VOLUME,
};

export const sneakyLaughSound: SoundEffect = {
  key: 'SAD',
  audioHowler: generateHowlerAudio(assets.sounds.sneakyLaughSound, SOUND_EFFECTS_VOLUME),
  volume: SOUND_EFFECTS_VOLUME,
};

export function getPlayerJoinedSound(userKey: string, timestamp: number): SoundEffect {
  return {
    ...playerJoinedSound,
    key: `PLAYER_JOINED_${userKey}_${timestamp.toString()}`,
  };
}

export const playerLeftSound: SoundEffect = {
  key: 'PLAYER_JOINED',
  audioHowler: generateHowlerAudio(assets.sounds.randomPlayerSound, SOUND_EFFECTS_VOLUME),
  volume: SOUND_EFFECTS_VOLUME,
};

export function getPlayerLeftSound(userKey: string, timestamp: number): SoundEffect {
  return {
    ...playerJoinedSound,
    key: `PLAYER_LEFT_${userKey}_${timestamp.toString()}`,
  };
}

export function getRandomPlayerSound(key: string): SoundEffect {
  return {
    ...randomPlayerSound,
    key,
  };
}

export function getTimesUpSound(key: string): SoundEffect {
  return {
    ...timesUpSound,
    key,
  };
}

export function getCorrectAnswerSound(key: string): SoundEffect {
  return {
    ...celebrationSound,
    key,
  };
}

export function getFakeAnswerSound(key: string): SoundEffect {
  return {
    ...sneakyLaughSound,
    key,
  };
}

export function getNoVotersSound(key: string): SoundEffect {
  return {
    ...sadSound,
    key,
  };
}

// text entering

// text exiting

// correct answer

// fake answer

// no votes

// thing submitted

// points received
