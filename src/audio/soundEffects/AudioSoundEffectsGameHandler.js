// @flow

import { soundsLog } from 'utils/logging';
import type { SessionModel } from '../../firebase/bananart/session/models';
import { getSessionState } from '../../firebase/bananart/session/state';
import { sessionStates } from '../../firebase/bananart/session/models';
import {
  getCorrectAnswerSound,
  getFakeAnswerSound,
  getNoVotersSound,
  getRandomPlayerSound,
  getTimesUpSound,
} from './sounds';
import { audioSoundEffects } from './AudioSoundEffects';
import {
  getGameCurrentRound,
  getRoundKey,
  getRoundState,
  hasUserSubmittedDrawing,
} from '../../firebase/bananart/game/round/state';
import {
  getCurrentGameFromSession,
  getGameState,
  getGameUsersList,
} from '../../firebase/bananart/game/state';
import { gameStates } from '../../firebase/bananart/game/models';
import type { GameModel } from '../../firebase/bananart/game/models';
import type { RoundModel } from '../../firebase/bananart/game/round/models';
import { roundStates } from '../../firebase/bananart/game/round/models';
import {
  getAnswerRevealDelay,
  getEntryAnswerIndex,
  getEntryAnswerViaAnswerIndex,
  getEntryKey,
  getEntryState,
  getGameCurrentEntry,
  getUserEntryAnswerNumberOfVotes,
  hasUserSubmittedGuess,
  hasUserSubmittedVote,
} from '../../firebase/bananart/game/entry/state';
import { entryStates } from '../../firebase/bananart/game/entry/models';
import type { EntryAnswerModel, EntryModel } from '../../firebase/bananart/game/entry/models';
import { timeout } from '../../utils/async';

function triggerRoundDrawingUserSubmittedSoundEffect(roundKey: string, userKey: string) {
  const sound = getRandomPlayerSound(`ROUND_${roundKey}_USER_${userKey}`);
  audioSoundEffects.playSoundEffect(sound);
}

function triggerRoundDrawingSoundEffects(game: GameModel, round: RoundModel) {
  const roundKey = getRoundKey(round);
  const users = getGameUsersList(game);
  users.forEach(user => {
    if (hasUserSubmittedDrawing(game, roundKey, user.key)) {
      triggerRoundDrawingUserSubmittedSoundEffect(roundKey, user.key);
    }
  });
}

function triggerRoundDrawingCompletedSoundEffects(game: GameModel, round: RoundModel) {
  const roundKey = getRoundKey(round);
  const sound = getTimesUpSound(`ROUND_${roundKey}`);
  audioSoundEffects.playSoundEffect(sound);
}

function triggerEntryGuessSubmittedSoundEffect(entryKey: string, userKey: string) {
  const sound = getRandomPlayerSound(`ENTRY_${entryKey}_GUESS_USER_${userKey}`);
  audioSoundEffects.playSoundEffect(sound);
}

function triggerEntryGuessingSoundEffects(game: GameModel, entry: EntryModel) {
  const entryKey = getEntryKey(entry);
  const users = getGameUsersList(game);
  users.forEach(user => {
    if (hasUserSubmittedGuess(entry, user.key)) {
      triggerEntryGuessSubmittedSoundEffect(entryKey, user.key);
    }
  });
}

function triggerEntryGuessingTimesUpSoundEffects(game: GameModel, entry: EntryModel) {
  const entryKey = getEntryKey(entry);
  const sound = getTimesUpSound(`ENTRY_GUESSING_${entryKey}`);
  audioSoundEffects.playSoundEffect(sound);
}

function triggerEntryVoteSubmittedSoundEffect(entryKey: string, userKey: string) {
  const sound = getRandomPlayerSound(`ENTRY_${entryKey}_VOTE_USER_${userKey}`);
  audioSoundEffects.playSoundEffect(sound);
}

function triggerEntryVotingSoundEffects(game: GameModel, entry: EntryModel) {
  const entryKey = getEntryKey(entry);
  const users = getGameUsersList(game);
  users.forEach(user => {
    if (hasUserSubmittedVote(entry, user.key)) {
      triggerEntryVoteSubmittedSoundEffect(entryKey, user.key);
    }
  });
}

function triggerEntryVotingTimesUpSoundEffects(game: GameModel, entry: EntryModel) {
  const entryKey = getEntryKey(entry);
  const sound = getTimesUpSound(`ENTRY_VOTING_${entryKey}`);
  audioSoundEffects.playSoundEffect(sound);
}

async function triggerEntryAnswerRevealedSoundEffects(
  game: GameModel,
  entry: EntryModel,
  answer: EntryAnswerModel
) {
  const key = `ENTRY_ANSWER_REVEALED_${answer.key}_${entry.key}`;
  const timerDelay = getAnswerRevealDelay(entry, answer.key);
  let sound;
  if (answer.prompt) {
    const numberOfVotes = getUserEntryAnswerNumberOfVotes(entry, answer.key);
    if (numberOfVotes > 0) {
      sound = getCorrectAnswerSound(key);
    } else {
      sound = getNoVotersSound(key);
    }
  } else {
    sound = getFakeAnswerSound(key);
  }
  if (timerDelay > 0) {
    await timeout(timerDelay);
  }
  audioSoundEffects.playSoundEffect(sound);
}

function triggerEntryResultsSoundEffects(game: GameModel, entry: EntryModel) {
  const answerIndex = getEntryAnswerIndex(entry);
  const answer = answerIndex > -1 ? getEntryAnswerViaAnswerIndex(entry, answerIndex) : null;
  if (!answer) return;
  triggerEntryAnswerRevealedSoundEffects(game, entry, answer);
}

function triggerRoundVotingSoundEffects(game: GameModel, round: RoundModel) {
  const entry = getGameCurrentEntry(game);
  if (!entry) return;
  const entryState = getEntryState(entry);

  switch (entryState) {
    case entryStates.guessingIntro:
    case entryStates.guessing:
      triggerEntryGuessingSoundEffects(game, entry);
      break;
    case entryStates.guessingSubmitted:
    case entryStates.guessingTimesUp:
      triggerEntryGuessingTimesUpSoundEffects(game, entry);
      break;
    case entryStates.votingIntro:
    case entryStates.voting:
      triggerEntryVotingSoundEffects(game, entry);
      break;
    case entryStates.votingSubmitted:
    case entryStates.votingTimesUp:
      triggerEntryVotingTimesUpSoundEffects(game, entry);
      break;
    case entryStates.results:
      triggerEntryResultsSoundEffects(game, entry);
      break;
    default:
      break;
  }
}

function triggerRoundSoundEffects(game: GameModel, round: RoundModel) {
  const roundState = getRoundState(round);

  switch (roundState) {
    case roundStates.drawing:
      triggerRoundDrawingSoundEffects(game, round);
      break;
    case roundStates.drawingSubmitted:
    case roundStates.drawingTimesUp:
      triggerRoundDrawingCompletedSoundEffects(game, round);
      break;
    case roundStates.voting:
      triggerRoundVotingSoundEffects(game, round);
      break;
    default:
      break;
  }
}

function triggerGamePlayingSoundEffects(game: GameModel) {
  const round = getGameCurrentRound(game);

  if (!round) return;

  triggerRoundSoundEffects(game, round);
}

function triggerSessionPlayingSoundEffects(session: SessionModel) {
  soundsLog.log('triggerSessionPlayingSoundEffects');
  const game = getCurrentGameFromSession(session);

  if (!game) return;

  const gameState = getGameState(game);

  switch (gameState) {
    case gameStates.playing:
      triggerGamePlayingSoundEffects(game);
      break;
    default:
      break;
  }
}

function triggerGameSoundEffects(session: SessionModel) {
  soundsLog.log('triggerGameSoundEffects');
  const sessionState = getSessionState(session);
  switch (sessionState) {
    case sessionStates.playing:
      triggerSessionPlayingSoundEffects(session);
      break;
    default:
      break;
  }
}

export class AudioSoundEffectsGameHandler {
  sessionState: SessionModel;

  triggerGameSoundEffects(session: SessionModel) {
    if (!session) return;
    this.sessionState = session;
    triggerGameSoundEffects(session);
  }
}

export const audioSoundEffectsGameHandler = new AudioSoundEffectsGameHandler();
