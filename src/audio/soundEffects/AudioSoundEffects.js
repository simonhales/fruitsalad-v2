// @flow
import { audioLog } from 'utils/logging';
import type { SoundEffect } from './sounds';
import { getSoundEffectsMuted } from '../../utils/env';

class AudioSoundEffects {
  playedSoundEffects: Array<string> = [];

  playingSoundEffects: {
    [string]: SoundEffect,
  } = {};

  muted: boolean = getSoundEffectsMuted();

  playSoundEffect(sound: SoundEffect) {
    audioLog.log(`Playing sound effect ${sound.key}`);
    if (this.playingSoundEffects[sound.key] || this.playedSoundEffects.includes(sound.key)) {
      return;
    }
    this.playingSoundEffects[sound.key] = sound;
    sound.audioHowler.mute(this.muted);
    sound.audioHowler.play();
    sound.audioHowler.once('end', () => {
      this.handleSoundFinishedPlaying(sound);
    });
  }

  handleSoundFinishedPlaying(sound: SoundEffect) {
    delete this.playingSoundEffects[sound.key];
    this.playedSoundEffects.push(sound.key);
  }

  setMuted(muted: boolean) {
    this.muted = muted;
    (Object.values(this.playingSoundEffects): any).forEach((sound: SoundEffect) => {
      sound.audioHowler.mute(this.muted);
    });
  }
}

export const audioSoundEffects = new AudioSoundEffects();
