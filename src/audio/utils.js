// @flow
import { Howl } from 'howler';
import { audioLog } from 'utils/logging';

const VOICE_VOLUME = 1;

export function loadAudioTrack(src: string, generated: boolean): Promise<any> {
  return new Promise((resolve, reject) => {
    const audio = new Howl({
      volume: VOICE_VOLUME,
      src: [src],
      loop: false,
    });
    audioLog.log('Loading audio track');
    audio.once('load', () => {
      audioLog.log('Loaded audio track');
      resolve(audio);
    });
    audio.once('loaderror', () => {
      audioLog.error('Failed to load audio track');
      reject();
    });
  });
}

export function prependBase64Code(src: string) {
  return `data:audio/mp3;base64,${src}`;
}

export function generateHowlerAudio(src: string, volume: number) {
  return new Howl({
    volume,
    src: [src],
    loop: false,
  });
}
