// @flow

import analytics from '../analytics';

type ErrorContent = {
  code: string,
  message: string,
  fatal?: boolean,
};

export class BaseError extends Error {
  constructor(errorContent: ErrorContent) {
    super(errorContent.message);
    this.name = 'BaseError';
    const fatal = errorContent.fatal !== undefined ? errorContent.fatal : false;
    analytics.exceptions.logException(errorContent.message, fatal);
  }
}

export class ValidationError extends BaseError {
  constructor(errorContent: ErrorContent) {
    super(errorContent);
    this.name = 'ValidationError';
  }
}

export class ControllerActionError extends BaseError {
  constructor(errorContent: ErrorContent) {
    super(errorContent);
    this.name = 'ControllerActionError';
  }
}

export class DomError extends BaseError {
  constructor(errorContent: ErrorContent) {
    errorContent.fatal = true;
    super(errorContent);
    this.name = 'DomError';
  }
}
