import React, { Component } from 'react';
import assets from '../../utils/assets';
import styles from './styles';

class LoadingView extends Component {
  render() {
    return (
      <div className={styles.containerClass}>
        <div className={styles.contentClass}>
          <img
            className={styles.imageClass}
            src={assets.animations.bananaCatGif}
            alt="Banana-cat running animation"
          />
        </div>
        <div className={styles.titleWrapperClass}>
          <h3 className={styles.titleClass}>LOADING</h3>
        </div>
      </div>
    );
  }
}

export default LoadingView;
