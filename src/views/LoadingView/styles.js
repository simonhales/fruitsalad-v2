import { css } from 'emotion';
import utils from '../../styles/utils';
import { getResponsiveFont } from '../../styles/typography';

const containerClass = css`
  ${utils.layout.flexFullCentered};
  ${utils.layout.fixedFullView};
`;

const contentClass = css`
  text-align: center;
  position: relative;
`;

const titleWrapperClass = css`
  ${utils.layout.flexFullCentered};
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const titleClass = css`
  color: ${utils.colors.white};
  ${utils.fonts.families.heading};
  line-height: 1;
  font-weight: 700;
  text-shadow: 0 0 5px rgba(0, 0, 0, 0.12), 0 1px 1px rgba(0, 0, 0, 0.1);

  ${utils.breakpoints.desktop} {
    font-size: 60px;
    margin-top: 37px;
  }

  ${utils.breakpoints.mobile} {
    font-size: 50px;
    margin-top: 11%;
  }
`;

const imageClass = css`
  //margin-top: -30px;

  ${utils.breakpoints.desktop} {
    //margin-top: -50px;
    margin-left: 20px;
    width: 410px;
    height: 212px;
  }

  @media only screen and (max-width: 430px) {
    width: 100%;
    height: calc(100vw * ${212 / 410});
  }
`;

export default {
  containerClass,
  contentClass,
  imageClass,
  titleWrapperClass,
  titleClass,
};
