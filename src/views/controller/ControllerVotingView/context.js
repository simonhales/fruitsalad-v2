// @flow
import React from 'react';
import { submitUserEntryVote } from '../../../firebase/bananart/game/entry/actions';
import type { ControllerSessionContextState } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import {
  getContextCurrentEntry,
  getContextCurrentGame,
  getContextCurrentUserKey,
  getContextSession,
} from '../../../state/context/ControllerSessionContext/state';
import { getSessionKey } from '../../../firebase/bananart/session/state';
import { getGameKey, getTimerResolvedTimestamp } from '../../../firebase/bananart/game/state';
import {
  getEntryAnswersList,
  getEntryKey,
  getEntryUserAnswerKey,
  isEntryStateVoting,
} from '../../../firebase/bananart/game/entry/state';
import { withControllerSessionContext } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import ControllerVotingView from './ControllerVotingView';

const mapStateToProps = (state: ControllerSessionContextState) => {
  const userKey = getContextCurrentUserKey(state);
  const session = getContextSession(state);
  const sessionKey = getSessionKey(session);
  const game = getContextCurrentGame(state);
  if (!game) {
    throw new Error(`No current game`);
  }
  const gameKey = getGameKey(game);
  const entry = getContextCurrentEntry(state);
  if (!entry) {
    throw new Error(`No current entry`);
  }
  const entryKey = getEntryKey(entry);
  const answers = getEntryAnswersList(entry);
  const userAnswerKey = getEntryUserAnswerKey(entry, userKey);
  const countdownActive = isEntryStateVoting(entry);
  const timerResolvedTimestamp = getTimerResolvedTimestamp(game);
  return {
    answers,
    countdownActive,
    timerResolvedTimestamp,
    userAnswerKey,
    selectAnswer: (answerKey: string) =>
      submitUserEntryVote(answerKey, sessionKey, gameKey, entryKey, userKey),
  };
};

export default withControllerSessionContext(mapStateToProps)(ControllerVotingView);
