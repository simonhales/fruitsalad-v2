import { css } from 'emotion';
import utils from '../../../styles/utils/index';
import {
  controllerBodyStyles,
  controllerContainerStyles,
  controllerMainStyles,
} from '../../../styles/shared/controller';
// import { controllerContainerPadding } from '../ControllerDrawingView/styles';

const containerClass = css`
  ${controllerContainerStyles};
`;

const mainClass = css`
  ${controllerMainStyles};
  height: calc(100vh - 5px - 10px);
  display: flex;
  flex-direction: column;
`;

const bodyClass = css`
  ${controllerBodyStyles};
  flex: 1;
  display: flex;
  flex-direction: column;
`;

const answersClass = css`
  flex: 1;
`;

const instructionClass = css`
  margin: 15px 0;
  text-align: center;
`;

export default {
  containerClass,
  mainClass,
  bodyClass,
  answersClass,
  instructionClass,
};
