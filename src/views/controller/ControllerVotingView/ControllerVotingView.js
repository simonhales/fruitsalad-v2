// @flow
import React, { Component } from 'react';
import { controllerLog } from 'utils/logging';
import { controllerButtonProps } from '../../../components/controller/ControllerButton/ControllerButton';
import styles from './styles';
import { InstructionText } from '../../../components/elements';
import ControllerPendingView from '../ControllerPendingView/ControllerPendingView';
import analytics from '../../../analytics';
import ControllerHeader from '../../../components/controller/ControllerHeader/ControllerHeader';
import ControllerCountdownTimer from '../../../components/controller/ControllerCountdownTimer/ControllerCountdownTimer';
import { ENTRY_VOTING_TIMER } from '../../../constants/timers';
import type { EntryAnswerModel } from '../../../firebase/bananart/game/entry/models';
import AnswersList from '../../../components/controller/AnswersList/AnswersList';

type Props = {
  answers: Array<EntryAnswerModel>,
  countdownActive: boolean,
  timerResolvedTimestamp: number,
  userAnswerKey: string | null,
  selectAnswer: (answerKey: string) => Promise<any>,
};

type State = {
  submitted: boolean,
};

class ControllerVotingView extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      submitted: false,
    };
  }

  handleSelectAnswer = (answerKey: string) => {
    const { selectAnswer, userAnswerKey } = this.props;
    if (answerKey === userAnswerKey) return;
    selectAnswer(answerKey);
    this.setState({
      submitted: true,
    });
  };

  renderTimer() {
    const { countdownActive, timerResolvedTimestamp } = this.props;
    return (
      <ControllerCountdownTimer
        active={countdownActive}
        duration={ENTRY_VOTING_TIMER}
        resolvedTimestamp={timerResolvedTimestamp}
      />
    );
  }

  render() {
    const { answers, userAnswerKey } = this.props;
    const { submitted } = this.state;
    if (submitted) {
      return <ControllerPendingView title="your vote is submitted" />;
    }
    return (
      <div className={styles.containerClass}>
        <div className={styles.mainClass}>
          <ControllerHeader text="vote for the answer" timer={this.renderTimer()} />
          <div className={styles.bodyClass}>
            <div className={styles.answersClass}>
              <AnswersList
                answers={answers}
                userAnswerKey={userAnswerKey}
                selectAnswer={this.handleSelectAnswer}
              />
            </div>
            <div className={styles.instructionClass}>
              <InstructionText>
                vote for which one of these you think
                <br />
                is the actual original prompt
              </InstructionText>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ControllerVotingView;
