import { css } from 'emotion';
import utils from '../../../styles/utils/index';

const titleClass = css`
  position: absolute;
  top: ${utils.spacing.small}px;
  left: 0;
  right: 0;
  text-align: center;
`;

export default {
  titleClass,
};
