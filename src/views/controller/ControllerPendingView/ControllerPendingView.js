// @flow
import React, { Component } from 'react';
import { FlexCentered, MediumHeading, SmallHeading } from '../../../components/elements';
import assets from '../../../utils/assets/index';
import styles from './styles';

type Props = {
  title?: string,
  message?: string,
};

class ControllerPendingView extends Component<Props> {
  static defaultProps = {
    title: '',
    message: 'WATCH THE TV',
  };

  render() {
    const { title, message } = this.props;
    return (
      <FlexCentered>
        {title && <SmallHeading className={styles.titleClass}>{title}</SmallHeading>}
        <div>
          <img src={assets.animations.tvCatGif} alt="TV-cat running animation" />
          <MediumHeading centered>{message}</MediumHeading>
        </div>
      </FlexCentered>
    );
  }
}

export default ControllerPendingView;
