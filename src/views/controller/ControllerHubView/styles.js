import { css } from 'emotion';
import utils from 'styles/utils';
import { controllerMediumHeading } from '../../../styles/typography';
import animations from '../../../styles/animations';

const containerClass = css`
  height: 100%;
  display: flex;
  flex-direction: column;
  ${utils.transition.transition(['transform', 'opacity'])};

  .${utils.classNames.controllerNavChatSelected} & {
    transform: translateX(25px);
    opacity: 0.5;
  }
`;

const bodyClass = css`
  background: ${utils.colors.lightBackground};
  margin-top: 5px;
  width: calc(100vw - 15px);
  padding: 0 ${utils.spacing.small}px;
  flex: 1;
  display: flex;
  flex-direction: column;
  overflow: hidden;
`;

const bodyHeaderClass = css`
  ${controllerMediumHeading};
  border-bottom: 1px solid #c4d4ea;
  padding-top: 15px;
  padding-bottom: 15px;
  padding-right: 40px;

  ${utils.breakpoints.desktop} {
    padding-left: 40px;
    text-align: center;
  }
`;

const bodyHeaderTextClass = css`
  ${utils.breakpoints.desktop} {
    max-width: 500px;
    margin: 0 auto;
  }
`;

const bodyListClass = css`
  flex: 1;
  overflow-y: auto;
  padding-bottom: 150px;
`;

const footerClass = css`
  position: relative;
  padding: 0 20px;
  margin-top: -2px;
`;

const footerInnerClass = css`
  position: relative;
  top: -10px;

  .${utils.classNames.controllerNavChatSelected} & {
    ${animations.includeAnimation(animations.animations.slideOutLeft, { duration: '500ms' })};
  }
`;

const footerSettingsClass = css`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 100%;
  display: flex;
  justify-content: center;
  margin-bottom: 10px;
`;

export default {
  containerClass,
  bodyClass,
  bodyHeaderClass,
  bodyHeaderTextClass,
  bodyListClass,
  footerClass,
  footerInnerClass,
  footerSettingsClass,
};
