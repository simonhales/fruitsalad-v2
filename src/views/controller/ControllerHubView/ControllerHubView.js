// @flow
import React, { Component } from 'react';
import { Textfit } from 'react-textfit';
import styles from './styles';
import type { SessionUserModel } from '../../../firebase/bananart/session/user/models';
import UserOptionsModal from '../../../components/UserOptionsModal/UserOptionsModal';
import ControllerActionButton from '../../../components/controller/ControllerActionButton/ControllerActionButton';
import ControllerButton, {
  controllerButtonProps,
} from '../../../components/controller/ControllerButton/ControllerButton';
import ControllerPlayersList, {
  mapSessionUsersToPlayersList,
} from '../../../components/controller/ControllerPlayersList/ControllerPlayersList';
import ControllerOfflineWarning from '../../../components/controller/ControllerOfflineWarning/ControllerOfflineWarning';
import { controllerLog } from '../../../utils/logging';

type Props = {
  canGameStart: boolean,
  isUserHost: boolean,
  sessionHost: string,
  sessionUsers: Array<SessionUserModel>,
  setUser: (user: SessionUserModel) => void,
  startGame: () => Promise<any>,
};

type State = {
  startingGame: boolean,
};

class ControllerHubView extends Component<Props, State> {
  unmounted = false;

  constructor(props: Props) {
    super(props);
    this.state = {
      startingGame: false,
    };
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  handleStartGame = () => {
    if (!this.canGameStart()) return;
    controllerLog.log('Starting game');
    const { startGame } = this.props;
    this.setState({
      startingGame: true,
    });
    startGame().catch(e => {
      console.error(e);
      if (!this.unmounted) {
        this.setState({
          startingGame: false,
        });
      }
    });
  };

  handlePlayerSelected = (userKey: string) => {
    const { isUserHost } = this.props;
    if (!isUserHost) return;
    const { startingGame } = this.state;
    if (startingGame) return;
    this.selectPlayer(userKey);
  };

  selectPlayer = (userKey: string) => {
    const { sessionUsers } = this.props;
    const user = sessionUsers.find(sessionUser => sessionUser.key === userKey);
    if (user) {
      const { setUser } = this.props;
      setUser(user);
    } else {
      console.warn(`ControllerHubView.handlePlayerSelected:: user not found: ${userKey}`);
    }
  };

  canGameStart() {
    const { canGameStart } = this.props;
    const { startingGame } = this.state;
    return canGameStart && !startingGame;
  }

  render() {
    const { isUserHost, sessionUsers, sessionHost } = this.props;
    const { startingGame } = this.state;
    return (
      <React.Fragment>
        <div className={styles.containerClass}>
          <div className={styles.bodyClass}>
            <header className={styles.bodyHeaderClass}>
              <div className={styles.bodyHeaderTextClass}>
                <Textfit mode="single">fruitsalad.party/lemons</Textfit>
              </div>
            </header>
            <div className={styles.bodyListClass}>
              <ControllerPlayersList
                onClick={this.handlePlayerSelected}
                players={mapSessionUsersToPlayersList(sessionUsers, sessionHost)}
              />
            </div>
          </div>
          {isUserHost && (
            <div className={styles.footerClass}>
              <div className={styles.footerInnerClass}>
                <div className={styles.footerSettingsClass}>
                  <ControllerButton onClick={() => {}} type={controllerButtonProps.types.soft}>
                    SETTINGS
                  </ControllerButton>
                </div>
                <div>
                  <ControllerActionButton
                    onClick={this.handleStartGame}
                    type={controllerButtonProps.types.fancy}
                  >
                    {startingGame ? 'STARTING...' : 'START GAME'}
                  </ControllerActionButton>
                </div>
              </div>
            </div>
          )}
        </div>
        <UserOptionsModal />
        <ControllerOfflineWarning />
      </React.Fragment>
    );
  }
}

export default ControllerHubView;
