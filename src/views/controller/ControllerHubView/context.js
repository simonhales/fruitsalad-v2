// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import type { ControllerSessionContextState } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import { withControllerSessionContext } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import {
  canSessionGameStart,
  getSessionHost,
  getSessionKey,
  getSessionUsers,
  isUserSessionHost,
} from '../../../firebase/bananart/session/state';
import type { SessionUserModel } from '../../../firebase/bananart/session/user/models';
import { setUserOptionsModal } from '../../../state/redux/ui/reducer';
import type { ReduxState } from '../../../state/redux/shared/models';
import ControllerHubView from './ControllerHubView';
import { createAndStartGame } from '../../../firebase/bananart/session/actions';

type Props = {
  canGameStart: boolean,
  isUserHost: boolean,
  sessionHost: string,
  sessionUsers: Array<SessionUserModel>,
  setUser: (user: SessionUserModel) => void,
  startGame: () => Promise<any>,
};

class ControllerHubViewWithContext extends Component<Props> {
  render() {
    const { canGameStart, isUserHost, sessionUsers, sessionHost, setUser, startGame } = this.props;
    return (
      <ControllerHubView
        canGameStart={canGameStart}
        isUserHost={isUserHost}
        sessionHost={sessionHost}
        sessionUsers={sessionUsers}
        setUser={setUser}
        startGame={startGame}
      />
    );
  }
}

const mapContextStateToProps = (state: ControllerSessionContextState) => {
  const { currentUserKey, session } = state;
  const isUserHost = session ? isUserSessionHost(currentUserKey, session) : false;
  const canGameStart = session ? canSessionGameStart(session) : false;
  const startGame = session ? () => createAndStartGame(session) : () => Promise.reject();
  return {
    canGameStart,
    isUserHost,
    sessionHost: getSessionHost(session),
    sessionUsers: getSessionUsers(session),
    startGame,
  };
};

const mapStateToProps = (state: ReduxState) => ({});

const mapDispatchToProps = dispatch => ({
  setUser: (user: SessionUserModel) => dispatch(setUserOptionsModal(user)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withControllerSessionContext(mapContextStateToProps)(ControllerHubViewWithContext));
