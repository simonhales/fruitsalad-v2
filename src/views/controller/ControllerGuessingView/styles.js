import { css } from 'emotion';
import utils from '../../../styles/utils/index';
import {
  controllerBodyStyles,
  controllerContainerStyles,
  controllerFooterStyles,
  controllerMainStyles,
} from '../../../styles/shared/controller';
// import { controllerContainerPadding } from '../ControllerDrawingView/styles';

const containerClass = css`
  ${controllerContainerStyles};
`;

const mainClass = css`
  ${controllerMainStyles};
`;

const bodyClass = css`
  ${controllerBodyStyles};
  margin-top: 80px;
`;

const instructionClass = css`
  margin-top: 15px;
  text-align: center;
`;

const footerClass = css`
  ${controllerFooterStyles};
`;

export default {
  containerClass,
  mainClass,
  bodyClass,
  instructionClass,
  footerClass,
};
