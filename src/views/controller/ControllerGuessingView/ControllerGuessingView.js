// @flow
import React, { Component } from 'react';
import { controllerLog } from 'utils/logging';
import ControllerButton, {
  controllerButtonProps,
} from '../../../components/controller/ControllerButton/ControllerButton';
import ControllerBodyLayout from '../../../components/ControllerBodyLayout/ControllerBodyLayout';
import styles from './styles';
import { ControllerInstructionsTitle, InstructionText } from '../../../components/elements';
import ControllerInput from '../../../components/ControllerInput/ControllerInput';
import ControllerPendingView from '../ControllerPendingView/ControllerPendingView';
import { withControllerSessionContext } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import type { ControllerSessionContextState } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import analytics from '../../../analytics';
import ControllerHeader from '../../../components/controller/ControllerHeader/ControllerHeader';
import ControllerCountdownTimer from '../../../components/controller/ControllerCountdownTimer/ControllerCountdownTimer';
import { DRAWING_TIMER, ENTRY_GUESSING_TIMER } from '../../../constants/timers';
import ControllerFooter from '../../../components/controller/ControllerFooter/ControllerFooter';
import ControllerActionButton from '../../../components/controller/ControllerActionButton/ControllerActionButton';

const GUESS_MAX_LENGTH = 128;

type Props = {
  countdownActive: boolean,
  timerResolvedTimestamp: number,
  submitGuess: (guess: string) => Promise<any>,
};

type State = {
  guess: string,
  submitting: boolean,
  submitted: boolean,
};

class ControllerGuessingView extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      guess: '',
      submitting: false,
      submitted: false,
    };
  }

  canSubmit() {
    const { guess } = this.state;
    return guess.length > 1;
  }

  handleFormSubmit = (event: SyntheticInputEvent<HTMLFormElement>) => {
    event.preventDefault();
    analytics.events.guessing.submittedViaForm();
    this.handleSubmit();
    return false;
  };

  handleButtonSubmit = () => {
    analytics.events.guessing.submittedViaButton();
    this.handleSubmit();
  };

  handleSubmit = () => {
    const { guess, submitting } = this.state;
    if (submitting) return;
    if (!this.canSubmit()) return;
    this.setState({
      submitting: true,
    });
    const { submitGuess } = this.props;
    analytics.events.guessing.guessSubmitting();
    submitGuess(guess)
      .then(this.handleGuessSubmitted)
      .catch(error => {
        controllerLog.error(error);
        this.setState({
          submitting: false,
        });
      });
  };

  handleGuessSubmitted = () => {
    const { guess } = this.state;
    analytics.events.guessing.guessSubmitted(guess);
    this.setState({
      submitted: true,
    });
  };

  handleInputChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const inputValue = event.target.value.toString().toLowerCase();
    this.setState({
      guess: inputValue.substring(0, GUESS_MAX_LENGTH),
    });
  };

  renderFooterButton() {
    const { submitting } = this.state;
    return (
      <ControllerActionButton
        onClick={this.handleButtonSubmit}
        type={controllerButtonProps.types.fancy}
        disabled={!this.canSubmit()}
      >
        {submitting ? 'SUBMITTING' : 'SUBMIT'}
      </ControllerActionButton>
    );
  }

  renderTimer() {
    const { countdownActive, timerResolvedTimestamp } = this.props;
    return (
      <ControllerCountdownTimer
        active={countdownActive}
        duration={ENTRY_GUESSING_TIMER}
        resolvedTimestamp={timerResolvedTimestamp}
      />
    );
  }

  render() {
    const { guess, submitted } = this.state;
    if (submitted) {
      return <ControllerPendingView title="your guess is submitted" />;
    }
    return (
      <form className={styles.containerClass} onSubmit={this.handleFormSubmit}>
        <div className={styles.mainClass}>
          <ControllerHeader text="guess what they drew" timer={this.renderTimer()} />
          <div className={styles.bodyClass}>
            <ControllerInput
              value={guess}
              handleChange={this.handleInputChange}
              placeholder="enter your guess here"
            />
            <div className={styles.instructionClass}>
              <InstructionText>
                you’re trying to fool everyone else,
                <br />
                so write anything really,
                <br />
                as long as it’s convincing ;)
              </InstructionText>
            </div>
          </div>
        </div>
        <div className={styles.footerClass}>
          <ControllerFooter>{this.renderFooterButton()}</ControllerFooter>
        </div>
      </form>
    );
  }
}

export default ControllerGuessingView;
