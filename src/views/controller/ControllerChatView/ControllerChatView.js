// @flow
import React, { Component } from 'react';
import { ControllerContext } from 'components/controller/ControllerBase/ControllerBase';
import utils from 'styles/utils';
import ControllerButton, {
  controllerButtonProps,
} from '../../../components/controller/ControllerButton/ControllerButton';
import styles from './styles';
import { TinyHeading } from '../../../components/elements';
import type { EntryStates } from '../../../firebase/bananart/game/entry/models';
import type { RoundStates } from '../../../firebase/bananart/game/round/models';
import { roundStates } from '../../../firebase/bananart/game/round/models';
import { entryStates } from '../../../firebase/bananart/game/entry/models';
import analytics from '../../../analytics';
import ControllerActionButton from '../../../components/controller/ControllerActionButton/ControllerActionButton';
import DrawingCanvas from '../../../components/DrawingCanvas/DrawingCanvas';
import PreventScroll from '../../../components/PreventScroll/PreventScroll';
import type { ControllerSessionContextState } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import { getValidSessionKey } from '../../../firebase/bananart/session/state';
import { withControllerSessionContext } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import {
  clearSessionUserChatDrawing,
  updateSessionUserChatDrawing,
} from '../../../firebase/bananart/chat/actions';
import { getContextCurrentUserKey } from '../../../state/context/ControllerSessionContext/state';
import { getRoundState } from '../../../firebase/bananart/game/round/state';
import { getEntryState } from '../../../firebase/bananart/game/entry/state';

type Props = {
  chatDisabled: boolean,
  entryState: EntryStates | null,
  roundState: RoundStates | null,
  clearUserChatDrawing: () => Promise<any>,
  updateUserChatDrawing: (drawing: string) => Promise<any>,
};

type State = {
  drawingStarted: boolean,
};

class ControllerChatView extends Component<Props, State> {
  drawingCanvasRef: ?{
    current: null | DrawingCanvas,
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      drawingStarted: false,
    };
    this.drawingCanvasRef = React.createRef();
  }

  canClearDrawing() {
    const { drawingStarted } = this.state;
    return drawingStarted;
  }

  handleClearDrawing = () => {
    const { clearUserChatDrawing } = this.props;
    if (this.drawingCanvasRef && this.drawingCanvasRef.current) {
      this.drawingCanvasRef.current.clearDrawing();
      clearUserChatDrawing();
    }
  };

  handleDrawingMouseUp = (drawing: string) => {
    const { updateUserChatDrawing } = this.props;
    this.setState({
      drawingStarted: true,
    });
    updateUserChatDrawing(drawing);
  };

  handleDrawingCleared = () => {
    this.setState({
      drawingStarted: false,
    });
  };

  handleReturnToPlay = (goToPlay: () => void) => {
    goToPlay();
    analytics.events.controller.returnToPlay();
  };

  renderFooter() {
    return (
      <ControllerActionButton
        className={utils.classNames.controllerButtonTypeFancyShadowShift}
        onClick={this.handleClearDrawing}
        disabled={!this.canClearDrawing()}
      >
        CLEAR CHAT
      </ControllerActionButton>
    );
  }

  renderDisabledPopup() {
    const { chatDisabled } = this.props;

    if (!chatDisabled) return null;

    let message = 'chat is disabled';
    let buttonText = 'RETURN TO PLAY';

    const { roundState } = this.props;
    const { entryState } = this.props;

    if (roundState === roundStates.drawing) {
      message = 'you need to submit your drawing';
      buttonText = 'GO TO DRAWING';
    } else if (entryState === entryStates.guessing) {
      message = 'you need to submit your guess';
      buttonText = 'GO TO GUESS';
    } else if (entryState === entryStates.voting) {
      message = 'you need to submit your vote';
      buttonText = 'GO TO VOTE';
    }

    return (
      <div className={styles.disabledContainerClass}>
        <div className={styles.disabledContentClass}>
          <TinyHeading className={styles.disabledTitleClass}>{message}</TinyHeading>
          <div className={styles.disabledButtonContainerClass}>
            <ControllerContext.Consumer>
              {({ goToPlay }) => (
                <ControllerButton
                  onClick={() => this.handleReturnToPlay(goToPlay)}
                  type={controllerButtonProps.types.vibrantSmaller}
                >
                  {buttonText}
                </ControllerButton>
              )}
            </ControllerContext.Consumer>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <React.Fragment>
        <div className={styles.containerClass}>
          <div className={styles.chatContainerClass}>
            <PreventScroll>
              <div className={styles.chatDrawingContainerWrapperClass}>
                <div className={styles.chatDrawingContainerClass}>
                  <DrawingCanvas
                    onMouseUp={this.handleDrawingMouseUp}
                    onClear={this.handleDrawingCleared}
                    ref={this.drawingCanvasRef}
                  />
                </div>
              </div>
            </PreventScroll>
            {this.renderFooter()}
          </div>
        </div>
        {this.renderDisabledPopup()}
      </React.Fragment>
    );
  }
}

const mapContextStateToProps = (state: ControllerSessionContextState) => {
  const { session, currentRound, currentEntry } = state;
  const userKey = getContextCurrentUserKey(state);
  const sessionKey = getValidSessionKey(session);
  const roundState = currentRound ? getRoundState(currentRound) : '';
  const entryState = currentEntry ? getEntryState(currentEntry) : '';
  return {
    sessionKey,
    updateUserChatDrawing: (drawing: string) =>
      updateSessionUserChatDrawing(userKey, drawing, sessionKey),
    clearUserChatDrawing: () => clearSessionUserChatDrawing(userKey, sessionKey),
    roundState,
    entryState,
  };
};

export default withControllerSessionContext(mapContextStateToProps)(ControllerChatView);
