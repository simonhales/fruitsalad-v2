import { css } from 'emotion';
import { transparentize } from 'polished';
import utils from '../../../styles/utils/index';

const containerClass = css`
  height: 100%;
  padding: ${utils.spacing.small}px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const chatContainerClass = css`
  ${utils.breakpoints.mobile} {
    width: 100%;
  }
`;

const chatDrawingContainerWrapperClass = css`
  margin-bottom: 10px;
  ${utils.breakpoints.mobile} {
    padding: 0 5px;
  }
`;

const chatDrawingContainerClass = css`
  width: 100%;
  height: calc(100vw - ${utils.spacing.small * 2}px - 10px);
  background-color: ${utils.colors.lightBackground};

  ${utils.breakpoints.desktop} {
    width: 500px;
    height: 500px;
  }
`;

const disabledContainerClass = css`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: ${transparentize(0.5, utils.colors.bgDarkened)};
  display: flex;
  align-items: center;
  justify-content: center;
`;

const disabledContentClass = css`
  background-color: ${utils.colors.white};
  width: 100%;
  text-align: center;
`;

const disabledTitleClass = css`
  margin: ${utils.spacing.small}px 0;
`;

const disabledButtonContainerClass = css`
  ${utils.measurements.controllerActionHeight};
`;

export default {
  containerClass,
  chatContainerClass,
  chatDrawingContainerWrapperClass,
  chatDrawingContainerClass,
  disabledContainerClass,
  disabledContentClass,
  disabledTitleClass,
  disabledButtonContainerClass,
};
