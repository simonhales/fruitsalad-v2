// @flow
import React, { Component } from 'react';
import ControllerPendingView from '../ControllerPendingView/ControllerPendingView';
import ControllerDrawingViewWithContext from '../ControllerDrawingView/context';
import ControllerGuessingViewWithContext from '../ControllerGuessingView/context';
import ControllerVotingViewWithContext from '../ControllerVotingView/context';
import { withControllerSessionContext } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import type { ControllerSessionContextState } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import type { GameModel } from '../../../firebase/bananart/game/models';
import type { RoundModel } from '../../../firebase/bananart/game/round/models';
import type { EntryModel } from '../../../firebase/bananart/game/entry/models';
import {
  isSessionStatePending,
  isSessionStatePlaying,
} from '../../../firebase/bananart/session/state';
import {
  isRoundDrawingStage,
  isRoundVotingStage,
} from '../../../firebase/bananart/game/round/state';
import {
  isEntryGuessingStage,
  isEntryVotingStage,
  isUserEntryArtist,
} from '../../../firebase/bananart/game/entry/state';
import ControllerHubViewWithContext from '../ControllerHubView/context';
import { isGamePlayingStage, isUserInGame } from '../../../firebase/bananart/game/state';
import { getContextCurrentUserKey } from '../../../state/context/ControllerSessionContext/state';

type Props = {
  session: SessionModel | null,
  currentUserKey: string,
  currentGame: GameModel | null,
  currentRound: RoundModel | null,
  currentEntry: EntryModel | null,
};

const UserResponsibleView = () => <ControllerPendingView title="you drew this" />;

class ControllerPlayView extends Component<Props> {
  getControllerPlayView() {
    const { session, currentUserKey, currentGame, currentRound, currentEntry } = this.props;

    if (!session) {
      return <ControllerPendingView />;
    }

    if (isSessionStatePlaying(session)) {
      if (currentGame) {
        if (!isUserInGame(currentUserKey, currentGame)) {
          return (
            <ControllerPendingView title="game is already in progress" message="GAME IN PROGRESS" />
          );
        }

        if (isGamePlayingStage(currentGame)) {
          if (currentRound) {
            if (isRoundDrawingStage(currentRound)) {
              return <ControllerDrawingViewWithContext />;
            }
            if (isRoundVotingStage(currentRound)) {
              if (currentEntry) {
                if (isUserEntryArtist(currentEntry, currentUserKey)) {
                  return <UserResponsibleView />;
                }
                if (isEntryGuessingStage(currentEntry)) {
                  return <ControllerGuessingViewWithContext />;
                }
                if (isEntryVotingStage(currentEntry)) {
                  return <ControllerVotingViewWithContext />;
                }
              }
            }
          }
        }
      }
    } else if (isSessionStatePending(session)) {
      return <ControllerHubViewWithContext />;
    }

    return <ControllerPendingView />;
  }

  render() {
    return this.getControllerPlayView();
  }
}

const mapStateToProps = (state: ControllerSessionContextState) => ({
  session: state.session,
  currentGame: state.currentGame,
  currentRound: state.currentRound,
  currentEntry: state.currentEntry,
  currentUserKey: getContextCurrentUserKey(state),
});

export default withControllerSessionContext(mapStateToProps)(ControllerPlayView);
