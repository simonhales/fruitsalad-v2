// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import ControllerBodyLayout from '../../../components/ControllerBodyLayout/ControllerBodyLayout';
import styles from './styles';
import ControllerButton, {
  controllerButtonProps,
} from '../../../components/controller/ControllerButton/ControllerButton';
import type { ReduxState } from '../../../state/redux/shared/models';
import { getCurrentUserKey, getReduxSessionKey } from '../../../state/redux/firebase/state';
import ControllerInput from '../../../components/ControllerInput/ControllerInput';
import { addUserToSession as fbAddUserToSession } from '../../../firebase/bananart/session/actions';

const JOIN_STATES = {
  name: 'name',
  fruit: 'fruit',
};

type JoinStates = $Keys<typeof JOIN_STATES>;

const NAME_MAX_LENGTH = 10;

type Props = {
  sessionKey: string,
  addUserToSession: (name: string) => void,
};

type State = {
  nameInput: string,
  joining: boolean,
  joinState: JoinStates,
};

class ControllerJoinView extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      nameInput: '',
      joining: false,
      joinState: JOIN_STATES.name,
    };
  }

  isValidName() {
    const { nameInput } = this.state;
    const strippedName = nameInput.trim();
    return strippedName && strippedName.length >= 2;
  }

  canJoin() {
    const { joining } = this.state;
    if (joining) return false;
    return true;
  }

  handleJoin = () => {
    const { addUserToSession } = this.props;
    if (!this.canJoin()) return;
    this.setState({
      joining: true,
    });
    const { nameInput } = this.state;
    addUserToSession(nameInput);
  };

  handleInputChange = (event: SyntheticInputEvent<HTMLInputElement>) => {
    const inputValue = event.target.value.toString();
    this.setState({
      nameInput: inputValue.substring(0, NAME_MAX_LENGTH),
    });
  };

  handleFormSubmit = (event: SyntheticInputEvent<HTMLFormElement>) => {
    event.preventDefault();
    this.handleSubmitName();
    return false;
  };

  handleSubmitName = () => {
    if (this.isValidName()) {
      this.setState({
        joinState: JOIN_STATES.fruit,
      });
    }
  };

  renderNameFooter() {
    return (
      <ControllerButton
        disabled={!this.isValidName()}
        onClick={this.handleSubmitName}
        type={controllerButtonProps.types.vibrant}
      >
        NEXT
      </ControllerButton>
    );
  }

  renderFruitFooter() {
    const { joining } = this.state;
    return (
      <ControllerButton
        disabled={!this.canJoin()}
        onClick={this.handleJoin}
        type={controllerButtonProps.types.vibrant}
      >
        {joining ? 'JOINING' : 'JOIN'}
      </ControllerButton>
    );
  }

  renderFooter() {
    const { joinState } = this.state;
    if (joinState === JOIN_STATES.name) {
      return this.renderNameFooter();
    }
    return this.renderFruitFooter();
  }

  renderNameContent() {
    const { nameInput } = this.state;
    return (
      <form onSubmit={this.handleFormSubmit}>
        <ControllerInput
          value={nameInput}
          placeholder="enter your name"
          handleChange={this.handleInputChange}
        />
      </form>
    );
  }

  renderFruitContent() {
    return <div>FRUIT GO HERE</div>;
  }

  renderContent() {
    const { joinState } = this.state;
    if (joinState === JOIN_STATES.name) {
      return this.renderNameContent();
    }
    return this.renderFruitContent();
  }

  render() {
    // const { sessionKey } = this.props; // todo
    const sessionKey = 'bananas';
    return (
      <div className={styles.containerStyles}>
        <ControllerBodyLayout footer={this.renderFooter()}>
          <div className={styles.mainClass}>
            <div className={styles.titleClass}>{`joining "${sessionKey}"`}</div>
            <div className={styles.bodyClass}>{this.renderContent()}</div>
          </div>
        </ControllerBodyLayout>
      </div>
    );
  }
}

const mapStateToProps = (state: ReduxState) => {
  const sessionKey = getReduxSessionKey(state.firebase);
  const userKey = getCurrentUserKey(state.firebase);
  return {
    sessionKey,
    addUserToSession: (name: string) => fbAddUserToSession(userKey, name, sessionKey),
  };
};

export default connect(mapStateToProps)(ControllerJoinView);
