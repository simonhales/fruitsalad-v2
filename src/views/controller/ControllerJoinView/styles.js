// @flow
import { css } from 'emotion';
import utils from 'styles/utils';
import { smallHeading } from '../../../styles/typography';

const containerStyles = css`
  ${utils.layout.fixedFullView};
`;

const mainClass = css`
  display: flex;
  flex-direction: column;
  height: 100%;
  overflow: hidden;
`;

const titleClass = css`
  ${smallHeading};
  color: ${utils.colors.dark};
  text-align: center;
  padding: ${utils.spacing.small}px;
`;

const bodyClass = css`
  ${utils.layout.sidePadding};
  flex: 1;
`;

export default {
  containerStyles,
  titleClass,
  mainClass,
  bodyClass,
};
