// @flow
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
import { connect } from 'react-redux';
import ControllerPlayView from '../ControllerPlayView/ControllerPlayView';
import ControllerSessionContext from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import type { ReduxState } from '../../../state/redux/shared/models';
import { getSessionFromState } from '../../../state/redux/rawFirebase/state';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import ControllerBase from '../../../components/controller/ControllerBase/ControllerBase';
import { getCurrentUserKey } from '../../../state/redux/firebase/state';
import ControllerOnlineUserTracker from '../../../components/controller/ControllerOnlineUserTracker/ControllerOnlineUserTracker';
import { getCurrentGameFromSession } from '../../../firebase/bananart/game/state';
import { getGameCurrentRound } from '../../../firebase/bananart/game/round/state';
import { getGameCurrentEntry } from '../../../firebase/bananart/game/entry/state';
import { isUserChatDisabled } from '../../../state/controller/user';

type Props = {
  currentUserKey: string,
  session: SessionModel | null,
};

type State = {};

export class ControllerViewRaw extends Component<Props, State> {
  render() {
    const { currentUserKey, session } = this.props;
    const currentGame = getCurrentGameFromSession(session);
    const currentRound = currentGame ? getGameCurrentRound(currentGame) : null;
    const currentEntry = currentGame ? getGameCurrentEntry(currentGame) : null;
    const userChatDisabled = session ? isUserChatDisabled(session, currentUserKey) : false;
    return (
      <ControllerSessionContext.Provider
        value={{
          currentUserKey,
          session,
          currentGame,
          currentEntry,
          currentRound,
        }}
      >
        <ControllerOnlineUserTracker>
          <ControllerBase chatDisabled={userChatDisabled}>
            <ControllerPlayView />
          </ControllerBase>
        </ControllerOnlineUserTracker>
      </ControllerSessionContext.Provider>
    );
  }
}

const mapStateToProps = (state: ReduxState) => {
  const session = getSessionFromState(state.rawFirebase);
  const currentUserKey = getCurrentUserKey(state.firebase);
  return {
    currentUserKey,
    session,
  };
};

export default connect(mapStateToProps)(ControllerViewRaw);
