// @flow
import React, { Component } from 'react';
import { controllerLog } from 'utils/logging';
import styles from './styles';
import ControllerHeader from '../../../components/controller/ControllerHeader/ControllerHeader';
import ControllerFooter from '../../../components/controller/ControllerFooter/ControllerFooter';
import { controllerButtonProps } from '../../../components/controller/ControllerButton/ControllerButton';
import ControllerActionButton from '../../../components/controller/ControllerActionButton/ControllerActionButton';
import ControllerCountdownTimer from '../../../components/controller/ControllerCountdownTimer/ControllerCountdownTimer';
import { DRAWING_TIMER } from '../../../constants/timers';
import DrawingCanvas from '../../../components/DrawingCanvas/DrawingCanvas';
import ControllerPendingView from '../ControllerPendingView/ControllerPendingView';
import type { EntryPromptModel } from '../../../firebase/bananart/game/entry/models';

type Props = {
  countdownActive: boolean,
  timerResolvedTimestamp: number,
  userPrompt: EntryPromptModel,
  submitDrawing: (data: string) => Promise<any>,
};

type State = {
  firstStrokeMade: boolean,
  minTimeThreshold: boolean,
  submitting: boolean,
  submitted: boolean,
};

class ControllerDrawingView extends Component<Props, State> {
  canSubmitTimeout: TimeoutID;

  canvasRef: ?{
    current: null | DrawingCanvas,
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      firstStrokeMade: false,
      minTimeThreshold: false,
      submitting: false,
      submitted: false,
    };
    this.canvasRef = React.createRef();
  }

  componentDidMount() {
    this.canSubmitTimeout = setTimeout(this.setCanSubmit, 2000);
  }

  componentWillUnmount() {
    clearTimeout(this.canSubmitTimeout);
    this.submitOnUnmount();
  }

  submitOnUnmount() {
    const { submitting, submitted } = this.state;
    if (submitting || submitted || !this.canSubmit()) {
      return;
    }
    const drawingData = this.getDrawingData();
    if (!drawingData) {
      return;
    }
    const { submitDrawing } = this.props;
    submitDrawing(drawingData);
  }

  canSubmit() {
    const { firstStrokeMade, minTimeThreshold } = this.state;
    return firstStrokeMade && minTimeThreshold;
  }

  setCanSubmit = () => {
    this.setState({
      minTimeThreshold: true,
    });
  };

  getDrawingData() {
    if (this.canvasRef && this.canvasRef.current) {
      const canvas: DrawingCanvas = this.canvasRef.current;
      const data = canvas.getSaveData();
      if (!data) {
        controllerLog.warn(`No save data retrieved from canvas`);
        return null;
      }
      return data;
    }
    return null;
  }

  handleSubmitDrawing = () => {
    const { submitting, submitted } = this.state;
    if (submitting || submitted) return;
    if (!this.canSubmit()) return;
    const drawingData = this.getDrawingData();
    if (!drawingData) {
      return;
    }
    this.setState({
      submitting: true,
    });
    const { submitDrawing } = this.props;
    submitDrawing(drawingData)
      .then(() => {
        this.setState({
          submitting: false,
          submitted: true,
        });
      })
      .catch(error => {
        controllerLog.error(error);
        this.setState({
          submitting: false,
          submitted: true,
        });
      });
  };

  handleMouseUp = () => {
    this.setState({
      firstStrokeMade: true,
    });
  };

  renderFooterButton() {
    const { submitting } = this.state;
    return (
      <ControllerActionButton
        onClick={this.handleSubmitDrawing}
        type={controllerButtonProps.types.fancy}
        disabled={!this.canSubmit()}
      >
        {submitting ? 'SUBMITTING' : 'SUBMIT'}
      </ControllerActionButton>
    );
  }

  renderTimer() {
    const { countdownActive, timerResolvedTimestamp } = this.props;
    return (
      <ControllerCountdownTimer
        active={countdownActive}
        duration={DRAWING_TIMER}
        resolvedTimestamp={timerResolvedTimestamp}
      />
    );
  }

  render() {
    const { userPrompt } = this.props;
    const { submitted } = this.state;
    if (submitted) {
      return <ControllerPendingView title="drawing submitted" />;
    }
    return (
      <div className={styles.containerClass}>
        <div className={styles.mainClass}>
          <ControllerHeader text="draw the following" timer={this.renderTimer()} />
          <div className={styles.bodyClass}>
            <div className={styles.bodyTextClass}>{userPrompt.text}</div>
            <div className={styles.drawingContainerClass}>
              <div className={styles.drawingClass}>
                <DrawingCanvas
                  onMouseUp={this.handleMouseUp}
                  onClear={() => {}}
                  ref={this.canvasRef}
                />
              </div>
            </div>
          </div>
        </div>
        <div className={styles.footerClass}>
          <ControllerFooter>{this.renderFooterButton()}</ControllerFooter>
        </div>
      </div>
    );
  }
}

export default ControllerDrawingView;
