// @flow
import React from 'react';
import { submitUserEntryDrawing } from '../../../firebase/bananart/game/entry/actions';
import type { ControllerSessionContextState } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import { withControllerSessionContext } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import ControllerDrawingView from './ControllerDrawingView';
import {
  getContextCurrentGame,
  getContextCurrentRound,
  getContextCurrentUserKey,
  getContextSession,
} from '../../../state/context/ControllerSessionContext/state';
import { getSessionKey } from '../../../firebase/bananart/session/state';
import { getGameKey, getTimerResolvedTimestamp } from '../../../firebase/bananart/game/state';
import {
  getRoundKey,
  getUserEntryForRound,
  getUserPromptForRound,
  isRoundStateDrawing,
} from '../../../firebase/bananart/game/round/state';
import { getEntryKey } from '../../../firebase/bananart/game/entry/state';

const mapStateToProps = (state: ControllerSessionContextState) => {
  const userKey = getContextCurrentUserKey(state);
  const session = getContextSession(state);
  const sessionKey = getSessionKey(session);
  const game = getContextCurrentGame(state);
  if (!game) {
    throw new Error(`No current game`);
  }
  const gameKey = getGameKey(game);
  const round = getContextCurrentRound(state);
  if (!round) {
    throw new Error(`No current round`);
  }
  const roundKey = getRoundKey(round);
  const userEntry = getUserEntryForRound(game, roundKey, userKey);
  const entryKey = getEntryKey(userEntry);
  const countdownActive = isRoundStateDrawing(round);
  const userPrompt = getUserPromptForRound(game, roundKey, userKey);
  const timerResolvedTimestamp = getTimerResolvedTimestamp(game);
  return {
    countdownActive,
    userPrompt,
    timerResolvedTimestamp,
    submitDrawing: (data: string) =>
      submitUserEntryDrawing(data, sessionKey, gameKey, entryKey, userKey),
  };
};

export default withControllerSessionContext(mapStateToProps)(ControllerDrawingView);
