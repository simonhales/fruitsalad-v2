import { css } from 'emotion';
import {
  controllerBodyStyles,
  controllerContainerStyles,
  controllerFooterStyles,
  controllerMainStyles,
} from '../../../styles/shared/controller';
import { controllerBodyText } from '../../../styles/typography';
import utils from '../../../styles/utils';

const containerClass = css`
  ${controllerContainerStyles};
`;

const mainClass = css`
  ${controllerMainStyles};
`;

const bodyClass = css`
  ${controllerBodyStyles};
`;

const bodyTextClass = css`
  ${controllerBodyText};
  text-align: center;
  text-transform: lowercase;
`;

const drawingContainerClass = css`
  margin-top: 25px;

  ${utils.breakpoints.mobileHeightSmaller} {
    margin-top: 10px;
  }
`;

const sidePadding = 13;
const sidePaddingIncreased = 20;

const drawingClass = css`
  width: calc(100vw - ${sidePadding * 2}px);
  height: calc(100vw - ${sidePadding * 2}px);
  border: 3px solid #c4d4ea;
  margin: 0 auto;

  ${utils.breakpoints.mobile} {
    ${utils.breakpoints.mobileHeightSmaller} {
      width: calc(100vw - ${sidePaddingIncreased * 2}px);
      height: calc(100vw - ${sidePaddingIncreased * 2}px);
      border-width: 2px;
    }
  }

  ${utils.breakpoints.desktop} {
    width: calc(100vh - 300px);
    height: calc(100vh - 300px);
  }
`;

const footerClass = css`
  ${controllerFooterStyles};
`;

export default {
  containerClass,
  mainClass,
  bodyClass,
  bodyTextClass,
  drawingContainerClass,
  drawingClass,
  footerClass,
};
