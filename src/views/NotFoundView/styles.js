// @flow
import { css } from 'emotion';
import utils from 'styles/utils';
import { smallHeading } from '../../styles/typography';

const containerClass = css`
  ${utils.layout.fixedFullView};
  ${utils.layout.flexFullCentered};
`;

const contentClass = css`
  position: relative;
`;

const imageClass = css`
  ${utils.breakpoints.mobile} {
    width: 100vw;
    height: 100vw;
  }

  ${utils.breakpoints.desktop} {
    width: 500px;
    height: 500px;
  }
`;

const infoClass = css`
  ${utils.layout.sidePadding};
  position: absolute;
  left: 0;
  right: 0;
  text-align: center;
`;

const titleClass = css`
  ${smallHeading};
  color: ${utils.colors.dark};
`;

const subtitleClass = css`
  margin-top: 10px;
  color: ${utils.colors.darkLight};
  font-weight: ${utils.weights.bold};
`;

const bottomContainerClass = css`
  ${utils.measurements.controllerActionHeight};
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
`;

export default {
  containerClass,
  contentClass,
  infoClass,
  imageClass,
  titleClass,
  subtitleClass,
  bottomContainerClass,
};
