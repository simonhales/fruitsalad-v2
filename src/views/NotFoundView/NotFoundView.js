// @flow
import * as React from 'react';
import assets from '../../utils/assets';
import styles from './styles';

type Props = {
  message?: React.Node,
  subtitle?: React.Node,
  bottomContent?: React.Node,
};

const NotFoundView = ({ message, subtitle, bottomContent }: Props) => (
  <div className={styles.containerClass}>
    <div className={styles.contentClass}>
      <div className={styles.infoClass}>
        {message && <div className={styles.titleClass}>{message}</div>}
        {subtitle && <div className={styles.subtitleClass}>{subtitle}</div>}
      </div>
      <div className={styles.imageClass}>
        <img src={assets.drawings.notFound} alt="404 not found depiction" />
      </div>
    </div>
    {bottomContent && <div className={styles.bottomContainerClass}>{bottomContent}</div>}
  </div>
);

NotFoundView.defaultProps = {
  message: null,
  subtitle: null,
  bottomContent: null,
};

export default NotFoundView;
