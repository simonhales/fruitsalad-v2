// @flow
import { css } from 'emotion';
import utils from 'styles/utils/index';
import assets from '../../../utils/assets';
import { tiledBackgroundImage } from '../../../styles/images';

const containerClass = css`
  ${utils.layout.fixedFullView};
  background-color: ${utils.colors.displayDarkBackground};
  ${tiledBackgroundImage(assets.patterns.darkFruitPattern)};
`;

export default {
  containerClass,
};
