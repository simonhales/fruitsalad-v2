// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from './styles';
import type { ReduxState } from '../../../state/redux/shared/models';
import {
  getSessionChatFromState,
  getSessionWrapperFromState,
} from '../../../state/redux/rawFirebase/state';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import {
  getSessionFromSessionWrapper,
  getSessionKeyFromSession,
} from '../../../firebase/bananart/session/state';
import DisplaySessionContext from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import type { SessionChatModel } from '../../../firebase/bananart/chat/models';
import FullscreenWrapper from '../../../components/display/FullscreenWrapper/FullscreenWrapper';
import AudioManager from '../../../components/display/AudioManager/AudioManager';
import { getAudioDisabled } from '../../../utils/env';
import DisplayStateView from '../../../components/display/DisplayStateView/DisplayStateView';
import { getCurrentGameFromSession } from '../../../firebase/bananart/game/state';
import { getGameCurrentRound } from '../../../firebase/bananart/game/round/state';
import { getGameCurrentEntry } from '../../../firebase/bananart/game/entry/state';
import HostOnly from '../../../components/HostOnly/HostOnly';
import GameHostController from '../../../components/GameHostController/GameHostController';

type Props = {
  session: SessionModel | null,
  sessionKey: string,
  sessionChat: SessionChatModel,
  preview?: boolean,
};

type State = {
  drawings: {
    [string]: string,
  },
  addDrawing: (drawingKey: string, drawing: string) => void,
};

const soundDisabled = getAudioDisabled();

class DisplayView extends Component<Props, State> {
  static defaultProps = {
    preview: false,
  };

  addDrawing: (drawingKey: string, drawing: string) => void;

  constructor(props: Props) {
    super(props);

    this.addDrawing = (drawingKey: string, drawing: string) => {
      const { drawings } = this.state;
      this.setState({
        drawings: {
          ...drawings,
          [drawingKey]: drawing,
        },
      });
    };

    this.state = {
      drawings: {},
      addDrawing: this.addDrawing,
    };
  }

  render() {
    const { preview, session, sessionKey, sessionChat } = this.props;
    const { addDrawing, drawings } = this.state;
    const currentGame = getCurrentGameFromSession(session);
    const currentRound = currentGame ? getGameCurrentRound(currentGame) : null;
    const currentEntry = currentGame ? getGameCurrentEntry(currentGame) : null;
    return (
      <DisplaySessionContext.Provider
        value={{
          sessionKey,
          session,
          sessionChat,
          currentGame,
          currentRound,
          currentEntry,
          drawings,
          addDrawing,
        }}
      >
        {session && !soundDisabled && <AudioManager session={session} />}
        <FullscreenWrapper>
          <div className={styles.containerClass}>
            {session && <DisplayStateView session={session} />}
          </div>
        </FullscreenWrapper>
        {!preview && (
          <HostOnly>
            <GameHostController />
          </HostOnly>
        )}
      </DisplaySessionContext.Provider>
    );
  }
}

const mapStateToProps = (state: ReduxState) => {
  const sessionWrapper = getSessionWrapperFromState(state.rawFirebase);
  const session = sessionWrapper ? getSessionFromSessionWrapper(sessionWrapper) : null;
  const sessionKey = session ? getSessionKeyFromSession(session) : '';
  const sessionChat = getSessionChatFromState(state.rawFirebase);
  return {
    session,
    sessionKey,
    sessionChat,
  };
};

export default connect(mapStateToProps)(DisplayView);
