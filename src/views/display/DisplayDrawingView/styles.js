// @flow

import { css } from 'emotion';
import { displayViewContent } from '../../../utils/display';

const containerClass = css`
  ${displayViewContent};
`;

export default {
  containerClass,
};
