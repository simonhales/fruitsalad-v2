// @flow
import React, { Component } from 'react';
import type { RoundStates } from '../../../firebase/bananart/game/round/models';
import DisplayDrawingView from './DisplayDrawingView';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { getRoundState } from '../../../firebase/bananart/game/round/state';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';

type WithContextProps = {
  roundState: RoundStates,
};

class DisplayDrawingViewWithContext extends Component<WithContextProps> {
  render() {
    const { roundState } = this.props;
    return <DisplayDrawingView roundState={roundState} />;
  }
}

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentRound } = state;
  if (!currentRound) {
    throw new Error(`No current round`);
  }
  const roundState = getRoundState(currentRound);
  return {
    roundState,
  };
};

export default withDisplaySessionContext(mapStateToProps)(DisplayDrawingViewWithContext);
