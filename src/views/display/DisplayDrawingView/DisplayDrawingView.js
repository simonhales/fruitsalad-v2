// @flow
import React, { Component } from 'react';
import styles from './styles';
import DisplayViewTransitions from '../../../components/display/DisplayViewTransitions/DisplayViewTransitions';
import DrawingIntro from '../../../components/display/DrawingIntro/DrawingIntro';
import type { RoundStates } from '../../../firebase/bananart/game/round/models';
import { roundStates } from '../../../firebase/bananart/game/round/models';
import DrawingUsersProgressWithContext from '../../../components/display/DrawingUsersProgress/context';
import CountdownTimer from '../../../components/display/CountdownTimer/CountdownTimer';
import { DRAWING_TIMER } from '../../../constants/timers';

type Props = {
  roundState: RoundStates,
};

class DisplayDrawingView extends Component<Props> {
  shouldComponentUpdate(nextProps: Props) {
    // eslint-disable-next-line react/destructuring-assignment
    return this.props.roundState !== nextProps.roundState;
  }

  getIntroMessage(): string {
    const { roundState } = this.props;
    switch (roundState) {
      case roundStates.drawingTimesUp:
        return `time's up`;
      case roundStates.drawingSubmitted:
        return `submitted`;
      default:
        return 'drawing';
    }
  }

  render() {
    const { roundState } = this.props;
    const introMinimised = roundState !== roundStates.drawingIntro;
    const activeTimer = roundState === roundStates.drawing;
    const showPhonePrompt =
      roundState === roundStates.drawingIntro || roundState === roundStates.drawing;
    return (
      <DisplayViewTransitions className={styles.containerClass}>
        <DrawingIntro
          message={this.getIntroMessage()}
          minimised={introMinimised}
          showPhonePrompt={showPhonePrompt}
        />
        <DrawingUsersProgressWithContext />
        <CountdownTimer active={activeTimer} duration={DRAWING_TIMER} />
      </DisplayViewTransitions>
    );
  }
}

export default DisplayDrawingView;
