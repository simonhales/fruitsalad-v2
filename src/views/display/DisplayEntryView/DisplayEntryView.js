// @flow
import React, { Component } from 'react';
import styles from './styles';
import EntryIntro from '../../../components/display/EntryIntro/EntryIntro';
import type { EntryStates } from '../../../firebase/bananart/game/entry/models';
import { entryStates } from '../../../firebase/bananart/game/entry/models';
import EntryDrawings, {
  entryDrawingsLayouts,
} from '../../../components/display/EntryDrawings/EntryDrawings';
import EntryStageIntro, {
  ENTRY_INTRO_TITLES,
} from '../../../components/display/EntryStageIntro/EntryStageIntro';
import NoDrawingsMessage from '../../../components/NoDrawingsMessage/NoDrawingsMessage';
import { DRAWING_TIMER, ENTRY_GUESSING_TIMER, ENTRY_VOTING_TIMER } from '../../../constants/timers';
import CountdownTimer from '../../../components/display/CountdownTimer/CountdownTimer';
import type { EntryDrawingsLayouts } from '../../../components/display/EntryDrawings/EntryDrawings';
import DisplayEntryAnswers from '../../../components/display/DisplayEntryAnswers/DisplayEntryAnswers';
import DisplayEntryHighlightedAnswer from '../../../components/display/DisplayEntryHighlightedAnswer/DisplayEntryHighlightedAnswer';
import DisplayEntryResults from '../../../components/display/DisplayEntryResults/DisplayEntryResults';
import DisplayEntryCompleted from '../../../components/display/DisplayEntryCompleted/DisplayEntryCompleted';

function areDrawingsVisible(state: EntryStates): boolean {
  return (
    state !== entryStates.pending &&
    state !== entryStates.completed &&
    state !== entryStates.noDrawings
  );
}

function isGuessingIntroVisible(state: EntryStates): boolean {
  return (
    state === entryStates.guessingIntro ||
    state === entryStates.guessing ||
    state === entryStates.guessingTimesUp ||
    state === entryStates.guessingSubmitted
  );
}

function guessingIntroIsPhonePromptVisible(state: EntryStates): boolean {
  return state === entryStates.guessingIntro || state === entryStates.guessing;
}

function isVotingIntroVisible(state: EntryStates): boolean {
  return (
    state === entryStates.votingIntro ||
    state === entryStates.voting ||
    state === entryStates.votingTimesUp ||
    state === entryStates.votingSubmitted
  );
}

function areDrawingsFaded(state: EntryStates): boolean {
  return (
    state === entryStates.intro ||
    state === entryStates.guessingIntro ||
    state === entryStates.votingIntro
  );
}

function votingIntroIsPhonePromptVisible(state: EntryStates): boolean {
  return state === entryStates.votingIntro || state === entryStates.voting;
}

function showNoDrawingsMessage(state: EntryStates): boolean {
  return state === entryStates.noDrawings;
}

function isGuessingTimerActive(state: EntryStates): boolean {
  return state === entryStates.guessing;
}

function isVotingTimerActive(state: EntryStates): boolean {
  return state === entryStates.voting;
}

function getDrawingsLayout(state: EntryStates): EntryDrawingsLayouts {
  switch (state) {
    case entryStates.votingIntro:
    case entryStates.voting:
    case entryStates.votingSubmitted:
    case entryStates.votingTimesUp:
      return entryDrawingsLayouts.small;
    case entryStates.results:
    case entryStates.completed:
      return entryDrawingsLayouts.smallResults;
    default:
      return entryDrawingsLayouts.big;
  }
}

function areAnswersVisible(state: EntryStates): boolean {
  return (
    state === entryStates.votingIntro ||
    state === entryStates.voting ||
    state === entryStates.votingSubmitted ||
    state === entryStates.votingTimesUp ||
    state === entryStates.results
  );
}

function isIntroExpanded(state: EntryStates): boolean {
  switch (state) {
    case entryStates.guessingIntro:
    case entryStates.guessingSubmitted:
    case entryStates.guessingTimesUp:
    case entryStates.votingIntro:
    case entryStates.votingTimesUp:
    case entryStates.votingSubmitted:
      return false;
    default:
      return true;
  }
}

function isIntroVisible(state: EntryStates): boolean {
  switch (state) {
    case entryStates.guessingIntro:
    case entryStates.guessing:
    case entryStates.guessingSubmitted:
    case entryStates.guessingTimesUp:
    case entryStates.votingIntro:
    case entryStates.voting:
    case entryStates.votingTimesUp:
    case entryStates.votingSubmitted:
    case entryStates.completed:
      return true;
    default:
      return false;
  }
}

function isIntroPhonePromptVisible(state: EntryStates): boolean {
  switch (state) {
    case entryStates.guessingIntro:
    case entryStates.guessing:
    case entryStates.votingIntro:
    case entryStates.voting:
      return true;
    default:
      return false;
  }
}

function getIntroTagline(state: EntryStates): string {
  switch (state) {
    case entryStates.votingIntro:
    case entryStates.voting:
      return 'stage 2a';
    case entryStates.guessingIntro:
    case entryStates.guessing:
      return 'stage 2a';
    default:
      return '';
  }
}

function getIntroTitle(state: EntryStates): string {
  switch (state) {
    case entryStates.votingIntro:
    case entryStates.voting:
      return ENTRY_INTRO_TITLES.voting;
    case entryStates.guessingIntro:
    case entryStates.guessing:
      return ENTRY_INTRO_TITLES.guessing;
    case entryStates.guessingTimesUp:
    case entryStates.votingTimesUp:
      return ENTRY_INTRO_TITLES.timesUp;
    case entryStates.guessingSubmitted:
    case entryStates.votingSubmitted:
      return ENTRY_INTRO_TITLES.done;
    case entryStates.completed:
      return ENTRY_INTRO_TITLES.scoreboard;
    default:
      return '';
  }
}

function areResultsVisible(state: EntryStates): boolean {
  return state === entryStates.results;
}

function areAnswersPushedDown(state: EntryStates): boolean {
  return state === entryStates.results;
}

function isCompletedVisible(state: EntryStates): boolean {
  return state === entryStates.completed;
}

type Props = {
  entryState: EntryStates,
};

class DisplayEntryView extends Component<Props> {
  render() {
    const { entryState } = this.props;
    return (
      <div className={styles.containerClass}>
        <EntryDrawings
          layout={getDrawingsLayout(entryState)}
          showDrawings={areDrawingsVisible(entryState)}
          faded={areDrawingsFaded(entryState)}
        />
        <DisplayEntryAnswers
          pushedDown={areAnswersPushedDown(entryState)}
          visible={areAnswersVisible(entryState)}
        />
        <DisplayEntryResults visible={areResultsVisible(entryState)} />
        <DisplayEntryCompleted visible={isCompletedVisible(entryState)} />
        <EntryIntro introIsVisible={entryState === entryStates.intro} />
        <EntryStageIntro
          visible={isIntroVisible(entryState)}
          expanded={isIntroExpanded(entryState)}
          phonePromptVisible={isIntroPhonePromptVisible(entryState)}
          tagline={getIntroTagline(entryState)}
          title={getIntroTitle(entryState)}
        />
        <NoDrawingsMessage visible={showNoDrawingsMessage(entryState)} />
        <DisplayEntryHighlightedAnswer visible={entryState === entryStates.results} />
        <CountdownTimer
          active={isGuessingTimerActive(entryState)}
          duration={ENTRY_GUESSING_TIMER}
        />
        <CountdownTimer active={isVotingTimerActive(entryState)} duration={ENTRY_VOTING_TIMER} />
      </div>
    );
  }
}

export default DisplayEntryView;
