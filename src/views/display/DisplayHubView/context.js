// @flow
import React, { Component } from 'react';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import DisplayHubView from './DisplayHubView';
import { getSessionSlotCards } from '../../../state/bananart/session/state';
import { getSessionUsers, isSessionStateStarting } from '../../../firebase/bananart/session/state';
import type { SlotCardModel } from '../../../state/bananart/session/models';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';

type WithContextProps = {
  gameIsStarting: boolean,
  sessionKey: string,
  slots: Array<SlotCardModel>,
};

class DisplayHubViewWithContext extends Component<WithContextProps> {
  render() {
    return <DisplayHubView {...this.props} />;
  }
}

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { session, sessionKey } = state;
  const sessionUsers = getSessionUsers(session);
  const gameIsStarting = session ? isSessionStateStarting(session) : false;
  return {
    gameIsStarting,
    sessionKey,
    slots: getSessionSlotCards(sessionUsers, session),
  };
};

export default withDisplaySessionContext(mapStateToProps)(DisplayHubViewWithContext);
