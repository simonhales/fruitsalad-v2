// @flow
import React, { Component } from 'react';
import { cx } from 'emotion';
import { Flipper, Flipped } from 'react-flip-toolkit';
import { Textfit } from 'react-textfit';
import { Transition } from 'react-spring';
import utils from 'styles/utils';
import styles from './styles';
import DisplaySiteURL, {
  DisplaySiteURLProps,
} from '../../../components/text/DisplaySiteURL/DisplaySiteURL';
import SplitText from '../../../components/text/SplitText/SplitText';
import TextWiggle, { TextWiggleProps } from '../../../components/text/TextWiggle/TextWiggle';
import RowOfCards from '../../../components/display/RowOfCards/RowOfCards';
import type { SlotCardModel } from '../../../state/bananart/session/models';
import GameStartingMessage from '../../../components/display/GameStartingMessage/GameStartingMessage';
import DisplayViewTransitions from '../../../components/display/DisplayViewTransitions/DisplayViewTransitions';

type Props = {
  gameIsStarting: boolean,
  sessionKey: string,
  slots: Array<SlotCardModel>,
};

type State = {
  justLoadedPhase: boolean,
};

class DisplayHubView extends Component<Props, State> {
  justLoadedTimeout: TimeoutID;

  toggle = true;

  constructor(props: Props) {
    super(props);
    this.state = {
      justLoadedPhase: true,
    };
  }

  componentDidMount() {
    this.justLoadedTimeout = setTimeout(() => {
      this.setState({
        justLoadedPhase: false,
      });
    }, 7000);
  }

  componentWillUnmount() {
    clearTimeout(this.justLoadedTimeout);
  }

  isLotsOfPlayersState() {
    const { slots } = this.props;
    return slots.length > 6;
  }

  renderTextSecionWrapper() {
    const { gameIsStarting } = this.props;
    return (
      <Transition from={{ opacity: 0 }} enter={{ opacity: 1 }} leave={{ opacity: 0 }}>
        {gameIsStarting
          ? transitionStyles => (
              <div className={styles.textContentWrapperClass} style={transitionStyles}>
                {this.renderGameStartingTextSection()}
              </div>
            )
          : transitionStyles => (
              <div className={styles.textContentWrapperClass} style={transitionStyles}>
                {this.renderTextSection()}
              </div>
            )}
      </Transition>
    );
  }

  renderGameStartingTextSection() {
    return <GameStartingMessage />;
  }

  renderTextSection() {
    const { sessionKey } = this.props;
    const lotsOfPlayersState = this.isLotsOfPlayersState();
    return (
      <Flipper className={styles.textContentClass} flipKey={lotsOfPlayersState ? '12' : '6'}>
        <Flipped flipId="textFirstWrapper">
          <div className={styles.textFirstWrapper}>
            <div className={styles.labelClass}>
              <span>
                <TextWiggle>
                  <SplitText text="on your phone go to" />
                </TextWiggle>
              </span>
            </div>
            <div className={styles.siteClass}>
              <DisplaySiteURL
                size={
                  lotsOfPlayersState
                    ? DisplaySiteURLProps.sizes.smaller
                    : DisplaySiteURLProps.sizes.default
                }
              />
            </div>
          </div>
        </Flipped>
        <Flipped flipId="textSecondWrapper">
          <div className={styles.textSecondWrapper}>
            <div className={styles.codeContainerClass}>
              <div className={styles.largerLabelClass}>
                <span>
                  <TextWiggle amount={TextWiggleProps.amount.reduced}>
                    <SplitText text="enter this code" />
                  </TextWiggle>
                </span>
              </div>
              <div className={styles.codeClassWrapperClass}>
                <div className={styles.codeClass}>
                  <Textfit mode="single" min={20} max={100}>
                    <span>{sessionKey}</span>
                  </Textfit>
                </div>
              </div>
            </div>
            <div className={styles.smallerLabelClass}>
              <span>
                <TextWiggle>
                  <SplitText text="the host must start the game" />
                </TextWiggle>
              </span>
            </div>
          </div>
        </Flipped>
      </Flipper>
    );
  }

  render() {
    const { slots } = this.props;
    const { justLoadedPhase } = this.state;
    return (
      <DisplayViewTransitions
        className={cx(styles.containerClass, {
          [utils.classNames.displayHubJustLoadedPhase]: justLoadedPhase,
          [utils.classNames.displayHubLotsOfPlayers]: this.isLotsOfPlayersState(),
        })}
      >
        <div className={styles.textContainerClass}>{this.renderTextSecionWrapper()}</div>
        <div className={styles.bottomContainerClass}>
          <RowOfCards slots={slots} />
        </div>
      </DisplayViewTransitions>
    );
  }
}

export default DisplayHubView;
