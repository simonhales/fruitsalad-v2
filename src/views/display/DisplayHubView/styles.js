import { css } from 'emotion';
import utils from 'styles/utils/index';
import {
  displayHelpText,
  getResponsiveDesktopFontSize,
  getResponsiveFont,
} from '../../../styles/typography';
import animations from '../../../styles/animations';
import assets from '../../../utils/assets';
import { tiledBackgroundImage } from '../../../styles/images';
import { displayViewContent } from '../../../utils/display';

const lotsOfPlayersState = `.${utils.classNames.displayHubLotsOfPlayers} & `;

const containerClass = css`
  ${displayViewContent};
  display: flex;
  flex-direction: column;
  padding: ${utils.spacing.medium}px;
`;

const textContainerClass = css`
  position: relative;
  flex: 1;
`;

const textContentWrapperClass = css`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;

const textContentClass = css`
  ${lotsOfPlayersState} {
    width: 100%;
    display: flex;
    align-items: center;
  }
`;

const textFirstWrapper = css`
  ${lotsOfPlayersState} {
    width: 50%;
  }
`;

const textSecondWrapper = css`
  ${lotsOfPlayersState} {
    width: 50%;
  }
`;

const text = css`
  ${displayHelpText};

  > span {
    opacity: 0.62;
  }
`;

const labelClass = css`
  ${animations.includeAnimation(
    animations.animations.slideFadeUp,
    { duration: '500ms', delay: '1500ms' },
    true
  )};
  ${text};
  ${getResponsiveFont(getResponsiveDesktopFontSize(52), '30px')};

  ${lotsOfPlayersState} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(40), '30px')};
  }
`;

const largerLabelClass = css`
  ${text};
  ${getResponsiveFont(getResponsiveDesktopFontSize(60), '30px')};

  ${lotsOfPlayersState} {
    white-space: nowrap;
    ${getResponsiveFont(getResponsiveDesktopFontSize(40), '30px')};
  }
`;

const smallerLabelClass = css`
  ${animations.includeAnimation(
    animations.animations.slideFadeUp,
    { duration: '1000ms', delay: '4500ms' },
    true
  )};
  ${text};
  margin-top: 60px;
  ${getResponsiveFont(getResponsiveDesktopFontSize(40), '30px')};

  ${lotsOfPlayersState} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(30), '20px')};
    margin-top: 30px;
  }
`;

const codeClassWrapperClass = css`
  margin-left: 45px;
  ${lotsOfPlayersState} {
    flex: 1;
    overflow: hidden;
    padding: 5px;
  }
`;

const codeContainerClass = css`
  ${animations.includeAnimation(
    animations.animations.slideFadeUp,
    { duration: '500ms', delay: '2250ms' },
    true
  )};
  display: flex;
  justify-content: center;
  align-items: center;

  ${lotsOfPlayersState} {
    width: 100%;
  }
`;

const codeBackgroundClass = css`
  /* Rectangle 10: */
  background: #47a8c4;
  /* Sketch doesnt export pattern fills at this point */
  background-image: linear-gradient(
    -135deg,
    rgba(255, 255, 255, 0.1) 0%,
    rgba(92, 187, 215, 0) 100%
  );
  box-shadow: 0 2px 4px 0 rgba(26, 103, 126, 0.32), 2px 2px 0 0 rgba(17, 65, 86, 0.27),
    inset -1px -1px 0 0 rgba(255, 255, 255, 0.16), inset 1px 1px 0 0 rgba(255, 255, 255, 0.5);
  border-radius: 22px;
  padding: 20px;
  ${tiledBackgroundImage(assets.patterns.fruitPattern)};
`;

const codeClass = css`
  ${codeBackgroundClass};
  ${utils.fonts.families.displayFancy};
  ${getResponsiveFont(getResponsiveDesktopFontSize(74.5), '30px')};
  color: #ffffff;
  letter-spacing: 2.75px;
  text-shadow: 1px 2px 0 rgba(41, 125, 148, 0.44), 0 2px 1px rgba(79, 140, 165, 0.44);
  height: 10vh;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;

  span {
    display: inline-block;
    position: relative;
    top: 5px;
  }
`;

const siteClass = css`
  margin: 45px 0;
`;

const slotClass = css`
  height: 350px;
  height: 32vh;
  width: 290px;
  width: 26vh;
  position: relative;
`;

const slotEmptyClass = css`
  ${getResponsiveFont(getResponsiveDesktopFontSize(204), '30px')};
  ${utils.fonts.families.displayFancy};
  content: '?';
  position: absolute;
  top: 10px;
  left: 10px;
  right: 10px;
  bottom: 10px;
  background-color: #75c2e3;
  border-radius: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #64b4d7;
  box-shadow: inset 0 -1px 1px 0 rgba(55, 125, 157, 0.11),
    inset 2px 1px 1px 0 rgba(52, 115, 144, 0.07);
  opacity: 0.9;
`;

const slotContentClass = css`
  position: relative;
  width: 100%;
  height: 100%;
`;

const bottomContainerClass = css`
  position: relative;
`;

export default {
  containerClass,
  textContainerClass,
  textContentWrapperClass,
  textContentClass,
  textFirstWrapper,
  textSecondWrapper,
  siteClass,
  labelClass,
  largerLabelClass,
  smallerLabelClass,
  codeClassWrapperClass,
  codeContainerClass,
  codeClass,
  bottomContainerClass,
  slotClass,
  slotEmptyClass,
  slotContentClass,
};
