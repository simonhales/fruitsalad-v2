// @flow
import React from 'react';
import styles from './styles';
import { FancyHeading } from '../../../components/elements';
import SplitText from '../../../components/text/SplitText/SplitText';
import TextWiggle from '../../../components/text/TextWiggle/TextWiggle';

const DisplayRoundCompletedView = () => (
  <div className={styles.containerClass}>
    <FancyHeading>
      <TextWiggle>
        <SplitText text="final round" />
      </TextWiggle>
    </FancyHeading>
  </div>
);

export default DisplayRoundCompletedView;
