// @flow

import { css } from 'emotion';
import { absoluteFullView, flexFullCentered } from '../../../styles/utils/layout';

const containerClass = css`
  ${absoluteFullView};
  ${flexFullCentered};
`;

export default {
  containerClass,
};
