// @flow
import React, { Component } from 'react';
import styles from './styles';
import DisplayViewTransitions from '../../../components/display/DisplayViewTransitions/DisplayViewTransitions';
import DisplayEntryView from '../DisplayEntryView/DisplayEntryView';
import type { EntryModel } from '../../../firebase/bananart/game/entry/models';
import { getEntryKey, getEntryState } from '../../../firebase/bananart/game/entry/state';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import DrawingsManager from '../../../components/display/DrawingsManager/DrawingsManager';

type Props = {
  entry: EntryModel,
};

class DisplayVotingView extends Component<Props> {
  render() {
    const { entry } = this.props;
    const entryKey = getEntryKey(entry);
    const entryState = getEntryState(entry);
    return (
      <React.Fragment>
        <DisplayViewTransitions className={styles.containerClass}>
          <DisplayEntryView entryState={entryState} key={entryKey} />
        </DisplayViewTransitions>
        <DrawingsManager />
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentEntry } = state;
  if (!currentEntry) {
    throw new Error(`No current entry.`);
  }
  return {
    entry: currentEntry,
  };
};

export default withDisplaySessionContext(mapStateToProps)(DisplayVotingView);
