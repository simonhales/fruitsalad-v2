// @flow
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import DisplayHubView from '../../display/DisplayHubView/DisplayHubView';
import styles from '../../display/DisplayView/styles';
import { DUMMY_HUB_SLOTS } from '../../../data/dummy/session';
import DevControllerHubView from './controller/DevControllerHubView';
import DevNarratorView from '../DevNarratorView/DevNarratorView';

const DevDisplayHubView = () => <DisplayHubView slots={DUMMY_HUB_SLOTS} sessionKey="lemons" />;

type MatchProps = {
  match: {
    url: string,
  },
};

const DisplayView = ({ match }: MatchProps) => (
  <div className={styles.containerClass}>
    <Switch>
      <Route path={`${match.url}/hub`} component={DevDisplayHubView} />
    </Switch>
  </div>
);

const DevControllerView = ({ match }: MatchProps) => (
  <Switch>
    <Route path={`${match.url}/hub`} component={DevControllerHubView} />
  </Switch>
);

const DevView = ({ match }: MatchProps) => (
  <React.Fragment>
    <Switch>
      <Route path={`${match.url}/narrator`} component={DevNarratorView} />
      <Route path={`${match.url}/display`} component={DisplayView} />
      <Route path={`${match.url}/controller`} component={DevControllerView} />
    </Switch>
  </React.Fragment>
);

export default DevView;
