// @flow
import React from 'react';
import { ControllerViewRaw } from '../../../controller/ControllerView/ControllerView';
import { DUMMY_SESSION } from '../../../../data/dummy/session';

const userKey = 'chiao';

const DevControllerHubView = () => (
  <ControllerViewRaw currentUserKey={userKey} session={DUMMY_SESSION} />
);

export default DevControllerHubView;
