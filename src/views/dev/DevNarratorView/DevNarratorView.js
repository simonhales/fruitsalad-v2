// @flow
import React, { Component } from 'react';
import AudioManager from '../../../components/display/AudioManager/AudioManager';

type Props = {};

class DevNarratorView extends Component<Props> {
  render() {
    return (
      <div>
        <AudioManager />
        DevNarratorView
      </div>
    );
  }
}

export default DevNarratorView;
