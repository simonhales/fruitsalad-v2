// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import ControllerActionButton from '../../../components/controller/ControllerActionButton/ControllerActionButton';
import {
  addBotToSession,
  kickUserFromSession,
  setSessionState,
} from '../../../firebase/bananart/session/actions';
import styles from './styles';
import { sessionStates } from '../../../firebase/bananart/session/models';
import ControllerButton, {
  controllerButtonProps,
} from '../../../components/controller/ControllerButton/ControllerButton';
import AuthWrapper from '../../../components/AuthWrapper/AuthWrapper';
import DisplaySessionFirebaseConnector from '../../../components/display/DisplaySessionFirebaseConnector/DisplaySessionFirebaseConnector';
import SetSessionKey from '../../../components/SetSessionKey/SetSessionKey';
import type { ReduxState } from '../../../state/redux/shared/models';
import { getReduxSessionKey } from '../../../state/redux/firebase/state';
import {
  getSessionFromState,
  getSessionWrapperFromState,
} from '../../../state/redux/rawFirebase/state';
import type { SessionModel, SessionStates } from '../../../firebase/bananart/session/models';
import {
  getSessionFromSessionWrapper,
  getSessionState,
  getSessionUsers,
} from '../../../firebase/bananart/session/state';
import { roundStates } from '../../../firebase/bananart/game/round/models';
import {
  getCurrentGameFromSession,
  getGameEntriesList,
  getGameKey,
  getGameRoundsList,
  getGameState,
  getRoundEntriesList,
} from '../../../firebase/bananart/game/state';
import { gameStates } from '../../../firebase/bananart/game/models';
import {
  setGameCurrentEntry,
  setGameCurrentRound,
  setGameState,
} from '../../../firebase/bananart/game/actions';
import type { GameModel, GameStates } from '../../../firebase/bananart/game/models';
import type { RoundModel, RoundStates } from '../../../firebase/bananart/game/round/models';
import {
  getGameCurrentRound,
  getRoundKey,
  getRoundState,
} from '../../../firebase/bananart/game/round/state';
import { setRoundState } from '../../../firebase/bananart/game/round/actions';
import {
  getEntryAnswersToRevealSorted,
  getEntryKey,
  getEntryState,
  getGameCurrentEntry,
} from '../../../firebase/bananart/game/entry/state';
import type { EntryStates } from '../../../firebase/bananart/game/entry/models';
import {
  setEntryAnswerRevealIndex,
  setEntryState,
} from '../../../firebase/bananart/game/entry/actions';
import { entryStates } from '../../../firebase/bananart/game/entry/models';

type Props = {
  sessionKey: string,
  session: SessionModel | null,
};

class DebugView extends Component<Props> {
  getSessionKey() {
    const { sessionKey } = this.props;
    return sessionKey;
  }

  addBot = () => {
    const sessionKey = this.getSessionKey();
    addBotToSession(sessionKey);
  };

  removeUser = (userKey: string) => {
    const sessionKey = this.getSessionKey();
    kickUserFromSession(userKey, sessionKey);
  };

  setSessionState = (state: SessionStates) => {
    const sessionKey = this.getSessionKey();
    setSessionState(sessionKey, state);
  };

  setEntryState = (state: EntryStates, gameKey: string, entryKey: string) => {
    const sessionKey = this.getSessionKey();
    setEntryState(state, entryKey, sessionKey, gameKey);
  };

  setRoundState = (state: RoundStates, gameKey: string, roundKey: string) => {
    const sessionKey = this.getSessionKey();
    setRoundState(state, sessionKey, gameKey, roundKey);
  };

  setGameState = (state: GameStates, gameKey: string) => {
    const sessionKey = this.getSessionKey();
    setGameState(sessionKey, gameKey, state);
  };

  setCurrentRound = (gameKey: string, roundKey: string) => {
    const sessionKey = this.getSessionKey();
    setGameCurrentRound(sessionKey, gameKey, roundKey);
  };

  setCurrentEntry = (gameKey: string, entryKey: string) => {
    const sessionKey = this.getSessionKey();
    setGameCurrentEntry(sessionKey, gameKey, entryKey);
  };

  setCurrentEntryAnswerIndex = (gameKey: string, entryKey: string, index: number) => {
    const sessionKey = this.getSessionKey();
    setEntryAnswerRevealIndex(index, sessionKey, gameKey, entryKey);
  };

  renderGameSection(game: GameModel) {
    const gameKey = getGameKey(game);
    return (
      <div className={styles.sectionClass}>
        <div className={styles.labelClass}>game: {getGameState(game)}</div>
        <div>
          {Object.keys(gameStates).map(state => (
            <div className={styles.buttonClass} key={state}>
              <ControllerButton
                type={controllerButtonProps.types.soft}
                onClick={() => this.setGameState(state, gameKey)}
              >
                SET STATE: {state}
              </ControllerButton>
            </div>
          ))}
        </div>
      </div>
    );
  }

  renderRoundSection(game: GameModel) {
    const currentRound = getGameCurrentRound(game);
    if (!currentRound) return null;
    const gameKey = getGameKey(game);
    const roundKey = getRoundKey(currentRound);
    return (
      <div className={styles.sectionClass}>
        <div className={styles.labelClass}>round: {getRoundState(currentRound)}</div>
        <div>
          {Object.keys(roundStates).map(state => (
            <div className={styles.buttonClass} key={state}>
              <ControllerButton
                type={controllerButtonProps.types.soft}
                onClick={() => this.setRoundState(state, gameKey, roundKey)}
              >
                SET STATE: {state}
              </ControllerButton>
            </div>
          ))}
        </div>
      </div>
    );
  }

  renderEntrySection(game: GameModel) {
    const currentEntry = getGameCurrentEntry(game);
    if (!currentEntry) return null;
    const gameKey = getGameKey(game);
    const entryKey = getEntryKey(currentEntry);
    return (
      <div className={styles.sectionClass}>
        <div className={styles.labelClass}>entry: {getEntryState(currentEntry)}</div>
        <div>
          {Object.keys(entryStates).map(state => (
            <div className={styles.buttonClass} key={state}>
              <ControllerButton
                type={controllerButtonProps.types.soft}
                onClick={() => this.setEntryState(state, gameKey, entryKey)}
              >
                SET STATE: {state}
              </ControllerButton>
            </div>
          ))}
        </div>
      </div>
    );
  }

  renderEntryOptions() {
    const { session } = this.props;
    if (!session) return null;
    const currentGame = getCurrentGameFromSession(session);
    if (!currentGame) return null;
    return <React.Fragment>{this.renderEntrySection(currentGame)}</React.Fragment>;
  }

  renderEntryAnswers() {
    const { session } = this.props;
    if (!session) return null;
    const currentGame = getCurrentGameFromSession(session);
    if (!currentGame) return null;
    const currentEntry = getGameCurrentEntry(currentGame);
    if (!currentEntry) return null;
    const answers = getEntryAnswersToRevealSorted(currentEntry);
    const gameKey = getGameKey(currentGame);
    const entryKey = getEntryKey(currentEntry);
    return (
      <div className={styles.sectionClass}>
        <div className={styles.labelClass}>
          answers to reveal index: {currentEntry.currentAnswerRevealIndex}
        </div>
        <div
          className={styles.userRemoveClass}
          onClick={() => this.setCurrentEntryAnswerIndex(gameKey, entryKey, -1)}
        >
          set to -1
        </div>
        <div>
          {answers.map((answer, index) => (
            <div
              className={styles.userRemoveClass}
              onClick={() => this.setCurrentEntryAnswerIndex(gameKey, entryKey, index)}
              key={answer.key}
            >
              {answer.key}
            </div>
          ))}
        </div>
      </div>
    );
  }

  renderEntryExtraOptions() {
    return <React.Fragment>{this.renderEntryAnswers()}</React.Fragment>;
  }

  renderRoundEntries(game: GameModel, round: RoundModel) {
    const gameKey = getGameKey(game);
    const entries = getRoundEntriesList(game, round);
    return (
      <div>
        {entries.map(entry => (
          <div
            onClick={() => this.setCurrentEntry(gameKey, entry.key)}
            className={styles.userRemoveClass}
            key={entry.key}
          >
            entry: {entry.key}
          </div>
        ))}
      </div>
    );
  }

  renderAllRoundsSection(game: GameModel) {
    const gameKey = getGameKey(game);
    const currentRound = getGameCurrentRound(game);
    const rounds = getGameRoundsList(game);
    const currentEntry = getGameCurrentEntry(game);
    return (
      <div className={styles.sectionClass}>
        <div className={styles.labelClass}>
          rounds
          {currentRound && ` (${currentRound.key})`}
        </div>
        <div className={styles.labelClass}>
          currentEntry
          {currentEntry && ` (${currentEntry.key})`}
        </div>
        <div className={styles.usersListClass}>
          {rounds.map(round => (
            <div className={styles.userClass} key={round.key}>
              <div>{round.key}</div>
              <div
                onClick={() => this.setCurrentRound(gameKey, round.key)}
                className={styles.userRemoveClass}
              >
                set current round
              </div>
              {this.renderRoundEntries(game, round)}
            </div>
          ))}
        </div>
      </div>
    );
  }

  renderAllEntriesSection(game: GameModel) {
    const currentEntry = getGameCurrentEntry(game);
    const entries = getGameEntriesList(game);
    return (
      <div className={styles.sectionClass}>
        <div className={styles.labelClass}>
          entries
          {currentEntry && ` (${currentEntry.key})`}
        </div>
        <div className={styles.usersListClass}>
          {entries.map(entry => (
            <div className={styles.userClass} key={entry.key}>
              <div>{entry.key}</div>
              <div onClick={() => {}} className={styles.userRemoveClass}>
                set current entry
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }

  renderCurrentSelectionSection(game: GameModel) {
    return <React.Fragment>{this.renderAllRoundsSection(game)}</React.Fragment>;
  }

  renderGameOptions() {
    const { session } = this.props;
    if (!session) return null;
    const currentGame = getCurrentGameFromSession(session);
    if (!currentGame) return null;
    return (
      <React.Fragment>
        {this.renderCurrentSelectionSection(currentGame)}
        {this.renderGameSection(currentGame)}
        {this.renderRoundSection(currentGame)}
      </React.Fragment>
    );
  }

  renderUsersSection() {
    const { session } = this.props;
    const users = getSessionUsers(session);
    return (
      <div className={styles.sectionClass}>
        <div className={styles.labelClass}>users</div>
        <div>
          <ControllerActionButton onClick={this.addBot}>ADD BOT</ControllerActionButton>
        </div>
        <div className={styles.usersListClass}>
          {users.map(user => (
            <div className={styles.userClass} key={user.key}>
              <div>
                {user.name} {user.key}
              </div>
              <div onClick={() => this.removeUser(user.key)} className={styles.userRemoveClass}>
                remove user
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }

  render() {
    const { session } = this.props;
    return (
      <div className={styles.containerClass}>
        <div className={styles.columnClass}>
          {this.renderUsersSection()}
          <div className={styles.sectionClass}>
            <div className={styles.labelClass}>session: {session && getSessionState(session)}</div>
            <div>
              {Object.keys(sessionStates).map(state => (
                <div className={styles.buttonClass} key={state}>
                  <ControllerButton
                    type={controllerButtonProps.types.soft}
                    onClick={() => this.setSessionState(state)}
                  >
                    SET STATE: {state}
                  </ControllerButton>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className={styles.columnClass}>{this.renderGameOptions()}</div>
        <div className={styles.columnClass}>{this.renderEntryOptions()}</div>
        <div className={styles.columnClass}>{this.renderEntryExtraOptions()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state: ReduxState) => {
  const sessionWrapper = getSessionWrapperFromState(state.rawFirebase);
  const session = sessionWrapper ? getSessionFromSessionWrapper(sessionWrapper) : null;
  return {
    sessionKey: getReduxSessionKey(state.firebase),
    session,
  };
};

const ConnectedDebugView = connect(mapStateToProps)(DebugView);

const Wrapper = () => (
  <SetSessionKey>
    <AuthWrapper>
      <DisplaySessionFirebaseConnector>
        <ConnectedDebugView />
      </DisplaySessionFirebaseConnector>
    </AuthWrapper>
  </SetSessionKey>
);

export default Wrapper;
