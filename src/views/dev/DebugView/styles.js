// @flow

import { css } from 'emotion';

const containerClass = css`
  display: flex;
`;

const columnClass = css`
  margin: 20px;
  flex: 1;
`;

const sectionClass = css`
  background: rgba(0, 0, 0, 0.15);
  padding: 20px;
  margin: 20px 0;
  text-align: center;
`;

const labelClass = css`
  color: white;
  margin-bottom: 10px;
`;

const buttonClass = css`
  margin-top: 10px;
`;

const usersListClass = css`
  max-height: 200px;
  overflow-y: auto;
`;

const userClass = css`
  margin-top: 15px;
`;

const userRemoveClass = css`
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }
`;

export default {
  containerClass,
  columnClass,
  sectionClass,
  labelClass,
  buttonClass,
  usersListClass,
  userClass,
  userRemoveClass,
};
