// @flow

export const GAME_INTRO_TIMER = 30;
export const GAME_RESULTS_TIMER = 20;
export const DRAWING_INTRO_TIMER = 7 * 1000;
export const DRAWING_TIMER = 120 * 1000;
export const DRAWING_COMPLETED = 4 * 1000;
export const ROUND_RESULTS_TIMER = 10 * 1000;
export const ENTRY_INTRO_TIMER = 5 * 1000;
export const ENTRY_NO_DRAWINGS_TIMER = 4 * 1000;
export const ENTRY_GUESSING_INTRO_TIMER = 4 * 1000;
export const ENTRY_GUESSING_TIMER = 40 * 1000;
export const ENTRY_GUESSING_COMPLETED_TIMER = 4 * 1000;
export const ENTRY_VOTING_INTRO_TIMER = 4 * 1000;
export const ENTRY_VOTING_TIMER = 25 * 1000;
export const ENTRY_VOTING_COMPLETED_TIMER = 4 * 1000;
export const ENTRY_RESULTS_TIMER = 2 * 1000;
export const ENTRY_COMPLETED_TIMER = 4 * 1000;
export const ENTRY_COMPLETED_POST_SCORE_TIMER = 6 * 1000;

const ENTRY_ANSWER_INITIAL_DELAY = 3000;
const ENTRY_ANSWER_USER_DELAY = 500;
const ENTRY_ANSWER_REVEAL_DELAY = 1000;
const ENTRY_ANSWER_ARTIST_DELAY = 250;

export function getVoterAnimationDelay(index: number): number {
  return ENTRY_ANSWER_INITIAL_DELAY + index * ENTRY_ANSWER_USER_DELAY;
}

export function getArtistAnimationDelay(index: number, totalVoters: number): number {
  const lastVoterDelay = getVoterAnimationDelay(totalVoters - 1);
  return lastVoterDelay + ENTRY_ANSWER_REVEAL_DELAY + index * ENTRY_ANSWER_ARTIST_DELAY;
}

export function getAnswerResultsDuration(totalVoters: number, totalArtists: number): number {
  const artistDelay = getArtistAnimationDelay(totalArtists - 1, totalVoters);
  return artistDelay + 3000;
}
