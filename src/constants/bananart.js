export const DEFAULT_NUMBER_OF_ROUNDS = 2;

export const MINIMUM_NUMBER_OF_PLAYERS = 3;

export const PROMPT_KEY = 'prompt';
