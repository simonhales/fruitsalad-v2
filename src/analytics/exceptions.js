// @flow
import ReactGA from 'react-ga';

const logException = (description: string, fatal: boolean = false) => {
  ReactGA.exception({
    description,
    fatal,
  });
};

export default {
  logException,
};
