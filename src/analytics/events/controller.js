// @flow
import { sendEvent } from './shared';

const CONTROLLER_CATEGORY = 'controller';

function sessionNotFound(sessionKey: string) {
  return sendEvent({
    category: CONTROLLER_CATEGORY,
    action: 'SessionNotFound',
    label: `sessionKey::${sessionKey}`,
  });
}

function goToChat() {
  return sendEvent({
    category: CONTROLLER_CATEGORY,
    action: 'GoToChat',
  });
}

function returnToPlay() {
  return sendEvent({
    category: CONTROLLER_CATEGORY,
    action: 'ReturnToPlay',
  });
}

function sendChatMessage() {
  return sendEvent({
    category: CONTROLLER_CATEGORY,
    action: 'SendChatMessage',
  });
}

function chatTabClicked() {
  return sendEvent({
    category: CONTROLLER_CATEGORY,
    action: 'ChatTabClicked',
  });
}

function playTabClicked() {
  return sendEvent({
    category: CONTROLLER_CATEGORY,
    action: 'PlayTabClicked',
  });
}

export default {
  sessionNotFound,
  goToChat,
  returnToPlay,
  sendChatMessage,
  chatTabClicked,
  playTabClicked,
};
