// @flow
import { sendEvent } from './shared';

const CONTROLLER_GUESSING = 'guessing';

function submittedViaForm() {
  return sendEvent({
    category: CONTROLLER_GUESSING,
    action: 'SubmittedViaForm',
  });
}

function submittedViaButton() {
  return sendEvent({
    category: CONTROLLER_GUESSING,
    action: 'SubmittedViaButton',
  });
}

function guessSubmitting() {
  return sendEvent({
    category: CONTROLLER_GUESSING,
    action: 'GuessSubmitting',
  });
}

function guessSubmitted(guess: string) {
  return sendEvent({
    category: CONTROLLER_GUESSING,
    action: 'GuessSubmitted',
    label: `guess: ${guess}`,
  });
}

export default {
  submittedViaForm,
  submittedViaButton,
  guessSubmitting,
  guessSubmitted,
};
