// @flow

import { sendEvent } from './shared';

const HUB_CATEGORY = 'hub';

function promoteUserToHost(userKey: string) {
  return sendEvent({
    category: HUB_CATEGORY,
    action: 'PromoteUserToHost',
    label: `userKey::${userKey}`,
  });
}

function kickUserFromSession(userKey: string) {
  return sendEvent({
    category: HUB_CATEGORY,
    action: 'KickUserFromSession',
    label: `userKey::${userKey}`,
  });
}

export default {
  promoteUserToHost,
  kickUserFromSession,
};
