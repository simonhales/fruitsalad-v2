// @flow
import { sendEvent } from './shared';

const DISPLAY_CATEGORY = 'display';

function sessionNotFound(sessionKey: string) {
  return sendEvent({
    category: DISPLAY_CATEGORY,
    action: 'SessionNotFound',
    label: `sessionKey::${sessionKey}`,
  });
}

export default {
  sessionNotFound,
}
