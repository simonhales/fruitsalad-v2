// @flow
import controller from './controller';
import guessing from './guessing';
import hub from './hub';
import display from './display';

export default {
  controller,
  display,
  guessing,
  hub,
};
