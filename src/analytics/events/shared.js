// @flow
import ReactGA from 'react-ga';

export type EventArguments = {
  category: string,
  action: string,
  label?: string,
};

export function sendEvent(eventArgs: EventArguments) {
  ReactGA.event(eventArgs);
}
