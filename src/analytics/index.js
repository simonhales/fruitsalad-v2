// @flow
import ReactGA from 'react-ga';
import { getEnvGoogleAnalyticsId, isDevMode } from '../utils/env';
import events from './events/index';
import exceptions from './exceptions';

ReactGA.initialize(getEnvGoogleAnalyticsId(), {
  debug: isDevMode(),
});

export default {
  events,
  exceptions,
};
