// @flow
import * as React from 'react';
import type { SessionModel } from '../../firebase/bananart/session/models';
import DisplayHubViewWithContext from '../../views/display/DisplayHubView/context';
import { getSessionState } from '../../firebase/bananart/session/state';
import { sessionStates } from '../../firebase/bananart/session/models';
import { getGameCurrentRound, getRoundState } from '../../firebase/bananart/game/round/state';
import { getCurrentGameFromSession, getGameState } from '../../firebase/bananart/game/state';
import { gameStates } from '../../firebase/bananart/game/models';
import type { RoundModel } from '../../firebase/bananart/game/round/models';
import { roundStates } from '../../firebase/bananart/game/round/models';
import DisplayDrawingViewWithContext from '../../views/display/DisplayDrawingView/context';
import DisplayVotingView from '../../views/display/DisplayVotingView/DisplayVotingView';
import DisplayRoundCompletedView from '../../views/display/DisplayRoundCompletedView/DisplayRoundCompletedView';

export type DisplayViewModel = {
  key: string,
  component: () => React.Node,
};

const unknownDisplayView: DisplayViewModel = {
  key: 'unknown',
  component: () => <React.Fragment />,
};

function getRoundDisplayView(round: RoundModel): DisplayViewModel {
  const roundState = getRoundState(round);

  switch (roundState) {
    case roundStates.drawingIntro:
    case roundStates.drawing:
    case roundStates.drawingSubmitted:
    case roundStates.drawingTimesUp:
      return {
        key: 'drawing',
        component: () => <DisplayDrawingViewWithContext />,
      };
    case roundStates.voting:
      return {
        key: 'voting',
        component: () => <DisplayVotingView />,
      };
    case roundStates.completed:
      return {
        key: 'roundCompleted',
        component: () => <DisplayRoundCompletedView />,
      };
    default:
      break;
  }

  return unknownDisplayView;
}

function getSessionPlayingDisplayView(session: SessionModel): DisplayViewModel {
  const game = getCurrentGameFromSession(session);

  if (game) {
    const gameState = getGameState(game);

    if (gameState === gameStates.playing) {
      const round = getGameCurrentRound(game);

      if (round) {
        return getRoundDisplayView(round);
      }
    }
  }

  return unknownDisplayView;
}

export function getDisplayView(session: SessionModel): DisplayViewModel {
  const sessionState = getSessionState(session);

  switch (sessionState) {
    case sessionStates.pending:
    case sessionStates.starting:
      return {
        key: 'hub',
        component: () => <DisplayHubViewWithContext />,
      };
    case sessionStates.playing:
      return getSessionPlayingDisplayView(session);
    default:
      break;
  }

  return unknownDisplayView;
}
