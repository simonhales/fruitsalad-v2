// @flow

import type { DisplaySessionContextState } from './DisplaySessionContext';
import type {
  SessionChatModel,
  SessionChatUserDrawingModel,
} from '../../../firebase/bananart/chat/models';

export function getSessionChat(state: DisplaySessionContextState): SessionChatModel {
  return state.sessionChat;
}

export function getUserChatDrawing(
  state: DisplaySessionContextState,
  userKey: string
): SessionChatUserDrawingModel | null {
  const sessionChat = getSessionChat(state);
  if (sessionChat.hasOwnProperty(userKey)) {
    const userChat = sessionChat[userKey];
    if (userChat.drawings) {
      const drawingKeys = Object.keys(userChat.drawings);
      if (drawingKeys.length > 0) {
        return userChat.drawings[drawingKeys[0]];
      }
    }
  }
  return null;
}
