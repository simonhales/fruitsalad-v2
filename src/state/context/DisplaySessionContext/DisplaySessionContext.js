// @flow
import * as React from 'react';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import type { GameModel } from '../../../firebase/bananart/game/models';
import type { RoundModel } from '../../../firebase/bananart/game/round/models';
import type { EntryModel } from '../../../firebase/bananart/game/entry/models';
import type { SessionChatModel } from '../../../firebase/bananart/chat/models';

export type DisplaySessionContextState = {
  sessionKey: string,
  session: SessionModel | null,
  sessionChat: SessionChatModel,
  currentGame?: GameModel | null,
  currentRound?: RoundModel | null,
  currentEntry?: EntryModel | null,
  drawings: {
    [string]: string,
  },
  addDrawing: (drawingKey: string, drawing: string) => void,
};

const defaultState: DisplaySessionContextState = {
  sessionKey: '',
  session: null,
  sessionChat: {},
  currentGame: null,
  currentRound: null,
  currentEntry: null,
  drawings: {},
  addDrawing: () => {},
};

const DisplaySessionContext = React.createContext(defaultState);

export default DisplaySessionContext;

type MapStateToProps = (
  state: DisplaySessionContextState,
  props: {
    [string]: any,
  }
) => {};

const defaultMapStateToProps = () => ({});

export function withDisplaySessionContext(
  mapStateToProps: MapStateToProps = defaultMapStateToProps
) {
  return function WrappedComponent(Component: any) {
    // had to cast to any due to flow issues with React.Node
    return function Wrapper(props: {}) {
      return (
        <DisplaySessionContext.Consumer>
          {value => <Component {...props} {...mapStateToProps(value, props)} />}
        </DisplaySessionContext.Consumer>
      );
    };
  };
}
