/* eslint-disable prettier/prettier */
// @flow
import * as React from 'react';
import type { GameModel } from '../../../firebase/bananart/game/models';
import type { RoundModel } from '../../../firebase/bananart/game/round/models';
import type { EntryModel } from '../../../firebase/bananart/game/entry/models';
import type { SessionModel } from '../../../firebase/bananart/session/models';

export type ControllerSessionContextState = {
  currentUserKey: string,
  session: SessionModel | null,
  currentGame?: GameModel | null,
  currentRound?: RoundModel | null,
  currentEntry?: EntryModel | null,
};

const defaultState: ControllerSessionContextState = {
  currentUserKey: '',
  session: null,
  currentGame: null,
  currentRound: null,
  currentEntry: null,
}

const ControllerSessionContext = React.createContext(defaultState);

export default ControllerSessionContext;

type MapStateToProps = (state: ControllerSessionContextState) => {};

function defaultMapStateToProps(state: {}): {} {
  return state;
}

export function withControllerSessionContext(mapStateToProps: MapStateToProps = defaultMapStateToProps) {
  return function WrappedComponent(Component: any) { // had to cast to any due to flow issues with React.Node
    return function Wrapper(props: {}) {
      return (
        <ControllerSessionContext.Consumer>
          {
            value => (
              <Component {...props} {...mapStateToProps(value)}/>
            )
          }
        </ControllerSessionContext.Consumer>
      )
    }
  }
}
