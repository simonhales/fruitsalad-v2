// @flow
import type { ControllerSessionContextState } from './ControllerSessionContext';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import type { GameModel } from '../../../firebase/bananart/game/models';
import type { RoundModel } from '../../../firebase/bananart/game/round/models';
import type {EntryModel} from '../../../firebase/bananart/game/entry/models';

export function getContextCurrentUserKey(state: ControllerSessionContextState): string {
  return state.currentUserKey;
}

export function getContextSession(state: ControllerSessionContextState): SessionModel {
  const { session } = state;
  if (!session) {
    throw new Error(`Session is missing`);
  }
  return session;
}

export function getContextCurrentGame(state: ControllerSessionContextState): GameModel | null {
  const { currentGame } = state;
  return currentGame || null;
}

export function getContextCurrentRound(state: ControllerSessionContextState): RoundModel | null {
  const { currentRound } = state;
  return currentRound || null;
}


export function getContextCurrentEntry(state: ControllerSessionContextState): EntryModel | null {
  const { currentEntry } = state;
  return currentEntry || null;
}
