// @flow

import type { SessionModel } from '../../firebase/bananart/session/models';
import {
  getGameCurrentRound,
  getRoundKey,
  hasUserSubmittedDrawing,
  isGameDrawingMainStates,
} from '../../firebase/bananart/game/round/state';
import { getCurrentGameFromSession, isUserInGame } from '../../firebase/bananart/game/state';
import {
  getGameCurrentEntry,
  hasUserSubmittedGuess,
  hasUserSubmittedVote,
  isGameGuessingMainStates,
  isGameVotingMainStates,
} from '../../firebase/bananart/game/entry/state';
import { isSessionStatePlaying } from '../../firebase/bananart/session/state';

export function isUserChatDisabled(session: SessionModel, userKey: string): boolean {
  if (!isSessionStatePlaying(session)) {
    return false;
  }

  const game = getCurrentGameFromSession(session);

  if (!game) return false;

  if (!isUserInGame(userKey, game)) {
    return true;
  }

  if (isGameDrawingMainStates(game)) {
    const round = getGameCurrentRound(game);
    if (round) {
      const roundKey = getRoundKey(round);
      return !hasUserSubmittedDrawing(game, roundKey, userKey);
    }
  } else if (isGameGuessingMainStates(game)) {
    const entry = getGameCurrentEntry(game);
    if (entry) {
      return !hasUserSubmittedGuess(entry, userKey);
    }
  } else if (isGameVotingMainStates(game)) {
    const entry = getGameCurrentEntry(game);
    if (entry) {
      return !hasUserSubmittedVote(entry, userKey);
    }
  }

  return false;
}
