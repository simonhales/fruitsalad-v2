// @flow
import type { SlotCardModel } from './models';
import type { SessionUserModel } from '../../../firebase/bananart/session/user/models';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import { isSessionStatePending } from '../../../firebase/bananart/session/state';

function getKeyFromUserKeyAndJoinTimestamp(key: string, timestamp: number) {
  return `${key}:${timestamp.toString()}`;
}

export function getSessionSlotCards(
  users: Array<SessionUserModel>,
  session: SessionModel | null
): Array<SlotCardModel> {
  const userSlots: Array<SlotCardModel> = users.map(user => ({
    key: getKeyFromUserKeyAndJoinTimestamp(user.key, user.timestamps.joined),
    empty: false,
    user: {
      name: user.name,
      disabled: !user.online,
      joinedTimestamp: user.timestamps.joined,
      key: user.key,
    },
  }));
  const sessionStatePending = session ? isSessionStatePending(session) : true;
  const userSlotsThreshold = sessionStatePending ? 5 : 6;
  const numberOfEmptySlots =
    userSlots.length > userSlotsThreshold ? 12 - userSlots.length : 6 - userSlots.length;
  const showEmptySlots = sessionStatePending;
  const emptySlots: Array<SlotCardModel> = Array.from({ length: numberOfEmptySlots }).map(
    (item, index) => ({
      key: (userSlots.length + index).toString(),
      empty: true,
      emptyVisible: showEmptySlots,
    })
  );
  return userSlots.concat(emptySlots);
}
