import { getSessionSlotCards } from './state';
import {
  MOCK_SESSION_FIVE_USERS,
  MOCK_SESSION_SEVEN_USERS,
} from '../../../../__mocks__/bananart/mockSessionUsers';

jest.mock('react-ga');

describe('state -> session:: state', () => {
  describe('checks for correct number of slots', () => {
    test('getSessionSlotCards for 5 players returns 6 slots', () => {
      const slots = getSessionSlotCards(MOCK_SESSION_FIVE_USERS, null);
      expect(slots.length).toEqual(6);
    });
    test('getSessionSlotCards for 7 players returns 12 slots', () => {
      const slots = getSessionSlotCards(MOCK_SESSION_SEVEN_USERS, null);
      expect(slots.length).toEqual(12);
    });
  });
  describe('checks returned slots', () => {
    let slots;

    beforeEach(() => {
      slots = getSessionSlotCards([], null);
    });

    test('there are 6 empty slots', () => {
      expect(slots.length).toEqual(6);
      let numberOfEmptySlots = 0;
      slots.forEach(slot => {
        if (slot.empty) numberOfEmptySlots += 1;
      });
      expect(numberOfEmptySlots).toEqual(6);
    });
  });
});
