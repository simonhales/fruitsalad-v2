// @flow

export type SlotCardUserModel = {
  name: string,
  disabled: boolean,
  joinedTimestamp: number,
  key: string,
};

export type SlotCardModel = {
  key: string,
  empty: boolean,
  emptyVisible?: boolean,
  user?: SlotCardUserModel,
};

export type UserSlotCardModel = {
  key: string,
  empty: boolean,
  user: SlotCardUserModel,
};
