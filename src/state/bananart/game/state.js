// @flow

import type { SlotCardModel, UserSlotCardModel } from '../session/models';
import type { GameModel } from '../../../firebase/bananart/game/models';
import {
  getGameUsersList,
  getGameUsersListSortedByScore,
} from '../../../firebase/bananart/game/state';
import type { GameUserModel } from '../../../firebase/bananart/game/user/models';
import {
  getGameCurrentRound,
  hasUserSubmittedDrawing,
} from '../../../firebase/bananart/game/round/state';

export function getGameSlotCards(game: GameModel): Array<UserSlotCardModel> {
  const users = getGameUsersListSortedByScore(game);
  const round = getGameCurrentRound(game);
  return users.map((user: GameUserModel) => ({
    key: user.key,
    empty: false,
    user: {
      name: user.name,
      disabled: round ? !hasUserSubmittedDrawing(game, round.key, user.key) : false,
      joinedTimestamp: 0,
      key: user.key,
    },
  }));
}
