// @flow
import type { SessionUserModel } from '../../../firebase/bananart/session/user/models';

export type UIState = {
  userOptionsModal: SessionUserModel | null,
};

export const initialUIState: UIState = {
  userOptionsModal: null,
};

const SET_USER_OPTIONS_MODAL = 'SET_USER_OPTIONS_MODAL';

type GenericAction = {
  type: string,
  payload?: {},
};

export function setUserOptionsModal(user: SessionUserModel | null): GenericAction {
  return {
    type: SET_USER_OPTIONS_MODAL,
    payload: {
      user,
    },
  };
}

function handleSetUserOptionsModal(state: UIState, { user }): UIState {
  return {
    ...state,
    userOptionsModal: user,
  };
}

const ACTION_HANDLERS = {
  [SET_USER_OPTIONS_MODAL]: handleSetUserOptionsModal,
};

export default function uiReducer(state: UIState = initialUIState, action: GenericAction) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action.payload) : state;
}
