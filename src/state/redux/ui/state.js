// @flow
import type { UIState } from './reducer';
import type { SessionUserModel } from '../../../firebase/bananart/session/user/models';

export function getUserOptionsModal(state: UIState): SessionUserModel | null {
  return state.userOptionsModal;
}
