// @flow

import type { FirebaseState } from './reducer';

export function getCurrentUserKey(state: FirebaseState): string {
  return state.currentUserKey ? state.currentUserKey : '';
}

export function getReduxSessionKey(state: FirebaseState): string {
  return state.sessionKey;
}

export function isUsingMockSession(state: FirebaseState): boolean {
  return state.usingMockSession;
}
