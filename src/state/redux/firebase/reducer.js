// @flow

import type { SessionModel } from '../../../firebase/bananart/session/models';

export type FirebaseState = {
  currentUserKey?: string | null,
  mockSession: SessionModel | null,
  usingMockSession: boolean,
  sessionKey: string,
  clientKey: string,
};

export const initialFirebaseState: FirebaseState = {
  mockSession: null,
  usingMockSession: false,
  sessionKey: '',
  clientKey: '',
};

type GenericAction = {
  type: string,
  payload?: {},
};

const SET_SESSION_KEY = 'SET_SESSION_KEY';

export function setSessionKey(sessionKey: string): GenericAction {
  return {
    type: SET_SESSION_KEY,
    payload: {
      sessionKey,
    },
  };
}

function handleSetSessionKey(state: FirebaseState, { sessionKey }): FirebaseState {
  return {
    ...state,
    sessionKey: sessionKey.toLowerCase(),
  };
}

const SET_USING_MOCK_SESSION = 'SET_USING_MOCK_SESSION';

export function setUsingMockSession(using: boolean): GenericAction {
  return {
    type: SET_USING_MOCK_SESSION,
    payload: {
      using,
    },
  };
}

function handleSetUsingMockSession(state: FirebaseState, { using }): FirebaseState {
  return {
    ...state,
    usingMockSession: using,
  };
}

const SET_CURRENT_USER_KEY = 'SET_CURRENT_USER_KEY';

export function setCurrentUserKey(userKey: string): GenericAction {
  return {
    type: SET_CURRENT_USER_KEY,
    payload: {
      userKey,
    },
  };
}

function handleSetCurrentUserKey(state: FirebaseState, { userKey }): FirebaseState {
  return {
    ...state,
    currentUserKey: userKey,
  };
}

const SET_MOCK_SESSION = 'SET_MOCK_SESSION';

export function setMockSession(session: SessionModel): GenericAction {
  return {
    type: SET_MOCK_SESSION,
    payload: {
      session,
    },
  };
}

function handleSetMockSession(state: FirebaseState, { session }): FirebaseState {
  return {
    ...state,
    mockSession: session,
  };
}

const ACTION_HANDLERS = {
  [SET_USING_MOCK_SESSION]: handleSetUsingMockSession,
  [SET_CURRENT_USER_KEY]: handleSetCurrentUserKey,
  [SET_MOCK_SESSION]: handleSetMockSession,
  [SET_SESSION_KEY]: handleSetSessionKey,
};

export default function firebaseReducer(
  state: FirebaseState = initialFirebaseState,
  action: GenericAction
) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action.payload) : state;
}
