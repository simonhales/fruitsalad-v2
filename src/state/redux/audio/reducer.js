// @flow
import { audioMusic } from 'audio/music/AudioMusic';
import { audioNarrator } from 'audio/narrator/AudioNarrator';
import { audioSoundEffects } from 'audio/soundEffects/AudioSoundEffects';

export type AudioState = {
  musicMuted: boolean,
  narratorMuted: boolean,
  soundEffectsMuted: boolean,
};

export const initialAudioState: AudioState = {
  musicMuted: audioMusic.muted,
  narratorMuted: audioNarrator.muted,
  soundEffectsMuted: audioSoundEffects.muted,
};

type GenericAction = {
  type: string,
  payload?: {},
};

const SET_MUSIC_MUTED = 'SET_MUSIC_MUTED';
const SET_NARRATOR_MUTED = 'SET_NARRATOR_MUTED';
const SET_SOUND_EFFECTS_MUTED = 'SET_SOUND_EFFECTS_MUTED';
const SET_ALL_MUTED = 'SET_ALL_MUTED';

export function setMusicMuted(muted: boolean): GenericAction {
  return {
    type: SET_MUSIC_MUTED,
    payload: {
      muted,
    },
  };
}

function handleSetMusicMuted(state: AudioState, { muted }: { muted: boolean }): AudioState {
  audioMusic.setMuted(muted);
  return {
    ...state,
    musicMuted: muted,
  };
}

export function setNarratorMuted(muted: boolean): GenericAction {
  return {
    type: SET_NARRATOR_MUTED,
    payload: {
      muted,
    },
  };
}

function handleSetNarratorMuted(state: AudioState, { muted }: { muted: boolean }): AudioState {
  audioNarrator.setMuted(muted);
  return {
    ...state,
    narratorMuted: muted,
  };
}

export function setSoundEffectsMuted(muted: boolean): GenericAction {
  return {
    type: SET_SOUND_EFFECTS_MUTED,
    payload: {
      muted,
    },
  };
}

function handleSetSoundEffectsMuted(state: AudioState, { muted }: { muted: boolean }): AudioState {
  audioSoundEffects.setMuted(muted);
  return {
    ...state,
    soundEffectsMuted: muted,
  };
}

export function setAllMuted(muted: boolean): GenericAction {
  return {
    type: SET_ALL_MUTED,
    payload: {
      muted,
    },
  };
}

function handleSetAllMuted(state: AudioState, { muted }: { muted: boolean }): AudioState {
  audioMusic.setMuted(muted);
  audioNarrator.setMuted(muted);
  audioSoundEffects.setMuted(muted);
  return {
    ...state,
    musicMuted: muted,
    narratorMuted: muted,
    soundEffectsMuted: muted,
  };
}

const ACTION_HANDLERS = {
  [SET_MUSIC_MUTED]: handleSetMusicMuted,
  [SET_NARRATOR_MUTED]: handleSetNarratorMuted,
  [SET_SOUND_EFFECTS_MUTED]: handleSetSoundEffectsMuted,
  [SET_ALL_MUTED]: handleSetAllMuted,
};

export default function audioReducer(state: AudioState = initialAudioState, action: GenericAction) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action.payload) : state;
}
