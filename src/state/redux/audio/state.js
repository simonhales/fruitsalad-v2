// @flow

import type { AudioState } from './reducer';

export function isAllAudioMuted(state: AudioState): boolean {
  return state.musicMuted && state.narratorMuted && state.soundEffectsMuted;
}
