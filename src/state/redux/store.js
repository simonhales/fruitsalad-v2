import { createStoreWithFirebase, initialReduxState, rootReducer } from './reducers';

const configureStore = () => {
  const store = createStoreWithFirebase(rootReducer, initialReduxState);

  if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
      module.hot.accept('./reducers', () => {
        store.replaceReducer(rootReducer);
      });
    }
  }

  return store;
};

export const store = configureStore();

export default configureStore;
