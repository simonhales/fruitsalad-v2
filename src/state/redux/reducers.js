// @flow
import { combineReducers, createStore, compose } from 'redux';
import { reactReduxFirebase, firebaseReducer as reduxFirebaseReducer } from 'react-redux-firebase';
import firebase from 'firebase/index';
import uiReducer, { initialUIState } from './ui/reducer';
import firebaseReducer, { initialFirebaseState } from './firebase/reducer';
import type { ReduxState } from './shared/models';
import type { EnvProcess } from '../../utils/env';
import audioReducer, { initialAudioState } from './audio/reducer';

declare var process: EnvProcess;

const firebaseConfig = {
  apiKey: process.env.REACT_APP_FIREBASE_API_KEY,
  authDomain: process.env.REACT_APP_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_FIREBASE_DATABASE_URL,
  projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_FIREBASE_MESSAGING_SENDER_ID,
};

// react-redux-firebase config
const rrfConfig = {
  userProfile: 'users',
};

// Initialize firebase instance
firebase.initializeApp(firebaseConfig);

// Add reactReduxFirebase enhancer when making store creator
export const createStoreWithFirebase = compose(
  reactReduxFirebase(firebase, rrfConfig) // firebase instance as first argument
)(createStore);

export const rootReducer = combineReducers({
  audio: audioReducer,
  rawFirebase: reduxFirebaseReducer,
  firebase: firebaseReducer,
  ui: uiReducer,
});

export const initialReduxState: ReduxState = {
  audio: initialAudioState,
  rawFirebase: {},
  firebase: initialFirebaseState,
  ui: initialUIState,
};
