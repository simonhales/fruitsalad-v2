// @flow
import type { SessionModel, SessionWrapperModel } from '../../../firebase/bananart/session/models';
import type { SessionChatModel } from '../../../firebase/bananart/chat/models';

export type RawFirebaseState = {
  data?: {
    session?: SessionModel,
    sessionWrapper?: SessionWrapperModel,
    sessionChat?: SessionChatModel,
  },
};

export function getSessionFromState(state: RawFirebaseState): SessionModel | null {
  return state.data && state.data.session ? state.data.session : null;
}

export function getSessionFromStateRaw(state: RawFirebaseState): SessionModel | typeof undefined {
  return state.data && state.data.hasOwnProperty('session') ? state.data.session : undefined;
}

export function getSessionWrapperFromState(state: RawFirebaseState): SessionWrapperModel | null {
  return state.data && state.data.sessionWrapper ? state.data.sessionWrapper : null;
}

export function getSessionWrapperFromStateRaw(
  state: RawFirebaseState
): SessionWrapperModel | typeof undefined {
  return state.data && state.data.hasOwnProperty('sessionWrapper')
    ? state.data.sessionWrapper
    : undefined;
}

export function getSessionChatFromState(state: RawFirebaseState): SessionChatModel {
  return state.data && state.data.hasOwnProperty('sessionChat') && state.data.sessionChat
    ? state.data.sessionChat
    : {};
}
