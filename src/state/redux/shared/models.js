// @flow
import type { FirebaseState } from '../firebase/reducer';
import type { UIState } from '../ui/reducer';
import type { RawFirebaseState } from '../rawFirebase/state';
import type { AudioState } from '../audio/reducer';

export type ReduxState = {
  audio: AudioState,
  rawFirebase: RawFirebaseState,
  firebase: FirebaseState,
  ui: UIState,
};
