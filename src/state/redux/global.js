// @flow
import { store } from './store';
import { isUsingMockSession } from './firebase/state';
import type { ReduxState } from './shared/models';

export function globalIsUsingMockSession(): boolean {
  const state: ReduxState = store.getState();
  return isUsingMockSession(state.firebase);
}
