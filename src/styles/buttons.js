// @flow

import { css } from 'emotion';
import transparentize from 'polished/lib/color/transparentize';
import utils from './utils';



export const buttonHover = css`
  ${utils.transition.transition(['transform'])};

  &:hover {
    transform: scale(1.05);
  }
`;

export const smallButton = css`
  ${utils.fonts.families.chewy};
  background-color: ${utils.colors.vibrant};
  cursor: pointer;
  color: #ffffff;
  letter-spacing: 1px;
  font-size: 23.5px;
  padding: 7px 7px 5px 7px;
  text-align: center;
  box-shadow: 5px 5px 0 ${transparentize(0.05, utils.colors.vibrantShadow)};
`;

export const borderButton = css`
  cursor: pointer;
  letter-spacing: 1px;
  font-size: 23.5px;
  padding: 7px 7px 5px 7px;
  border: 2px solid #ffffff;
`;
