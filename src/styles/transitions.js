// @flow
const defaultDuration = '300ms';

const defaultEase = 'ease';

export function defaultEasing(properties: Array<string>): string {
  const value = properties
    .map(property => `${property} ${defaultDuration} ${defaultEase}`)
    .join(', ');
  return `transition: ${value}`;
}
