import { css } from 'emotion';
import { transparentize } from 'polished';
import utils from './utils';

export const inputReset = css`
  background: none;
  font-size: inherit;
  color: inherit;
  padding: 0;
  border: 0;

  &:focus {
    outline: none;
  }
`;

export const inputPlaceholder = content => css`
  &::-webkit-input-placeholder {
    ${content};
  }
  &:-moz-placeholder {
    ${content};
  }
  &::-moz-placeholder {
    ${content};
  }
  &:-ms-input-placeholder {
    ${content};
  }
`;

export const solidInput = css`
  ${utils.fonts.families.display};
  ${inputReset};
  display: block;
  width: 100%;
  background-color: #ffffff;
  padding: 10px;
  border: 3px solid ${utils.colors.lightBackgroundDarker};
  font-size: 22px;
  text-align: center;
  color: ${utils.colors.controllerText};

  ${inputPlaceholder(`
    color: ${utils.colors.controllerText};
  `)};

  &:focus {
    border-color: ${utils.colors.controllerText};

    ${inputPlaceholder(`
      color: ${transparentize(0.5, utils.colors.controllerText)};
    `)};
  }
`;
