import { css } from 'emotion';
import facepaint from 'facepaint';
import breakpoints from './breakpoints';

export const controllerActionMQ = facepaint([
  breakpoints.desktop,
  '@media only screen and (max-height: 570px)',
  '@media only screen and (max-height: 470px)',
  '@media only screen and (max-height: 430px)',
]);

const controllerActionHeight = css`
  ${controllerActionMQ({
    height: ['70px', '10vh', '60px', '50px', '40px'],
  })};
`;

export default {
  controllerActionHeight,
};
