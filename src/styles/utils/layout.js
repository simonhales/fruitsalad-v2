import { css } from 'emotion';
import spacing from './spacing';

const fixedFullView = css`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

export const absoluteFullView = css`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

export const flexFullCentered = css`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const maxCenteredContent = css`
  width: 100%;
  max-width: 1280px;
  margin-left: auto;
  margin-right: auto;
`;

const sidePadding = css`
  padding-left: ${spacing.small}px;
  padding-right: ${spacing.small}px;
`;

export default {
  absoluteFullView,
  fixedFullView,
  flexFullCentered,
  maxCenteredContent,
  sidePadding,
};
