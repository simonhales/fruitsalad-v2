// @flow
import { css } from 'emotion';

const nunitoFontFamily = css`
  font-family: 'Nunito', Arial, 'Helvetica Neue', Helvetica, sans-serif;
`;

const snigletFontFamily = css`
  font-family: 'Sniglet', Arial, 'Helvetica Neue', Helvetica, sans-serif;
  font-weight: 800;
`;

const chewyFontFamily = css`
  font-family: 'Chewy', Arial, 'Helvetica Neue', Helvetica, sans-serif;
`;

const defaultFontFamily = nunitoFontFamily;

const displayFancyFontFamily = snigletFontFamily;

const displayFontFamily = chewyFontFamily;

const headingFontFamily = chewyFontFamily;

export default {
  nunito: nunitoFontFamily,
  sniglet: snigletFontFamily,
  chewy: chewyFontFamily,
  default: defaultFontFamily,
  heading: headingFontFamily,
  displayFancy: displayFancyFontFamily,
  display: displayFontFamily,
};
