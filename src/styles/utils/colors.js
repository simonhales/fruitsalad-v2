export default {
  background: '#A4D9FF',
  bgDarkened: '#8BC7F2',
  bgInactive: '#7bbeef',
  white: '#FFFFFF',
  darkLight: '#5c9dcc',
  dark: '#2e5980',
  vibrant: '#ee337b',
  vibrantShadow: '#a6689e',
  richBackground: '#00b7f1',
  displayDarkBackground: '#5EA6C4',
  displayBackground: '#93CEE7',
  controllerBackground: '#9FC9EA',
  lightBackground: '#F1F7FE',
  lightBackgroundDarker: '#C4D4EA',
  controllerText: '#6C88AF',
  brushColor: '#30556d',
  lightBlue: '#a1bde2',
  orange: 'rgba(255, 132, 92, 0.87)',
};
