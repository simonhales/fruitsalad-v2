export default {
  desktop: '@media only screen and (min-width: 651px)',
  mobile: '@media only screen and (max-width: 650px)',
  mobileSmaller: '@media only screen and (max-width: 330px)',
  mobileHeightSmaller: '@media only screen and (max-height: 490px)',
};
