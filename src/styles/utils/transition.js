// @flow

function transition(properties: Array<string>) {
  const styles = [];
  properties.forEach(property => {
    const style = `${property} 300ms ease`;
    styles.push(style);
  });
  return `transition: ${styles.join(', ')}`;
}

export default {
  transition,
};
