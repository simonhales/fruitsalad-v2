import fonts from './fonts/index';
import colors from './colors';
import layout from './layout';
import breakpoints from './breakpoints';
import measurements from './measurements';
import spacing from './spacing';
import weights from './weights';
import classNames from './classNames';
import zindexes from './zindexes';
import transition from './transition';

export default {
  breakpoints,
  colors,
  fonts,
  layout,
  measurements,
  spacing,
  weights,
  classNames,
  zindexes,
  transition,
};
