// @flow

import { css } from 'emotion';

export function tiledBackgroundImage(url: string): string {
  return css`
    background-image: url(${url});
    background-repeat: repeat;
  `;
}
