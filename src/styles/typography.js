// @flow
import { css } from 'emotion';
import { getNumberFromString } from '../utils/numbers';
import { stripNumberFromString } from '../utils/strings';
import utils from './utils';
import { controllerActionMQ } from './utils/measurements';
import animations from './animations';

export function calculateLineHeight(fontSize: number, lineHeight: number): number {
  return lineHeight / fontSize;
}

const getMinFontSize = (dimension: string, minBreakpoint: number, minSize: string) =>
  `
  @media (max-${dimension}: ${minBreakpoint}px) {
    font-size: ${minSize};
  }
  `;

const getMaxFontSize = (dimension: string, responsiveFontUnitless: number, maxSize: string) => {
  const maxBreakpoint = (getNumberFromString(maxSize) / responsiveFontUnitless) * 100;
  return `
  @media (max-${dimension}: ${maxBreakpoint}px) {
    font-size: ${maxSize};
  }
  `;
};

export const getResponsiveFont = (
  responsiveSize: string,
  minSize: string,
  maxSize?: string,
  fallbackSize?: string
) => {
  const responsiveFontUnitless = getNumberFromString(responsiveSize);
  const dimension = stripNumberFromString(responsiveSize) === 'vh' ? 'height' : 'width';
  const minBreakpoint = (getNumberFromString(minSize) / responsiveFontUnitless) * 100;
  return css`
    ${getMinFontSize(dimension, minBreakpoint, minSize)};

    ${maxSize ? getMaxFontSize(dimension, responsiveFontUnitless, maxSize) : ''};

    ${fallbackSize ? `font-size: ${fallbackSize}` : ''};

    font-size: ${responsiveSize};
  `;
};

const DESIGN_DESKTOP_WIDTH = 1920;

export function getResponsiveDesktopFontSize(fontSize: number) {
  return `${(fontSize / DESIGN_DESKTOP_WIDTH) * 100}vw`;
}

const DESIGN_MOBILE_WIDTH = 375;

export function getResponsiveMobileFontSize(fontSize: number) {
  return `${(fontSize / DESIGN_MOBILE_WIDTH) * 100}vw`;
}

export const mediumHeading = css`
  ${utils.fonts.families.display};
  color: ${utils.colors.white};
  line-height: 1;

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(44), '14px')};
  }

  ${utils.breakpoints.mobile} {
    ${getResponsiveFont(getResponsiveMobileFontSize(44), '14px')};
  }
`;

export const smallHeading = css`
  ${utils.fonts.families.display};
  color: ${utils.colors.white};
  line-height: 1;

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(32), '14px')};
  }

  ${utils.breakpoints.mobile} {
    ${getResponsiveFont(getResponsiveMobileFontSize(24), '14px')};
  }
`;

export const tinyHeading = css`
  ${utils.fonts.families.heading};
  color: ${utils.colors.darkLight};
  line-height: 1;
  font-weight: 700;

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont('3vw', '40px')};
  }

  ${utils.breakpoints.mobile} {
    ${getResponsiveFont('4.5vh', '12px')};
  }
`;

export const smallLetterSpacing = css`
  letter-spacing: 1px;
`;

export const moderateLetterSpacing = css`
  letter-spacing: 1.5px;
`;

export const mediumButtonFont = css`
  ${utils.fonts.families.displayFancy};
  letter-spacing: 2px;
  ${getResponsiveFont(getResponsiveMobileFontSize(33.5), '14px')};

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(33.5), '14px')};
  }
`;

export const smallButtonFont = css`
  ${utils.fonts.families.displayFancy};
  letter-spacing: 1.79px;
  ${getResponsiveFont(getResponsiveMobileFontSize(21.5), '14px')};

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(21.5), '14px')};
  }
`;

export const instructionsTitle = css`
  ${getResponsiveFont('3vh', '14px')};
  color: ${utils.colors.darkLight};
  font-weight: 700;
`;

export const mediumFont = css`
  ${utils.fonts.families.displayFancy};
  letter-spacing: 1.5px;
  ${getResponsiveFont(getResponsiveMobileFontSize(37.5), '14px')};

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(37.5), '14px')};
  }
`;

export const controllerMediumHeading = css`
  ${utils.fonts.families.chewy};
  color: ${utils.colors.controllerText};
  font-size: 27.5px;
  letter-spacing: 1.51px;
`;

export const mediumLabel = css`
  ${utils.fonts.families.chewy};
  ${getResponsiveFont(getResponsiveMobileFontSize(37.5), '14px')};

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(37.5), '14px')};
  }
`;

export const fancyText = css`
  ${utils.fonts.families.displayFancy};
  color: #ffe800;
  letter-spacing: 3.49px;
  text-shadow: 3px 2px 0 #ffffff, 7px 8px 0 rgba(79, 140, 165, 0.84);
`;

export const largeThickHeading = css`
  ${fancyText};

  ${getResponsiveFont(getResponsiveMobileFontSize(37.5), '14px')};

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(75), '14px')};
  }
`;

export const displayHelpText = css`
  ${utils.fonts.families.display};
  color: #227ea9;
  text-shadow: -1px -1px rgba(255, 255, 255, 0.4);
`;

export const displayInstructionText = css`
  ${utils.fonts.families.display};
  color: ${utils.colors.white};
  ${getResponsiveFont(getResponsiveDesktopFontSize(50), '14px')};
  line-height: ${calculateLineHeight(50, 64)};
`;

export const displayInstructionLargeText = css`
  ${utils.fonts.families.display};
  color: ${utils.colors.white};
  ${getResponsiveFont(getResponsiveDesktopFontSize(70), '14px')};
  line-height: ${calculateLineHeight(70, 64)};
`;

export const displayInstructionSmallText = css`
  ${utils.fonts.families.display};
  color: ${utils.colors.white};
  ${getResponsiveFont(getResponsiveDesktopFontSize(40), '14px')};
  line-height: ${calculateLineHeight(40, 64)};
`;

export const controllerHeaderText = css`
  ${utils.fonts.families.display};
  ${getResponsiveFont(getResponsiveMobileFontSize(18), '14px')};
  color: ${utils.colors.lightBlue};
  letter-spacing: 0.66px;

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(24), '14px')};
  }
`;

export const controllerBodyText = css`
  ${utils.fonts.families.display};
  ${getResponsiveFont(getResponsiveMobileFontSize(27.5), '14px')};
  color: ${utils.colors.controllerText};
  letter-spacing: 1.01px;

  ${utils.breakpoints.desktop} {
    ${getResponsiveFont(getResponsiveDesktopFontSize(50), '14px')};
  }
`;

export const controllerInstructionText = css`
  ${utils.fonts.families.display};
  ${getResponsiveFont(getResponsiveDesktopFontSize(18), '14px')};
  line-height: ${23 / 18};
  color: ${utils.colors.lightBlue};
  letter-spacing: 0.66px;
`;

export const answerText = css`
  ${utils.fonts.families.chewy};
  ${getResponsiveFont(getResponsiveDesktopFontSize(75), '14px')};
  color: #ffffff;
  letter-spacing: -0.4px;
  text-shadow: 0 2px 4px rgba(65, 153, 191, 0.38);
  text-transform: lowercase;
`;
