// @flow

const DESKTOP_DESIGN_HEIGHT = 1080;
const DESKTOP_DESIGN_WIDTH = 1920;

export function getResponsiveWidthValue(size: number, baseHeight: number): string {
  return `${(size / baseHeight) * 100}vw`;
}

export function getResponsiveHeightValue(size: number, baseHeight: number): string {
  return `${(size / baseHeight) * 100}vh`;
}

export function getDesktopResponsiveHeightValue(size: number): string {
  return getResponsiveHeightValue(size, DESKTOP_DESIGN_HEIGHT);
}

export function getDesktopResponsiveWidthValue(size: number): string {
  return getResponsiveWidthValue(size, DESKTOP_DESIGN_WIDTH);
}
