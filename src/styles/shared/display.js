// @flow

export const DRAWINGS_SMALL_TOP_OFFSET = 125;
export const DRAWINGS_EXTRA_TOP_OFFSET = 100;
