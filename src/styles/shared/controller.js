// @flow

import { css } from 'emotion';
import utils from '../utils';

export const controllerContainerStyles = css`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const controllerMainStyles = css`
  //height: calc(100vh - 5px - 60px);
  //height: calc(100vh - 5px - 120px);
  background-color: ${utils.colors.lightBackground};
  flex: 1;
  margin-top: 5px;
  margin-bottom: 10px;
  overflow: hidden;
`;

export const controllerBodyStyles = css`
  padding: 0 13px;
`;

export const controllerFooterStyles = css`
  //flex: 1;
  position: relative;
  top: -10px;
`;
