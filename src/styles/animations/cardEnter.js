import { css, keyframes } from 'emotion';

const defaults = css`
  transform: rotate3d(1, 1, 1, 120deg);
`;

const frames = keyframes`
    0%
    {
        ${defaults};
    }
    100%
    {
        transform: rotate3d(1, 1, 1, 0deg);
    }
`;

export default {
  frames,
  defaults,
};
