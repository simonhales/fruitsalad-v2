import { css, keyframes } from 'emotion';

const defaults = css``;

const frames = keyframes`
  0% {
    transform: rotate(0);
    opacity: 1;
    transform-origin: bottom left;
  }
  50% {
    opacity: 1;
  }
  100% {
    opacity: 0;
    transform: rotate(85deg);
    transform-origin: bottom left;
  }
`;

export default {
  defaults,
  frames,
};
