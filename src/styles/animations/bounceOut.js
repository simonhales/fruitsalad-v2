import { css, keyframes } from 'emotion';

const defaults = css``;

const frames = keyframes`
  0% {
    transform: scale(1);
  }
  25% {
    transform: scale(0.95);
  }
  50% {
    opacity: 1;
    transform: scale(1.1);
  }
  100% {
    opacity: 0;
    transform: scale(0.3);
  }
`;

export default {
  defaults,
  frames,
};
