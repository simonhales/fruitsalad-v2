import { css, keyframes } from 'emotion';

const defaults = css``;

const frames = keyframes`
    0%
    {
        transform: scale(1);
    }
    33%
    {
        transform: scale(0.98);
    }
    66%
    {
        transform: scale(1.02);
    }
`;

export default {
  frames,
  defaults,
};
