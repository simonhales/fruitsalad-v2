import { css, keyframes } from 'emotion';

const defaults = css`
  opacity: 0;
`;

const frames = keyframes`
    0%
    {
        ${defaults};
    }
    100%
    {
        opacity: 1;
    }
`;

export default {
  frames,
  defaults,
};
