// @flow
import { css, keyframes } from 'emotion';
import slideFadeUp from './slideFadeUp';
import textWiggle from './textWiggle';
import textWiggleReduced from './textWiggleReduced';
import bounceOut from './bounceOut';
import slideOutLeft from './slideOutLeft';
import swivelAway from './swivelAway';
import slideInLeft from './slideInLeft';
import slideInRight from './slideInRight';
import fadeIn from './fadeIn';
import slideInUp from './slideInUp';
import slideInDown from './slideInDown';
import slideInDownAngle from './slideInDownAngle';
import shake from './shake';
import cardEnter from './cardEnter';

const fadeInKeyframes = keyframes`
    0%
    {
        opacity: 0;
    }
    100%
    {
        opacity: 1;
    }
`;

const slideUpKeyframes = keyframes`
    0%
    {
        transform: translateY(100%);
    }
    100%
    {
        transform: translateY(0);
    }
`;

const animation = (animationName, duration) => css`
  animation: ${animationName} ${duration} ease forwards;
`;

const fadeInAnimation = (duration: string = '300ms') => animation(fadeInKeyframes, duration);
const slideUpAnimation = (duration: string = '300ms') => animation(slideUpKeyframes, duration);

const bounceKeyframes = keyframes`
    0%
    {
        transform: scale(0.95);
        opacity: 0;
    }
    100%
    {
        transform: scale(1);
        opacity: 1;
    }
`;

const bounceInTextAnimation = css`
  opacity: 0;
  transform-origin: center bottom;
  animation: ${bounceKeyframes} 500ms ease forwards;
`;

type AnimationProps = {
  duration?: string,
  ease?: string,
  delay?: string,
  timing?: string,
};

const defaultProps = {
  duration: '300ms',
  ease: 'ease',
  delay: '0s',
  timing: 'forwards',
};

type AnimationBundle = {
  frames: string,
  defaults: string,
};

function includeAnimation(
  { frames, defaults }: AnimationBundle,
  props: AnimationProps = defaultProps,
  includeDefaults: boolean = false
): string {
  const {
    duration = defaultProps.duration,
    ease = defaultProps.ease,
    delay = defaultProps.delay,
    timing = defaultProps.timing,
  } = props;
  return css`
    ${includeDefaults ? defaults : ''};
    animation: ${frames} ${duration} ${delay} ${ease} ${timing};
  `;
}

export default {
  fadeInAnimation,
  slideUpAnimation,
  bounceInTextAnimation,
  includeAnimation,
  animations: {
    cardEnter,
    bounceOut,
    fadeIn,
    slideFadeUp,
    slideOutLeft,
    slideInLeft,
    slideInRight,
    slideInUp,
    slideInDown,
    slideInDownAngle,
    swivelAway,
    textWiggle,
    textWiggleReduced,
    shake,
  },
  keyframes: {
    bounce: bounceKeyframes,
  },
};
