import { css, keyframes } from 'emotion';

const defaults = css`
  opacity: 0;
`;

const frames = keyframes`
  0% {
    opacity: 0;
    transform: translateY(-200%);
  }
  100% {
    opacity: 1;
    transform: translateY(0);
  }
`;

export default {
  defaults,
  frames,
};
