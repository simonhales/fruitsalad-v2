import { css, keyframes } from 'emotion';

const defaults = css`
  opacity: 0;
`;

const frames = keyframes`
  0% {
    opacity: 0;
    transform: translateX(100%);
  }
  100% {
    opacity: 1;
    transform: translateX(0);
  }
`;

export default {
  defaults,
  frames,
};
