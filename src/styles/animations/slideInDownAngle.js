import { css, keyframes } from 'emotion';

const defaults = css`
  opacity: 0;
`;

const frames = keyframes`
  0% {
    opacity: 0;
    transform: translateY(-150%) rotate(0);
  }
  100% {
    opacity: 1;
    transform: translateY(0) rotate(-7deg);
  }
`;

export default {
  defaults,
  frames,
};
