import { css, keyframes } from 'emotion';

const defaults = css`
  opacity: 0;
  transform: translateY(100%);
`;

const frames = keyframes`
    0%
    {
        ${defaults};
    }
    100%
    {
        transform: translateY(0);
        opacity: 1;
    }
`;

export default {
  frames,
  defaults,
};
