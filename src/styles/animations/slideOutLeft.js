import { css, keyframes } from 'emotion';

const defaults = css``;

const frames = keyframes`
  0% {
    transform: translateX(0);
  }
  100% {
    opacity: 0;
    transform: translateX(-100%);
  }
`;

export default {
  defaults,
  frames,
};
