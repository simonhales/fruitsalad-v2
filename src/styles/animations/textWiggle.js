import { css, keyframes } from 'emotion';

const defaults = css``;

const frames = keyframes`
    0%
    {
        transform: scale(1);
    }
    33%
    {
        transform: scale(0.97);
    }
    66%
    {
        transform: scale(1.05);
    }
`;

export default {
  frames,
  defaults,
};
