import { injectGlobal } from 'emotion';
import reset from './reset';
import utils from './utils';

export default injectGlobal`
    ${reset};
    
    * {
        box-sizing: border-box;
    }
    
    body {
        ${utils.fonts.families.default};
        background-color: ${utils.colors.background};
        color: ${utils.colors.dark};
        line-height: 1;
    }
    
    img {
        max-width: 100%;
        max-height: 100%;
    }
    
    strong {
        font-weight: 900;
    }
    
`;
