// @flow

export const posedStates = {
  visible: 'visible',
  hidden: 'hidden',
  enter: 'enter',
  exit: 'exit',
};

export const posedVisibleState = {
  y: 0,
  scale: 1,
  opacity: 1,
};

export const posedHiddenState = {
  y: 50,
  scale: 0.5,
  opacity: 0,
};
