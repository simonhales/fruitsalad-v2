// @flow

import type { DatabaseChatModel } from './bananart/chat/models';
import type {DatabaseDataModel} from './bananart/data/models';

export interface FirebaseReference {
  child(path: string): FirebaseReference;
  key: string | null;
  onDisconnect(): void;
  parent: FirebaseReference | null;
  push(value?: any, onComplete?: (a: any | null) => any): Promise<any>;
  remove(onComplete?: (a: any | null) => any): Promise<any>;
  root: FirebaseReference;
  set(value: any, onComplete?: (a: any | null) => any): Promise<any>;
  setPriority(priority: string | number | null, onComplete: (a: any | null) => any): Promise<any>;
  setWithPriority(
    newVal: any,
    newPriority: string | number | null,
    onComplete?: (a: any | null) => any
  ): Promise<any>;
  transaction(
    transactionUpdate: (a: any) => any,
    onComplete?: (a: any | null, b: boolean, c: any | null) => any,
    applyLocally?: boolean
  ): Promise<any>;
  update(values: Object, onComplete?: (a: any | null) => any): Promise<any>;
}

export type FirebaseAuthUser = {
  user: {
    uid: string,
  },
};

export type FirebaseDatabaseStructure = {
  admins: {},
  chat: DatabaseChatModel,
  data: DatabaseDataModel,
  sessions: {},
};
