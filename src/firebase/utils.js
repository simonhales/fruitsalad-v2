// @flow
import firebase from 'firebase/index';

export function generateClientKey() {
  try {
    return firebase
      .database()
      .ref('anything') // ref doesn't matter
      .push().key;
  } catch (e) {
    console.error(e);
    throw new Error(`Failed to generate client key`);
  }
}
