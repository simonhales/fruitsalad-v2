import firebasemock from 'firebase-mock';

const firebaseMockDatabase = new firebasemock.MockFirebase();

const mockFirebaseSdk = new firebasemock.MockFirebaseSdk(
  // use null if your code does not use RTDB
  path => (path ? firebaseMockDatabase.child(path) : firebaseMockDatabase),
  // use null if your code does not use AUTHENTICATION
  () => null,
  // use null if your code does not use FIRESTORE
  () => null,
  // use null if your code does not use STORAGE
  () => null,
  // use null if your code does not use MESSAGING
  () => null
);

const firebase = mockFirebaseSdk.initializeApp();

export default firebase;
