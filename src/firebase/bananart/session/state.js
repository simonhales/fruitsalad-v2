// @flow

import type { SessionModel, SessionStates, SessionWrapperModel } from './models';
import { sessionStates } from './models';
import type { SessionUserModel } from './user/models';
import { ValidationError } from '../../../errors';
import { invalidSessionKeyError } from '../../../components/UserOptionsModal/errors';
import { MINIMUM_NUMBER_OF_PLAYERS } from '../../../constants/bananart';

export function getSessionFromSessionWrapper(sessionWrapper: SessionWrapperModel): SessionModel {
  return sessionWrapper.session;
}

export function getSessionKeyFromSession(session: SessionModel): string {
  return session.key;
}

export function getSessionState(session: SessionModel): SessionStates {
  return session.secured.state;
}

export function isSessionStatePlaying(session: SessionModel) {
  const sessionState = getSessionState(session);
  return sessionState === sessionStates.playing;
}

export function isSessionStatePending(session: SessionModel): boolean {
  const sessionState = getSessionState(session);
  return sessionState === sessionStates.pending;
}

export function isSessionStateStarting(session: SessionModel): boolean {
  const sessionState = getSessionState(session);
  return sessionState === sessionStates.starting;
}

export function getSessionUsers(session: SessionModel | null): Array<SessionUserModel> {
  if (session && session.users && session.users !== null) {
    const { users } = session;
    return Object.keys(users).map(userKey => users[userKey]);
  }
  return [];
}

export function getSessionHost(session: SessionModel | null): string {
  if (session && session.secured.host) {
    return session.secured.host;
  }
  return '';
}

export function getSessionKey(session: SessionModel): string {
  return session.key;
}

export function getValidSessionKey(session: SessionModel | null): string {
  if (!session) {
    throw new ValidationError({
      code: `NO_SESSION`,
      message: `No session`,
      fatal: true,
    });
  }
  const sessionKey = getSessionKey(session);
  return sessionKey;
}

export function hasUserJoinedSession(userKey: string, session: SessionModel): boolean {
  const users = getSessionUsers(session);
  return users.map(user => user.key).includes(userKey);
}

export function isUserSessionHost(userKey: string, session: SessionModel): boolean {
  const sessionHost = getSessionHost(session);
  return userKey === sessionHost;
}

export function canSessionGameStart(session: SessionModel): boolean {
  const users = getSessionUsers(session);
  return users.length >= MINIMUM_NUMBER_OF_PLAYERS;
}

export function getSessionStartGameInitiatedTimestamp(session: SessionModel): number {
  return session.secured &&
    session.secured.timestamps &&
    session.secured.timestamps.startGameInitiated
    ? session.secured.timestamps.startGameInitiated
    : 0;
}

export function getSessionDisplayHost(session: SessionModel): string | null {
  const { displayHost } = session.secured;
  return displayHost || null;
}

export function getSessionDisplayHostLastActive(session: SessionModel): number {
  const { displayHostLastActive } = session.secured;
  return displayHostLastActive || 0;
}
