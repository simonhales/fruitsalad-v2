// @flow
import { gameLog } from 'utils/logging';
import {
  getSessionDisplayHostRef,
  getSessionGamesRef,
  getSessionRef,
  getSessionSecuredRef,
  getSessionsRef,
  getSessionStateRef,
  getSessionUsersRef,
} from './refs';
import type { SessionModel, SessionStates, SessionWrapperModel } from './models';
import type { FirebaseReference } from '../../models';
import { generateUser } from './generator';
import { generateSessionUserBot } from './user/generator';
import { getSessionKey, getSessionUsers } from './state';
import { generateGame } from '../game/generator';
import { getSortedUserPrompts } from '../data/prompts/actions';
import type { SessionConfigModel } from './config/models';
import { sessionStates } from './models';
import firebase from '../../index';

export function promoteUserToHostWithRef(
  userKey: string,
  sessionSecuredRef: FirebaseReference
): Promise<any> {
  return sessionSecuredRef.update({
    host: userKey,
  });
}

export function promoteUserToHost(userKey: string, sessionKey: string): Promise<any> {
  const sessionSecuredRef = getSessionSecuredRef(sessionKey);
  return promoteUserToHostWithRef(userKey, sessionSecuredRef);
}

export function kickUserFromSessionWithRef(
  userKey: string,
  usersRef: FirebaseReference
): Promise<any> {
  return usersRef.update({
    [userKey]: null,
  });
}

export function kickUserFromSession(userKey: string, sessionKey: string): Promise<any> {
  const usersRef = getSessionUsersRef(sessionKey);
  return kickUserFromSessionWithRef(userKey, usersRef);
}

export function setSessionWithRef(
  sessionKey: string,
  sessionWrapper: SessionWrapperModel,
  sessionsRef: FirebaseReference
): Promise<any> {
  return sessionsRef.update({
    [sessionKey]: sessionWrapper,
  });
}

export function setSession(sessionKey: string, sessionWrapper: SessionWrapperModel): Promise<any> {
  const sessionsRef = getSessionsRef();
  return setSessionWithRef(sessionKey, sessionWrapper, sessionsRef);
}

export function addUserToSession(userKey: string, name: string, sessionKey: string): Promise<any> {
  const user = generateUser(userKey, name);
  const usersRef = getSessionUsersRef(sessionKey);
  return usersRef.update({
    [userKey]: user,
  });
}

export function addBotToSession(sessionKey: string): Promise<any> {
  const usersRef = getSessionUsersRef(sessionKey);
  // eslint-disable-next-line prefer-destructuring
  const key = usersRef.push().key;
  const bot = generateSessionUserBot(key);
  return usersRef.update({
    [key]: bot,
  });
}

export function setSessionState(sessionKey: string, state: SessionStates): Promise<any> {
  const stateRef = getSessionStateRef(sessionKey);
  return stateRef.set(state);
}

export async function createAndStartGame(session: SessionModel): Promise<any> {
  gameLog.log('Creating and starting game');
  const sessionKey = getSessionKey(session);

  setSessionState(sessionKey, sessionStates.starting);

  const sessionRef = getSessionRef(sessionKey);
  const sessionGamesRef = getSessionGamesRef(sessionKey);
  const users = getSessionUsers(session);
  const gameKey = sessionGamesRef.push().key;
  const userKeys = users.map(user => user.key);
  const sessionConfig: SessionConfigModel = {
    // todo - dynamic
    familyFriendly: true,
    numberOfRounds: 2,
  };
  const sortedPrompts = await getSortedUserPrompts(userKeys);
  const game = await generateGame(gameKey, sessionConfig, users, sortedPrompts);
  gameLog.log('Generated game', game);
  await sessionGamesRef.update({
    [gameKey]: game,
  });
  await sessionRef.update({
    'secured/currentGame': gameKey,
  });
  return setSessionState(sessionKey, sessionStates.playing);
}

export async function setSessionDisplayHost(clientKey: string, sessionKey: string): Promise<any> {
  const displayHostRef = getSessionDisplayHostRef(sessionKey);
  return displayHostRef.set(clientKey);
}

export async function updateSessionDisplayHostLastActive(sessionKey: string): Promise<any> {
  const sessionSecuredRef = getSessionSecuredRef(sessionKey);
  return sessionSecuredRef.update({
    displayHostLastActive: firebase.database.ServerValue.TIMESTAMP,
  });
}
