// @flow
import firebase from 'firebase';

export function getSessionsRef() {
  return firebase.database().ref(`sessions`);
}

export function getSessionWrapperRef(sessionKey: string) {
  const sessionsRef = getSessionsRef();
  return sessionsRef.child(sessionKey);
}

export function getSessionRef(sessionKey: string) {
  const sessionWrapperRef = getSessionWrapperRef(sessionKey);
  return sessionWrapperRef.child('session');
}

export function getSessionSecuredRef(sessionKey: string) {
  const sessionRef = getSessionRef(sessionKey);
  return sessionRef.child('secured');
}

export function getSessionUsersRef(sessionKey: string) {
  const sessionRef = getSessionRef(sessionKey);
  return sessionRef.child('users');
}

export function getSessionStateRef(sessionKey: string) {
  const sessionRef = getSessionRef(sessionKey);
  return sessionRef.child('secured').child('state');
}

export function getSessionUserRef(sessionKey: string, userKey: string) {
  const sessionUsersRef = getSessionUsersRef(sessionKey);
  return sessionUsersRef.child(userKey);
}

export function getSessionPath(sessionKey: string): string {
  return `/sessions/${sessionKey}`;
}

export function getSessionGamesRef(sessionKey: string) {
  const sessionRef = getSessionRef(sessionKey);
  return sessionRef.child('secured').child('games');
}

export function getSessionDisplayHostRef(sessionKey: string) {
  const sessionRef = getSessionRef(sessionKey);
  return sessionRef.child('secured').child('displayHost');
}
