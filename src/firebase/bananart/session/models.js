// @flow
import type { GameModel } from '../game/models';
import type { SessionUserModel } from './user/models';

export const sessionStates = {
  pending: 'pending',
  starting: 'starting',
  playing: 'playing',
  completed: 'completed',
};

export type SessionStates = $Keys<typeof sessionStates>;

export type SessionGamesModel = {
  [string]: GameModel,
};

export type SessionUsersModel = {
  [string]: SessionUserModel,
};

export type SessionModel = {
  key: string,
  users?: SessionUsersModel | null,
  visitors?: {} | null,
  secured: {
    currentGame?: string | null,
    games?: SessionGamesModel | null,
    timestamps: {
      created: number,
      startGameInitiated?: number,
    },
    state: SessionStates,
    host?: string,
    displayHost?: string,
    displayHostLastActive?: number,
  },
};

export type SessionWrapperModel = {
  key: string,
  session: SessionModel,
};
