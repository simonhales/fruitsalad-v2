// @flow
import firebase from 'firebase/index';
import type { SessionModel, SessionWrapperModel } from './models';
import { sessionStates } from './models';
import type { SessionUserModel } from './user/models';

export function generateUser(key: string, name: string): SessionUserModel {
  return {
    key,
    name,
    online: true,
    timestamps: {
      joined: firebase.database.ServerValue.TIMESTAMP,
    },
    isBot: false,
  };
}

export function generateSession(key: string): SessionModel {
  return {
    key,
    users: null,
    visitors: null,
    secured: {
      currentGame: null,
      games: null,
      timestamps: {
        created: firebase.database.ServerValue.TIMESTAMP,
      },
      state: sessionStates.pending,
    },
  };
}

export function generateSessionWrapper(key: string): SessionWrapperModel {
  return {
    key,
    session: generateSession(key),
    drawings: {
      chat: {},
    },
  };
}
