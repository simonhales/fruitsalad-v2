import firebase from 'firebase/index';
import { generateSession } from './generator';
import { sessionStates } from './models';

jest.mock('react-ga');
jest.mock('../../index.js');

describe('session generator', () => {
  test('it generates a new session', () => {
    const key = 'SESSION_KEY';
    const session = generateSession(key);
    expect(session).toEqual({
      key,
      visitors: null,
      users: null,
      secured: {
        currentGame: null,
        games: null,
        timestamps: {
          created: firebase.database.ServerValue.TIMESTAMP,
        },
        state: sessionStates.pending,
      },
    });
  });
});
