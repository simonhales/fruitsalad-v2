// @flow

export type SessionConfigModel = {
  familyFriendly: boolean,
  numberOfRounds: number,
};
