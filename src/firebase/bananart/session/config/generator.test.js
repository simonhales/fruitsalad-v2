import { generateSessionConfig } from './generator';
import { DEFAULT_NUMBER_OF_ROUNDS } from '../../../../constants/bananart';

describe('session config generator', () => {
  test('it generates a new session config', () => {
    const sessionConfig = generateSessionConfig();
    expect(sessionConfig).toEqual({
      familyFriendly: true,
      numberOfRounds: DEFAULT_NUMBER_OF_ROUNDS,
    });
  });
});
