// @flow
import type { SessionConfigModel } from './models';

export function getNumberOfRounds(sessionConfig: SessionConfigModel): number {
  return sessionConfig.numberOfRounds;
}
