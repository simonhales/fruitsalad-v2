// @flow

import type { SessionConfigModel } from './models';
import { DEFAULT_NUMBER_OF_ROUNDS } from '../../../../constants/bananart';

export function generateSessionConfig(): SessionConfigModel {
  return {
    familyFriendly: true,
    numberOfRounds: DEFAULT_NUMBER_OF_ROUNDS,
  };
}
