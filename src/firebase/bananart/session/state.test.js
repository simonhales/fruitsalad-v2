import {
  DUMMY_SESSION,
  DUMMY_SESSION_USERS,
  generateDummySessionUsers,
} from '../../../data/dummy/session';
import { sessionStates } from './models';
import {
  getSessionHost,
  getSessionUsers,
  isSessionStatePending,
  isSessionStatePlaying,
} from './state';

jest.mock('react-ga');

describe('firebase -> bananart -> session:: state', () => {
  let session = {};

  beforeEach(() => {
    session = {
      ...DUMMY_SESSION,
    };
  });

  test('isSessionStatePlaying returns true', () => {
    session.secured.state = sessionStates.playing;
    expect(isSessionStatePlaying(session)).toEqual(true);
  });

  test('isSessionStatePlaying returns false', () => {
    session.secured.state = sessionStates.pending;
    expect(isSessionStatePlaying(session)).toEqual(false);
  });

  test('isSessionPending returns true', () => {
    session.secured.state = sessionStates.pending;
    expect(isSessionStatePending(session)).toEqual(true);
  });

  test('isSessionPending returns false', () => {
    session.secured.state = sessionStates.playing;
    expect(isSessionStatePending(session)).toEqual(false);
  });

  test('getSessionUsers returns empty array', () => {
    session.users = null;
    expect(getSessionUsers(session)).toEqual([]);
  });

  test('getSessionUsers returns users array of correct length', () => {
    session.users = generateDummySessionUsers(DUMMY_SESSION_USERS);
    const users = getSessionUsers(session);
    expect(users.length).toEqual(Object.keys(session.users).length);
  });

  test('getSessionHost returns no host', () => {
    session.secured.host = null;
    expect(getSessionHost(session)).toEqual('');
  });

  test('getSessionHost returns host', () => {
    session.secured.host = 'simon';
    expect(getSessionHost(session)).toEqual(session.secured.host);
  });
});
