import * as firebase from 'firebase';
import { generateSessionUser } from './generator';

describe('session user generator', () => {
  test('it generates a new session user', () => {
    const key = 'USER_KEY';
    const name = 'USER_NAME';
    const user = generateSessionUser(key, name);
    expect(user).toEqual({
      key,
      name,
      isBot: false,
      online: true,
      timestamps: {
        joined: firebase.database.ServerValue.TIMESTAMP,
      },
    });
  });
});
