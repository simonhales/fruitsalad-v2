// @flow

export type SessionUserModel = {
  key: string,
  name: string,
  online: boolean,
  isBot: boolean,
  timestamps: {
    joined: number,
  },
};
