// @flow
import * as firebase from 'firebase';
import type { SessionUserModel } from './models';
import { shuffleArray } from '../../../../utils/array';

export function generateSessionUser(
  key: string,
  name: string,
  isBot: boolean = false
): SessionUserModel {
  return {
    key,
    name,
    online: true,
    isBot,
    timestamps: {
      joined: firebase.database.ServerValue.TIMESTAMP,
    },
  };
}

const BOT_NAMES = [
  'Botmon',
  'Mr Bot',
  'Madam Bot',
  'Miss Bot',
  'Botinator',
  'Robot',
  'Bender',
  'Not Bot',
  'Free Bot',
  'Bot Bot',
];

export function generateSessionUserBot(key: string) {
  const botNames = shuffleArray(BOT_NAMES);
  const name = botNames[0];
  return generateSessionUser(key, name, true);
}
