// @flow

import type { SessionModel } from '../models';
import type { SessionUserModel } from './models';
import { getSessionUsers } from '../state';

export function getSessionUser(session: SessionModel, userKey: string): ?SessionUserModel {
  const users = getSessionUsers(session);
  return users.find(user => user.key === userKey);
}

export function isSessionUserOffline(session: SessionModel, userKey: string): boolean {
  const user = getSessionUser(session, userKey);
  return user ? !user.online : false;
}
