import firebase from 'firebase/index';
import { DUMMY_SESSION } from '../../../data/dummy/session';
import { kickUserFromSession, promoteUserToHost, setSessionWithRef } from './actions';
import { getSessionRef, getSessionsRef } from './refs';
import { getSessionUsers } from './state';

jest.mock('react-ga');
jest.mock('../../index.js');

describe('firebase -> bananart -> session:: actions', () => {
  let session;

  beforeEach(() => {
    session = {
      ...DUMMY_SESSION,
    };
    const sessionWrapper = {
      key: session.key,
      session,
    };
    const sessionsRef = getSessionsRef();
    setSessionWithRef(session.key, sessionWrapper, sessionsRef);
    sessionsRef.flush();
  });

  test('promote user to host', () => {
    const sessionRef = getSessionRef(session.key);
    const sessionData = sessionRef.getData();
    expect(sessionData.host).toEqual(session.host);
    const newHost = 'simon';
    promoteUserToHost(newHost, session.key);
    sessionRef.flush();
    const updatedSessionData = sessionRef.getData();
    expect(updatedSessionData.secured.host).toEqual(newHost);
  });

  test('kick user from session', () => {
    const sessionRef = getSessionRef(session.key);
    const sessionData = sessionRef.getData();
    const originalUsers = getSessionUsers(session);
    const userToKick = originalUsers[0];
    const sessionUsers = getSessionUsers(sessionData);
    const matchedUser = sessionUsers.find(user => user.key === userToKick.key);
    expect(matchedUser).not.toBeNull();
    kickUserFromSession(userToKick.key, session.key);
    sessionRef.flush();
    const updatedSessionData = sessionRef.getData();
    const updatedSessionUsers = getSessionUsers(updatedSessionData);
    const includesKickedUser = updatedSessionUsers.map(user => user.key).includes(userToKick.key);
    expect(includesKickedUser).toEqual(false);
  });
});
