import * as firebase from 'firebase';
import { generateBananartPrompt } from './generator';

describe('bananart prompts', () => {
  test('it generates a new prompt', () => {
    const key = 'KEY';
    const text = 'TEXT';
    const prompt = generateBananartPrompt(key, text, true, []);
    expect(prompt).toEqual({
      disabled: false,
      familyFriendly: true,
      key,
      text,
      tags: {},
      timestamps: {
        added: firebase.database.ServerValue.TIMESTAMP,
      },
    });
  });
});
