// @flow
import * as firebase from 'firebase';
import type { BananartPromptModel, BananartPromptTags } from './models';

function getTags(tagKeys: Array<string>): BananartPromptTags {
  const tags = {};
  tagKeys.forEach(tag => {
    tags[tag] = true;
  });
  return tags;
}

export function generateBananartPrompt(
  key: string,
  text: string,
  familyFriendly: boolean,
  tagKeys: Array<string>
): BananartPromptModel {
  return {
    key,
    disabled: false,
    familyFriendly,
    text,
    tags: getTags(tagKeys),
    timestamps: {
      added: firebase.database.ServerValue.TIMESTAMP,
    },
  };
}
