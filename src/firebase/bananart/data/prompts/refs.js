// @flow

import { getDataRef } from '../refs';

export function getPromptsRef() {
  const dataRef = getDataRef();
  return dataRef.child('prompts');
}
