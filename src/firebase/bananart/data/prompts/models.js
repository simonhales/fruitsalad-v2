// @flow

export type BananartPromptTagModel = {
  key: string,
  label: string,
};

export type BananartPromptTags = {
  // tag => key
  [string]: boolean,
};

export type BananartPromptModel = $ReadOnly<{
  key: string,
  disabled: boolean,
  familyFriendly: boolean,
  text: string,
  tags: BananartPromptTags,
  timestamps: {
    added: number,
  },
}>;

export type DataPromptsModel = {
  [string]: BananartPromptModel,
};
