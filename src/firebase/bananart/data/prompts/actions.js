// @flow

import { getPromptsRef } from './refs';
import type { BananartPromptModel, DataPromptsModel } from './models';
import { getMultipleUsersData } from '../users/actions';
import { getSortedPromptsViaUserData } from './state';
import { gameLog } from '../../../../utils/logging';

export function getAllPrompts(): Promise<DataPromptsModel> {
  const promptsRef = getPromptsRef();
  return promptsRef.once('value').then(snapshot => snapshot.val());
}

export async function getSortedUserPrompts(
  userKeys: Array<string>
): Promise<Array<BananartPromptModel>> {
  gameLog.log('Getting sorted user prompts');
  const prompts = await getAllPrompts();
  const promptsList: Array<BananartPromptModel> = Object.keys(prompts).map(key => prompts[key]);
  const usersData = await getMultipleUsersData(userKeys);
  const sortedPrompts = getSortedPromptsViaUserData(promptsList, usersData);
  return sortedPrompts;
}
