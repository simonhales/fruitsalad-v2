// @flow

import type { BananartUserModel } from '../users/models';
import type { BananartPromptModel } from './models';
import { shuffleArray } from '../../../../utils/array';

type PromptsHistory = {
  [string]: number,
};

function getUsersPromptsHistory(usersData: Array<BananartUserModel>): PromptsHistory {
  const promptsHistory = {};
  usersData.forEach((data: BananartUserModel) => {
    if (data.prompts) {
      Object.keys(data.prompts).forEach(key => {
        if (promptsHistory[key]) {
          promptsHistory[key] += data.prompts[key];
        } else {
          promptsHistory[key] = data.prompts[key];
        }
      });
    }
  });
  return promptsHistory;
}

// todo - filter by tags
export function getSortedPromptsViaUserData(
  prompts: Array<BananartPromptModel>,
  usersData: Array<BananartUserModel>
): Array<BananartPromptModel> {
  const shuffledPrompts = shuffleArray(prompts);
  const promptsHistory = getUsersPromptsHistory(usersData);
  return shuffledPrompts.sort((promptKeyA, promptKeyB) => {
    const promptAWeight = promptsHistory[promptKeyA] ? promptsHistory[promptKeyA] : 0;
    const promptBWeight = promptsHistory[promptKeyB] ? promptsHistory[promptKeyB] : 0;
    return promptAWeight - promptBWeight;
  });
}
