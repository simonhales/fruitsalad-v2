// @flow
import firebase from 'firebase';

export function getDataRef() {
  return firebase.database().ref(`data`);
}
