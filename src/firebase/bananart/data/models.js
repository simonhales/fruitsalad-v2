// @flow

import type { BananartUserModel } from './users/models';
import type { DataPromptsModel } from './prompts/models';

export type DatabaseDataModel = {
  prompts: DataPromptsModel,
  users: {
    [string]: BananartUserModel,
  },
};
