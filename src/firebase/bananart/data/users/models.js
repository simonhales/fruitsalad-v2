// @flow

export type BananartUserModel = {
  key: string,
  names: {
    [string]: boolean,
  },
  prompts: {
    [string]: number,
  },
  users: {
    [string]: boolean,
  },
  games: {
    [string]: boolean,
  },
};
