// @flow

import { getDataRef } from '../refs';

export function getDataUsersRef() {
  const dataRef = getDataRef();
  return dataRef.child('users');
}

export function getUserDataRef(userKey: string) {
  const usersRef = getDataUsersRef();
  return usersRef.child(userKey);
}
