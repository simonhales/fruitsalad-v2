// @flow

import { getUserDataRef } from './refs';
import type { BananartUserModel } from './models';

export function getUserData(userKey: string): Promise<BananartUserModel> {
  const userRef = getUserDataRef(userKey);
  return userRef.once('value').then(snapshot => snapshot.val());
}

export function getMultipleUsersData(userKeys: Array<string>): Promise<Array<BananartUserModel>> {
  const promises = [];
  userKeys.forEach((userKey: string) => {
    promises.push(getUserData(userKey));
  });
  return Promise.all(promises).then(results => results.filter(userData => userData));
}
