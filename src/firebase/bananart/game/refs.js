// @flow

import { getSessionGamesRef } from '../session/refs';

export function getGameRef(sessionKey: string, gameKey: string) {
  const gamesRef = getSessionGamesRef(sessionKey);
  return gamesRef.child(gameKey);
}

export function getGameStateRef(sessionKey: string, gameKey: string) {
  const gameRef = getGameRef(sessionKey, gameKey);
  return gameRef.child('state');
}

export function getGameCurrentRoundRef(sessionKey: string, gameKey: string) {
  const gameRef = getGameRef(sessionKey, gameKey);
  return gameRef.child('currentRound');
}

export function getGameCurrentEntryRef(sessionKey: string, gameKey: string) {
  const gameRef = getGameRef(sessionKey, gameKey);
  return gameRef.child('currentEntry');
}

export function getGameTimerRef(sessionKey: string, gameKey: string) {
  const gameRef = getGameRef(sessionKey, gameKey);
  return gameRef.child('timer');
}

export function getGameMilestonesRef(sessionKey: string, gameKey: string) {
  const gameRef = getGameRef(sessionKey, gameKey);
  return gameRef.child('milestones');
}

export function getGameIntroSkippedRef(sessionKey: string, gameKey: string) {
  const gameMilestonesRef = getGameMilestonesRef(sessionKey, gameKey);
  return gameMilestonesRef.child('introSkipped');
}
