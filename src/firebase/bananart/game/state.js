// @flow

import type { SessionModel } from '../session/models';
import type { GameModel, GameStates, GameTimer } from './models';
import { gameStates } from './models';
import type { GameUserModel } from './user/models';
import type { RoundModel } from './round/models';
import type { EntryArtistModel, EntryModel } from './entry/models';
import { getEntryArtists, getEntryDrawingKeys } from './entry/state';

export function getGameFromSession(gameKey: string, session: SessionModel): GameModel | null {
  return session.secured.games ? session.secured.games[gameKey] : null;
}

export function getCurrentGameFromSession(session: SessionModel | null): GameModel | null {
  if (!session) return null;
  const currentGameKey = session.secured.currentGame ? session.secured.currentGame : null;
  if (!currentGameKey) return null;
  return getGameFromSession(currentGameKey, session);
}

export function getGameState(game: GameModel): GameStates {
  return game.state;
}

export function getGameKey(game: GameModel): string {
  return game.key;
}

export function isGamePlayingStage(game: GameModel): boolean {
  const state = getGameState(game);
  return state === gameStates.playing;
}

export function getGameUsersList(game: GameModel): Array<GameUserModel> {
  const { users } = game;
  return users ? Object.keys(users).map(userKey => users[userKey]) : [];
}

export function getGameUsersListSortedByScore(game: GameModel): Array<GameUserModel> {
  const users = getGameUsersList(game);
  return users.sort((userA, userB) => userB.score - userA.score);
}

export function getGameRoundsList(game: GameModel): Array<RoundModel> {
  const { rounds } = game;
  return rounds
    ? Object.keys(rounds)
        .map(key => rounds[key])
        .sort((roundA: RoundModel, roundB: RoundModel) => roundA.order - roundB.order)
    : [];
}

export function getGameEntriesList(game: GameModel): Array<EntryModel> {
  const { entries } = game;
  return entries
    ? Object.keys(entries)
        .map(key => entries[key])
        .sort((entryA: EntryModel, entryB: EntryModel) => entryA.entryOrder - entryB.entryOrder)
    : [];
}

export function getRoundEntriesList(game: GameModel, round: RoundModel): Array<EntryModel> {
  const { entries } = game;
  const { entries: roundEntries } = round;
  return Object.keys(roundEntries)
    .map(key => entries[key])
    .sort(
      (entryA: EntryModel, entryB: EntryModel) =>
        roundEntries[entryA.key].order - roundEntries[entryB.key].order
    );
}

export function isGamePlayingState(game: GameModel): boolean {
  const gameState = getGameState(game);
  return gameState === gameStates.playing;
}

export function getGameTimer(game: GameModel): GameTimer | null {
  const { timer } = game;
  return timer || null;
}

export function isTimerStillActiveTimer(timerKey: string, game: GameModel): boolean {
  const timer = getGameTimer(game);
  if (timer && timer.key === timerKey) {
    return true;
  }
  return false;
}

export function getGameUserViaKey(game: GameModel, userKey: string): GameUserModel {
  const { users } = game;
  return users[userKey];
}

export function getGameDrawingsKeys(game: GameModel): Array<string> {
  let drawings = [];
  const entries = getGameEntriesList(game);
  entries.forEach(entry => {
    drawings = drawings.concat(getEntryDrawingKeys(entry));
  });
  return drawings;
}

export function getGameUserCurrentScore(game: GameModel, userKey: string): number {
  const user = getGameUserViaKey(game, userKey);
  return user.score;
}

export function getTimerResolvedTimestamp(game: GameModel): number {
  if (!game.timer) return 0;
  return game.timer.resolvedTimestamp ? game.timer.resolvedTimestamp : 0;
}

export function isUserInGame(userKey: string, game: GameModel): boolean {
  const users = getGameUsersList(game);
  return users.map(user => user.key).includes(userKey);
}
