/* eslint-disable import/prefer-default-export */
// @flow
import * as firebase from 'firebase';
import type { GameEntries, GameModel, GameRounds, GameUsers } from './models';
import type { EntryModel } from './entry/models';
import { gameStates } from './models';
import type { SessionUserModel } from '../session/user/models';
import { generateGameUser } from './user/generator';
import { generateRound } from './round/generator';
import { getSessionsRef } from '../session/refs';
import { shuffleArray } from '../../../utils/array';
import { generateEntry } from './entry/generator';
import type { BananartPromptModel } from '../data/prompts/models';
import type { SessionConfigModel } from '../session/config/models';
import { getNumberOfRounds } from '../session/config/state';
import { getFilteredUserEntries } from './entry/state';
import { gameLog } from '../../../utils/logging';

function getShuffledUserKeys(userKeys: Array<string>): Array<string> {
  return shuffleArray(userKeys.slice());
}

function getGroupSize(numberOfUsers: number): number {
  return numberOfUsers > 5 ? 2 : 1;
}

function generateGameEntries(
  numberOfRounds: number,
  numberOfGroups: number,
  userKeys: Array<string>,
  prompts: Array<BananartPromptModel>
): GameEntries {
  const gameEntries = {};
  const numberOfEntries = numberOfRounds * numberOfGroups;
  const groupSize = getGroupSize(userKeys.length);

  if (numberOfEntries > prompts.length) {
    throw new Error(
      `There are only ${prompts.length} prompts but there are ${numberOfEntries} entries.`
    );
  }

  const shuffledUserKeysPerRound: Array<Array<string>> = [];

  const entryIndexes: Array<Array<number>> = [];

  for (let roundIndex = 0; roundIndex < numberOfRounds; roundIndex += 1) {
    shuffledUserKeysPerRound.push(getShuffledUserKeys(userKeys));
    const roundEntryIndexes: Array<number> = [];
    for (let groupIndex = 0; groupIndex < numberOfGroups; groupIndex += 1) {
      roundEntryIndexes.push(groupIndex * roundIndex);
    }
    entryIndexes.push(roundEntryIndexes);
  }

  for (let entryIndex = 0; entryIndex < numberOfEntries; entryIndex += 1) {
    const roundIndex = Math.floor(entryIndex / numberOfGroups);

    let groupUserKeys: Array<string> = [];

    for (let userIndex = 0; userIndex < groupSize; userIndex += 1) {
      const userKeysToChooseFrom: Array<string> = shuffledUserKeysPerRound[roundIndex];
      if (userKeysToChooseFrom.length > 0) {
        groupUserKeys = groupUserKeys.concat(userKeysToChooseFrom.splice(0, 1));
      }
    }

    const entryKey = getSessionsRef().push().key;

    const playerUserKeys = userKeys.filter(userKey => !groupUserKeys.includes(userKey));

    gameEntries[entryKey] = generateEntry(
      entryKey,
      entryIndex,
      prompts[entryIndex],
      playerUserKeys,
      groupUserKeys
    );
  }

  return gameEntries;
}

function generateGameRounds(
  numberOfRounds: number,
  gameEntries: GameEntries,
  numberOfGroups: number
): GameRounds {
  const rounds = {};
  const entries: Array<EntryModel> = Object.keys(gameEntries).map(
    entryKey => gameEntries[entryKey]
  );

  const sortedEntries = entries.sort((entryA, entryB) => entryA.entryOrder - entryB.entryOrder);

  for (let i = 0, len = numberOfRounds; i < len; i += 1) {
    const roundEntries = sortedEntries.slice(
      i * numberOfGroups,
      numberOfGroups + i * numberOfGroups
    );
    const roundKey = getSessionsRef().push().key;
    rounds[roundKey] = generateRound(roundKey, i, roundEntries);
  }

  return rounds;
}

function generateGameUsers(
  sessionUsers: Array<SessionUserModel>,
  entries: Array<EntryModel>
): GameUsers {
  const gameUsers = {};
  sessionUsers.forEach(user => {
    const userEntryKeys = getFilteredUserEntries(entries, user.key).map(entry => entry.key);
    gameUsers[user.key] = generateGameUser(user, userEntryKeys);
  });
  return gameUsers;
}

function getNumberOfGroups(numberOfUsers: number): number {
  return numberOfUsers > 5 ? Math.ceil(numberOfUsers / 2) : numberOfUsers;
}

export function generateGame(
  key: string,
  sessionConfig: SessionConfigModel,
  sessionUsers: Array<SessionUserModel>,
  prompts: Array<BananartPromptModel>
): GameModel {
  gameLog.log('Generating game');
  const userKeys = sessionUsers.map(user => user.key);
  const numberOfUsers = userKeys.length;
  const numberOfRounds = getNumberOfRounds(sessionConfig);
  const numberOfGroups = getNumberOfGroups(numberOfUsers);
  const entries = generateGameEntries(numberOfRounds, numberOfGroups, userKeys, prompts);
  const users = generateGameUsers(
    sessionUsers,
    Object.keys(entries).map(entryKey => entries[entryKey])
  );
  const rounds = generateGameRounds(numberOfRounds, entries, numberOfGroups);
  return {
    key,
    state: gameStates.pending,
    milestones: {
      completed: false,
      introSkipped: false,
    },
    timestamps: {
      created: firebase.database.ServerValue.TIMESTAMP,
      completed: null,
    },
    currentEntry: null,
    entries,
    currentRound: null,
    rounds,
    users,
  };
}
