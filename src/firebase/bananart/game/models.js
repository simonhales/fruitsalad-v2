// @flow

import type { EntryModel } from './entry/models';
import type { RoundModel } from './round/models';
import type { GameUserModel } from './user/models';

export const gameStates = {
  pending: 'pending',
  intro: 'intro',
  playing: 'playing',
  results: 'results',
  completed: 'completed',
};

export type GameStates = $Keys<typeof gameStates>;

export type GameEntries = {
  // entry -> key
  [string]: EntryModel,
};

export type GameRounds = {
  // round -> key
  [string]: RoundModel,
};

export type GameUsers = {
  // user -> key
  [string]: GameUserModel,
};

export type GameTimer = {
  key: string,
  resolvedTimestamp: number,
};

export type GameModel = {
  key: string,
  state: GameStates,
  milestones: {
    completed: boolean,
    introSkipped: boolean,
  },
  timestamps: {
    created: number,
    completed?: number | null,
  },
  currentEntry?: string | null,
  entries: GameEntries,
  currentRound?: string | null,
  rounds: GameRounds,
  timer?: GameTimer | null,
  users: GameUsers,
};
