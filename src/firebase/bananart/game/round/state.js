// @flow

import type { RoundModel, RoundStates } from './models';
import { roundStates } from './models';
import type { GameModel } from '../models';
import type { EntryModel, EntryPromptModel } from '../entry/models';
import {
  getGameRoundsList,
  getGameUsersList,
  getRoundEntriesList,
  isGamePlayingState,
} from '../state';
import { getEntryKey, getEntryState, getFilteredUserEntries } from '../entry/state';
import { entryStates } from '../entry/models';

export function getRoundState(round: RoundModel): RoundStates {
  return round.state;
}

export function isRoundDrawingStage(round: RoundModel): boolean {
  return (
    round.state === roundStates.drawingIntro ||
    round.state === roundStates.drawing ||
    round.state === roundStates.drawingSubmitted ||
    round.state === roundStates.drawingTimesUp
  );
}

export function isRoundVotingStage(round: RoundModel): boolean {
  return round.state === roundStates.voting;
}

export function getRoundFromGame(roundKey: string, game: GameModel): RoundModel | null {
  if (game.rounds && game.rounds.hasOwnProperty(roundKey)) {
    return game.rounds[roundKey];
  }
  throw new Error(`Round ${roundKey} not found in game.`);
}

export function getGameCurrentRound(game: GameModel): RoundModel | null {
  const currentRoundKey = game.currentRound ? game.currentRound : null;
  if (!currentRoundKey) return null;
  return getRoundFromGame(currentRoundKey, game);
}

export function getRoundKey(round: RoundModel): string {
  return round.key;
}

export function getGameRound(game: GameModel, roundKey: string): RoundModel {
  const { rounds } = game;
  return rounds[roundKey];
}

export function getUserEntryForRound(
  game: GameModel,
  roundKey: string,
  userKey: string
): EntryModel {
  const round = getGameRound(game, roundKey);
  const entries = getRoundEntriesList(game, round);
  const userEntries = getFilteredUserEntries(entries, userKey);
  if (userEntries.length === 0) {
    throw new Error(`Failed to find user ${userKey} entry for round ${roundKey}`);
  }
  return userEntries[0];
}

export function getUserPromptForRound(
  game: GameModel,
  roundKey: string,
  userKey: string
): EntryPromptModel {
  const userEntry = getUserEntryForRound(game, roundKey, userKey);
  return userEntry.prompt;
}

export function hasUserSubmittedDrawing(
  game: GameModel,
  roundKey: string,
  userKey: string
): boolean {
  const userEntry = getUserEntryForRound(game, roundKey, userKey);
  return userEntry.group[userKey].drawingSubmitted;
}

export function isRoundVotingState(round: RoundModel): boolean {
  const roundState = getRoundState(round);
  return roundState === roundStates.voting;
}

export function isGameDrawingMainStates(game: GameModel): boolean {
  if (!isGamePlayingState(game)) return false;
  const round = getGameCurrentRound(game);
  if (!round) return false;
  const roundState = getRoundState(round);
  return roundState === roundStates.drawing || roundState === roundStates.drawingIntro;
}

export function isRoundStateDrawing(round: RoundModel): boolean {
  const state = getRoundState(round);
  return state === roundStates.drawing;
}

export function getGameNextRoundKey(game: GameModel): string | null {
  const rounds = getGameRoundsList(game);
  const nextRound = rounds.find(round => {
    const roundState = getRoundState(round);
    return roundState === roundStates.pending;
  });
  if (nextRound) {
    return getRoundKey(nextRound);
  }
  return null;
}

export function areAllRoundDrawingsSubmitted(game: GameModel, roundKey: string): boolean {
  let allDrawingsSubmitted = true;
  const users = getGameUsersList(game);
  users.forEach(user => {
    if (!hasUserSubmittedDrawing(game, roundKey, user.key)) {
      allDrawingsSubmitted = false;
    }
  });
  return allDrawingsSubmitted;
}

export function getRoundNextEntryKey(game: GameModel, round: RoundModel): string | null {
  const entries = getRoundEntriesList(game, round);
  const nextEntry = entries.find(entry => {
    const entryState = getEntryState(entry);
    return entryState === entryStates.pending;
  });
  if (nextEntry) {
    return getEntryKey(nextEntry);
  }
  return null;
}

export function isRoundFinalRoundInGame(game: GameModel, roundKey: string): boolean {
  const rounds = getGameRoundsList(game);

  const roundIndex = rounds.map(round => round.key).indexOf(roundKey);

  return roundIndex === rounds.length - 1;
}
