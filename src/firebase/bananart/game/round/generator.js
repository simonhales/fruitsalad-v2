// @flow
import type { RoundEntries, RoundModel } from './models';
import { roundStates } from './models';
import type { EntryModel } from '../entry/models';
import { randomIntFromInterval } from '../../../../utils/numbers';

function generateRoundEntries(entries: Array<EntryModel>): RoundEntries {
  const roundEntries = {};
  entries.forEach(entry => {
    roundEntries[entry.key] = {
      order: randomIntFromInterval(0, 10),
    };
  });
  return roundEntries;
}

export function generateRound(
  roundKey: string,
  order: number,
  entries: Array<EntryModel>
): RoundModel {
  return {
    key: roundKey,
    entries: generateRoundEntries(entries),
    order,
    milestones: {
      completed: false,
    },
    state: roundStates.pending,
  };
}
