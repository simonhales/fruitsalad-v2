import { generateRound } from './generator';
import mockEntry from '../../../../../__mocks__/bananart/mockEntry';
import { roundStates } from './models';

describe('round generator', () => {
  test('it generates a new round', () => {
    const roundKey = 'ROUND_KEY';
    const roundOrder = 0;
    const round = generateRound(roundKey, roundOrder, [mockEntry]);
    expect(round).toEqual({
      key: roundKey,
      order: roundOrder,
      state: roundStates.pending,
      milestones: {
        completed: false,
      },
      entries: {
        [mockEntry.key]: {
          order: round.entries[mockEntry.key].order,
        },
      },
    });
  });
});
