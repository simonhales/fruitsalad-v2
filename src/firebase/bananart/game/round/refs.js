// @flow

import { getGameRef } from '../refs';

export function getRoundsRef(sessionKey: string, gameKey: string) {
  const gameRef = getGameRef(sessionKey, gameKey);
  return gameRef.child('rounds');
}

export function getRoundRef(roundKey: string, sessionKey: string, gameKey: string) {
  const roundsRef = getRoundsRef(sessionKey, gameKey);
  return roundsRef.child(roundKey);
}

export function getRoundStateRef(roundKey: string, sessionKey: string, gameKey: string) {
  const roundRef = getRoundRef(roundKey, sessionKey, gameKey);
  return roundRef.child('state');
}
