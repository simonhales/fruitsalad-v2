// @flow

import type { RoundStates } from './models';
import { getRoundStateRef } from './refs';
import { gameLog } from '../../../../utils/logging';
import type { GameModel } from '../models';
import { getGameUsersList } from '../state';
import { randomIntFromInterval } from '../../../../utils/numbers';
import type { GameUserModel } from '../user/models';
import { submitUserEntryDrawing } from '../entry/actions';
import { DUMMY_BOT_DRAWING } from '../../../../data/bot';
import { getUserEntryForRound } from './state';
import { getEntryKey } from '../entry/state';

export function setRoundState(
  state: RoundStates,
  sessionKey: string,
  gameKey: string,
  roundKey: string
): Promise<any> {
  gameLog.log(`Setting round state to "${state}".`);
  const roundStateRef = getRoundStateRef(roundKey, sessionKey, gameKey);
  return roundStateRef.set(state);
}

function submitBotDrawing(
  bot: GameUserModel,
  sessionKey: string,
  gameKey: string,
  roundKey: string,
  game: GameModel
) {
  const userKey = bot.key;
  const entry = getUserEntryForRound(game, roundKey, userKey);
  const entryKey = getEntryKey(entry);
  submitUserEntryDrawing(DUMMY_BOT_DRAWING, sessionKey, gameKey, entryKey, userKey);
}

export function getGameBots(game: GameModel): Array<GameUserModel> {
  const users = getGameUsersList(game);
  const bots = users.filter(user => !!user.isBot);
  return bots;
}

export function submitBotDrawings(
  sessionKey: string,
  gameKey: string,
  roundKey: string,
  game: GameModel
) {
  const bots = getGameBots(game);

  const maxTime = 20;

  bots.forEach(bot => {
    const delay = randomIntFromInterval(1, maxTime) * 1000;
    setTimeout(() => {
      submitBotDrawing(bot, sessionKey, gameKey, roundKey, game);
    }, delay);
  });
}
