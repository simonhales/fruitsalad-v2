// @flow

export const roundStates = {
  pending: 'pending',
  drawingIntro: 'drawingIntro',
  drawing: 'drawing',
  drawingSubmitted: 'drawingSubmitted',
  drawingTimesUp: 'drawingTimesUp',
  voting: 'voting',
  results: 'results',
  completed: 'completed',
};

export type RoundStates = $Keys<typeof roundStates>;

export type RoundEntries = {
  // entry -> key
  [string]: {
    order: number,
  },
};

export type RoundModel = $ReadOnly<{
  key: string,
  entries: RoundEntries,
  order: number,
  milestones: {
    completed: boolean,
  },
  state: RoundStates,
}>;
