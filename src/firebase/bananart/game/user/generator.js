// @flow

import type { SessionUserModel } from '../../session/user/models';
import type { GameUserModel } from './models';

function generateGameUserEntries(entryKeys: Array<string>) {
  const entries = {};
  entryKeys.forEach(entryKey => {
    entries[entryKey] = true;
  });
  return entries;
}

export function generateGameUser(
  sessionUser: SessionUserModel,
  entryKeys: Array<string>
): GameUserModel {
  return {
    key: sessionUser.key,
    name: sessionUser.name,
    entries: generateGameUserEntries(entryKeys),
    score: 0,
    isBot: Object.prototype.hasOwnProperty.call(sessionUser, 'isBot') ? sessionUser.isBot : false,
  };
}
