// @flow
export type GameUserModel = $ReadOnly<{
  key: string,
  name: string,
  score: number,
  entries: {
    // entry -> key
    [string]: boolean,
  },
  isBot?: boolean,
}>;
