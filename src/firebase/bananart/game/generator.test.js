import * as firebase from 'firebase';
import { generateGame } from './generator';
import { gameStates } from './models';

describe('game generator', () => {
  test('it generates a new game', () => {
    const sessionConfig = {
      familyFriendly: false,
      numberOfRounds: 0,
    };
    const sessionUsers = [];
    const prompts = [];
    const game = generateGame('game_key', sessionConfig, sessionUsers, prompts);
    expect(game).toEqual({
      key: 'game_key',
      currentEntry: null,
      currentRound: null,
      milestones: {
        completed: false,
        introSkipped: false,
      },
      state: gameStates.pending,
      timer: {
        key: null,
        timestamp: null,
      },
      timestamps: {
        completed: null,
        created: firebase.database.ServerValue.TIMESTAMP,
      },
      entries: game.entries,
      rounds: game.rounds,
      users: game.users,
    });
  });
});
