import { generateEntry } from './generator';
import mockPrompt from '../../../../../__mocks__/bananart/mockPrompt';
import { entryStates } from './models';

describe('entry generator', () => {
  test('it generates a new entry', () => {
    const entryKey = 'ENTRY_KEY';
    const entryOrder = 0;
    const prompt = mockPrompt;
    const userKey = 'USER_A_KEY';
    const groupUserKey = 'USER_B_KEY';
    const entry = generateEntry(entryKey, entryOrder, prompt, [userKey], [groupUserKey]);
    expect(entry).toEqual({
      key: entryKey,
      answers: null,
      answersRevealOrder: null,
      currentAnswerRevealIndex: -1,
      guesses: {},
      group: {
        [groupUserKey]: {
          drawing: null,
          drawingSubmitted: false,
        },
      },
      entryOrder,
      milestones: {
        guessesSubmitted: false,
        votesSubmitted: false,
        completed: false,
      },
      prompt: {
        key: prompt.key,
        text: prompt.text,
      },
      state: entryStates.pending,
      users: {
        [userKey]: {
          key: userKey,
          answerKey: null,
          votes: {},
          votesSubmitted: 0,
        },
      },
    });
  });
});
