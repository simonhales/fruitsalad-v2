// @flow
import { gameLog } from 'utils/logging';
import type {
  EntryAnswerModel,
  EntryAnswers,
  EntryGroupModel,
  EntryGuessModel,
  EntryModel,
  EntryPromptModel,
  EntryUsers,
} from './models';
import { entryStates } from './models';
import type { BananartPromptModel } from '../../data/prompts/models';
import type { GameModel } from '../models';
import {
  getEntryAnswersList,
  getEntryAnswerVotesTotal,
  getEntryArtistKeysList,
  getEntryFromGame,
} from './state';
import { getEntryRef } from './refs';
import { randomIntFromInterval } from '../../../../utils/numbers';
import { PROMPT_KEY } from '../../../../constants/bananart';

function generateEntryGroup(userKeys: Array<string>): EntryGroupModel {
  const group: EntryGroupModel = {};
  userKeys.forEach((userKey: string) => {
    group[userKey] = {
      drawing: null,
      drawingSubmitted: false,
    };
  });
  return group;
}

function generateEntryPrompt(prompt: BananartPromptModel): EntryPromptModel {
  return {
    key: prompt.key,
    text: prompt.text,
  };
}

function generateEntryUsers(userKeys: Array<string>): EntryUsers {
  const users: EntryUsers = {};
  userKeys.forEach((userKey: string) => {
    users[userKey] = {
      key: userKey,
      answerKey: null,
      votesSubmitted: 0,
      votes: {},
    };
  });
  return users;
}

export function generateEntry(
  entryKey: string,
  entryOrder: number,
  prompt: BananartPromptModel,
  userKeys: Array<string>,
  groupUserKeys: Array<string>
): EntryModel {
  return {
    key: entryKey,
    answers: null,
    answersRevealOrder: null,
    currentAnswerRevealIndex: -1,
    guesses: {},
    group: generateEntryGroup(groupUserKeys),
    entryOrder,
    milestones: {
      calculatedScores: false,
      guessesSubmitted: false,
      votesSubmitted: false,
      completed: false,
    },
    prompt: generateEntryPrompt(prompt),
    state: entryStates.pending,
    users: generateEntryUsers(userKeys),
  };
}

function generateEntryAnswer(
  guess: string,
  answerKey: string,
  order: number,
  prompt: boolean,
  usersKeys: Array<string>
): EntryAnswerModel {
  const users = {};
  usersKeys.forEach(userKey => {
    users[userKey] = true;
  });
  return {
    key: answerKey,
    order,
    prompt,
    text: guess,
    users,
    likes: {},
  };
}

function generateEntryPromptAnswer(entry: EntryModel) {
  const { prompt } = entry;
  const artists = getEntryArtistKeysList(entry);
  return generateEntryAnswer(prompt.text, PROMPT_KEY, randomIntFromInterval(0, 10), true, artists);
}

type Guess = {
  guess: string,
  users: Array<string>,
};

export function generateEntryAnswers(
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  entry: EntryModel
): EntryAnswers {
  const { guesses } = entry;
  const combinedGuesses: {
    [string]: Guess,
  } = {};
  Object.keys(guesses).forEach(userKey => {
    const guess: EntryGuessModel = guesses[userKey];
    const guessText = guess.text.toLowerCase();
    if (combinedGuesses[guessText]) {
      combinedGuesses[guessText] = {
        ...combinedGuesses[guessText],
        users: combinedGuesses[guessText].users.concat([userKey]),
      };
    } else {
      combinedGuesses[guessText] = {
        guess: guessText,
        users: [userKey],
      };
    }
  });
  const answers: EntryAnswers = {};
  const entryRef = getEntryRef(entryKey, sessionKey, gameKey);
  Object.keys(combinedGuesses).forEach(guessKey => {
    const guess: Guess = combinedGuesses[guessKey];
    const answerKey: string = entryRef.push().key;
    const order = randomIntFromInterval(0, 10);
    answers[answerKey] = generateEntryAnswer(guess.guess, answerKey, order, false, guess.users);
  });
  const promptAnswer = generateEntryPromptAnswer(entry);
  answers[promptAnswer.key] = promptAnswer;
  gameLog.log(`Generated answers`, answers);
  return answers;
}

export function generateEntryAnswersRevealOrder(entry: EntryModel) {
  const answers = getEntryAnswersList(entry);
  const answersToReveal = {};
  answers.forEach(answer => {
    if (answer.prompt) {
      answersToReveal[answer.key] = {
        key: answer.key,
        order: 9999,
      };
    } else {
      const totalVotes = getEntryAnswerVotesTotal(answer.key, entry);
      if (totalVotes > 0) {
        answersToReveal[answer.key] = {
          key: answer.key,
          order: totalVotes,
        };
      }
    }
  });
  return answersToReveal;
}
