// @flow

import { getGameRef } from '../refs';

export function getEntriesRef(sessionKey: string, gameKey: string) {
  const gameRef = getGameRef(sessionKey, gameKey);
  return gameRef.child('entries');
}

export function getEntryRef(entryKey: string, sessionKey: string, gameKey: string) {
  const entriesRef = getEntriesRef(sessionKey, gameKey);
  return entriesRef.child(entryKey);
}

export function getEntryStateRef(entryKey: string, sessionKey: string, gameKey: string) {
  const entryRef = getEntryRef(entryKey, sessionKey, gameKey);
  return entryRef.child('state');
}

export function getEntryUserArtistRef(
  entryKey: string,
  sessionKey: string,
  gameKey: string,
  userKey: string
) {
  const entryRef = getEntryRef(entryKey, sessionKey, gameKey);
  return entryRef.child('group').child(userKey);
}

export function getEntryUserContestantRef(
  entryKey: string,
  sessionKey: string,
  gameKey: string,
  userKey: string
) {
  const entryRef = getEntryRef(entryKey, sessionKey, gameKey);
  return entryRef.child('users').child(userKey);
}

export function getEntryGuessesRef(entryKey: string, sessionKey: string, gameKey: string) {
  const entryRef = getEntryRef(entryKey, sessionKey, gameKey);
  return entryRef.child('guesses');
}

export function getEntryUserGuessRef(
  entryKey: string,
  sessionKey: string,
  gameKey: string,
  userKey: string
) {
  const entryGuessesRef = getEntryGuessesRef(entryKey, sessionKey, gameKey);
  return entryGuessesRef.child(userKey);
}

export function getEntryAnswerRevealIndexRef(
  entryKey: string,
  sessionKey: string,
  gameKey: string
) {
  const entryRef = getEntryRef(entryKey, sessionKey, gameKey);
  return entryRef.child('currentAnswerRevealIndex');
}
