// @flow
import { gameLog } from 'utils/logging';
import type { EntryAnswerModel, EntryModel, EntryStates } from './models';
import {
  getEntryAnswerRevealIndexRef,
  getEntryRef,
  getEntryStateRef,
  getEntryUserArtistRef,
  getEntryUserContestantRef,
  getEntryUserGuessRef,
} from './refs';
import { uploadUserEntryDrawing } from '../../images/session';
import type { GameModel } from '../models';
import { getGameBots } from '../round/actions';
import { randomIntFromInterval } from '../../../../utils/numbers';
import { generateEntryAnswers, generateEntryAnswersRevealOrder } from './generator';
import {
  getEntryAnswersList,
  getEntryUserAnswerKey,
  getUserEntryScore,
  isUserEntryArtist,
} from './state';
import { getGameUserCurrentScore, getGameUsersList } from '../state';
import { getGameRef } from '../refs';

export function setEntryState(
  state: EntryStates,
  entryKey: string,
  sessionKey: string,
  gameKey: string
): Promise<any> {
  const entryStateRef = getEntryStateRef(entryKey, sessionKey, gameKey);
  return entryStateRef.set(state);
}

export function submitUserEntryDrawing(
  data: string,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  userKey: string
): Promise<any> {
  const userEntryRef = getEntryUserArtistRef(entryKey, sessionKey, gameKey, userKey);
  const drawingKey = userEntryRef.push().key;
  userEntryRef.update({
    drawing: drawingKey,
  });
  return uploadUserEntryDrawing(data, gameKey, drawingKey).then(() =>
    userEntryRef.update({
      drawingSubmitted: true,
    })
  );
}

export function submitUserEntryGuess(
  guess: string,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  userKey: string
): Promise<any> {
  gameLog.log(
    `Submitting user ${userKey} guess for entry ${entryKey} in game ${gameKey} for session ${sessionKey}`
  );
  const userEntryGuessRef = getEntryUserGuessRef(entryKey, sessionKey, gameKey, userKey);
  return userEntryGuessRef.set({
    key: userKey,
    text: guess,
  });
}

export function submitUserEntryVote(
  answerKey: string,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  userKey: string
): Promise<any> {
  gameLog.log(
    `Submitting user ${userKey} vote for ${answerKey} in entry ${entryKey} in game ${gameKey} for session ${sessionKey}`
  );
  const userRef = getEntryUserContestantRef(entryKey, sessionKey, gameKey, userKey);
  return userRef.update({
    votesSubmitted: 1,
    votes: {
      [answerKey]: true,
    },
  });
}

export function setEntryAnswerRevealIndex(
  answerRevealIndex: number,
  sessionKey: string,
  gameKey: string,
  entryKey: string
): Promise<any> {
  gameLog.log(
    `Setting entry ${entryKey} answer reveal index to ${answerRevealIndex} in game ${gameKey} for session ${sessionKey}`
  );
  const entryAnswerRevealIndexRef = getEntryAnswerRevealIndexRef(entryKey, sessionKey, gameKey);
  return entryAnswerRevealIndexRef.set(answerRevealIndex);
}

function submitBotGuess(sessionKey: string, gameKey: string, entryKey: string, userKey: string) {
  submitUserEntryGuess(Math.random().toString(), sessionKey, gameKey, entryKey, userKey);
}

export function submitBotsGuesses(
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  game: GameModel,
  entry: EntryModel
) {
  gameLog.log(`Submitting bot guesses.`);
  const bots = getGameBots(game);
  const maxTime = 20;
  bots.forEach(bot => {
    if (!isUserEntryArtist(entry, bot.key)) {
      const delay = randomIntFromInterval(1, maxTime) * 1000;
      setTimeout(() => {
        submitBotGuess(sessionKey, gameKey, entryKey, bot.key);
      }, delay);
    }
  });
}

function submitBotVote(
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  userKey: string,
  entry: EntryModel
) {
  const botAnswerKey = getEntryUserAnswerKey(entry, userKey);
  const answers = getEntryAnswersList(entry);
  const filteredAnswers = answers.filter(answer => answer.key !== botAnswerKey);
  if (filteredAnswers.length === 0) {
    gameLog.log(`0 filtered answers, can't submit a vote for the bot.`);
    return;
  }
  const randomIndex = randomIntFromInterval(0, filteredAnswers.length * 2);
  const reducedIndex =
    randomIndex < filteredAnswers.length ? randomIndex : filteredAnswers.length - 1;
  const answerKey = filteredAnswers[reducedIndex].key;
  submitUserEntryVote(answerKey, sessionKey, gameKey, entryKey, userKey);
}

export function submitBotsVotes(
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  game: GameModel,
  entry: EntryModel
) {
  gameLog.log(`Submitting bot votes.`);
  const bots = getGameBots(game);
  const maxTime = 10;
  bots.forEach(bot => {
    if (!isUserEntryArtist(entry, bot.key)) {
      const delay = randomIntFromInterval(1, maxTime) * 1000;
      setTimeout(() => {
        submitBotVote(sessionKey, gameKey, entryKey, bot.key, entry);
      }, delay);
    }
  });
}

export function generateAndSetEntryAnswers(
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  entry: EntryModel
): Promise<any> {
  const entryRef = getEntryRef(entryKey, sessionKey, gameKey);
  const answers = generateEntryAnswers(sessionKey, gameKey, entryKey, entry);
  const update = {
    answers,
  };
  Object.keys(answers).forEach(answerKey => {
    const answer: EntryAnswerModel = answers[answerKey];
    if (!answer.prompt) {
      Object.keys(answer.users).forEach(userKey => {
        update[`users/${userKey}/answerKey`] = answerKey;
      });
    }
  });
  gameLog.log(`Updating entry data with`, update);
  return entryRef.update(update);
}

export function generateAndSetEntryAnswersRevealOrder(
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  entry: EntryModel
): Promise<any> {
  const entryRef = getEntryRef(entryKey, sessionKey, gameKey);
  const answersRevealOrder = generateEntryAnswersRevealOrder(entry);
  return entryRef.child('answersRevealOrder').set(answersRevealOrder);
}

export function updateUserScores(
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  entry: EntryModel,
  game: GameModel
): Promise<any> {
  const gameRef = getGameRef(sessionKey, gameKey);
  const users = getGameUsersList(game);
  const update = {};
  users.forEach(user => {
    const entryScore = getUserEntryScore(entry, user.key);
    const currentUserScore = getGameUserCurrentScore(game, user.key);
    const newScore = entryScore + currentUserScore;
    update[`${user.key}/score`] = newScore;
  });
  return gameRef.child('users').update(update);
}

export function setEntryMilestoneScoresCalculated(
  sessionKey: string,
  gameKey: string,
  entryKey: string
): Promise<any> {
  const entryRef = getEntryRef(entryKey, sessionKey, gameKey);
  return entryRef.child('milestones').update({
    calculatedScores: true,
  });
}
