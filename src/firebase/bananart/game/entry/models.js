// @flow

export const entryStates = {
  pending: 'pending',
  intro: 'intro',
  noDrawings: 'noDrawings',
  guessingIntro: 'guessingIntro',
  guessing: 'guessing',
  guessingSubmitted: 'guessingSubmitted',
  guessingTimesUp: 'guessingTimesUp',
  votingIntro: 'votingIntro',
  voting: 'voting',
  votingSubmitted: 'votingSubmitted',
  votingTimesUp: 'votingTimesUp',
  results: 'results',
  completed: 'completed',
};

export type EntryStates = $Keys<typeof entryStates>;

export type EntryMilestones = $ReadOnly<{
  guessesSubmitted: boolean,
  votesSubmitted: boolean,
  completed: boolean,
  calculatedScores: boolean,
}>;

export type EntryAnswerModel = $ReadOnly<{
  key: string,
  order: number,
  prompt: boolean,
  text: string,
  users: {
    // user => key
    [string]: boolean,
  },
  likes: {
    // user => key
    [string]: boolean,
  },
}>;

export type EntryAnswers = {
  // answer => key
  [string]: EntryAnswerModel,
};

export type EntryGuessModel = $ReadOnly<{
  key: string, // user => key
  text: string,
}>;

export type EntryGuesses = $ReadOnly<{
  // user => key
  [string]: EntryGuessModel,
}>;

export type EntryPromptModel = $ReadOnly<{
  key: string,
  text: string,
}>;

export type EntryUserModel = $ReadOnly<{
  key: string,
  answerKey?: string | null,
  votesSubmitted: number,
  votes: {
    // answer => key
    [string]: boolean,
  },
}>;

export type EntryUsers = {
  // user => key
  [string]: EntryUserModel,
};

export type EntryArtistModel = {
  drawing?: string | null, // drawing => key
  drawingSubmitted: boolean,
};

export type EntryGroupModel = {
  // user => key
  [string]: EntryArtistModel,
};

export type EntryModel = $ReadOnly<{
  key: string,
  answers?: EntryAnswers | null,
  answersRevealOrder?: {
    // answer => key
    [string]: {
      key: string,
      order: number,
    },
  } | null,
  currentAnswerRevealIndex: number,
  guesses: EntryGuesses,
  group: EntryGroupModel,
  entryOrder: number,
  milestones: EntryMilestones,
  prompt: EntryPromptModel,
  state: EntryStates,
  users: EntryUsers,
}>;
