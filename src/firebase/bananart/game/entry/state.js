// @flow
import type {
  EntryAnswerModel,
  EntryArtistModel,
  EntryGroupModel,
  EntryGuessModel,
  EntryModel,
  EntryPromptModel,
  EntryStates,
  EntryUserModel,
} from './models';
import { entryStates } from './models';
import type { GameModel } from '../models';
import { getGameUsersList, getRoundEntriesList, isGamePlayingState } from '../state';
import {
  getGameCurrentRound,
  getRoundKey,
  isRoundFinalRoundInGame,
  isRoundVotingState,
} from '../round/state';
import { getAnswerResultsDuration, getArtistAnimationDelay } from '../../../../constants/timers';
import type { RoundModel } from '../round/models';
import type { GameUserModel } from '../user/models';
import { SCORE_CORRECT_VOTE, SCORE_TRICKED } from '../../../../constants/scores';
import { PROMPT_KEY } from '../../../../constants/bananart';

export function getEntryState(entry: EntryModel): EntryStates {
  return entry.state;
}

export function getFilteredUserEntries(
  entries: Array<EntryModel>,
  userKey: string
): Array<EntryModel> {
  return entries.filter(entry => Object.keys(entry.group).includes(userKey));
}

export function isEntryGuessingStage(entry: EntryModel): boolean {
  return (
    entry.state === entryStates.guessingIntro ||
    entry.state === entryStates.guessing ||
    entry.state === entryStates.guessingSubmitted ||
    entry.state === entryStates.guessingTimesUp
  );
}

export function isEntryVotingStage(entry: EntryModel): boolean {
  return (
    entry.state === entryStates.votingIntro ||
    entry.state === entryStates.voting ||
    entry.state === entryStates.votingSubmitted ||
    entry.state === entryStates.votingTimesUp
  );
}

export function getEntryFromGame(entryKey: string, game: GameModel): EntryModel {
  if (game.entries && game.entries.hasOwnProperty(entryKey)) {
    return game.entries[entryKey];
  }
  throw new Error(`Can't find entry ${entryKey} within game`);
}

export function getGameCurrentEntry(game: GameModel): EntryModel | null {
  const currentEntryKey = game.currentEntry ? game.currentEntry : null;
  if (!currentEntryKey) return null;
  return getEntryFromGame(currentEntryKey, game);
}

export function getEntryKey(entry: EntryModel): string {
  return entry.key;
}

export function isGameGuessingMainStates(game: GameModel): boolean {
  if (!isGamePlayingState(game)) return false;
  const round = getGameCurrentRound(game);
  if (!round) return false;
  if (!isRoundVotingState(round)) return false;
  const entry = getGameCurrentEntry(game);
  if (!entry) return false;
  const entryState = getEntryState(entry);
  return entryState === entryStates.guessingIntro || entryState === entryStates.guessing;
}

export function getEntryContestantsList(entry: EntryModel): Array<EntryUserModel> {
  const { users } = entry;
  return Object.keys(users).map(userKey => users[userKey]);
}

export function getEntryGuesses(entry: EntryModel): Array<EntryGuessModel> {
  const { guesses } = entry;
  return guesses ? Object.keys(guesses).map(key => guesses[key]) : [];
}

export function hasUserSubmittedGuess(entry: EntryModel, userKey: string): boolean {
  const entryContestants = getEntryContestantsList(entry);
  const entryContestantsKeys = entryContestants.map(user => user.key);
  if (!entryContestantsKeys.includes(userKey)) {
    // user is not a contestant so we'll consider their guess submitted
    return true;
  }
  const guesses = getEntryGuesses(entry);
  const userGuess = guesses.find(guess => guess.key === userKey);
  return !!userGuess;
}

export function isGameVotingMainStates(game: GameModel): boolean {
  if (!isGamePlayingState(game)) return false;
  const round = getGameCurrentRound(game);
  if (!round) return false;
  if (!isRoundVotingState(round)) return false;
  const entry = getGameCurrentEntry(game);
  if (!entry) return false;
  const entryState = getEntryState(entry);
  return entryState === entryStates.votingIntro || entryState === entryStates.voting;
}

export function hasUserSubmittedVote(entry: EntryModel, userKey: string): boolean {
  const entryContestants = getEntryContestantsList(entry);
  const entryContestantsKeys = entryContestants.map(user => user.key);
  if (!entryContestantsKeys.includes(userKey)) {
    // user is not a contestant so we'll consider their guess submitted
    return true;
  }
  const user = entryContestants.find(entryUser => entryUser.key === userKey);
  if (!user) {
    throw new Error(`Failed to map user ${userKey} to an entry contestant.`);
  }
  return !!user.votes;
}

export function isUserEntryArtist(entry: EntryModel, userKey: string): boolean {
  const { group } = entry;
  if (!group) {
    throw new Error(`Entry is missing a group`);
  }
  const artistKeys = Object.keys(group);
  return artistKeys.includes(userKey);
}

export function getEntryAnswersList(entry: EntryModel): Array<EntryAnswerModel> {
  const { answers } = entry;
  return answers ? Object.keys(answers).map(key => answers[key]) : [];
}

export function getEntryUserAnswerKey(entry: EntryModel, userKey: string): string | null {
  const entryContestants = getEntryContestantsList(entry);
  const user = entryContestants.find(entryUser => entryUser.key === userKey);
  if (!user) return null;
  return user.answerKey ? user.answerKey : null;
}

export function getEntryUserAnswer(entry: EntryModel, userKey: string): EntryAnswerModel | null {
  const userAnswerKey = getEntryUserAnswerKey(entry, userKey);
  if (!userAnswerKey) return null;
  const answers = getEntryAnswersList(entry);
  const userAnswer = answers.find(answer => answer.key === userAnswerKey);
  return userAnswer || null;
}

export function isEntryStateVoting(entry: EntryModel): boolean {
  const state = getEntryState(entry);
  return state === entryStates.voting;
}

export function isEntryStateGuessing(entry: EntryModel): boolean {
  const state = getEntryState(entry);
  return state === entryStates.guessing;
}

export function getEntryArtists(entry: EntryModel): EntryGroupModel {
  const { group } = entry;
  if (!group) {
    throw new Error(`Entry is missing a group`);
  }
  return group;
}

export function getEntryArtistKeysList(entry: EntryModel): Array<string> {
  const { group } = entry;
  if (!group) {
    throw new Error(`Entry is missing a group`);
  }
  const artistKeys = Object.keys(group);
  return artistKeys;
}

export function hasEntryArtistSubmittedDrawing(entry: EntryModel, userKey: string): boolean {
  const artists = getEntryArtists(entry);
  const artist = artists[userKey];
  if (!artist) {
    throw new Error(`Artist ${userKey} not found in entry artists.`);
  }
  return artist.drawingSubmitted;
}

export function doesEntryHaveDrawingsSubmitted(entry: EntryModel): boolean {
  let drawingsSubmitted = false;
  const artistKeys = getEntryArtistKeysList(entry);
  artistKeys.forEach(userKey => {
    if (hasEntryArtistSubmittedDrawing(entry, userKey)) {
      drawingsSubmitted = true;
    }
  });
  return drawingsSubmitted;
}

export function areAllEntryGuessesSubmitted(entry: EntryModel): boolean {
  let guessesSubmitted = true;

  const contestants = getEntryContestantsList(entry);

  contestants.forEach(user => {
    if (!hasUserSubmittedGuess(entry, user.key)) {
      guessesSubmitted = false;
    }
  });

  return guessesSubmitted;
}

export function areAllEntryVotesSubmitted(entry: EntryModel): boolean {
  let votesSubmitted = true;

  const contestants = getEntryContestantsList(entry);

  contestants.forEach(user => {
    if (!hasUserSubmittedVote(entry, user.key)) {
      votesSubmitted = false;
    }
  });

  return votesSubmitted;
}

export function getEntryAnswersToRevealList(entry: EntryModel): Array<EntryAnswerModel> {
  const answers = entry.answers ? entry.answers : {};
  const answersRevealOrder = entry.answersRevealOrder ? entry.answersRevealOrder : {};
  return Object.keys(answersRevealOrder)
    .map(answerKey => {
      if (!answers[answerKey]) {
        throw new Error(`Couldn't map answer key ${answerKey} to the answers.`);
      }
      return answers[answerKey];
    })
    .sort(
      (answerA: EntryAnswerModel, answerB: EntryAnswerModel) =>
        answersRevealOrder[answerA.key].order - answersRevealOrder[answerB.key].order
    );
}

export function getNextEntryAnswerIndex(entry: EntryModel): number {
  const { currentAnswerRevealIndex } = entry;
  const answersToReveal = getEntryAnswersToRevealList(entry);
  if (currentAnswerRevealIndex === answersToReveal.length - 1) {
    throw new Error(`Already reached final entry answer at index: ${currentAnswerRevealIndex}`);
  }
  return currentAnswerRevealIndex + 1;
}

export function allEntryAnswersRevealed(entry: EntryModel): boolean {
  const { currentAnswerRevealIndex } = entry;
  const answersToReveal = getEntryAnswersToRevealList(entry);
  return currentAnswerRevealIndex === answersToReveal.length - 1;
}

export function getEntryAnswerVotersKeys(answerKey: string, entry: EntryModel): Array<string> {
  const votersKeys: Array<string> = [];
  const contestants = getEntryContestantsList(entry);
  contestants.forEach(user => {
    const votes = user.votes ? user.votes : {};
    if (votes[answerKey]) {
      votersKeys.push(user.key);
    }
  });
  return votersKeys;
}

export function getEntryAnswerVotesTotal(answerKey: string, entry: EntryModel): number {
  let totalVotes = 0;
  const contestants = getEntryContestantsList(entry);
  contestants.forEach(user => {
    const votes = user.votes ? user.votes : {};
    if (votes[answerKey]) {
      totalVotes += 1;
    }
  });
  return totalVotes;
}

export function calculateEntryAnswerRevealTimerDuration(
  answerKey: string,
  entry: EntryModel
): number {
  const totalVotes = getEntryAnswerVotesTotal(answerKey, entry);
  const artists = getEntryArtistKeysList(entry);
  return getAnswerResultsDuration(totalVotes, artists.length);
}

export function getEntryAnswerByRevealIndex(entry: EntryModel, index: number): EntryAnswerModel {
  const answersToReveal = getEntryAnswersToRevealList(entry);
  if (index >= answersToReveal.length) {
    throw new Error(
      `Answer index of ${index} exceeds size ${answersToReveal.length} of answers to reveal.`
    );
  }
  return answersToReveal[index];
}

export function getAnswerKey(answer: EntryAnswerModel): string {
  return answer.key;
}

export function isFinalEntryInRound(game: GameModel, round: RoundModel, entryKey: string): boolean {
  const roundEntries = getRoundEntriesList(game, round);

  const entryIndex = roundEntries.map(entry => entry.key).indexOf(entryKey);

  return entryIndex === roundEntries.length - 1;
}

export function isEntryFinalEntryInGame(game: GameModel, entryKey: string): boolean {
  const round = getGameCurrentRound(game);

  if (!round) {
    throw new Error(`No current round found.`);
  }

  const roundKey = getRoundKey(round);

  const isFinalRound = isRoundFinalRoundInGame(game, roundKey);

  if (!isFinalRound) return false;

  return isFinalEntryInRound(game, round, entryKey);
}

export function getEntryArtistsNames(game: GameModel, entry: EntryModel): Array<string> {
  const artists = getEntryArtistKeysList(entry);
  const users = getGameUsersList(game);
  return artists
    .map(userKey => {
      const match = users.find(user => user.key === userKey);
      if (!match) {
        throw new Error(`Couldn't match user...`);
      }
      return match;
    })
    .map((user: GameUserModel) => user.name);
}

export function getEntryAnswersToRevealSorted(entry: EntryModel): Array<EntryAnswerModel> {
  const { answers, answersRevealOrder } = entry;
  if (!answersRevealOrder || !answers) return [];
  return Object.keys(answersRevealOrder)
    .sort(
      (answerKeyA: string, answerKeyB: string) =>
        answersRevealOrder[answerKeyA].order - answersRevealOrder[answerKeyB].order
    )
    .map(answerKey => answers[answerKey]);
}

export function getEntryAnswerIndex(entry: EntryModel): number {
  return entry.currentAnswerRevealIndex;
}

export function getEntryAnswerViaAnswerIndex(
  entry: EntryModel,
  answerIndex: number
): EntryAnswerModel {
  const answersToReveal = getEntryAnswersToRevealSorted(entry);
  if (answerIndex > answersToReveal.length - 1) {
    throw new Error(
      `Answer index ${answerIndex} exceeds answers to reveal length ${answersToReveal.length}`
    );
  }
  return answersToReveal[answerIndex];
}

export function getRevealedAnswersKeys(entry: EntryModel): Array<string> {
  const answerIndex = getEntryAnswerIndex(entry);
  const answersToReveal = getEntryAnswersToRevealSorted(entry);
  return answersToReveal.splice(0, answerIndex).map(answer => answer.key);
}

export function getAnswerRevealDelay(entry: EntryModel, answerKey: string): number {
  const totalVotes = getEntryAnswerVotesTotal(answerKey, entry);
  const artists = getEntryArtistKeysList(entry);
  return getArtistAnimationDelay(artists.length - 1, totalVotes) + 500;
}

export function getAnswerCulpritsKeys(answer: EntryAnswerModel): Array<string> {
  return Object.keys(answer.users);
}

export function getEntryDrawingKeys(entry: EntryModel): Array<string> {
  const drawings = [];
  const artists = getEntryArtists(entry);
  Object.keys(artists).forEach(userKey => {
    const artist: EntryArtistModel = artists[userKey];
    if (artist.drawing) {
      drawings.push(artist.drawing);
    }
  });
  return drawings;
}

export function getEntryPrompt(entry: EntryModel): EntryPromptModel {
  return entry.prompt;
}

export function getUserEntryAnswer(entry: EntryModel, userKey: string): string {
  if (isUserEntryArtist(entry, userKey)) {
    const prompt = getEntryPrompt(entry);
    return prompt.text;
  }
  const userAnswer = getEntryUserAnswer(entry, userKey);
  return userAnswer ? userAnswer.text : '';
}

export function getUserEntryAnswerNumberOfVotes(entry: EntryModel, answerKey: string): number {
  const answerVotersKey = getEntryAnswerVotersKeys(answerKey, entry);
  return answerVotersKey.length;
}

export function getUserEntryVotedKey(entry: EntryModel, userKey: string): string | null {
  const { users } = entry;
  const user = users[userKey];
  if (!user) return null;
  const { votes } = user;
  if (!votes) return null;
  return Object.keys(votes)[0];
}

export function getUserEntryVotedScore(entry: EntryModel, userKey: string): number {
  if (isUserEntryArtist(entry, userKey)) {
    return 0;
  }
  const userVotedKey = getUserEntryVotedKey(entry, userKey);
  if (!userVotedKey) return 0;
  if (userVotedKey === PROMPT_KEY) {
    return SCORE_CORRECT_VOTE;
  }
  return 0;
}

export function getUserEntryVotersScore(entry: EntryModel, userKey: string): number {
  const answerKey = getEntryUserAnswerKey(entry, userKey);
  if (!answerKey) return 0;
  const numberOfVoters = getUserEntryAnswerNumberOfVotes(entry, answerKey);
  if (isUserEntryArtist(entry, userKey)) {
    return numberOfVoters * SCORE_CORRECT_VOTE;
  }
  return numberOfVoters * SCORE_TRICKED;
}

export function getUserEntryScore(entry: EntryModel, userKey: string): number {
  const votedScore = getUserEntryVotedScore(entry, userKey);
  return votedScore + getUserEntryVotersScore(entry, userKey);
}

export function hasEntryScoresBeenCalculated(entry: EntryModel): boolean {
  return entry.milestones.calculatedScores;
}
