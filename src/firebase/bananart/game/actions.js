// @flow
import { gameLog } from 'utils/logging';
import type { GameStates } from './models';
import {
  getGameCurrentEntryRef,
  getGameCurrentRoundRef,
  getGameStateRef,
  getGameTimerRef,
} from './refs';

export function setGameState(sessionKey: string, gameKey: string, state: GameStates): Promise<any> {
  gameLog.log(`Setting game state to "${state}".`);
  const gameStateRef = getGameStateRef(sessionKey, gameKey);
  return gameStateRef.set(state);
}

export function setGameCurrentRound(
  sessionKey: string,
  gameKey: string,
  roundKey: string
): Promise<any> {
  gameLog.log(`Setting game current round to "${roundKey}".`);
  const gameCurrentRoundRef = getGameCurrentRoundRef(sessionKey, gameKey);
  return gameCurrentRoundRef.set(roundKey);
}

export function setGameCurrentEntry(
  sessionKey: string,
  gameKey: string,
  entryKey: string
): Promise<any> {
  gameLog.log(`Setting game current entry to "${entryKey}".`);
  const gameCurrentEntryRef = getGameCurrentEntryRef(sessionKey, gameKey);
  return gameCurrentEntryRef.set(entryKey);
}

export function setGameTimerKey(
  sessionKey: string,
  gameKey: string,
  resolvedTimestamp: number
): string {
  gameLog.log(`Setting game timer.`);
  const timerRef = getGameTimerRef(sessionKey, gameKey);
  const timerKey = timerRef.push().key;
  timerRef.update({
    key: timerKey,
    resolvedTimestamp,
  });
  return timerKey;
}

export function clearGameTimer(sessionKey: string, gameKey: string): Promise<any> {
  gameLog.log(`Clearing game timer.`);
  const timerRef = getGameTimerRef(sessionKey, gameKey);
  return timerRef.set(null);
}
