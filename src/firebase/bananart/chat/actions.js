// @flow

import { gameLog } from 'utils/logging';
import { generateSessionChatUserDrawing } from './generator';
import { getChatRef, getSessionChatRef } from './refs';

export function updateSessionUserChatDrawing(
  userKey: string,
  drawing: string,
  sessionKey: string
): Promise<any> {
  gameLog.log('updateSessionUserChatDrawing');
  const drawingKey = getChatRef().push().key;
  const drawingUpdate = generateSessionChatUserDrawing(drawing, drawingKey);
  const sessionChatRef = getSessionChatRef(sessionKey);
  return sessionChatRef.update({
    [userKey]: {
      drawings: {
        [drawingKey]: drawingUpdate,
      },
    },
  });
}

export function clearSessionUserChatDrawing(userKey: string, sessionKey: string): Promise<any> {
  const sessionChatRef = getSessionChatRef(sessionKey);
  return sessionChatRef.update({
    [userKey]: {
      drawings: {},
    },
  });
}
