// @flow

export type SessionChatUserDrawingModel = {
  key: string,
  drawing: string,
  timestamps: {
    created: number,
  },
};

export type SessionChatUserModel = {
  drawings: {
    [string]: SessionChatUserDrawingModel,
  },
};

export type SessionChatModel = {
  [string]: SessionChatUserModel,
};

export type DatabaseChatModel = {
  [string]: SessionChatModel,
};
