// @flow
import firebase from 'firebase/index';

export function getChatRef() {
  return firebase.database().ref(`chat`);
}

export function getSessionChatRef(sessionKey: string) {
  const chatRef = getChatRef();
  return chatRef.child(sessionKey);
}

export function getSessionChatPath(sessionKey: string): string {
  return `/chat/${sessionKey}`;
}
