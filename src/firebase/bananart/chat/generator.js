// @flow
import * as firebase from 'firebase';
import type { SessionChatUserDrawingModel } from './models';

export function generateSessionChatUserDrawing(
  drawing: string,
  key: string
): SessionChatUserDrawingModel {
  return {
    key,
    drawing,
    timestamps: {
      created: firebase.database.ServerValue.TIMESTAMP,
    },
  };
}
