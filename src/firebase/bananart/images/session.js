// @flow
import 'isomorphic-fetch';
import firebase from 'firebase';

export function getUserEntryDrawingPath(gameKey: string, drawingKey: string): string {
  return `/games/${gameKey}/${drawingKey}`;
}

export function uploadUserEntryDrawing(drawing: string, gameKey: string, drawingKey: string) {
  return firebase
    .storage()
    .ref(getUserEntryDrawingPath(gameKey, drawingKey))
    .putString(drawing);
}

export function fetchUserEntryDrawing(gameKey: string, drawingKey: string): Promise<any> {
  return firebase
    .storage()
    .ref(getUserEntryDrawingPath(gameKey, drawingKey))
    .getDownloadURL()
    .then((url: string) => fetch(url))
    .then(response => response.text());
}
