// @flow
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
import firebase from 'firebase/index';
import { connect } from 'react-redux';
import { setCurrentUserKey as reduxSetCurrentUserKey } from '../../state/redux/firebase/reducer';
import type { FirebaseAuthUser } from '../../firebase/models';
import type { ReduxState } from '../../state/redux/shared/models';
import { getCurrentUserKey } from '../../state/redux/firebase/state';
import LoadingView from '../../views/LoadingView/LoadingView';
import {
  getDevFirebaseEmail,
  getDevFirebasePassword,
  isDevFirebaseMode,
  isDevFirebaseModeWithVars,
  isDevMode,
  useDefaultUserKey,
} from '../../utils/env';
import { DEFAULT_USER_KEY } from '../../constants/development';

type Props = {
  children: React.Node,
  currentUserKey: string,
  setCurrentUserKey: (userKey: string) => void,
};

class AuthWrapper extends Component<Props> {
  componentDidMount() {
    if (isDevFirebaseModeWithVars()) {
      this.signInDev();
    } else {
      this.signInAnonymously();
    }
  }

  signInDev() {
    const { setCurrentUserKey } = this.props;
    const email = getDevFirebaseEmail();
    const password = getDevFirebasePassword();
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((data: FirebaseAuthUser) => {
        const userKey = data.user.uid;
        setCurrentUserKey(userKey);
      })
      .catch(error => {
        throw error;
        // todo - handle error
      });
  }

  signInAnonymously() {
    const { setCurrentUserKey } = this.props;
    firebase
      .auth()
      .signInAnonymously()
      .then((data: FirebaseAuthUser) => {
        const userKey = data.user.uid;
        setCurrentUserKey(userKey);
      })
      .catch(error => {
        throw error;
        // todo - handle error
      });
  }

  render() {
    const { children, currentUserKey } = this.props;
    if (currentUserKey) {
      return children;
    }
    return <LoadingView />;
  }
}

const mapStateToProps = (state: ReduxState) => ({
  currentUserKey: getCurrentUserKey(state.firebase),
});

const mapDispatchToProps = dispatch => ({
  setCurrentUserKey: (userKey: string) => dispatch(reduxSetCurrentUserKey(userKey)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthWrapper);
