// @flow
import React, { Component } from 'react';
import type { DisplaySessionContextState } from '../../state/context/DisplaySessionContext/DisplaySessionContext';
import { withDisplaySessionContext } from '../../state/context/DisplaySessionContext/DisplaySessionContext';
import type { SessionModel } from '../../firebase/bananart/session/models';
import { GameHandler } from '../../host/game/handler';
import { hostLog } from '../../utils/logging';
import { getSessionKey } from '../../firebase/bananart/session/state';
import { updateSessionDisplayHostLastActive } from '../../firebase/bananart/session/actions';

type Props = {
  session: SessionModel,
};

class GameHostController extends Component<Props> {
  gameHandler: GameHandler = new GameHandler();

  hostOnlineInterval: IntervalID;

  componentDidMount() {
    hostLog.log(`GameHostController mounted.`);
    this.updateGameHandler();
    this.startHostOnlineInterval();
  }

  componentDidUpdate() {
    this.updateGameHandler();
  }

  componentWillUnmount() {
    clearInterval(this.hostOnlineInterval);
  }

  startHostOnlineInterval() {
    this.hostOnlineInterval = setInterval(this.updateHostOnline, 10000);
  }

  updateHostOnline = () => {
    const { session } = this.props;
    const sessionKey = getSessionKey(session);
    updateSessionDisplayHostLastActive(sessionKey);
  };

  updateGameHandler() {
    const { session } = this.props;
    this.gameHandler.updateSession(session);
  }

  render() {
    return null;
  }
}

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { session } = state;
  if (!session) {
    throw new Error(`Invalid session in GameHostController`);
  }
  return {
    session,
  };
};

export default withDisplaySessionContext(mapStateToProps)(GameHostController);
