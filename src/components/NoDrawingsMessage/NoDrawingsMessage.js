// @flow

import React from 'react';
import posed, { PoseGroup } from 'react-pose';
import { AbsoluteFullCentered, FancyHeading } from '../elements';
import SplitText from '../text/SplitText/SplitText';
import TextWiggle from '../text/TextWiggle/TextWiggle';
import { posedHiddenState, posedStates, posedVisibleState } from '../../animation/posed';

const Wrapper = posed.div({
  [posedStates.enter]: {
    ...posedVisibleState,
  },
  [posedStates.exit]: {
    ...posedHiddenState,
  },
});

type Props = {
  visible: boolean,
};

const NoDrawingsMessage = ({ visible }: Props) => (
  <AbsoluteFullCentered>
    <PoseGroup>
      {visible && (
        <Wrapper key="heading">
          <FancyHeading>
            <TextWiggle>
              <SplitText text="no drawings found" />
            </TextWiggle>
          </FancyHeading>
        </Wrapper>
      )}
    </PoseGroup>
  </AbsoluteFullCentered>
);

export default NoDrawingsMessage;
