// @flow
import { css } from 'emotion';
import utils from 'styles/utils';
import { mediumFont } from '../../styles/typography';
import animations from '../../styles/animations';
import { defaultEasing } from '../../styles/transitions';

const containerClass = css`
  ${animations.slideUpAnimation()};
  position: relative;
  width: 100%;
  background-color: ${utils.colors.white};
  align-self: flex-end;
  padding: ${utils.spacing.small}px;
  color: ${utils.colors.controllerText};
  text-align: center;
`;

const headerClass = css`
  ${mediumFont};
  font-weight: ${utils.weights.bold};
`;

const optionsClass = css`
  margin: ${utils.spacing.small}px 0;
`;

const optionClass = css`
  ${defaultEasing(['opacity'])};

  &:not(:first-child) {
    margin-top: 5px;
  }
`;

const busyOptionClass = css`
  opacity: 0.5;
`;

const hiddenOptionClass = css`
  opacity: 0;
`;

export default {
  containerClass,
  headerClass,
  optionsClass,
  optionClass,
  busyOptionClass,
  hiddenOptionClass,
};
