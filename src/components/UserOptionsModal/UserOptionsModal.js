// @flow
import React, { Component } from 'react';
import { StateMachine } from 'react-machinery';
import { connect } from 'react-redux';
import { controllerLog } from 'utils/logging';
import { cx } from 'emotion';
import Modal from '../Modal/Modal';
import styles from './styles';
import ModalButton, { modalButtonTypes } from '../ModalButton/ModalButton';
import type { ReduxState } from '../../state/redux/shared/models';
import { getUserOptionsModal } from '../../state/redux/ui/state';
import type { SessionUserModel } from '../../firebase/bananart/session/user/models';
import { setUserOptionsModal } from '../../state/redux/ui/reducer';
import {
  kickUserFromSession as fbKickUserFromSession,
  promoteUserToHost as fbPromoteUserToHost,
} from '../../firebase/bananart/session/actions';
import { getValidSessionKey } from '../../firebase/bananart/session/state';
import { ControllerActionError } from '../../errors';
import { failedToKickUserFromSession, failedToPromoteUserToHost } from './errors';
import { userActionFailedToast } from '../../utils/toasts';
import analytics from '../../analytics';
import type { ControllerSessionContextState } from '../../state/context/ControllerSessionContext/ControllerSessionContext';
import { withControllerSessionContext } from '../../state/context/ControllerSessionContext/ControllerSessionContext';
import ModalPortal from '../ModalPortal/ModalPortal';

const buttonStates = {
  prompt: 'prompt',
  confirm: 'confirm',
};

type ButtonStates = $Keys<typeof buttonStates>;

type buttonProps = {
  transitionTo: (state: string) => void,
};

type confirmButtonProps = {
  onClick: () => void,
};

const promoteToHostStates = [
  {
    name: buttonStates.prompt,
    validTransitions: [buttonStates.confirm],
    render: ({ transitionTo }: buttonProps) => (
      <ModalButton onClick={() => transitionTo(buttonStates.confirm)}>promote to host</ModalButton>
    ),
  },
  {
    name: buttonStates.confirm,
    render: ({ onClick }: confirmButtonProps) => (
      <ModalButton onClick={onClick} type={modalButtonTypes.filledWithLabel}>
        <span>are you sure?</span>
        <span>yes, promote to host</span>
      </ModalButton>
    ),
  },
];

const kickPlayerStates = [
  {
    name: buttonStates.prompt,
    validTransitions: [buttonStates.confirm],
    render: ({ transitionTo }: buttonProps) => (
      <ModalButton onClick={() => transitionTo(buttonStates.confirm)}>kick player</ModalButton>
    ),
  },
  {
    name: buttonStates.confirm,
    render: ({ onClick }: confirmButtonProps) => (
      <ModalButton onClick={onClick} type={modalButtonTypes.filledWithLabel}>
        <span>are you sure?</span>
        <span>yes, kick player</span>
      </ModalButton>
    ),
  },
];

// const banPlayerStates = [
//   {
//     name: buttonStates.prompt,
//     validTransitions: [buttonStates.confirm],
//     render: ({ transitionTo }: buttonProps) => (
//       <ModalButton onClick={() => transitionTo(buttonStates.confirm)}>ban player</ModalButton>
//     ),
//   },
//   {
//     name: buttonStates.confirm,
//     render: ({ onClick }: confirmButtonProps) => (
//       <ModalButton onClick={onClick} type={modalButtonTypes.filledWithLabel}>
//         <span>are you sure?</span>
//         <span>yes, ban player</span>
//       </ModalButton>
//     ),
//   },
// ];

type Props = {
  user: SessionUserModel,
  resetUser: () => void,
  promoteUserToHost: (userKey: string) => Promise<any>,
  kickUserFromSession: (userKey: string) => Promise<any>,
};

type State = {
  promoteButtonState: ButtonStates,
  kickButtonState: ButtonStates,
  // banButtonState: ButtonStates,
  promotingUser: boolean,
  kickingUser: boolean,
  banningUser: boolean,
};

class UserOptionsModal extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      promoteButtonState: buttonStates.prompt,
      kickButtonState: buttonStates.prompt,
      // banButtonState: buttonStates.prompt,
      promotingUser: false,
      kickingUser: false,
      banningUser: false,
    };
  }

  close = () => {
    const { resetUser } = this.props;
    resetUser();
  };

  handleClose = () => {
    this.close();
  };

  handlePromoteToHost = () => {
    const { promotingUser } = this.state;
    if (promotingUser) return;
    this.setState({
      promotingUser: true,
    });
    const { user, promoteUserToHost } = this.props;
    analytics.events.hub.promoteUserToHost(user.key);
    promoteUserToHost(user.key)
      .then(() => {
        this.setState({
          promotingUser: false,
        });
        this.close();
      })
      .catch(e => {
        controllerLog.error(e);
        throw new ControllerActionError(failedToPromoteUserToHost(user.key));
      })
      .catch(() => {
        this.setState({
          promotingUser: false,
        });
        userActionFailedToast('Something went wrong, please try again', 'PROMOTE_TO_HOST_FAILED');
      });
  };

  handleKickFromGame = () => {
    const { kickingUser } = this.state;
    if (kickingUser) return;
    this.setState({
      kickingUser: true,
    });
    const { user, kickUserFromSession } = this.props;
    analytics.events.hub.kickUserFromSession(user.key);
    kickUserFromSession(user.key)
      .then(() => {
        this.setState({
          kickingUser: true,
        });
        this.close();
      })
      .catch(e => {
        controllerLog.error(e);
        throw new ControllerActionError(failedToKickUserFromSession(user.key));
      })
      .catch(() => {
        this.setState({
          promotingUser: false,
        });
        userActionFailedToast(
          'Something went wrong, please try again',
          'KICK_USER_FROM_SESSION_FAILED'
        );
      });
  };

  // todo
  handleBanFromGame = () => {
    const { banningUser } = this.state;
    if (banningUser) return;
    this.setState({
      banningUser: true,
    });
    this.close();
  };

  render() {
    const { user } = this.props;
    const {
      kickButtonState,
      promoteButtonState,
      // banButtonState,
      promotingUser,
      kickingUser,
    } = this.state;
    return (
      <ModalPortal>
        <Modal close={this.handleClose}>
          <div className={styles.containerClass}>
            <header className={styles.headerClass}>{user.name}</header>
            <main className={styles.optionsClass}>
              <div
                className={cx(styles.optionClass, {
                  [styles.busyOptionClass]: promotingUser,
                  [styles.hiddenOptionClass]: kickingUser,
                })}
              >
                <StateMachine
                  getCurrentState={() => promoteButtonState}
                  setNewState={newState => this.setState({ promoteButtonState: newState })}
                  states={promoteToHostStates}
                  data={{
                    onClick: this.handlePromoteToHost,
                  }}
                />
              </div>
              <div
                className={cx(styles.optionClass, {
                  [styles.busyOptionClass]: kickingUser,
                  [styles.hiddenOptionClass]: promotingUser,
                })}
              >
                <StateMachine
                  getCurrentState={() => kickButtonState}
                  setNewState={newState => this.setState({ kickButtonState: newState })}
                  states={kickPlayerStates}
                  data={{
                    onClick: this.handleKickFromGame,
                  }}
                />
              </div>
              {/* <div className={styles.optionClass}> */}
              {/* <StateMachine */}
              {/* getCurrentState={() => banButtonState} */}
              {/* setNewState={newState => this.setState({ banButtonState: newState })} */}
              {/* states={banPlayerStates} */}
              {/* data={{ */}
              {/* onClick: this.handleBanFromGame, */}
              {/* }} */}
              {/* /> */}
              {/* </div> */}
            </main>
            <footer>
              <ModalButton onClick={this.handleClose} type={modalButtonTypes.filled}>
                cancel
              </ModalButton>
            </footer>
          </div>
        </Modal>
      </ModalPortal>
    );
  }
}

type WrapperProps = {
  user: SessionUserModel | null,
  resetUser: () => void,
  promoteUserToHost: (userKey: string) => Promise<any>,
  kickUserFromSession: (userKey: string) => Promise<any>,
};

const UserOptionsModalWrapper = (props: WrapperProps) => {
  const { user, ...otherProps } = props;
  if (user != null && !!user) {
    const newProps: Props = {
      user,
      ...otherProps,
    };
    return <UserOptionsModal {...newProps} />;
  }
  return null;
};

const mapStateToProps = (state: ReduxState) => ({
  user: getUserOptionsModal(state.ui),
});

const mapDispatchToProps = dispatch => ({
  resetUser: () => dispatch(setUserOptionsModal(null)),
});

const mapContextStateToProps = (state: ControllerSessionContextState) => {
  const { session } = state;
  const sessionKey = getValidSessionKey(session);
  return {
    sessionKey,
    promoteUserToHost: (userKey: string) => fbPromoteUserToHost(userKey, sessionKey),
    kickUserFromSession: (userKey: string) => fbKickUserFromSession(userKey, sessionKey),
  };
};

export default withControllerSessionContext(mapContextStateToProps)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(UserOptionsModalWrapper)
);
