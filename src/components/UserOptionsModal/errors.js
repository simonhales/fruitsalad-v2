// @flow

export const invalidSessionKeyError = (sessionKey: string | null) => ({
  code: 'INVALID_SESSION_KEY',
  message: `Invalid session key: ${sessionKey || typeof sessionKey}`,
});

export const failedToPromoteUserToHost = (userKey: string) => ({
  code: 'FAILED_TO_PROMOTE_USER',
  message: `Failed to promote user to host: ${userKey}`,
});


export const failedToKickUserFromSession = (userKey: string) => ({
  code: 'FAILED_TO_KICK_USER_FROM_SESSION',
  message: `Failed to kick user from session: ${userKey}`,
});
