// @flow
import React from 'react';
import { Textfit } from 'react-textfit';
import styles from './styles';

type Props = {
  children: any,
};

const FitPlainText = ({ children }: Props) => (
  <div className={styles.textClass}>
    <Textfit mode="single" forceSingleModeWidth={false}>
      {children}
    </Textfit>
  </div>
);

export default FitPlainText;
