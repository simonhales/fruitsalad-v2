// @flow

import { css } from 'emotion';
import { answerText } from '../../styles/typography';

const textClass = css`
  ${answerText};
  width: 100%;
  text-align: center;
`;

export default {
  textClass,
};
