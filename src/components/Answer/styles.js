// @flow
import { css } from 'emotion';
import { answerText } from '../../styles/typography';

const containerClass = css`
  width: 100%;
  height: 100%;
  //overflow: hidden;
  position: relative;
`;

const textClass = css`
  ${answerText};
  text-transform: lowercase;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  text-align: center;
  padding: 10px 20px;
  line-height: 102px;
`;

export default {
  containerClass,
  textClass,
};
