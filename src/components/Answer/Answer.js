// @flow
import React from 'react';
import { Textfit } from 'react-textfit';
import styles from './styles';

type Props = {
  text: string,
};

const Answer = ({ text }: Props) => (
  <div className={styles.containerClass}>
    <div className={styles.textClass}>
      <Textfit mode="single" forceSingleModeWidth={false}>
        <div>{text}</div>
      </Textfit>
    </div>
  </div>
);

export default Answer;
