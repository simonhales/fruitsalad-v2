// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DEFAULT_USER_KEY } from '../../constants/development';
import {
  setCurrentUserKey,
  setMockSession,
  setUsingMockSession as reduxSetUsingMockSession,
} from '../../state/redux/firebase/reducer';
import { DUMMY_SESSION } from '../../data/dummy/session';
import ControllerSessionReduxConnector from '../ControllerSessionReduxConnector/ControllerSessionReduxConnector';

type Props = {
  setDefaultUserKey: () => void,
  setDefaultSession: () => void,
  setUsingMockSession: (using: boolean) => void,
};

class ReduxController extends Component<Props> {
  constructor(props: Props) {
    super(props);
    const { setDefaultUserKey, setDefaultSession, setUsingMockSession } = this.props;
    setDefaultUserKey();
    setDefaultSession();
    setUsingMockSession(true);
  }

  componentWillUnmount() {
    const { setUsingMockSession } = this.props;
    setUsingMockSession(false);
  }

  render() {
    return <ControllerSessionReduxConnector />;
  }
}

const mapDispatchToProps = dispatch => ({
  setDefaultUserKey: () => dispatch(setCurrentUserKey(DEFAULT_USER_KEY)),
  setDefaultSession: () => dispatch(setMockSession(DUMMY_SESSION)),
  setUsingMockSession: (using: boolean) => dispatch(reduxSetUsingMockSession(using)),
});

export default connect(
  null,
  mapDispatchToProps
)(ReduxController);
