// @flow
import React from 'react';
import posed from 'react-pose';
import styles from './styles';

const states = {
  hidden: 'hidden',
  visible: 'visible',
};

const Container = posed.div({
  [states.hidden]: {
    scale: 0.5,
    opacity: 0,
  },
  [states.visible]: {
    scale: 1,
    opacity: 1,
  },
});

type Props = {
  visible: boolean,
};

const MiniLabel = ({ visible, ...flipProps }: Props) => (
  <div {...flipProps}>
    <Container className={styles.containerClass} pose={visible ? states.visible : states.hidden}>
      <div className={styles.textClass}>
        <span>check your phone</span>
      </div>
    </Container>
  </div>
);

export default React.memo(MiniLabel);
