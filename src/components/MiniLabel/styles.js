// @flow

import { css } from 'emotion';
import {
  displayHelpText,
  getResponsiveDesktopFontSize,
  getResponsiveFont,
} from '../../styles/typography';

const containerClass = css`
  margin-top: 30px;
  margin-bottom: 10px;
`;

const textClass = css`
  ${displayHelpText};
  ${getResponsiveFont(getResponsiveDesktopFontSize(48), '14px')};
  position: relative;

  &::before {
    content: '';
    position: absolute;
    top: -5px;
    left: 30px;
    right: 30px;
    bottom: -5px;
    transform: rotate(3deg);
    background: rgba(34, 181, 242, 0.42);
    border-radius: 10px;
  }

  span {
    position: relative;
    opacity: 0.75;
  }
`;

export default {
  containerClass,
  textClass,
};
