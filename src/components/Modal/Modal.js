/* eslint-disable jsx-a11y/click-events-have-key-events,jsx-a11y/no-static-element-interactions */
// @flow
import * as React from 'react';
import styles from './styles';

type Props = {
  children: React.Node,
  close: () => void,
};

const Modal = ({ children, close }: Props) => (
  <div className={styles.containerClass}>
    <div className={styles.backgroundClass} onClick={close} />
    {children}
  </div>
);

export default Modal;
