import { css } from 'emotion';
import utils from 'styles/utils';
import animations from '../../styles/animations';

const containerClass = css`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: ${utils.zindexes.modal};
  display: flex;
  align-items: center;
  justify-content: center;
`;

const backgroundClass = css`
  ${animations.fadeInAnimation()};
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
`;

export default {
  containerClass,
  backgroundClass,
};
