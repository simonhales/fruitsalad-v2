import { css } from 'emotion';
import utils from '../../styles/utils';

const containerClass = css`
  height: 100%;
  display: flex;
  flex-direction: column;
`;

const bodyClass = css`
  flex: 1;
  overflow: hidden;
`;

const footerClass = css`
  ${utils.measurements.controllerActionHeight};
`;

export default {
  containerClass,
  bodyClass,
  footerClass,
};
