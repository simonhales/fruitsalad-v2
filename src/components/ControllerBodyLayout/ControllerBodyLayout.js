// @flow
import * as React from 'react';
import styles from './styles';

type Props = {
  children: React.Node,
  footer: React.Node,
};

const ControllerBodyLayout = ({ children, footer }: Props) => (
  <div className={styles.containerClass}>
    <div className={styles.bodyClass}>{children}</div>
    <div className={styles.footerClass}>{footer}</div>
  </div>
);

export default ControllerBodyLayout;
