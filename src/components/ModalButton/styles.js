import { css } from 'emotion';
import utils from 'styles/utils';

const defaultButton = css`
  border: 2px solid ${utils.colors.lightBackgroundDarker};
  border-radius: 5px;
  font-weight: ${utils.weights.bold};
  padding: 10px;
`;

const filledButton = css`
  ${defaultButton};
  background-color: ${utils.colors.darkLight};
  border-color: ${utils.colors.darkLight};
  color: ${utils.colors.white};
`;

const buttonClass = css``;

const defaultButtonClass = css`
  ${defaultButton};
`;

const filledButtonClass = css`
  ${filledButton};
`;

const withLabelButtonClass = css`
  position: relative;

  span {
    display: block;

    &:first-child {
      font-size: 0.75em;
      position: absolute;
      top: 4px;
      left: 0;
      right: 0;
    }

    &:last-child {
      position: relative;
      top: 5px;
    }
  }
`;

export default {
  buttonClass,
  defaultButtonClass,
  filledButtonClass,
  withLabelButtonClass,
};
