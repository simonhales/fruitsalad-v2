// @flow
import * as React from 'react';
import { cx } from 'emotion';
import styles from './styles';

export const modalButtonTypes = {
  default: 'default',
  filled: 'filled',
  filledWithLabel: 'filledWithLabel',
};

type Props = {
  children: React.Node,
  onClick: () => void,
  type?: $Keys<typeof modalButtonTypes>,
};

const ModalButton = ({ children, onClick, type }: Props) => (
  <div
    className={cx(styles.buttonClass, {
      [styles.defaultButtonClass]: type === modalButtonTypes.default,
      [styles.filledButtonClass]:
        type === modalButtonTypes.filled || type === modalButtonTypes.filledWithLabel,
      [styles.withLabelButtonClass]: type === modalButtonTypes.filledWithLabel,
    })}
    onClick={onClick}
  >
    {children}
  </div>
);

ModalButton.defaultProps = {
  type: modalButtonTypes.default,
};

export default ModalButton;
