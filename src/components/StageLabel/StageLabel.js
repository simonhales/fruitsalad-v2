// @flow
import React from 'react';
import posed from 'react-pose';
import styles from './styles';

const states = {
  hidden: 'hidden',
  visible: 'visible',
  expanded: 'expanded',
};

const sharedStates = {
  [states.hidden]: {
    opacity: 0,
    scale: 0.5,
  },
  [states.visible]: {
    opacity: 1,
    scale: 1,
  },
};

const Tagline = posed.div({
  ...sharedStates,
  [states.visible]: {
    opacity: 1,
    scale: 1,
    delay: 200,
  },
});

const Title = posed.div({
  ...sharedStates,
  [states.expanded]: {
    opacity: 1,
    scale: 0.75,
  },
});

function getTaglinePose(visible: boolean, expanded: boolean): string {
  return visible && !expanded ? states.visible : states.hidden;
}

function getTitlePose(visible: boolean, expanded: boolean): string {
  if (visible) {
    if (expanded) {
      return states.expanded;
    }
    return states.visible;
  }
  return states.hidden;
}

type Props = {
  visible: boolean,
  expanded: boolean,
  tagline: string,
  title: string,
};

const StageLabel = ({ visible, expanded, tagline, title, ...flipProps }: Props) => (
  <div className={styles.containerClass} {...flipProps}>
    <Tagline className={styles.taglineClass} pose={getTaglinePose(visible, expanded)}>
      <span>{tagline}</span>
    </Tagline>
    <Title className={styles.titleClass} pose={getTitlePose(visible, expanded)}>
      <div className={styles.titleShadowClass} />
      <div className={styles.titleInnerClass}>
        <span>{title}</span>
      </div>
    </Title>
  </div>
);

export default React.memo(StageLabel);
