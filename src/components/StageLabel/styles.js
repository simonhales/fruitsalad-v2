// @flow

import { css } from 'emotion';
import {
  fancyText,
  getResponsiveDesktopFontSize,
  getResponsiveFont,
} from '../../styles/typography';

const containerClass = css`
  position: relative;
  text-align: center;
`;

const taglineClass = css`
  ${fancyText};
  ${getResponsiveFont(getResponsiveDesktopFontSize(44.5), '14px')};
  margin-bottom: 5px;
  position: absolute;
  bottom: 100%;
`;

const titleClass = css`
  ${fancyText};
  ${getResponsiveFont(getResponsiveDesktopFontSize(84.5), '14px')};
  position: relative;

  span {
    display: inline-block;
    position: relative;
  }
`;

const titleShadowClass = css`
  position: absolute;
  top: 0;
  left: 35px;
  right: 35px;
  bottom: 0;
  transform: rotate(-7deg);
  background: rgba(255, 132, 92, 0.87);
  border-radius: 20px;
  max-width: 350px;
  margin: 0 auto;
`;

const titleInnerClass = css``;

export default {
  containerClass,
  taglineClass,
  titleClass,
  titleShadowClass,
  titleInnerClass,
};
