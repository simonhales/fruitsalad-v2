// @flow

import { css } from 'emotion';
import { borderButton, buttonHover } from '../../../styles/buttons';

const buttonClass = css`
  ${buttonHover};
  ${borderButton};
  width: 35px;
  height: 35px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 10px;
  opacity: 0.5;

  &:hover {
    opacity: 1;
  }

  svg {
    display: block;
    width: 20px;
    position: relative;
    top: -1px;

    &.VolumeMuteIcon {
      top: -2px;
    }
  }
`;

export default {
  buttonClass,
};
