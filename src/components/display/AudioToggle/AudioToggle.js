// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import VolumeIcon from '../../../icons/VolumeIcon';
import styles from './styles';
import VolumeMuteIcon from '../../../icons/VolumeMuteIcon';
import type { ReduxState } from '../../../state/redux/shared/models';
import { isAllAudioMuted } from '../../../state/redux/audio/state';

type Props = {
  muted: boolean,
  onClick: () => void,
};

type State = {};

class AudioToggle extends Component<Props, State> {
  render() {
    const { muted, onClick } = this.props;
    return (
      <div className={styles.buttonClass} onClick={onClick}>
        {muted ? <VolumeMuteIcon /> : <VolumeIcon />}
      </div>
    );
  }
}

const mapStateToProps = (state: ReduxState) => ({
  muted: isAllAudioMuted(state.audio),
});

export default connect(mapStateToProps)(AudioToggle);
