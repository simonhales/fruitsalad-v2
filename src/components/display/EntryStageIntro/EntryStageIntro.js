// @flow
import React, { PureComponent } from 'react';
import { cx } from 'emotion';
import { Flipper, Flipped } from 'react-flip-toolkit';
import StageLabel from '../../StageLabel/StageLabel';
import MiniLabel from '../../MiniLabel/MiniLabel';
import styles from './styles';

export const ENTRY_INTRO_TITLES = {
  voting: 'voting',
  guessing: 'guessing',
  timesUp: "time's up!",
  done: 'done!',
  scoreboard: 'scoreboard',
};

type Props = {
  visible: boolean,
  expanded: boolean,
  tagline: string,
  title: string,
  phonePromptVisible: boolean,
};

class EntryStageIntro extends PureComponent<Props> {

  render() {
    const { visible, expanded, tagline, title, phonePromptVisible } = this.props;
    return (
      <Flipper
        className={cx(styles.containerClass, {
          [styles.containerExpandedClass]: expanded,
        })}
        flipKey={expanded ? 'expanded' : 'condensed'}
      >
        <Flipped flipId="stageLabel">
          <StageLabel visible={visible} expanded={expanded} tagline={tagline} title={title} />
        </Flipped>
        <Flipped flipId="miniLabel">
          <MiniLabel visible={phonePromptVisible} />
        </Flipped>
      </Flipper>
    );
  }
}

export default EntryStageIntro;
