// @flow

import { css } from 'emotion';
import { absoluteFullView, flexFullCentered } from '../../../styles/utils/layout';

const containerClass = css`
  ${absoluteFullView};
  ${flexFullCentered};
  flex-direction: column;
`;

const containerExpandedClass = css`
  justify-content: space-between;
`;

export default {
  containerClass,
  containerExpandedClass,
};
