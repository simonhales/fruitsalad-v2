// @flow
import React, { Component } from 'react';
import type { GameModel } from '../../../firebase/bananart/game/models';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { getGameDrawingsKeys, getGameKey } from '../../../firebase/bananart/game/state';
import { displayLog } from '../../../utils/logging';
import { fetchUserEntryDrawing } from '../../../firebase/bananart/images/session';
import { decompressDrawing } from '../../../utils/drawing';

type Props = {
  game: GameModel,
  addDrawing: (drawingKey: string, drawing: string) => void,
};

class DrawingsManager extends Component<Props> {
  drawings: {
    [string]: {
      loading: boolean,
    },
  } = {};

  loadGameDrawings(game: GameModel) {
    const drawings = getGameDrawingsKeys(game);
    const gameKey = getGameKey(game);
    drawings.forEach(drawingKey => {
      this.loadDrawing(drawingKey, gameKey);
    });
  }

  loadDrawing(drawingKey: string, gameKey: string) {
    if (this.drawings[drawingKey]) return;
    displayLog.log('Loading drawing.', drawingKey);
    this.drawings[drawingKey] = {
      loading: true,
    };
    fetchUserEntryDrawing(gameKey, drawingKey).then((data: string) => {
      displayLog.log('Loaded drawing.', drawingKey);
      const { addDrawing } = this.props;
      addDrawing(drawingKey, decompressDrawing(data));
      this.drawings[drawingKey] = {
        loading: false,
      };
    });
  }

  componentDidMount() {
    this.fetchNewDrawings();
  }

  componentDidUpdate() {
    this.fetchNewDrawings();
  }

  fetchNewDrawings() {
    const { game } = this.props;
    this.loadGameDrawings(game);
  }

  render() {
    return null;
  }
}

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentGame, addDrawing } = state;
  if (!currentGame) {
    throw new Error(`No current game.`);
  }
  return {
    game: currentGame,
    addDrawing,
  };
};

export default withDisplaySessionContext(mapStateToProps)(DrawingsManager);
