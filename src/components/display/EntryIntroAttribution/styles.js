// @flow

import { css } from 'emotion';
import { getDesktopResponsiveHeightValue } from '../../../styles/responsive';
import { fancyText, getResponsiveDesktopFontSize } from '../../../styles/typography';

const containerClass = css`
  text-align: center;
  ${fancyText};
  font-size: ${getResponsiveDesktopFontSize(44.5)};
`;

const labelClass = css``;

const namesClass = css`
  margin-top: ${getDesktopResponsiveHeightValue(35)};
`;

const mediumTextClass = css`
  font-size: ${getResponsiveDesktopFontSize(74.5)};
`;

const largeTextClass = css`
  font-size: ${getResponsiveDesktopFontSize(104.5)};
`;

export default {
  containerClass,
  labelClass,
  namesClass,
  mediumTextClass,
  largeTextClass,
};
