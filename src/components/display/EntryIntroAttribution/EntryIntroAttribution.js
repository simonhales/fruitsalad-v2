// @flow
import React from 'react';
import styles from './styles';

type Props = {
  names: Array<string>,
};

const EntryIntroAttribution = ({ names }: Props) => (
  <div className={styles.containerClass}>
    <div className={styles.labelClass}>
      <span className={styles.mediumTextClass}>"art"</span> by
    </div>
    <div className={styles.namesClass}>
      {names.map(name => (
        <span className={styles.largeTextClass}>{name}</span>
      ))}
    </div>
  </div>
);

export default EntryIntroAttribution;
