// @flow
import React, { Component } from 'react';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import type { DisplayViewModel } from '../../../state/display/view';
import { getDisplayView } from '../../../state/display/view';
import utils from '../../../styles/utils';

type Props = {
  session: SessionModel,
};

class DisplayStateView extends Component<Props> {
  render() {
    const { session } = this.props;
    const view: DisplayViewModel = getDisplayView(session);
    return (
      <TransitionGroup component="div" className={utils.classNames.displayStateView}>
        <CSSTransition
          key={view.key}
          timeout={1100}
          classNames={utils.classNames.displayStateViewTransitioning}
        >
          {view.component()}
        </CSSTransition>
      </TransitionGroup>
    );
  }
}

export default DisplayStateView;
