// @flow
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
import posed, { PoseGroup } from 'react-pose';
import styles from './styles';
import EntryIntroAttribution from '../EntryIntroAttribution/EntryIntroAttribution';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { getEntryArtists, getEntryArtistsNames } from '../../../firebase/bananart/game/entry/state';

const IntroWrapper = posed.div({
  enter: {
    y: 0,
    scale: 1,
    opacity: 1,
    delay: 300,
  },
  exit: {
    y: 50,
    scale: 0.5,
    opacity: 0,
    transition: { ease: 'anticipate', duration: 300 },
  },
});

type Props = {
  artists: Array<string>,
  introIsVisible: boolean,
};

type State = {};

class EntryIntro extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  render() {
    const { artists, introIsVisible } = this.props;
    return (
      <div className={styles.containerClass}>
        <PoseGroup>
          {introIsVisible && (
            <IntroWrapper key="INTRO_WRAPPER">
              <EntryIntroAttribution names={artists} />
            </IntroWrapper>
          )}
        </PoseGroup>
      </div>
    );
  }
}

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentGame, currentEntry } = state;
  if (!currentGame) {
    throw new Error(`No current game.`);
  }
  if (!currentEntry) {
    throw new Error(`No current entry.`);
  }
  return {
    artists: getEntryArtistsNames(currentGame, currentEntry),
  };
};

export default withDisplaySessionContext(mapStateToProps)(EntryIntro);
