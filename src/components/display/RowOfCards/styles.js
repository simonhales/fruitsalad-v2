// @flow
import { css } from 'emotion';
import { getResponsiveDesktopFontSize, getResponsiveFont } from '../../../styles/typography';
import utils from '../../../styles/utils';
import animations from '../../../styles/animations';

const containerClass = css`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

function iterateAnimationDelay(emptyDelay, contentDelay): string {
  let styles = '';
  for (let i = 1, len = 13; i < len; i += 1) {
    const style = `
      &:nth-child(${i}) {
        .${utils.classNames.rowOfCardsSlotEmpty} {
          animation-delay: ${emptyDelay + i * 50}ms;
        }
        
        .${utils.classNames.rowOfCardsSlotContent} {
          animation-delay: ${contentDelay + i * 50}ms;
        }
      }
    `;
    styles += `${style}`;
  }
  return styles;
}

function iterateMargin(): string {
  let styles = '';
  for (let i = 7, len = 13; i < len; i += 1) {
    styles += `
      &:nth-child(${i}) {
        margin-top: 15px;
      }
    `;
  }
  return styles;
}

const slotClass = css`
  height: 350px;
  //height: 32vh;
  height: 18vw;
  width: 290px;
  //width: 26vh;
  width: 15vw;
  position: relative;

  .${utils.classNames.rowOfCardsSlotEmpty} {
    ${animations.includeAnimation(
      animations.animations.slideFadeUp,
      { duration: '750ms', delay: '500ms' },
      true
    )};
  }

  ${iterateAnimationDelay(-50, 150)};

  .${utils.classNames.displayHubJustLoadedPhase} & {
    ${iterateAnimationDelay(550, 700)};
    .${utils.classNames.rowOfCardsSlotContent} {
      ${animations.includeAnimation(
        animations.animations.slideFadeUp,
        { duration: '600ms', delay: '650ms' },
        true
      )};
    }
  }

  ${iterateMargin()};
`;

const slotEmptyWrapperClass = css`
  position: absolute;
  top: 10px;
  left: 10px;
  right: 10px;
  bottom: 10px;
`;

const slotEmptyClass = css`
  ${getResponsiveFont(getResponsiveDesktopFontSize(204), '30px')};
  ${utils.fonts.families.displayFancy};
  content: '?';
  width: 100%;
  height: 100%;
  background-color: #75c2e3;
  border-radius: 20px;
  //opacity: 0.22;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #64b4d7;
  box-shadow: inset 0 -1px 1px 0 rgba(55, 125, 157, 0.11),
    inset 2px 1px 1px 0 rgba(52, 115, 144, 0.07);
`;

const slotContentClass = css`
  position: relative;
  width: 100%;
  height: 100%;
  z-index: 10;
`;

export default {
  containerClass,
  slotClass,
  slotEmptyWrapperClass,
  slotEmptyClass,
  slotContentClass,
};
