// @flow
import React from 'react';
import { cx } from 'emotion';
import posed, { PoseGroup } from 'react-pose';
import { Flipper, Flipped } from 'react-flip-toolkit';
import utils from 'styles/utils';
import PosedPlayerCard from '../PosedPlayerCard/PosedPlayerCard';
import styles from './styles';
import type { SlotCardModel } from '../../../state/bananart/session/models';
import PlayerCard from '../PlayerCard/PlayerCard';

type Props = {
  slots: Array<SlotCardModel>,
};

const POSES = {
  enter: {
    y: 0,
    scale: 1,
    opacity: 1,
  },
  exit: {
    y: 50,
    scale: 0.5,
    opacity: 0,
    transition: { ease: 'anticipate', duration: 300 },
  },
};

const EmptyCard = posed.div(POSES);

const RowOfCards = ({ slots }: Props) => (
  <Flipper className={styles.containerClass} flipKey={slots.length}>
    {slots.map(player => (
      <Flipped flipId={player.key} key={player.key}>
        <div className={styles.slotClass} key={player.key}>
          <div className={cx(styles.slotEmptyWrapperClass, utils.classNames.rowOfCardsSlotEmpty)}>
            <PoseGroup>
              {player.emptyVisible && (
                <EmptyCard className={cx(styles.slotEmptyClass)} key={player.key}>
                  <span>?</span>
                </EmptyCard>
              )}
            </PoseGroup>
          </div>
          <div className={cx(styles.slotContentClass, utils.classNames.rowOfCardsSlotContent)}>
            <PoseGroup>
              {!player.empty &&
                player.user && (
                  <PosedPlayerCard key={player.key}>
                    <PlayerCard
                      name={player.user.name}
                      disabled={player.user.disabled}
                      joinedTimestamp={player.user.joinedTimestamp}
                      userKey={player.user.key}
                    />
                  </PosedPlayerCard>
                )}
            </PoseGroup>
          </div>
        </div>
      </Flipped>
    ))}
  </Flipper>
);

export default RowOfCards;
