// @flow
import React from 'react';
import { cx } from 'emotion';
import styles from './styles';

type Props = {
  fake: boolean,
};

const AnswerStamp = ({ fake }: Props) => (
  <div className={styles.containerClass}>
    <div
      className={cx(styles.shadeClass, {
        [styles.fakeShadeClass]: fake,
      })}
    />
    <div className={styles.textClass}>{fake ? 'fake' : 'correct'}</div>
  </div>
);

export default AnswerStamp;
