// @flow

import { css } from 'emotion';
import { fancyText, getResponsiveDesktopFontSize } from '../../../styles/typography';
import { getDesktopResponsiveHeightValue } from '../../../styles/responsive';

const containerClass = css`
  position: relative;
`;

const shadeClass = css`
  width: ${getDesktopResponsiveHeightValue(380)};
  height: ${getDesktopResponsiveHeightValue(120)};
  transform: rotate(-7deg);
  background: rgba(255, 132, 92, 0.87);
  border-radius: 20px;
`;

const fakeShadeClass = css`
  background: rgba(92, 186, 255, 0.87);
`;

const textClass = css`
  ${fancyText};
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: ${getResponsiveDesktopFontSize(80)};
`;

export default {
  containerClass,
  shadeClass,
  fakeShadeClass,
  textClass,
};
