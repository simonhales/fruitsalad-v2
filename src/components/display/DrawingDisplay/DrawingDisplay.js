// @flow
import React, { PureComponent } from 'react';
import CanvasDraw from 'react-canvas-draw';
import { withResizeDetector } from 'react-resize-detector';
import styles from './styles';

type Props = {
  drawingKey: string | null,
  drawing: string | null,
  width: number,
};

class DrawingDisplay extends PureComponent<Props> {
  canvasRef: ?{
    current: null | {
      clear: () => void,
      loadSaveData: (drawing: string, immediate: boolean) => void,
    },
  };

  constructor(props: Props) {
    super(props);
    this.canvasRef = React.createRef();
  }

  componentDidMount() {
    this.loadDrawing();
  }

  componentDidUpdate() {
    this.loadDrawing();
  }

  loadDrawing() {
    const { drawing } = this.props;
    if (this.canvasRef && this.canvasRef.current && drawing) {
      const immediate = true;
      this.canvasRef.current.clear();
      if (this.canvasRef && this.canvasRef.current) {
        this.canvasRef.current.loadSaveData(drawing, immediate);
      }
    }
  }

  render() {
    const { drawing, width } = this.props;
    const canvasSize = width;
    if (!drawing) return null;
    return (
      <div className={styles.containerClass}>
        <CanvasDraw ref={this.canvasRef} canvasWidth={canvasSize} canvasHeight={canvasSize} />
      </div>
    );
  }
}

export default withResizeDetector(DrawingDisplay);
