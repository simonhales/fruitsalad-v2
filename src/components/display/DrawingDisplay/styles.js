// @flow

import { css } from 'emotion';

const containerClass = css`
  width: 100%;
  height: 100%;
  overflow: hidden;

  canvas {
    background-color: transparent !important;
  }
`;

export default {
  containerClass,
};
