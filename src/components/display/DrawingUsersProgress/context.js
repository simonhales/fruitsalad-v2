// @flow
import React from 'react';
import DrawingUsersProgress from './DrawingUsersProgress';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import type { UserSlotCardModel } from '../../../state/bananart/session/models';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { getGameSlotCards } from '../../../state/bananart/game/state';
import { getRoundState } from '../../../firebase/bananart/game/round/state';
import { roundStates } from '../../../firebase/bananart/game/round/models';

type Props = {
  slots: Array<UserSlotCardModel>,
  showSlots: boolean,
};

const DrawingUsersProgressWithContext = ({ slots, showSlots }: Props) => (
  <DrawingUsersProgress slots={slots} showSlots={showSlots} />
);

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentGame, currentRound } = state;
  const slots = currentGame ? getGameSlotCards(currentGame) : [];
  const showSlots = currentRound && getRoundState(currentRound) !== roundStates.drawingIntro;
  return {
    slots,
    showSlots,
  };
};

export default withDisplaySessionContext(mapStateToProps)(DrawingUsersProgressWithContext);
