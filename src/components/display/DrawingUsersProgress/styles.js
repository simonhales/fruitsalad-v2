// @flow

import { css } from 'emotion';
import utils from '../../../styles/utils';

const containerClass = css`
  ${utils.layout.absoluteFullView};
`;

const slotsClass = css`
  position: absolute;
  left: 0;
  right: 0;
  top: 100px;
  bottom: 100px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const slotsRowClass = css`
  display: flex;
  justify-content: center;
`;

const ratio = 350 / 290;
const width = 22;

const slotClass = css`
  width: ${width}vh;
  height: ${width * ratio}vh;
  margin: 2vh 2vh 2vh 2vh;
`;

export default {
  containerClass,
  slotsClass,
  slotsRowClass,
  slotClass,
};
