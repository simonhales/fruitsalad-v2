/* eslint-disable react/destructuring-assignment */
// @flow
import React, { Component } from 'react';
import posed from 'react-pose';
import shallowequal from 'shallowequal';
import styles from './styles';
import type { UserSlotCardModel } from '../../../state/bananart/session/models';
import PlayerCard from '../PlayerCard/PlayerCard';

const PARENT_POSES = {
  visible: {
    staggerChildren: 100,
  },
  invisible: {},
};

const POSES = {
  visible: {
    y: 0,
    opacity: 1,
  },
  invisible: {
    y: 50,
    opacity: 0,
  },
};

const SlotsParent = posed.div(PARENT_POSES);
const Slot = posed.div(POSES);

const MAX_USERS_PER_ROW = 5;

type Props = {
  slots: Array<UserSlotCardModel>,
  showSlots: boolean,
};

type State = {};

class DrawingUsersProgress extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  // shouldComponentUpdate(nextProps: Props) {
  //   if (this.props.showSlots !== nextProps.showSlots) {
  //     return true;
  //   }
  //   if (this.props.slots.length !== nextProps.slots.length) {
  //     return true;
  //   }
  //   return false;
  // }

  getSlotSlots() {
    const { slots } = this.props;
    if (slots.length <= MAX_USERS_PER_ROW) {
      return [slots];
    }
    const halfwayIndex = Math.floor(slots.length / 2);
    const firstHalf = slots.slice(0, halfwayIndex);
    const secondHalf = slots.slice(halfwayIndex, slots.length);
    return [firstHalf, secondHalf];
  }

  render() {
    const slotSlots = this.getSlotSlots();
    const { showSlots } = this.props;
    return (
      <div className={styles.containerClass}>
        <div className={styles.slotsClass}>
          {slotSlots.map((slots: Array<UserSlotCardModel>, index: number) => (
            <SlotsParent
              className={styles.slotsRowClass}
              key={index.toString()}
              pose={showSlots ? 'visible' : 'invisible'}
            >
              {slots.map((slot: UserSlotCardModel) => (
                <Slot className={styles.slotClass} key={slot.key}>
                  <PlayerCard
                    joinedTimestamp={slot.user.joinedTimestamp}
                    disabled={slot.user.disabled}
                    name={slot.user.name}
                    userKey={slot.user.key}
                  />
                </Slot>
              ))}
            </SlotsParent>
          ))}
        </div>
      </div>
    );
  }
}

export default DrawingUsersProgress;
