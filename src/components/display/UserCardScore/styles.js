// @flow

import { css } from 'emotion';

const containerClass = css`
  width: 100%;
  height: 100%;
  position: relative;
`;

const scoreContainerClass = css`
  position: absolute;
  left: 0;
  bottom: 0;
  right: 0;
`;

export default {
  containerClass,
  scoreContainerClass,
};
