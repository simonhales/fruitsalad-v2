// @flow
import React from 'react';
import styles from './styles';
import ScoreBadge from '../../ScoreBadge/ScoreBadge';
import UserCardPoints from '../UserCardPoints/UserCardPoints';

type Props = {
  children: any,
  score: number,
  points: number,
  showPoints: boolean,
};

const UserCardScore = ({ children, score, points, showPoints }: Props) => (
  <div className={styles.containerClass}>
    {children}
    <div className={styles.scoreContainerClass}>
      {showPoints ? <UserCardPoints points={points} /> : <ScoreBadge score={score} />}
    </div>
  </div>
);

export default React.memo(UserCardScore);
