// @flow
import React from 'react';
import DrawingDisplay from '../DrawingDisplay/DrawingDisplay';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';

type Props = {
  drawingKey: string,
  drawing: string | null,
};

const EntryDrawing = ({ drawingKey, drawing }: Props) => {
  if (!drawing) return null;
  return <DrawingDisplay drawingKey={drawingKey} drawing={drawing} />;
};

const mapStateToProps = (
  state: DisplaySessionContextState,
  { drawingKey }: { drawingKey: string }
) => {
  const { drawings } = state;
  return {
    drawing: drawings[drawingKey] ? drawings[drawingKey] : null,
  };
};

export default withDisplaySessionContext(mapStateToProps)(EntryDrawing);
