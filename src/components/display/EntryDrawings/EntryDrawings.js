// @flow
import React, { Component } from 'react';
import posed from 'react-pose';
import { cx } from 'emotion';
import { Flipper, Flipped } from 'react-flip-toolkit';
import utils from 'styles/utils/index';
import styles from './styles';
import EntryDrawing from '../EntryDrawing/EntryDrawing';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { getEntryDrawingKeys } from '../../../firebase/bananart/game/entry/state';

const states = {
  visible: 'visible',
  hidden: 'hidden',
};

const sharedStates = {
  [states.visible]: {
    x: 0,
    opacity: 1,
    delay: 1500,
    transition: {
      type: 'spring',
      stiffness: 200,
    },
  },
};

const LeftDrawingWrapper = posed.div({
  ...sharedStates,
  [states.hidden]: {
    x: -200,
    opacity: 0,
  },
});

const RightDrawingWrapper = posed.div({
  ...sharedStates,
  [states.hidden]: {
    x: 200,
    opacity: 0,
  },
});

const drawingsStates = {
  faded: 'faded',
  rest: 'rest',
};

const DrawingsContainer = posed.div({
  [drawingsStates.faded]: {
    opacity: 0.5,
  },
  [drawingsStates.rest]: {
    opacity: 1,
  },
});

export const entryDrawingsLayouts = {
  big: 'big',
  small: 'small',
  smallResults: 'smallResults',
};

export type EntryDrawingsLayouts = $Keys<typeof entryDrawingsLayouts>;

type Props = {
  faded: boolean,
  showDrawings: boolean,
  layout: EntryDrawingsLayouts,
  drawingKeys: Array<string>,
};

function getDrawingPose(showDrawings: boolean): string {
  return showDrawings ? states.visible : states.hidden;
}

class EntryDrawings extends Component<Props> {
  render() {
    const { faded, showDrawings, layout, drawingKeys } = this.props;
    const leftDrawing = drawingKeys[0] ? drawingKeys[0] : null;
    const rightDrawing = drawingKeys[1] ? drawingKeys[1] : null;
    return (
      <DrawingsContainer
        className={cx(styles.containerClass, {
          [utils.classNames.entryDrawingLayoutBig]: layout === entryDrawingsLayouts.big,
          [utils.classNames.entryDrawingLayoutSmall]: layout === entryDrawingsLayouts.small,
          [utils.classNames.entryDrawingLayoutSmallResults]:
            layout === entryDrawingsLayouts.smallResults,
        })}
        pose={faded ? drawingsStates.faded : drawingsStates.rest}
      >
        <Flipper className={styles.drawingsContainerClass} flipKey={layout}>
          <Flipped flipId="leftDrawing">
            <div className={styles.drawingWrapperClass}>
              <LeftDrawingWrapper key="leftDrawing" pose={getDrawingPose(showDrawings)}>
                <div
                  className={cx(styles.drawingClass, styles.leftDrawingClass, {
                    [styles.emptyDrawingClass]: !leftDrawing,
                  })}
                >
                  {leftDrawing && <EntryDrawing drawingKey={leftDrawing} />}
                </div>
              </LeftDrawingWrapper>
            </div>
          </Flipped>
          <Flipped flipId="rightDrawing">
            <div className={styles.drawingWrapperClass}>
              <RightDrawingWrapper key="rightDrawing" pose={getDrawingPose(showDrawings)}>
                <div
                  className={cx(styles.drawingClass, styles.rightDrawingClass, {
                    [styles.emptyDrawingClass]: !rightDrawing,
                  })}
                >
                  {rightDrawing && <EntryDrawing drawingKey={rightDrawing} />}
                </div>
              </RightDrawingWrapper>
            </div>
          </Flipped>
        </Flipper>
      </DrawingsContainer>
    );
  }
}

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentEntry } = state;
  if (!currentEntry) {
    throw new Error(`No current entry.`);
  }
  return {
    drawingKeys: getEntryDrawingKeys(currentEntry),
  };
};

export default withDisplaySessionContext(mapStateToProps)(EntryDrawings);
