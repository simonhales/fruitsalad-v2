// @flow

import { css } from 'emotion';
import utils from 'styles/utils/index';
import { absoluteFullView, flexFullCentered } from '../../../styles/utils/layout';
import {
  getDesktopResponsiveHeightValue,
  getDesktopResponsiveWidthValue,
} from '../../../styles/responsive';
import {
  DRAWINGS_EXTRA_TOP_OFFSET,
  DRAWINGS_SMALL_TOP_OFFSET,
} from '../../../styles/shared/display';

const bigLayoutState = `.${utils.classNames.entryDrawingLayoutBig} &`;
const smallLayoutState = `.${utils.classNames.entryDrawingLayoutSmall} &, .${
  utils.classNames.entryDrawingLayoutSmallResults
} &`;
const smallResultsLayoutState = `.${utils.classNames.entryDrawingLayoutSmallResults} &`;

const containerClass = css`
  ${absoluteFullView};
  ${flexFullCentered};
`;

const drawingsContainerClass = css`
  display: flex;
  justify-content: space-evenly;
  width: 100%;
  height: 100%;

  ${smallLayoutState} {
    justify-content: center;
  }
`;

const drawingWrapperClass = css`
  align-self: center;

  ${smallLayoutState} {
    align-self: flex-start;
  }
`;

const drawingClass = css`
  width: ${getDesktopResponsiveHeightValue(788)};
  height: ${getDesktopResponsiveHeightValue(788)};
  background-color: #ffffff;
  transition: transform 300ms ease;
`;

const emptyDrawingClass = css`
  opacity: 0;
`;

const leftDrawingClass = css`
  ${smallLayoutState} {
    transform: scale(0.63) translate(-15px, ${DRAWINGS_SMALL_TOP_OFFSET}px);
    transform-origin: right top;
  }
  ${smallResultsLayoutState} {
    transform: scale(0.63)
      translate(-15px, ${DRAWINGS_SMALL_TOP_OFFSET + DRAWINGS_EXTRA_TOP_OFFSET}px);
  }
`;
const rightDrawingClass = css`
  ${smallLayoutState} {
    transform: scale(0.63) translate(15px, ${DRAWINGS_SMALL_TOP_OFFSET}px);
    transform-origin: left top;
  }
  ${smallResultsLayoutState} {
    transform: scale(0.63)
      translate(15px, ${DRAWINGS_SMALL_TOP_OFFSET + DRAWINGS_EXTRA_TOP_OFFSET}px);
  }
`;

export default {
  containerClass,
  drawingsContainerClass,
  drawingWrapperClass,
  drawingClass,
  emptyDrawingClass,
  leftDrawingClass,
  rightDrawingClass,
};
