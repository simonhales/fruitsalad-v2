// @flow

import { css } from 'emotion';
import utils from 'styles/utils';
import {
  fancyText,
  getResponsiveDesktopFontSize,
  getResponsiveFont,
} from '../../../styles/typography';

const containerClass = css`
  display: flex;
  justify-content: center;
  width: 100%;
  transform: translateY(50%);
`;

const pointClass = css`
  ${fancyText};
  ${utils.fonts.families.chewy};
  ${getResponsiveFont(getResponsiveDesktopFontSize(75), '14px')};
`;

export default {
  containerClass,
  pointClass,
};
