// @flow
import React from 'react';
import styles from './styles';

type Props = {
  points: number,
};

const UserCardPoints = ({ points }: Props) => (
  <div className={styles.containerClass}>
    <div className={styles.pointClass}>{`+${points}`}</div>
  </div>
);

export default UserCardPoints;
