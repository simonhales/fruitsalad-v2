// @flow
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
import utils from 'styles/utils';
import Fullscreen from 'react-full-screen';
import { cx } from 'emotion';
import styles from './styles';
import AudioToggle from '../AudioToggle/AudioToggle';
import AudioSettingsModal from '../AudioSettingsModal/AudioSettingsModal';

type Props = {
  children: React.Node,
};

type State = {
  audioSettingsModalOpen: boolean,
  fullscreen: boolean,
};

class FullscreenWrapper extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      audioSettingsModalOpen: false,
      fullscreen: false,
    };
  }

  handleGoFullscreen = () => {
    this.setState({
      fullscreen: true,
    });
  };

  handleOnChange = (full: boolean) => {
    this.setState({
      fullscreen: full,
    });
  };

  handleCloseAudioSettingsModal = () => {
    this.setState({
      audioSettingsModalOpen: false,
    });
  };

  handleOpenAudioSettingsModal = () => {
    this.setState({
      audioSettingsModalOpen: true,
    });
  };

  render() {
    const { children } = this.props;
    const { audioSettingsModalOpen, fullscreen } = this.state;
    return (
      <Fullscreen enabled={fullscreen} onChange={this.handleOnChange}>
        <div
          className={cx(styles.containerClass, {
            [utils.classNames.displayIsFullscreen]: fullscreen,
          })}
        >
          {children}
          <div className={styles.optionsContainerClass}>
            <div className={styles.fullScreenButtonClass} onClick={this.handleGoFullscreen}>
              FULLSCREEN
            </div>
            <AudioToggle onClick={this.handleOpenAudioSettingsModal} />
          </div>
        </div>
        {audioSettingsModalOpen && (
          <AudioSettingsModal close={this.handleCloseAudioSettingsModal} />
        )}
      </Fullscreen>
    );
  }
}

export default FullscreenWrapper;
