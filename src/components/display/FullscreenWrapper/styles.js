// @flow
import utils from 'styles/utils';
import { css } from 'emotion';
import { borderButton, buttonHover, smallButton } from '../../../styles/buttons';

const containerClass = css`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const optionsContainerClass = css`
  position: absolute;
  top: ${utils.spacing.small}px;
  right: ${utils.spacing.small}px;
  z-index: ${utils.zindexes.displayOptions};
  display: flex;
`;

const fullScreenButtonClass = css`
  ${buttonHover};
  ${smallButton};

  .${utils.classNames.displayIsFullscreen} & {
    visibility: hidden;
  }
`;

export default {
  containerClass,
  optionsContainerClass,
  fullScreenButtonClass,
};
