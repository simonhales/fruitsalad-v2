// @flow

import { css } from 'emotion';
import { answerText } from '../../../styles/typography';
import animations from '../../../styles/animations';
import cardEnter from '../../../styles/animations/cardEnter';

const containerClass = css`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  text-align: center;
  height: 150px;
  display: flex;
  align-items: center;
  justify-content: center;
  line-height: 150px;
`;

const answerWrapperClass = css`
  width: 50%;
  margin: 0 auto;
  height: 100%;
  text-transform: lowercase;
  position: relative;
`;

const answerClass = css`
  ${answerText};
  ${animations.includeAnimation(
    animations.animations.slideFadeUp,
    { duration: '750ms', delay: '2000ms' },
    true
  )};
`;

const answerStampClass = css`
  position: absolute;
  top: 10%;
  left: 0;
  right: 0;
  bottom: -10%;
  display: flex;
  align-items: center;
  justify-content: center;

  ${animations.includeAnimation(
    animations.animations.cardEnter,
    { duration: '500ms', delay: '2000ms' },
    true
  )};
`;

export default {
  containerClass,
  answerWrapperClass,
  answerClass,
  answerStampClass,
};
