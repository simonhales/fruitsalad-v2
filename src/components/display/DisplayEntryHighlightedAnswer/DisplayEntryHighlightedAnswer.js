// @flow
import React from 'react';
import posed, { PoseGroup } from 'react-pose';
import { Textfit } from 'react-textfit';
import styles from './styles';
import { posedStates } from '../../../animation/posed';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import {
  getAnswerRevealDelay,
  getEntryAnswerIndex,
  getEntryAnswerViaAnswerIndex,
} from '../../../firebase/bananart/game/entry/state';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import type { EntryAnswerModel } from '../../../firebase/bananart/game/entry/models';
import AnswerStamp from '../AnswerStamp/AnswerStamp';

const AnswerWrapper = posed.div({
  [posedStates.enter]: {
    opacity: 1,
    delay: 1000,
  },
  [posedStates.exit]: {
    y: -50,
    opacity: 0,
  },
});

type Props = {
  answer: EntryAnswerModel,
  visible: boolean,
  answerRevealDelay: number,
};

const DisplayEntryHighlightedAnswer = ({ answer, visible, answerRevealDelay }: Props) => (
  <div className={styles.containerClass}>
    {visible &&
      answer && (
        <AnswerWrapper className={styles.answerWrapperClass} key={answer.key}>
          <div className={styles.answerClass}>
            <Textfit mode="single" forceSingleModeWidth={false}>
              {answer.text}
            </Textfit>
          </div>
          <div
            className={styles.answerStampClass}
            style={{ animationDelay: `${answerRevealDelay}ms` }}
          >
            <AnswerStamp fake={!answer.prompt} />
          </div>
        </AnswerWrapper>
      )}
  </div>
);

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentEntry } = state;
  if (!currentEntry) {
    throw new Error(`No current entry.`);
  }
  const answerIndex = getEntryAnswerIndex(currentEntry);
  const answer = answerIndex > -1 ? getEntryAnswerViaAnswerIndex(currentEntry, answerIndex) : null;
  const answerRevealDelay = answer ? getAnswerRevealDelay(currentEntry, answer.key) : 0;
  return {
    answer,
    answerRevealDelay,
  };
};

export default withDisplaySessionContext(mapStateToProps)(DisplayEntryHighlightedAnswer);
