// @flow
import React, { Component } from 'react';
import posed from 'react-pose';
import styles from './styles';

const Container = posed.div({
  activeTimer: {
    scale: 1,
    delay: 1000,
  },
  inactiveTimer: {
    scale: 0,
    transition: {
      stiffness: 200,
      damping: 50,
    },
  },
});

const Circle = posed.div({
  attention: {
    scale: 1.05,
    transition: {
      type: 'spring',
      stiffness: 200,
      damping: 0,
    },
  },
  rest: {
    scale: 1,
    transition: {
      type: 'tween',
      stiffness: 50,
      damping: 0,
    },
  },
});

const Time = posed.div({
  default: {},
  large: {
    scale: 1.5,
    y: 5,
    x: 20,
  },
});

type Props = {
  active: boolean,
  duration: number,
};

type State = {
  countdown: number,
  seekingAttention: boolean,
  startedCountdown: boolean,
};

class CountdownTimer extends Component<Props, State> {
  countdownTimeout: TimeoutID;

  seekingAttentionTimeout: TimeoutID;

  constructor(props: Props) {
    super(props);
    this.state = {
      countdown: props.duration / 1000,
      seekingAttention: false,
      startedCountdown: false,
    };
  }

  componentDidMount() {
    this.tryToStartCountdown();
  }

  componentWillUnmount() {
    clearTimeout(this.countdownTimeout);
    clearTimeout(this.seekingAttentionTimeout);
  }

  componentDidUpdate() {
    this.tryToStartCountdown();
  }

  tryToStartCountdown() {
    const { startedCountdown } = this.state;
    if (startedCountdown) return;
    const { active } = this.props;
    if (active) {
      this.startCountdown();
    }
  }

  startCountdown = () => {
    this.setState({
      startedCountdown: true,
    });
    this.startSeekingAttention();
    this.continueCountdown();
  };

  continueCountdown = () => {
    const { countdown } = this.state;
    const newCountdown = countdown - 1;
    this.setState({
      countdown: newCountdown,
    });
    if (newCountdown > 0) {
      this.countdownTimeout = setTimeout(this.continueCountdown, 1000);
    }
  };

  startSeekingAttention = () => {
    this.setState({
      seekingAttention: true,
    });
    this.seekingAttentionTimeout = setTimeout(this.pauseSeekingAttention, 1000);
  };

  pauseSeekingAttention = () => {
    this.setState({
      seekingAttention: false,
    });
    this.seekingAttentionTimeout = setTimeout(this.startSeekingAttention, 9000);
  };

  isActive() {
    const { active } = this.props;
    const { countdown } = this.state;
    return active && countdown > 0;
  }

  render() {
    const { countdown, seekingAttention } = this.state;
    return (
      <div className={styles.containerClass}>
        <Container
          className={styles.animatedContainerClass}
          pose={this.isActive() ? 'activeTimer' : 'inactiveTimer'}
        >
          <Circle className={styles.circleClass} pose={seekingAttention ? 'attention' : 'rest'} />
          <Time className={styles.textClass} pose={countdown < 10 ? 'large' : 'default'}>
            {countdown}
          </Time>
        </Container>
      </div>
    );
  }
}

export default CountdownTimer;
