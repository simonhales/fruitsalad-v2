// @flow

import { css } from 'emotion';
import utils from '../../../styles/utils/index';
import { getDesktopResponsiveHeightValue } from '../../../styles/responsive';
import { getResponsiveDesktopFontSize } from '../../../styles/typography';

const containerClass = css`
  position: absolute;
  right: 0;
  bottom: 0;
  transform: translate(35%, 35%);
`;

const animatedContainerClass = css`
  transform-origin: bottom right;
`;

const circleClass = css`
  width: ${getDesktopResponsiveHeightValue(170)};
  height: ${getDesktopResponsiveHeightValue(170)};
  background-color: ${utils.colors.orange};
  border-radius: 50%;
`;

const textClass = css`
  ${utils.fonts.families.displayFancy};
  color: ${utils.colors.white};
  position: absolute;
  top: 20%;
  left: 10%;
  font-size: ${getResponsiveDesktopFontSize(85)};
  letter-spacing: -0.4px;
`;

export default {
  containerClass,
  animatedContainerClass,
  circleClass,
  textClass,
};
