// @flow

import { css } from 'emotion';
import utils from '../../../styles/utils';
import { getResponsiveDesktopFontSize, getResponsiveFont } from '../../../styles/typography';
import { smallButton } from '../../../styles/buttons';

const containerClass = css`
  position: relative;
  background-color: #ffffff;
  text-align: center;
  border-radius: 20px;
  padding: 20px;
`;

const headerClass = css`
  ${utils.fonts.families.chewy};
  ${getResponsiveFont(getResponsiveDesktopFontSize(37.5), '14px')};
  padding-bottom: 10px;
`;

const optionClass = css`
  padding: 10px;
  border-top: 1px solid #cde2e8;
  cursor: pointer;
`;

const optionOffClass = css`
  opacity: 0.5;
`;

const audioLabelClass = css`
  display: inline-block;
  margin-right: 5px;
  font-weight: 700;
`;

const buttonClass = css`
  ${smallButton};
`;

export default {
  containerClass,
  headerClass,
  optionClass,
  optionOffClass,
  audioLabelClass,
  buttonClass,
};
