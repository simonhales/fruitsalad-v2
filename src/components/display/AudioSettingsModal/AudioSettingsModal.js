// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { cx } from 'emotion';
import Modal from '../../Modal/Modal';
import styles from './styles';
import type { ReduxState } from '../../../state/redux/shared/models';
import {
  setMusicMuted,
  setNarratorMuted,
  setSoundEffectsMuted,
} from '../../../state/redux/audio/reducer';

type Props = {
  close: () => void,
  musicMuted: boolean,
  narratorMuted: boolean,
  soundEffectsMuted: boolean,
  toggleMusic: (muted: boolean) => void,
  toggleNarrator: (muted: boolean) => void,
  toggleSoundEffects: (muted: boolean) => void,
};

class AudioSettingsModal extends Component<Props> {
  toggleMusic = () => {
    const { musicMuted, toggleMusic } = this.props;
    toggleMusic(!musicMuted);
  };

  toggleNarrator = () => {
    const { narratorMuted, toggleNarrator } = this.props;
    toggleNarrator(!narratorMuted);
  };

  toggleSoundEffects = () => {
    const { soundEffectsMuted, toggleSoundEffects } = this.props;
    toggleSoundEffects(!soundEffectsMuted);
  };

  handleClose = () => {
    const { close } = this.props;
    close();
  };

  render() {
    const { musicMuted, narratorMuted, soundEffectsMuted } = this.props;
    return (
      <div className={styles.containerClass}>
        <header className={styles.headerClass}>Audio Settings</header>
        <div>
          <div
            className={cx(styles.optionClass, {
              [styles.optionOffClass]: narratorMuted,
            })}
            onClick={this.toggleNarrator}
          >
            <span className={styles.audioLabelClass}>narrator</span>
            <span>audio is {narratorMuted ? 'off' : 'on'}</span>
          </div>
          <div
            className={cx(styles.optionClass, {
              [styles.optionOffClass]: soundEffectsMuted,
            })}
            onClick={this.toggleSoundEffects}
          >
            <span className={styles.audioLabelClass}>sound effects</span>
            <span>audio is {soundEffectsMuted ? 'off' : 'on'}</span>
          </div>
          <div
            className={cx(styles.optionClass, {
              [styles.optionOffClass]: musicMuted,
            })}
            onClick={this.toggleMusic}
          >
            <span className={styles.audioLabelClass}>music</span>
            <span>audio is {musicMuted ? 'off' : 'on'}</span>
          </div>
        </div>
        <div>
          <div className={styles.buttonClass} onClick={this.handleClose}>
            done
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: ReduxState) => ({
  musicMuted: state.audio.musicMuted,
  narratorMuted: state.audio.narratorMuted,
  soundEffectsMuted: state.audio.soundEffectsMuted,
});

const mapDispatchToProps = {
  toggleMusic: (muted: boolean) => setMusicMuted(muted),
  toggleNarrator: (muted: boolean) => setNarratorMuted(muted),
  toggleSoundEffects: (muted: boolean) => setSoundEffectsMuted(muted),
};

const ConnectedAudioSettingsModal = connect(
  mapStateToProps,
  mapDispatchToProps
)(AudioSettingsModal);

const ModalWrapper = ({ close }: { close: () => void }) => (
  <Modal close={close}>
    <ConnectedAudioSettingsModal close={close} />
  </Modal>
);

export default ModalWrapper;
