// @flow

import { css } from 'emotion';
import animations from '../../../styles/animations';
import { DRAWINGS_EXTRA_TOP_OFFSET } from '../../../styles/shared/display';
import utils from '../../../styles/utils';
import shake from '../../../styles/animations/shake';

const resultsState = `.${utils.classNames.entryAnswersResults} &`;
const highlightingState = `.${utils.classNames.entryAnswersHighlighting} &`;

const containerClass = css`
  position: absolute;
  top: ${(594 / 1040) * 100}%;
  left: 10px;
  right: 10px;
  bottom: 80px;
  transition: transform 300ms ease;
`;

const containerPushedDownClass = css`
  transform: translateY(${DRAWINGS_EXTRA_TOP_OFFSET - 30}px);
`;

const gridClass = css`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr 1fr;
  min-height: 0; /* NEW */
  min-width: 0; /* NEW; needed for Firefox */
  transition: opacity 500ms ease;

  ${resultsState} {
    opacity: 0.33;
  }

  ${highlightingState} {
    opacity: 1;
  }
`;

const blockClass = css`
  width: 100%;
  height: 100%;

  //overflow: hidden; /* NEW */
  min-width: 0; /* NEW; needed for Firefox */

  ${animations.includeAnimation(
    animations.animations.slideFadeUp,
    { duration: '750ms', delay: '500ms' },
    true
  )};

  &:nth-child(1) {
    grid-row-start: 1;
    grid-row-end: 1;
    grid-column-start: 2;
    grid-column-end: 2;
  }

  &:nth-child(2) {
    grid-row-start: 1;
    grid-row-end: 1;
    grid-column-start: 3;
    grid-column-end: 3;
  }

  &:nth-child(3) {
    grid-row-start: 2;
    grid-row-end: 2;
    grid-column-start: 2;
    grid-column-end: 2;
  }

  &:nth-child(4) {
    grid-row-start: 2;
    grid-row-end: 2;
    grid-column-start: 3;
    grid-column-end: 3;
  }
`;

const selectedBlockClass = css`
  ${resultsState} {
    opacity: 0;
  }

  ${highlightingState} {
    opacity: 1;
    ${animations.includeAnimation(animations.animations.shake, {
      ease: 'cubic-bezier(.36,.07,.19,.97)',
      duration: '1000ms',
      timing: 'infinite',
    })};
  }
`;

const revealedBlockClass = css`
  ${resultsState} {
    opacity: 0;
  }
`;

const answerClass = css`
  transition: opacity 500ms ease;
  width: 100%;
  height: 100%;
`;

export default {
  containerClass,
  containerPushedDownClass,
  gridClass,
  blockClass,
  selectedBlockClass,
  revealedBlockClass,
  answerClass,
};
