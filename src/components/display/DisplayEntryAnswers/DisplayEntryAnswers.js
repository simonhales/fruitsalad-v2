// @flow
import React, { Component } from 'react';
import utils from 'styles/utils/index';
import { cx } from 'emotion';
import styles from './styles';
import Answer from '../../Answer/Answer';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import {
  getEntryAnswerIndex,
  getEntryAnswersList,
  getEntryAnswerViaAnswerIndex,
  getEntryAnswerVotersKeys,
  getRevealedAnswersKeys,
} from '../../../firebase/bananart/game/entry/state';
import type { EntryAnswerModel } from '../../../firebase/bananart/game/entry/models';

function getAnimationDelay(index: number): string {
  let rowIndex = 0;
  switch (index) {
    case 2:
    case 3:
    case 6:
    case 7:
      rowIndex = 1;
      break;
    case 8:
    case 9:
    case 10:
    case 11:
      rowIndex = 2;
      break;
    default:
      rowIndex = 0;
  }
  return `${rowIndex * 100 + 500}ms`;
}

type Props = {
  answers: Array<EntryAnswerModel>,
  visible: boolean,
  highlightedAnswerIndex: number,
  highlightedAnswerKey: string,
  pushedDown: boolean,
  revealedAnswers: Array<string>,
};

type State = {
  highlightingAnswer: boolean,
  startedResults: boolean,
};

class DisplayEntryAnswers extends Component<Props, State> {
  highlightTimeout: TimeoutID;

  constructor(props: Props) {
    super(props);
    this.state = {
      highlightingAnswer: true,
      startedResults: false,
    };
  }

  componentDidMount() {
    const { visible, highlightedAnswerIndex } = this.props;
    if (visible && highlightedAnswerIndex > -1) {
      this.highlightAnimationTimeout();
    }
  }

  componentDidUpdate(previousProps: Props) {
    if (
      this.props.visible &&
      this.props.highlightedAnswerIndex !== previousProps.highlightedAnswerIndex
    ) {
      this.highlightAnimationTimeout();
    }
  }

  componentWillUnmount() {
    this.clearTimeout();
  }

  clearTimeout() {
    if (this.highlightTimeout) {
      clearTimeout(this.highlightTimeout);
    }
  }

  highlightAnimationTimeout() {
    this.setState({
      highlightingAnswer: true,
      startedResults: true,
    });
    this.clearTimeout();
    setTimeout(this.completeHighlightAnimation, 2000);
  }

  completeHighlightAnimation = () => {
    this.setState({
      highlightingAnswer: false,
    });
  };

  renderGrid() {
    const { answers, highlightedAnswerKey, revealedAnswers } = this.props;
    return (
      <div className={styles.gridClass}>
        {answers.map((answer, index) => (
          <div
            className={cx(styles.blockClass)}
            key={index.toString()}
            style={{
              animationDelay: getAnimationDelay(index),
            }}
          >
            <div
              className={cx(styles.answerClass, {
                [styles.selectedBlockClass]: highlightedAnswerKey === answer.key,
                [styles.revealedBlockClass]: revealedAnswers.includes(answer.key),
              })}
            >
              <Answer text={answer.text} />
            </div>
          </div>
        ))}
      </div>
    );
  }

  render() {
    const { pushedDown, visible } = this.props;
    const { highlightingAnswer, startedResults } = this.state;
    return (
      <div
        className={cx(styles.containerClass, {
          [styles.containerPushedDownClass]: pushedDown,
          [utils.classNames.entryAnswersResults]: startedResults,
          [utils.classNames.entryAnswersHighlighting]: highlightingAnswer,
        })}
      >
        {visible && this.renderGrid()}
      </div>
    );
  }
}

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentGame, currentEntry } = state;
  if (!currentGame) {
    throw new Error(`No current game.`);
  }
  if (!currentEntry) {
    throw new Error(`No current entry.`);
  }
  const answerIndex = getEntryAnswerIndex(currentEntry);
  const answer = answerIndex > -1 ? getEntryAnswerViaAnswerIndex(currentEntry, answerIndex) : null;
  const answers = getEntryAnswersList(currentEntry);
  const revealedAnswers = getRevealedAnswersKeys(currentEntry);
  return {
    answers,
    highlightedAnswerIndex: answerIndex,
    highlightedAnswerKey: answer ? answer.key : '',
    revealedAnswers,
  };
};

export default withDisplaySessionContext(mapStateToProps)(DisplayEntryAnswers);
