// @flow
import * as React from 'react';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { getGameUserViaKey } from '../../../firebase/bananart/game/state';
import type { GameUserModel } from '../../../firebase/bananart/game/user/models';

type Props = {
  children: any,
  userKey: string,
  user: GameUserModel,
};

const FetchGameUser = ({ children, user }: Props) => children({ user });

const mapStateToProps = (state: DisplaySessionContextState, { userKey }: { userKey: string }) => {
  const { currentGame } = state;
  if (!currentGame) {
    throw new Error(`No current game.`);
  }
  const user = getGameUserViaKey(currentGame, userKey);
  return {
    user,
  };
};

export default withDisplaySessionContext(mapStateToProps)(FetchGameUser);
