// @flow
import React from 'react';
import posed, { PoseGroup } from 'react-pose';
import styles, { DisplayEntryResultsArtist } from './styles';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { getArtistAnimationDelay, getVoterAnimationDelay } from '../../../constants/timers';
import { posedStates } from '../../../animation/posed';
import {
  getAnswerCulpritsKeys,
  getEntryAnswerIndex,
  getEntryAnswerViaAnswerIndex,
  getEntryAnswerVotersKeys,
} from '../../../firebase/bananart/game/entry/state';
import { getGameUserViaKey } from '../../../firebase/bananart/game/state';
import type { GameUserModel } from '../../../firebase/bananart/game/user/models';
import FetchGameUser from '../FetchGameUser/FetchGameUser';
import DisplayUserCard from '../DisplayUserCard/DisplayUserCard';

const VOTERS_PER_ROW = 3;

const Voters = ({ voters }: { voters: Array<any> }) => {
  const numberOfRows = Math.ceil(voters.length / VOTERS_PER_ROW);
  const rows = Array.from({ length: numberOfRows }).map(() => []);
  voters.forEach((user: any, index: number) => {
    const rowIndex = Math.floor(index / VOTERS_PER_ROW);
    const row = rows[rowIndex];
    row.push(user);
  });
  return (
    <div className={styles.votersClass}>
      {rows.map((row, rowIndex) => (
        <div className={styles.votersRowClass} key={rowIndex.toString()}>
          {row.map((user, index) => (
            <div className={styles.voterClass} key={index.toString()}>
              <div
                className={styles.voterInnerClass}
                style={{
                  animationDelay: `${getVoterAnimationDelay(index + rowIndex * VOTERS_PER_ROW)}ms`,
                }}
              >
                <DisplayUserCard userKey={user.key} />
              </div>
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

const Artists = ({ artists, numberOfVoters }: { artists: Array<any>, numberOfVoters: number }) => (
  <div className={styles.artistsClass}>
    {artists.map((artist, index) => (
      <DisplayEntryResultsArtist
        className={styles.artistClass}
        key={index.toString()}
        index={index}
      >
        <div
          className={styles.artistInnerClass}
          key={index.toString()}
          style={{
            animationDelay: `${getArtistAnimationDelay(index, numberOfVoters)}ms`,
          }}
        >
          <DisplayUserCard userKey={artist.key} />
        </div>
      </DisplayEntryResultsArtist>
    ))}
  </div>
);

const Container = posed.div({
  [posedStates.enter]: {
    y: 0,
    opacity: 1,
  },
  [posedStates.exit]: {
    y: 50,
    opacity: 0,
  },
});

type Props = {
  voters: Array<GameUserModel>,
  artists: Array<GameUserModel>,
  answerIndex: number,
  visible: boolean,
};

const DisplayEntryResults = ({ voters, artists, visible, answerIndex }: Props) => (
  <PoseGroup>
    {visible && (
      <Container className={styles.containerClass} key={`ENTRY_RESULTS_${answerIndex.toString()}`}>
        <Voters voters={voters} />
        <Artists artists={artists} numberOfVoters={voters.length} />
      </Container>
    )}
  </PoseGroup>
);

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentGame, currentEntry } = state;
  if (!currentGame) {
    throw new Error(`No current game.`);
  }
  if (!currentEntry) {
    throw new Error(`No current entry.`);
  }
  const answerIndex = getEntryAnswerIndex(currentEntry);
  const answer = getEntryAnswerViaAnswerIndex(currentEntry, answerIndex);
  if (!answer) {
    return {
      voters: [],
      artists: [],
      answerIndex,
    };
  }
  const answerKey = answer.key;
  const votersKeys = getEntryAnswerVotersKeys(answerKey, currentEntry);
  const voters = votersKeys.map(userKey => getGameUserViaKey(currentGame, userKey));
  const culpritsKeys = getAnswerCulpritsKeys(answer);
  const culprits = culpritsKeys.map(userKey => getGameUserViaKey(currentGame, userKey));
  return {
    voters,
    artists: culprits,
    answerIndex,
  };
};

export default withDisplaySessionContext(mapStateToProps)(DisplayEntryResults);
