// @flow

import { css } from 'emotion';
import styled from 'react-emotion';
import {
  getDesktopResponsiveHeightValue,
  getDesktopResponsiveWidthValue,
} from '../../../styles/responsive';
import animations from '../../../styles/animations';

const containerClass = css`
  position: absolute;
  left: ${getDesktopResponsiveWidthValue(50)};
  bottom: ${getDesktopResponsiveHeightValue(20)};
  right: ${getDesktopResponsiveWidthValue(50)};
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

const votersClass = css``;

const rowHeight = getDesktopResponsiveHeightValue(45);

const votersRowClass = css`
  display: flex;
  position: relative;
  height: ${rowHeight};

  &:nth-child(2) {
    left: ${getDesktopResponsiveWidthValue(50)};
  }

  &:nth-child(3) {
    left: ${getDesktopResponsiveWidthValue(100)};
  }

  &:nth-child(4) {
    left: ${getDesktopResponsiveWidthValue(150)};
  }
`;

const card = css`
  width: ${getDesktopResponsiveWidthValue(240)};
  height: ${getDesktopResponsiveWidthValue(290)};
`;

const voterClass = css`
  ${card};
  transform: translateY(-100%);
  position: relative;

  &:nth-child(2) {
    left: -${getDesktopResponsiveWidthValue(50)};
  }

  &:nth-child(3) {
    left: -${getDesktopResponsiveWidthValue(100)};
  }
`;

const voterInnerClass = css`
  width: 100%;
  height: 100%;
  ${animations.includeAnimation(
    animations.animations.cardEnter,
    {
      duration: '500ms',
    },
    true
  )};
`;

const artistsClass = css`
  padding-bottom: ${rowHeight};
  display: flex;
  flex-direction: row-reverse;
`;

const artistClass = css`
  ${card};
`;

const artistInnerClass = css`
  width: 100%;
  height: 100%;
  ${animations.includeAnimation(
    animations.animations.cardEnter,
    {
      duration: '500ms',
    },
    true
  )};
`;

type ArtistProps = {
  index: number,
};

export const DisplayEntryResultsArtist = styled('div')`
  ${({ index }: ArtistProps) =>
    `transform: translateX(${getDesktopResponsiveWidthValue(index * 30)});`};
`;

export default {
  containerClass,
  votersClass,
  votersRowClass,
  voterClass,
  voterInnerClass,
  artistsClass,
  artistClass,
  artistInnerClass,
};
