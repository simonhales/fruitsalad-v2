// @flow
import React from 'react';
import AuthWrapper from '../../AuthWrapper/AuthWrapper';
import SetSessionKey from '../../SetSessionKey/SetSessionKey';
import DisplayView from '../../../views/display/DisplayView/DisplayView';
import DisplaySessionFirebaseConnector from '../DisplaySessionFirebaseConnector/DisplaySessionFirebaseConnector';

const FirebaseDisplayPreview = () => (
  <SetSessionKey>
    <AuthWrapper>
      <DisplaySessionFirebaseConnector>
        <DisplayView preview />
      </DisplaySessionFirebaseConnector>
    </AuthWrapper>
  </SetSessionKey>
);

export default FirebaseDisplayPreview;
