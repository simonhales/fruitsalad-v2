// @flow
import React from 'react';
import FetchGameUser from '../FetchGameUser/FetchGameUser';
import type { GameUserModel } from '../../../firebase/bananart/game/user/models';
import PlayerCard from '../PlayerCard/PlayerCard';

type Props = {
  userKey: string,
};

const DisplayUserCard = ({ userKey }: Props) => (
  <FetchGameUser userKey={userKey}>
    {({ user }: { user: GameUserModel }) => (
      <PlayerCard userKey={userKey} name={user.name} disabled={false} joinedTimestamp={0} />
    )}
  </FetchGameUser>
);

export default DisplayUserCard;
