// @flow

import { css } from 'emotion';
import {
  getDesktopResponsiveHeightValue,
  getDesktopResponsiveWidthValue,
} from '../../../styles/responsive';

const containerClass = css`
  position: absolute;
  top: 10vh;
  left: 0;
  right: 0;
  bottom: 3vh;
`;

const gridClass = css`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  justify-content: space-around;
  height: 100%;
`;

const blockClass = css`
  width: ${getDesktopResponsiveWidthValue(286)};
  height: 50%;
  display: flex;
  flex-direction: column;
`;

const blockTopClass = css`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 10px 10px 10px;
`;

const blockCardClass = css`
  height: ${getDesktopResponsiveHeightValue(346)};
`;

export default {
  containerClass,
  gridClass,
  blockClass,
  blockTopClass,
  blockCardClass,
};
