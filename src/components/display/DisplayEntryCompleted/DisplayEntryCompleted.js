// @flow
import React from 'react';
import { Flipper, Flipped } from 'react-flip-toolkit';
import posed, { PoseGroup } from 'react-pose';
import styles from './styles';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import {
  getGameUserCurrentScore,
  getGameUsersListSortedByScore,
} from '../../../firebase/bananart/game/state';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import DisplayUserCard from '../DisplayUserCard/DisplayUserCard';
import FitPlainText from '../../FitPlainText/FitPlainText';
import {
  getUserEntryAnswer,
  getUserEntryScore,
  hasEntryScoresBeenCalculated,
} from '../../../firebase/bananart/game/entry/state';
import UserCardScore from '../UserCardScore/UserCardScore';
import { posedStates } from '../../../animation/posed';

type UserResult = {
  key: string,
  answer: string,
  score: number,
  entryScore: number,
};

function getUsersKeys(users: Array<UserResult>): string {
  return users.map(user => user.key).join(',');
}

const Block = posed.div({
  [posedStates.enter]: {
    scale: 1,
    opacity: 1,
    transition: { ease: 'anticipate', duration: 300 },
  },
  [posedStates.exit]: {
    scale: 0.5,
    opacity: 0,
  },
});

type Props = {
  visible: boolean,
  users: Array<UserResult>,
  scoresCalculated: boolean,
};

const DisplayEntryCompleted = ({ users, visible, scoresCalculated }: Props) => (
  <div className={styles.containerClass}>
    <Flipper className={styles.gridClass} flipKey={getUsersKeys(users)} spring="veryGentle">
      {users.map(user => (
        <Flipped flipId={user.key} key={user.key}>
          <PoseGroup>
            {visible && (
              <Block className={styles.blockClass} key={user.key}>
                <div className={styles.blockTopClass}>
                  <FitPlainText>{user.answer}</FitPlainText>
                </div>
                <div className={styles.blockCardClass}>
                  <UserCardScore
                    score={user.score}
                    points={user.entryScore}
                    showPoints={!scoresCalculated}
                  >
                    <DisplayUserCard userKey={user.key} />
                  </UserCardScore>
                </div>
              </Block>
            )}
          </PoseGroup>
        </Flipped>
      ))}
    </Flipper>
  </div>
);

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { currentGame, currentEntry } = state;
  if (!currentGame) {
    throw new Error(`No current game.`);
  }
  if (!currentEntry) {
    throw new Error(`No current entry.`);
  }
  const users = getGameUsersListSortedByScore(currentGame);
  const sortedUsers = users.map(user => ({
    key: user.key,
    answer: getUserEntryAnswer(currentEntry, user.key),
    score: getGameUserCurrentScore(currentGame, user.key),
    entryScore: getUserEntryScore(currentEntry, user.key),
  }));
  console.log('sortedUsers', sortedUsers);
  const scoresCalculated = hasEntryScoresBeenCalculated(currentEntry);
  return {
    scoresCalculated,
    users: sortedUsers,
  };
};

export default withDisplaySessionContext(mapStateToProps)(DisplayEntryCompleted);
