// @flow

import { css } from 'emotion';
import { largeThickHeading } from '../../../styles/typography';

const titleClass = css`
  ${largeThickHeading};
`;

export default {
  titleClass,
};
