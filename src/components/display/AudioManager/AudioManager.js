// @flow
import React, { Component } from 'react';
import { audioNarrator } from 'audio/narrator/AudioNarrator';
import { AudioNarratorGameHandler } from '../../../audio/narrator/AudioNarratorGameHandler';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import { AudioMusicGameHandler } from '../../../audio/music/AudioMusicGameHandler';
import { getMusicDisabled } from '../../../utils/env';
import { audioSoundEffectsGameHandler } from '../../../audio/soundEffects/AudioSoundEffectsGameHandler';

type Props = {
  session: SessionModel,
};

const musicEnabled = !getMusicDisabled();

class AudioManager extends Component<Props> {
  music: AudioMusicGameHandler;

  narrator: AudioNarratorGameHandler;

  constructor(props: Props) {
    super(props);
    const { session } = props;
    if (musicEnabled) {
      this.music = new AudioMusicGameHandler(session);
    }
    this.narrator = new AudioNarratorGameHandler(session);
    audioNarrator.setSession(session);
    audioSoundEffectsGameHandler.triggerGameSoundEffects(session);
  }

  componentDidUpdate(previousProps: Props) {
    const { session } = this.props;
    audioNarrator.setSession(session);
    if (musicEnabled) {
      this.music.triggerGameMusic(session, previousProps.session);
    }
    this.narrator.triggerGameSounds(session, previousProps.session);
    audioSoundEffectsGameHandler.triggerGameSoundEffects(session);
  }

  render() {
    return null;
  }
}

export default AudioManager;
