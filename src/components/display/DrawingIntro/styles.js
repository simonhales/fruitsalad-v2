// @flow

import { css } from 'emotion';
import {
  displayHelpText,
  displayInstructionLargeText,
  displayInstructionSmallText,
  displayInstructionText,
  fancyText,
  getResponsiveDesktopFontSize,
  getResponsiveFont,
} from '../../../styles/typography';
import animations from '../../../styles/animations';
import utils from '../../../styles/utils';

const baseAnimationDelay = 1000;
const extraAnimationDelay = baseAnimationDelay + 1750;

const minimisedState = `.${utils.classNames.drawingIntroMinimised} &`;

const containerClass = css`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 20px 0;

  &.${utils.classNames.drawingIntroMinimised} {
    justify-content: space-between;
  }
`;

const dropdownAnimation = css`
  ${animations.includeAnimation(
    animations.animations.slideInDown,
    {
      delay: `${baseAnimationDelay}ms`,
      duration: '750ms',
      ease: 'cubic-bezier(0.21, 1.55, 0.73, 1.55)',
    },
    true
  )};
`;

const topClass = css`
  position: relative;
`;

const taglineClass = css`
  ${fancyText};
  ${getResponsiveFont(getResponsiveDesktopFontSize(44.5), '14px')};
  margin-bottom: 5px;
  position: absolute;
  bottom: 100%;

  span {
    transition: opacity 300ms ease;

    ${minimisedState} {
      opacity: 0;
    }
  }

  ${animations.includeAnimation(
    animations.animations.fadeIn,
    {
      duration: '750ms',
      delay: `${baseAnimationDelay + 1000}ms`,
    },
    true
  )};
`;

const titleClass = css`
  ${fancyText};
  ${getResponsiveFont(getResponsiveDesktopFontSize(84.5), '14px')};
  position: relative;

  span {
    ${dropdownAnimation};
    display: inline-block;
    position: relative;
  }
`;

const titleShadeClass = css`
  position: absolute;
  top: 0;
  left: 35px;
  right: 35px;
  bottom: 0;
  transform: rotate(-7deg);
  background: rgba(255, 132, 92, 0.87);
  border-radius: 20px;
  max-width: 350px;
  margin: 0 auto;

  ${animations.includeAnimation(
    animations.animations.slideInDownAngle,
    {
      duration: '750ms',
      delay: `${baseAnimationDelay + 50}ms`,
      ease: 'cubic-bezier(0.21, 1.55, 0.73, 1.55)',
    },
    true
  )};
`;

const titleInnerClass = css`
  transition: transform 300ms ease;

  ${minimisedState} {
    transform: scale(0.75);
  }
`;

const middleClass = css`
  display: flex;
  align-items: center;
  margin: 30px 0;
`;

const phoneClass = css`
  width: 338px;
  height: 422px;
  background: rgba(86, 154, 185, 0.4);
  border-radius: 20px;
  margin: 0 140px;

  ${animations.includeAnimation(
    animations.animations.fadeIn,
    { delay: `${extraAnimationDelay}ms`, duration: '1000ms' },
    true
  )};
`;

const instructionSlideInRightClass = css`
  ${animations.includeAnimation(
    animations.animations.slideInRight,
    { delay: `${extraAnimationDelay + 1500}ms`, duration: '500ms' },
    true
  )};
`;
const instructionSlideInLeftClass = css`
  ${animations.includeAnimation(
    animations.animations.slideInLeft,
    { delay: `${extraAnimationDelay + 500}ms`, duration: '500ms' },
    true
  )};
`;

const instructionsClass = css`
  ${displayInstructionText};
  width: 400px;
`;

const instructionsRightAlignClass = css`
  text-align: right;
`;

const instructionLargeClass = css`
  ${displayInstructionLargeText};
`;

const instructionSmallClass = css`
  ${displayInstructionSmallText};
`;

const bottomClass = css``;

const phoneTextClass = css`
  ${displayHelpText};
  ${getResponsiveFont(getResponsiveDesktopFontSize(48), '14px')};
  position: relative;

  &::before {
    content: '';
    position: absolute;
    top: -5px;
    left: 30px;
    right: 30px;
    bottom: -5px;
    transform: rotate(3deg);
    background: rgba(34, 181, 242, 0.42);
    border-radius: 10px;
  }

  span {
    position: relative;
    opacity: 0.62;
  }

  ${animations.includeAnimation(
    animations.animations.slideInUp,
    { delay: `${extraAnimationDelay + 2500}ms`, duration: '500ms' },
    true
  )};
`;

export default {
  containerClass,
  topClass,
  taglineClass,
  titleClass,
  titleShadeClass,
  titleInnerClass,
  middleClass,
  phoneClass,
  instructionsClass,
  instructionSlideInRightClass,
  instructionSlideInLeftClass,
  instructionsRightAlignClass,
  instructionLargeClass,
  instructionSmallClass,
  bottomClass,
  phoneTextClass,
};
