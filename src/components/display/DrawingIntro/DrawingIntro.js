// @flow
import React, {
  PureComponent } from 'react';
import { Flipper, Flipped } from 'react-flip-toolkit';
import posed from 'react-pose';
import { cx } from 'emotion';
import styles from './styles';
import utils from '../../../styles/utils';

const PhonePrompt = posed.div({
  visible: {
    opacity: 1,
    y: 0,
  },
  hidden: {
    opacity: 0,
    y: 50,
  },
});

type Props = {
  minimised: boolean,
  showPhonePrompt: boolean,
  message: string,
};

type State = {};

class DrawingIntro extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {};
  }

  renderMiddle() {
    return (
      <Flipped flipId="middle">
        <div className={styles.middleClass}>
          <div
            className={cx(
              styles.instructionsClass,
              styles.instructionsRightAlignClass,
              styles.instructionSlideInLeftClass
            )}
          >
            <p>
              a <span className={styles.instructionLargeClass}>prompt</span> <br />
              <span className={styles.instructionSmallClass}>has been sent to</span> <br />
              your <span className={styles.instructionLargeClass}>phone</span>
            </p>
          </div>
          <div className={styles.phoneClass} />
          <div className={cx(styles.instructionsClass, styles.instructionSlideInRightClass)}>
            <p>
              <span className={styles.instructionLargeClass}>draw it</span> <br />
              as best you can
            </p>
          </div>
        </div>
      </Flipped>
    );
  }

  render() {
    const { message, minimised, showPhonePrompt } = this.props;
    return (
      <Flipper
        className={cx(styles.containerClass, {
          [utils.classNames.drawingIntroMinimised]: minimised,
        })}
        flipKey={minimised ? 'minimised' : 'maximised'}
      >
        <Flipped flipId="top">
          <div className={styles.topClass}>
            <div className={styles.taglineClass}>
              <span>stage 1</span>
            </div>
            <div className={styles.titleClass}>
              <div className={styles.titleShadeClass} />
              <div className={styles.titleInnerClass}>
                <span>{message}</span>
              </div>
            </div>
          </div>
        </Flipped>
        {!minimised && this.renderMiddle()}
        <Flipped flipId="bottom">
          <div className={styles.bottomClass}>
            <PhonePrompt pose={showPhonePrompt ? 'visible' : 'hidden'}>
              <p className={styles.phoneTextClass}>
                <span>check your phone</span>
              </p>
            </PhonePrompt>
          </div>
        </Flipped>
      </Flipper>
    );
  }
}

export default DrawingIntro;
