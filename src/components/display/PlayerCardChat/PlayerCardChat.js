// @flow
import React from 'react';
import type { DisplaySessionContextState } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { getUserChatDrawing } from '../../../state/context/DisplaySessionContext/state';
import { withDisplaySessionContext } from '../../../state/context/DisplaySessionContext/DisplaySessionContext';
import { decompressDrawing } from '../../../utils/drawing';
import DrawingDisplay from '../DrawingDisplay/DrawingDisplay';

const mapStateToProps = (state: DisplaySessionContextState, { userKey }: { userKey: string }) => {
  const userChatDrawing = getUserChatDrawing(state, userKey);
  const drawingKey = userChatDrawing ? userChatDrawing.key : null;
  const stringDrawingData = userChatDrawing ? userChatDrawing.drawing : null;
  let drawing = null;
  if (stringDrawingData) {
    drawing = decompressDrawing(stringDrawingData);
  }
  return {
    drawingKey,
    drawing,
  };
};

export default withDisplaySessionContext(mapStateToProps)(DrawingDisplay);
