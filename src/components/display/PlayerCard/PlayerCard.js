// @flow
import React, { Component, PureComponent } from 'react';
import { cx } from 'emotion';
import styles from './styles';
import PlayerCardChat from '../PlayerCardChat/PlayerCardChat';
import { getPlayerJoinedSound } from '../../../audio/soundEffects/sounds';
import { audioSoundEffectsGameHandler } from '../../../audio/soundEffects/AudioSoundEffectsGameHandler';

type Props = {
  joinedTimestamp: number,
  disabled: boolean,
  name: string,
  userKey: string,
};

class PlayerCard extends PureComponent<Props> {
  render() {
    const { name, disabled, userKey } = this.props;
    return (
      <div className={styles.cardClass}>
        <div
          className={cx(styles.containerClass, {
            [styles.disabledStateClass]: disabled,
          })}
        >
          <div className={styles.titleClass}>{name}</div>
          <div className={styles.mainContainerClass}>
            <PlayerCardChat userKey={userKey} />
          </div>
        </div>
      </div>
    );
  }
}

export default PlayerCard;
