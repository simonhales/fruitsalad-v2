import { css } from 'emotion';
import utils from 'styles/utils/index';
import { getResponsiveDesktopFontSize, getResponsiveFont } from '../../../styles/typography';

const cardClass = css`
  width: 100%;
  height: 100%;
`;

export const cardBorderWidth = 14;

const containerClass = css`
  ${utils.transition.transition(['transform', 'opacity'])};
  width: 100%;
  height: 100%;
  background: #dcf5ff;
  border: ${cardBorderWidth}px solid #ffffff;
  box-shadow: 5px 2px 4px 0 rgba(45, 95, 116, 0.18), 5px 12px 21px 0 rgba(66, 68, 92, 0.09);
  border-radius: 20px;
  display: flex;
  flex-direction: column;
  overflow: hidden;
`;

const disabledStateClass = css`
  transform: rotate(7deg) translateY(10px);
  transform-origin: center center;
  opacity: 0.7;
`;

const titleClass = css`
  ${getResponsiveFont(getResponsiveDesktopFontSize(34.5), '20px')};
  ${utils.fonts.families.displayFancy};
  background-color: #ffffff;
  color: #5693ad;
  text-align: center;
  text-transform: uppercase;
  padding: 2px 0 5px 0;
`;

const mainContainerClass = css`
  flex: 1;
`;

export default {
  cardClass,
  containerClass,
  disabledStateClass,
  titleClass,
  mainContainerClass,
};
