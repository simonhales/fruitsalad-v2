// @flow
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
import posed from 'react-pose';
import styles from './styles';
import PlayerCard from '../PlayerCard/PlayerCard';

type Props = {
  children: React.Node,
};

const POSES = {
  enter: {
    y: 0,
    scale: 1,
    opacity: 1,
    delay: 300,
  },
  exit: {
    y: 50,
    scale: 0.5,
    opacity: 0,
    transition: { ease: 'anticipate', duration: 300 },
  },
};

const Card = posed.div(POSES);

class PosedPlayerCard extends Component<Props> {
  render() {
    const { children, ...otherProps } = this.props;
    return (
      <Card className={styles.cardClass} {...otherProps}>
        {children}
      </Card>
    );
  }
}

export default PosedPlayerCard;
