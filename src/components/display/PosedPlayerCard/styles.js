import { css } from 'emotion';
import utils from 'styles/utils/index';
import { getResponsiveDesktopFontSize, getResponsiveFont } from '../../../styles/typography';

const cardClass = css`
  width: 100%;
  height: 100%;
`;

export default {
  cardClass,
};
