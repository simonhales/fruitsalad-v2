// @flow
import * as React from 'react';
import { cx } from 'emotion';
import styles from './styles';

type Props = {
  children: React.Node,
  className?: string,
};

const DisplayViewTransitions = ({ children, className }: Props) => (
  <div className={cx(styles.containerClass, className)}>{children}</div>
);

DisplayViewTransitions.defaultProps = {
  className: '',
};

export default DisplayViewTransitions;
