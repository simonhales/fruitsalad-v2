// @flow

import { css } from 'emotion';
import utils from '../../../styles/utils';

const containerClass = css`
  transform-origin: bottom left;

  &.${utils.classNames.displayStateViewTransitioning}-exit {
    opacity: 1;
    transform: rotate(0);
  }

  &.${utils.classNames.displayStateViewTransitioning}-exit.${utils.classNames
      .displayStateViewTransitioning}-exit-active {
    transition: transform 500ms ease, opacity 300ms 200ms ease;
    opacity: 0;
    transform: rotate(85deg);
  }

  &.${utils.classNames.displayStateViewTransitioning}-enter {
    opacity: 0;
    transform: rotate(-85deg);
  }

  &.${utils.classNames.displayStateViewTransitioning}-enter.${utils.classNames
      .displayStateViewTransitioning}-enter-active {
    transition: transform 500ms 300ms ease, opacity 500ms 300ms ease;
    opacity: 1;
    transform: rotate(0);
  }
`;

export default {
  containerClass,
};
