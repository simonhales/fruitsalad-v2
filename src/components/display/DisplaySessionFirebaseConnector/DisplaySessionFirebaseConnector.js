// @flow
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
import { firebaseConnect, isEmpty, isLoaded } from 'react-redux-firebase';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { getReduxSessionKey } from '../../../state/redux/firebase/state';
import type { ReduxState } from '../../../state/redux/shared/models';
import {
  getSessionWrapperFromState,
  getSessionWrapperFromStateRaw,
} from '../../../state/redux/rawFirebase/state';
import analytics from '../../../analytics';
import LoadingView from '../../../views/LoadingView/LoadingView';
import { getSessionPath } from '../../../firebase/bananart/session/refs';
import { getSessionChatPath } from '../../../firebase/bananart/chat/refs';

type Props = {
  loaded: boolean,
  invalid: boolean,
  sessionKey: string,
  children: React.Node,
};

class DisplaySessionFirebaseConnector extends Component<Props> {
  triggeredNotFoundAnalytics = false;

  renderNotFound() {
    const { sessionKey } = this.props;

    if (!this.triggeredNotFoundAnalytics) {
      this.triggeredNotFoundAnalytics = true;
      analytics.events.display.sessionNotFound(sessionKey);
    }

    return <div>NOT FOUND...</div>;
  }

  render() {
    const { loaded, invalid, children } = this.props;

    if (!loaded) {
      return <LoadingView />;
    }

    if (loaded && invalid) {
      return this.renderNotFound();
    }

    return children;
  }
}

const mapStateToProps = (state: ReduxState) => {
  const rawState = getSessionWrapperFromStateRaw(state.rawFirebase);
  const loaded = isLoaded(rawState);
  const empty = isEmpty(rawState);
  const invalid = loaded && empty;
  const sessionWrapper = getSessionWrapperFromState(state.rawFirebase);
  const sessionKey = getReduxSessionKey(state.firebase);
  return {
    loaded,
    invalid,
    sessionWrapper,
    sessionKey,
  };
};

export default firebaseConnect((props, store) => {
  const state: ReduxState = store.getState();
  const sessionKey = getReduxSessionKey(state.firebase);
  const queries = [
    {
      path: getSessionPath(sessionKey),
      storeAs: 'sessionWrapper',
    },
    {
      path: getSessionChatPath(sessionKey),
      storeAs: 'sessionChat',
    },
  ];
  return queries;
})(connect(mapStateToProps)(withRouter(DisplaySessionFirebaseConnector)));
