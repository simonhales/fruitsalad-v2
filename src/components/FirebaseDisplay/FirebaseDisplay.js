// @flow
import React from 'react';
import AuthWrapper from '../AuthWrapper/AuthWrapper';
import SetSessionKey from '../SetSessionKey/SetSessionKey';
import DisplayView from '../../views/display/DisplayView/DisplayView';
import DisplaySessionFirebaseConnector from '../display/DisplaySessionFirebaseConnector/DisplaySessionFirebaseConnector';

const FirebaseDisplay = () => (
  <SetSessionKey>
    <AuthWrapper>
      <DisplaySessionFirebaseConnector>
        <DisplayView />
      </DisplaySessionFirebaseConnector>
    </AuthWrapper>
  </SetSessionKey>
);

export default FirebaseDisplay;
