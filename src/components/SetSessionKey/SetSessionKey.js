// @flow
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { setSessionKey as reduxSetSessionKey } from '../../state/redux/firebase/reducer';

type Props = {
  children: React.Node,
  match: {
    params: {
      id: string,
    },
  },
  setSessionKey: (sessionKey: string) => void,
};

class SetSessionKey extends Component<Props> {
  constructor(props: Props) {
    super(props);
    const { match, setSessionKey } = props;
    if (!match.params.id) {
      throw new Error('no session key in params found');
      // todo handle better
    }
    setSessionKey(match.params.id);
  }

  render() {
    const { children } = this.props;
    return children;
  }
}

const mapDispatchToProps = dispatch => ({
  setSessionKey: (sessionKey: string) => dispatch(reduxSetSessionKey(sessionKey)),
});

export default withRouter(
  connect(
    null,
    mapDispatchToProps
  )(SetSessionKey)
);
