// @flow
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
import { connect } from 'react-redux';
import type { ReduxState } from '../../state/redux/shared/models';
import { getSessionFromState } from '../../state/redux/rawFirebase/state';
import type { SessionModel } from '../../firebase/bananart/session/models';
import { getCurrentUserKey } from '../../state/redux/firebase/state';
import { hasUserJoinedSession } from '../../firebase/bananart/session/state';
import ControllerJoinView from '../../views/controller/ControllerJoinView/ControllerJoinView';

type Props = {
  children: React.Node,
  currentUserKey: string,
  session: SessionModel | null,
};

class JoinSessionWrapper extends Component<Props> {
  render() {
    const { children, currentUserKey, session } = this.props;
    if (!session) return null;
    if (hasUserJoinedSession(currentUserKey, session)) {
      return children;
    }
    return <ControllerJoinView />;
  }
}

const mapStateToProps = (state: ReduxState) => ({
  session: getSessionFromState(state.rawFirebase),
  currentUserKey: getCurrentUserKey(state.firebase),
});

export default connect(mapStateToProps)(JoinSessionWrapper);
