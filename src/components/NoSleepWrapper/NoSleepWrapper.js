// @flow
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
import NoSleep from 'nosleep.js';
import { controllerLog } from '../../utils/logging';

const noSleep = new NoSleep();

type Props = {
  children: React.Node,
};

class NoSleepWrapper extends Component<Props> {
  componentDidMount() {
    window.addEventListener('click', this.handleEnableNoSleep);
    window.addEventListener('touchstart', this.handleEnableNoSleep);
    window.addEventListener('touchmove', this.handleEnableNoSleep);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.handleEnableNoSleep);
    window.removeEventListener('touchstart', this.handleEnableNoSleep);
    window.removeEventListener('touchmove', this.handleEnableNoSleep);
  }

  handleEnableNoSleep = () => {
    noSleep.enable();
    controllerLog.log('handleEnableNoSleep:: No Sleep Enabled');
    window.removeEventListener('click', this.handleEnableNoSleep);
    window.removeEventListener('touchstart', this.handleEnableNoSleep);
    window.removeEventListener('touchmove', this.handleEnableNoSleep);
  };

  render() {
    const { children } = this.props;
    return children;
  }
}

export default NoSleepWrapper;
