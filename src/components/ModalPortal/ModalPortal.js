// @flow
import { createPortal } from 'react-dom';
import * as React from 'react';
import { DomError } from '../../errors';

const modalDomNode = document.getElementById('modal-root');

if (!modalDomNode) {
  throw new DomError({
    code: 'INVALID_MODAL_DOM_NODE',
    message: 'modalDomNode is invalid',
  });
}

type Props = {
  children: React.Node,
};

const ModalPortal = ({ children }: Props) => createPortal(children, modalDomNode);

export default ModalPortal;
