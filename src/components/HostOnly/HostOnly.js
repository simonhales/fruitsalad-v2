// @flow
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
import { generateClientKey } from '../../firebase/utils';
import { getSessionDisplayHostRef } from '../../firebase/bananart/session/refs';
import { setSessionDisplayHost } from '../../firebase/bananart/session/actions';
import type { DisplaySessionContextState } from '../../state/context/DisplaySessionContext/DisplaySessionContext';
import {
  getSessionDisplayHost,
  getSessionDisplayHostLastActive,
  getSessionKey,
} from '../../firebase/bananart/session/state';
import { withDisplaySessionContext } from '../../state/context/DisplaySessionContext/DisplaySessionContext';
import { hostLog } from '../../utils/logging';

type Props = {
  children: React.Node,
  sessionDisplayHost: string | null,
  sessionDisplayHostLastActive: number,
  sessionKey: string,
};

class HostOnly extends Component<Props> {
  clientKey: string;

  sessionDisplayHostRef: any;

  constructor(props: Props) {
    super(props);
    this.clientKey = generateClientKey();
  }

  componentDidMount() {
    hostLog.log(`HostOnly mounted.`);
    const { sessionDisplayHost, sessionDisplayHostLastActive } = this.props;
    const hostTimeoutDuration = 15 * 10000;
    if (!sessionDisplayHost) {
      this.setAsSessionDisplayHost();
    } else if (sessionDisplayHostLastActive < Date.now() - hostTimeoutDuration) {
      hostLog.log(`The current host is inactive, so taking over.`);
      this.setAsSessionDisplayHost();
    } else {
      hostLog.log(`There is already a host and it's active.`);
    }
  }

  componentWillUnmount() {
    hostLog.log(`HostOnly unmounting.`);
    // not sure how necessary this is
    this.sessionDisplayHostRef.onDisconnect().cancel();
  }

  setAsSessionDisplayHost() {
    hostLog.log(`Setting as session host.`);
    const { sessionKey } = this.props;
    this.sessionDisplayHostRef = getSessionDisplayHostRef(sessionKey);
    setSessionDisplayHost(this.clientKey, sessionKey);
    this.sessionDisplayHostRef.onDisconnect().set(null);
  }

  isSessionDisplayHost() {
    const { sessionDisplayHost } = this.props;
    return sessionDisplayHost && sessionDisplayHost === this.clientKey;
  }

  render() {
    if (!this.isSessionDisplayHost()) return null;
    const { children } = this.props;
    return children;
  }
}

const mapStateToProps = (state: DisplaySessionContextState) => {
  const { session } = state;
  if (!session) {
    throw new Error(`Session is invalid in HostOnly context.`);
  }
  const sessionDisplayHost = getSessionDisplayHost(session);
  const sessionKey = getSessionKey(session);
  const sessionDisplayHostLastActive = getSessionDisplayHostLastActive(session);
  return {
    sessionDisplayHost,
    sessionDisplayHostLastActive,
    sessionKey,
  };
};

export default withDisplaySessionContext(mapStateToProps)(HostOnly);
