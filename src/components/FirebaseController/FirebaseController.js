// @flow
import React from 'react';
import AuthWrapper from '../AuthWrapper/AuthWrapper';
import ControllerSessionFirebaseConnector from '../ControllerSessionFirebaseConnector/ControllerSessionFirebaseConnector';
import SetSessionKey from '../SetSessionKey/SetSessionKey';
import JoinSessionWrapper from '../JoinSessionWrapper/JoinSessionWrapper';
import ControllerView from '../../views/controller/ControllerView/ControllerView';

const FirebaseController = () => (
  <SetSessionKey>
    <AuthWrapper>
      <ControllerSessionFirebaseConnector>
        <JoinSessionWrapper>
          <ControllerView />
        </JoinSessionWrapper>
      </ControllerSessionFirebaseConnector>
    </AuthWrapper>
  </SetSessionKey>
);

export default FirebaseController;
