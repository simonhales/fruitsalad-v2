// @flow
import React from 'react';
import { cx } from 'emotion';
import type { EntryAnswerModel } from '../../../firebase/bananart/game/entry/models';
import styles from './styles';

type AnswerProps = {
  answer: EntryAnswerModel,
  isUserAnswer: boolean,
  selectAnswer: (answerKey: string) => void,
};

const Answer = ({ answer, isUserAnswer, selectAnswer }: AnswerProps) => (
  <div
    className={cx(styles.answerClass, {
      [styles.yourAnswerClass]: isUserAnswer,
    })}
    onClick={() => selectAnswer(answer.key)}
  >
    {isUserAnswer && <div className={styles.yourAnswerMessageClass}>your answer</div>}
    <span>{answer.text}</span>
  </div>
);

type Props = {
  answers: Array<EntryAnswerModel>,
  selectAnswer: (answerKey: string) => void,
  userAnswerKey: string | null,
};

const AnswersList = ({ answers, selectAnswer, userAnswerKey }: Props) => (
  <div className={styles.containerClass}>
    {answers.map((answer: EntryAnswerModel) => (
      <Answer
        answer={answer}
        selectAnswer={selectAnswer}
        isUserAnswer={answer.key === userAnswerKey}
        key={answer.key}
      />
    ))}
  </div>
);

export default AnswersList;
