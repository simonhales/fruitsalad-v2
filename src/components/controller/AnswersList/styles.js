// @flow

import { css } from 'emotion';
import utils from '../../../styles/utils';

const containerClass = css`
  padding-top: 5px;
  height: 100%;
  overflow-y: auto;
`;

const answerClass = css`
  ${utils.fonts.families.display};
  margin-top: 10px;
  background-color: ${utils.colors.white};
  border: 3px solid ${utils.colors.lightBackgroundDarker};
  padding: 8px;
  font-size: 22px;
  color: ${utils.colors.controllerText};
  letter-spacing: 0.81px;
  text-align: center;
  position: relative;
  text-transform: lowercase;
`;

const yourAnswerClass = css`
  position: relative;
  opacity: 0.5;

  span {
    position: relative;
    top: 4px;
  }
`;

const yourAnswerMessageClass = css`
  position: absolute;
  top: 2px;
  left: 0;
  right: 0;
  text-align: center;
  font-size: 12px;
`;

export default {
  containerClass,
  answerClass,
  yourAnswerClass,
  yourAnswerMessageClass,
};
