import { DUMMY_SESSION_USER } from '../../../data/dummy/session';
import { getUserStatus } from './ControllerPlayersList';

describe('ControllerPlayersList:: getUserStatus', () => {
  let user;

  beforeEach(() => {
    user = {
      ...DUMMY_SESSION_USER,
    };
  });

  test('getUserStatus offline status', () => {
    user.online = false;
    const isHost = false;
    const status = getUserStatus(user, isHost);
    expect(status).toEqual('offline');
  });
  test('getUserStatus online status', () => {
    user.online = true;
    const isHost = false;
    const status = getUserStatus(user, isHost);
    expect(status).toEqual('');
  });
  test('getUserStatus offline host status', () => {
    user.online = false;
    const isHost = true;
    const status = getUserStatus(user, isHost);
    expect(status).toEqual('host,offline');
  });
  test('getUserStatus online host status', () => {
    user.online = true;
    const isHost = true;
    const status = getUserStatus(user, isHost);
    expect(status).toEqual('host');
  });
});
