import { css } from 'emotion';
import { transparentize } from 'polished';
import styled from 'react-emotion';
import { mediumFont } from '../../../styles/typography';
import utils from '../../../styles/utils/index';

const containerClass = css`
  padding-bottom: 50px;
`;

const faintColor = transparentize(0.75, utils.colors.lightBackgroundDarker);

const playerItemClass = css`
  ${mediumFont};
  color: ${utils.colors.controllerText};
  font-weight: ${utils.weights.bold};
  position: relative;
  margin-top: 15px;

  &.${utils.classNames.controllerPlayersListPlayerItemDisabled} {
    opacity: 0.5;
  }

  &:active {
    background-color: ${faintColor};
  }

  ${utils.breakpoints.desktop} {
    text-align: center;
  }
`;

const playerItemNameClass = css`
  display: block;

  &::after {
    ${utils.fonts.families.chewy};
    font-size: 12px;
    position: absolute;
    top: 100%;
    left: 2px;
    right: 0;
    margin-top: -4px;
  }
`;

function getNameStatus(props) {
  if (!props.status) return '';
  return `
    &::after
    {
        content: '(${props.status})';
    }
  `;
}

export const PlayerItemName = styled('span')`
  ${props => getNameStatus(props)};
`;

export default {
  containerClass,
  playerItemClass,
  playerItemNameClass,
};
