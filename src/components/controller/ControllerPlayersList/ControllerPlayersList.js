// @flow
import React from 'react';
import { cx } from 'emotion';
import utils from 'styles/utils/index';
import styles, { PlayerItemName } from './styles';
import type { SessionUserModel } from '../../../firebase/bananart/session/user/models';

type Player = {
  userKey: string,
  name: string,
  status: string,
  disabled: boolean,
};

export function getUserStatus(user: SessionUserModel, isHost: boolean): string {
  const statuses = [];
  if (isHost) {
    statuses.push('host');
  }
  if (!user.online) {
    statuses.push('offline');
  }
  return statuses.join(',');
}

export function mapSessionUsersToPlayersList(
  users: Array<SessionUserModel>,
  sessionHost: string
): Array<Player> {
  return users.map(user => ({
    userKey: user.key,
    name: user.name,
    status: getUserStatus(user, sessionHost === user.key),
    disabled: !user.online,
  }));
}

type PlayerProps = {
  userKey: string,
  name: string,
  status: string,
  disabled: boolean,
  onClick: (userKey: string) => void,
};

const PlayerItem = ({ name, status, disabled, userKey, onClick }: PlayerProps) => (
  <div
    className={cx(styles.playerItemClass, {
      [utils.classNames.controllerPlayersListPlayerItemDisabled]: disabled,
      [utils.classNames.controllerPlayersListPlayerItemLabel]: !!status,
    })}
    onClick={() => {
      onClick(userKey);
    }}
  >
    <PlayerItemName className={styles.playerItemNameClass} status={status}>
      {name}
    </PlayerItemName>
  </div>
);

type Props = {
  players: Array<Player>,
  onClick: (userKey: string) => void,
};

const ControllerPlayersList = ({ players, onClick }: Props) => (
  <div className={styles.containerClass}>
    {players.map(player => (
      <PlayerItem {...player} key={player.userKey} onClick={onClick} />
    ))}
  </div>
);

export default ControllerPlayersList;
