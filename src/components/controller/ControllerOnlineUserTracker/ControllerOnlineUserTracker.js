// @flow
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
import { getSessionUserRef } from '../../../firebase/bananart/session/refs';
import type { ControllerSessionContextState } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import { getValidSessionKey } from '../../../firebase/bananart/session/state';
import { withControllerSessionContext } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import { getContextCurrentUserKey } from '../../../state/context/ControllerSessionContext/state';
import { isSessionUserOffline } from '../../../firebase/bananart/session/user/state';

type Props = {
  children: React.Node,
  userKey: string,
  sessionKey: string,
  userIsOffline: boolean,
};

class ControllerOnlineUserTracker extends Component<Props> {
  sessionUserRef;

  componentDidMount() {
    const { userKey, sessionKey } = this.props;
    this.sessionUserRef = getSessionUserRef(sessionKey, userKey);
    this.sessionUserRef.update({
      online: true,
    });
    this.addListeners();
  }

  addListeners() {
    this.sessionUserRef.onDisconnect().update({
      online: false,
    });
  }

  componentDidUpdate(oldProps: Props) {
    const { userIsOffline } = this.props;
    if (!userIsOffline && oldProps.userIsOffline) {
      this.addListeners();
    }
  }

  componentWillUnmount() {
    this.sessionUserRef.onDisconnect().cancel();
  }

  render() {
    const { children } = this.props;
    return children;
  }
}

const mapContextStateToProps = (state: ControllerSessionContextState) => {
  const { session } = state;
  const userKey = getContextCurrentUserKey(state);
  const sessionKey = getValidSessionKey(session);
  const userIsOffline = session ? isSessionUserOffline(session, userKey) : false;
  return {
    userKey,
    sessionKey,
    userIsOffline,
  };
};

export default withControllerSessionContext(mapContextStateToProps)(ControllerOnlineUserTracker);
