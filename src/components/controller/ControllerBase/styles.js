import { css } from 'emotion';
import { transparentize } from 'polished';
import utils from '../../../styles/utils/index';
import { smallButton } from '../../../styles/buttons';
import assets from '../../../utils/assets';
import { tiledBackgroundImage } from '../../../styles/images';

export const controllerUnderlineHeight = 4;

const containerClass = css`
  ${utils.layout.fixedFullView};
  background-color: ${utils.colors.controllerBackground};
  ${tiledBackgroundImage(assets.patterns.fruitLightPattern)};
`;

const navClass = css`
  position: absolute;
  top: 12px;
  left: 0;
  right: 0;
  z-index: ${utils.zindexes.controllerNav};
  display: flex;
  justify-content: flex-end;

  &.${utils.classNames.controllerNavChatSelected} {
    justify-content: flex-start;
  }
`;

const navButtonClass = css`
  ${smallButton};
  width: 62px;
  transition: transform 300ms ease, opacity 300ms ease;

  .${utils.classNames.controllerNavChatDisabled}.${utils.classNames.controllerNavPlaySelected} & {
    transform: translateX(calc(100% - 6px));
    pointer-events: none;
    opacity: 0.5;
  }
`;

const bodyClass = css`
  width: 100%;
  height: 100%;
  position: relative;
  overflow: hidden;
`;

const moveTransition = css`
  ${utils.transition.transition(['transform', 'opacity'])};
`;

const playContainerClass = css`
  ${moveTransition};
  flex: 1;
  height: 100%;

  .${utils.classNames.controllerNavChatSelected} & {
    transform: translateX(-100%);
    pointer-events: none;
  }
`;

const chatContainerClass = css`
  ${moveTransition};
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  transform: translateX(100%);
  pointer-events: none;
  overflow: hidden;

  .${utils.classNames.controllerNavChatSelected} & {
    transform: translateX(0);
    pointer-events: auto;
  }
`;

export default {
  containerClass,
  navClass,
  navButtonClass,
  bodyClass,
  playContainerClass,
  chatContainerClass,
};
