// @flow
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
import { cx } from 'emotion';
import { Flipper, Flipped } from 'react-flip-toolkit';
import styles from './styles';
import utils from '../../../styles/utils/index';
import { roundStates } from '../../../firebase/bananart/game/round/models';
import analytics from '../../../analytics';
import ToastsWrapper from '../../ToastsWrapper/ToastsWrapper';
import ControllerChatView from '../../../views/controller/ControllerChatView/ControllerChatView';
import NoSleepWrapper from '../../NoSleepWrapper/NoSleepWrapper';

const ControllerViewTabs = {
  play: 'play',
  chat: 'chat',
};

const defaultTab = ControllerViewTabs.play;

export const ControllerContext = React.createContext({
  currentTab: defaultTab,
  goToChat: () => {},
  goToPlay: () => {},
});

type Props = {
  children: React.Node,
  chatDisabled: boolean,
};

type State = {
  currentTab: $Keys<typeof ControllerViewTabs>,
  goToChat: () => void,
  goToPlay: () => void,
};

class ControllerBase extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      currentTab: defaultTab,
      goToChat: this.goToChat,
      goToPlay: this.goToPlay,
    };
  }

  changeTab = (tab: $Keys<typeof ControllerViewTabs>) => {
    this.setState({
      currentTab: tab,
    });
  };

  goToPlay = () => {
    this.changeTab(ControllerViewTabs.play);
  };

  goToChat = () => {
    this.changeTab(ControllerViewTabs.chat);
  };

  handlePlayTabClicked = () => {
    analytics.events.controller.playTabClicked();
    this.goToPlay();
  };

  handleChatTabClicked = () => {
    analytics.events.controller.chatTabClicked();
    this.goToChat();
  };

  handleNavClicked = () => {
    const { currentTab } = this.state;
    if (currentTab === ControllerViewTabs.play) {
      const { chatDisabled } = this.props;
      if (chatDisabled) return;
      this.goToChat();
    } else {
      this.goToPlay();
    }
  };

  renderNav() {
    const { currentTab } = this.state;
    const { chatDisabled } = this.props;
    const playIsActive = currentTab === ControllerViewTabs.play;
    const chatIsActive = currentTab === ControllerViewTabs.chat;
    return (
      <nav
        className={cx(styles.navClass, {
          [utils.classNames.controllerNavPlaySelected]: playIsActive,
          [utils.classNames.controllerNavChatSelected]: chatIsActive,
          [utils.classNames.controllerNavChatDisabled]: chatDisabled,
        })}
      >
        <Flipper flipKey={currentTab}>
          <Flipped flipId="ControllerBase.NavButton">
            <div>
              <div className={styles.navButtonClass} onClick={this.handleNavClicked}>
                {playIsActive ? 'CHAT' : 'PLAY'}
              </div>
            </div>
          </Flipped>
        </Flipper>
      </nav>
    );
  }

  render() {
    const { children, chatDisabled } = this.props;
    const { currentTab } = this.state;
    const playIsActive = currentTab === ControllerViewTabs.play;
    const chatIsActive = currentTab === ControllerViewTabs.chat;
    return (
      <NoSleepWrapper>
        <ControllerContext.Provider value={this.state}>
          <React.Fragment>
            <div
              className={cx(styles.containerClass, {
                [utils.classNames.controllerNavPlaySelected]: playIsActive,
                [utils.classNames.controllerNavChatSelected]: chatIsActive,
              })}
            >
              {this.renderNav()}
              <main className={styles.bodyClass}>
                <div className={styles.playContainerClass}>{children}</div>
                <div className={styles.chatContainerClass}>
                  <ControllerChatView chatDisabled={chatDisabled} />
                </div>
              </main>
            </div>
            <ToastsWrapper />
          </React.Fragment>
        </ControllerContext.Provider>
      </NoSleepWrapper>
    );
  }
}

export default ControllerBase;
