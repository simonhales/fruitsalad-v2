// @flow
import * as React from 'react';
import ControllerButton, { controllerButtonProps } from '../ControllerButton/ControllerButton';
import type { ControllerButtonProps } from '../ControllerButton/ControllerButton';

type Props = ControllerButtonProps & {
  children: React.Node,
};

const ControllerActionButton = ({ children, ...otherProps }: Props) => (
  <ControllerButton type={controllerButtonProps.types.fancy} {...otherProps}>
    {children}
  </ControllerButton>
);

export default ControllerActionButton;
