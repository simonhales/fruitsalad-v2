// @flow
import { css } from 'emotion';
import { darken, transparentize } from 'polished';
import utils from '../../../styles/utils/index';
import { mediumButtonFont, smallButtonFont } from '../../../styles/typography';
import { defaultEasing } from '../../../styles/transitions';
import { controllerUnderlineHeight } from '../ControllerBase/styles';

const defaultButton = css`
  cursor: pointer;
`;

const fullButton = css`
  ${defaultButton};
  ${mediumButtonFont};
  cursor: pointer;
  display: flex;
  height: 100%;
  text-align: center;
  align-items: center;
  justify-content: center;
  ${defaultEasing(['color', 'background', 'border-color'])};
  padding-bottom: ${controllerUnderlineHeight}px;

  &:focus {
    outline: none;
  }
`;

const plainButton = css`
  ${defaultButton};
  ${fullButton};
  border-top: ${controllerUnderlineHeight}px solid ${utils.colors.bgDarkened};
  color: ${utils.colors.white};

  &:focus {
    background-color: rgba(0, 0, 0, 0.05);
  }
`;

const vibrantButton = css`
  ${defaultButton};
  ${fullButton};

  &.${utils.classNames.controllerButtonDisabled} {
    ${plainButton};
    cursor: default;
  }

  &.${utils.classNames.controllerButtonEnabled} {
    background-color: ${utils.colors.vibrant};
    color: ${utils.colors.white};
    padding-top: ${controllerUnderlineHeight}px;

    &:focus {
      background-color: ${darken(0.05, utils.colors.vibrant)};
    }
  }
`;

const fancyButton = css`
  ${defaultButton};
  ${mediumButtonFont};
  ${utils.transition.transition(['opacity'])};
  color: #ffffff;
  text-align: center;
  position: relative;

  .${utils.classNames.controllerButtonInner} {
    background-color: ${utils.colors.vibrant};
    position: relative;
    padding: 15px 15px 8px 15px;
    text-shadow: 0 1px 2px rgba(128, 77, 124, 0.24), 1px 1px rgba(128, 32, 103, 0.51);
  }

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: ${transparentize(0.05, utils.colors.vibrantShadow)};
    transform: translate(-10px, 5px);
  }

  &.${utils.classNames.controllerButtonTypeFancyShadowShift} {
    &::before {
      transform: translate(5px, 5px);
    }
  }

  &.${utils.classNames.controllerButtonDisabled} {
    opacity: 0.35;
    cursor: default;
  }
`;

const softButton = css`
  ${smallButtonFont};
  background-color: #ffffff;
  border: 3px solid ${utils.colors.vibrantShadow};
  color: ${utils.colors.vibrantShadow};
  text-align: center;
  padding: 15px 20px 10px 20px;
`;

const buttonClass = css`
  &.${utils.classNames.controllerButtonTypePlain} {
    ${plainButton};
  }
  &.${utils.classNames.controllerButtonTypeVibrant} {
    ${vibrantButton};
  }
  &.${utils.classNames.controllerButtonTypeVibrantSmaller} {
    ${vibrantButton};
    ${smallButtonFont};
  }
  &.${utils.classNames.controllerButtonTypeFancy} {
    ${fancyButton};
  }
  &.${utils.classNames.controllerButtonTypeSoft} {
    ${softButton};
  }
`;

export default {
  buttonClass,
};
