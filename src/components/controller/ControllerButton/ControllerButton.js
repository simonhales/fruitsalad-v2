// @flow
import * as React from 'react';
import { cx } from 'emotion';
import utils from '../../../styles/utils/index';
import styles from './styles';

export type ControllerButtonProps = {
  children: React.Node,
  className?: string,
  disabled?: boolean,
  type?: string,
  onClick: () => void,
};

export const controllerButtonProps = {
  types: {
    plain: utils.classNames.controllerButtonTypePlain,
    soft: utils.classNames.controllerButtonTypeSoft,
    fancy: utils.classNames.controllerButtonTypeFancy,
    vibrant: utils.classNames.controllerButtonTypeVibrant,
    vibrantSmaller: utils.classNames.controllerButtonTypeVibrantSmaller,
  },
};

const ControllerButton = ({
  children,
  className,
  disabled,
  onClick,
  type,
}: ControllerButtonProps) => (
  <div
    role="button"
    tabIndex={0}
    className={cx(
      styles.buttonClass,
      type,
      {
        [utils.classNames.controllerButtonEnabled]: !disabled,
        [utils.classNames.controllerButtonDisabled]: disabled,
      },
      className
    )}
    onClick={onClick}
    onKeyDown={onClick}
  >
    <div className={utils.classNames.controllerButtonInner}>{children}</div>
  </div>
);

ControllerButton.defaultProps = {
  className: '',
  disabled: false,
  type: controllerButtonProps.types.plain,
};

export default ControllerButton;
