// @flow
import React, { Component } from 'react';
import posed from 'react-pose';
import styles from './styles';

const containerPoses = {
  completed: 'completed',
  rest: 'rest',
};

const Container = posed.div({
  [containerPoses.completed]: {
    scale: 0,
  },
  [containerPoses.rest]: {
    scale: 1,
  },
});

const circlePoses = {
  attention: 'attention',
  rest: 'reset',
};
const Circle = posed.div({
  [circlePoses.attention]: {
    scale: 1.05,
    transition: {
      type: 'spring',
      stiffness: 200,
      damping: 0,
    },
  },
  [circlePoses.rest]: {
    scale: 1,
    transition: {
      type: 'tween',
      stiffness: 50,
      damping: 0,
    },
  },
});

const textPoses = {
  attention: 'attention',
  rest: 'reset',
};

const Text = posed.div({
  [textPoses.attention]: {
    scale: 1.25,
  },
  [textPoses.rest]: {
    scale: 1,
  },
});

function calculateInitialCountdown(duration: number, resolvedTimestamp: number): number {
  let finalDuration = duration;
  const offset = resolvedTimestamp - Date.now();
  const calculatedDuration = duration - offset;
  if (calculatedDuration > 0 && offset > 0) {
    finalDuration = calculatedDuration;
  }
  return Math.round(finalDuration / 1000);
}

type Props = {
  active: boolean,
  duration: number,
  resolvedTimestamp?: number,
};

type State = {
  countdown: number,
  startedCountdown: boolean,
};

class ControllerCountdownTimer extends Component<Props, State> {
  static defaultProps = {
    resolvedTimestamp: 0,
  };

  countdownTimeout: TimeoutID;

  constructor(props: Props) {
    super(props);
    this.state = {
      countdown: props.duration / 1000,
      startedCountdown: false,
    };
  }

  componentDidMount() {
    this.attemptToStartCountdown();
  }

  componentDidUpdate() {
    this.attemptToStartCountdown();
  }

  componentWillUnmount() {
    clearTimeout(this.countdownTimeout);
  }

  attemptToStartCountdown() {
    const { startedCountdown } = this.state;
    if (startedCountdown) return;
    const { active, duration, resolvedTimestamp = 0 } = this.props;
    if (!active) return;
    this.setState(
      {
        countdown: calculateInitialCountdown(duration, resolvedTimestamp),
        startedCountdown: true,
      },
      this.countdown
    );
  }

  countdown = () => {
    const { countdown } = this.state;
    if (countdown === 0) return;
    this.setState({
      countdown: countdown - 1,
    });
    this.countdownTimeout = setTimeout(this.countdown, 1000);
  };

  isHidden() {
    const { countdown } = this.state;
    const { active } = this.props;
    return !active || countdown === 0;
  }

  isAttentionSeeking() {
    const { countdown } = this.state;
    return countdown < 10;
  }

  render() {
    const { countdown } = this.state;
    return (
      <Container
        className={styles.containerClass}
        pose={this.isHidden() ? containerPoses.completed : containerPoses.rest}
      >
        <Circle
          className={styles.circleClass}
          pose={this.isAttentionSeeking() ? circlePoses.attention : circlePoses.rest}
        />
        <Text
          className={styles.textClass}
          pose={this.isAttentionSeeking() ? textPoses.attention : textPoses.rest}
        >
          {countdown}
        </Text>
      </Container>
    );
  }
}

export default ControllerCountdownTimer;
