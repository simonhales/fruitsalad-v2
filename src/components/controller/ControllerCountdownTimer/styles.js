// @flow

import { css } from 'emotion';
import utils from '../../../styles/utils';

const containerClass = css`
  ${utils.fonts.families.displayFancy};
  width: 100%;
  height: 100%;
  color: ${utils.colors.white};
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 27.5px;
  letter-spacing: 1.01px;
  position: relative;
  text-align: center;
`;

const circleClass = css`
  background-color: ${utils.colors.orange};
  border-radius: 50%;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

const textClass = css`
  position: relative;
  top: 3px;
`;

export default {
  containerClass,
  circleClass,
  textClass,
};
