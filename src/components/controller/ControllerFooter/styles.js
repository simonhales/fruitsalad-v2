// @flow

import { css } from 'emotion';

const containerClass = css`
  margin-top: -10px;
  padding: 0 20px;
`;

export default {
  containerClass,
};
