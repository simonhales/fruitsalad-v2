// @flow
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
import styles from './styles';

type Props = {
  children: React.Node,
};

class ControllerFooter extends Component<Props> {
  render() {
    const { children } = this.props;
    return <footer className={styles.containerClass}>{children}</footer>;
  }
}

export default ControllerFooter;
