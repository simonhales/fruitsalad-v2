// @flow
import React, { Component } from 'react';
import Modal from '../../Modal/Modal';
import ControllerActionButton from '../ControllerActionButton/ControllerActionButton';
import styles from './styles';
import { getSessionUserRef } from '../../../firebase/bananart/session/refs';
import type { ControllerSessionContextState } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import { getContextCurrentUserKey } from '../../../state/context/ControllerSessionContext/state';
import { getValidSessionKey } from '../../../firebase/bananart/session/state';
import { withControllerSessionContext } from '../../../state/context/ControllerSessionContext/ControllerSessionContext';
import { isSessionUserOffline } from '../../../firebase/bananart/session/user/state';
import ModalPortal from '../../ModalPortal/ModalPortal';

type Props = {
  userKey: string,
  sessionKey: string,
};

class ControllerOfflineWarning extends Component<Props> {
  sessionUserRef;

  componentDidMount() {
    const { userKey, sessionKey } = this.props;
    this.sessionUserRef = getSessionUserRef(sessionKey, userKey);
  }

  handleClose = () => {
    this.sessionUserRef.update({
      online: true,
    });
  };

  render() {
    return (
      <ModalPortal>
      <Modal close={this.handleClose}>
        <div className={styles.containerClass} onClick={this.handleClose}>
          <div className={styles.labelClass}>you are offline</div>
          <div>
            <ControllerActionButton onClick={this.handleClose}>RECONNECT</ControllerActionButton>
          </div>
        </div>
      </Modal>
      </ModalPortal>
    );
  }
}

type WrapperProps = {
  userKey: string,
  sessionKey: string,
  userIsOffline: boolean,
};

const ControllerOfflineWarningWrapper = ({ userIsOffline, ...otherProps }: WrapperProps) => {
  if (!userIsOffline) return null;
  return <ControllerOfflineWarning {...otherProps} />;
};

const mapContextStateToProps = (state: ControllerSessionContextState) => {
  const { session } = state;
  const userKey = getContextCurrentUserKey(state);
  const sessionKey = getValidSessionKey(session);
  const userIsOffline = session ? isSessionUserOffline(session, userKey) : false;
  return {
    userKey,
    sessionKey,
    userIsOffline,
  };
};

export default withControllerSessionContext(mapContextStateToProps)(
  ControllerOfflineWarningWrapper
);
