// @flow
import utils from 'styles/utils';
import { css } from 'emotion';
import { mediumLabel } from '../../../styles/typography';

const containerClass = css`
  text-align: center;
  position: relative;
`;

const labelClass = css`
  ${mediumLabel};
  color: ${utils.colors.white};
  margin-bottom: ${utils.spacing.small}px;
`;

export default {
  containerClass,
  labelClass,
};
