// @flow
import React from 'react';
import { ControllerContext } from 'components/controller/ControllerBase/ControllerBase';
import ControllerButton from '../ControllerButton/ControllerButton';
import analytics from '../../../analytics/index';

const ControllerGoToChatButton = () => (
  <ControllerContext.Consumer>
    {({ goToChat }) => (
      <ControllerButton
        onClick={() => {
          goToChat();
          analytics.events.controller.goToChat();
        }}
      >
        GO TO CHAT
      </ControllerButton>
    )}
  </ControllerContext.Consumer>
);

export default ControllerGoToChatButton;
