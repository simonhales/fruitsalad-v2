// @flow

import { css } from 'emotion';
import { controllerHeaderText } from '../../../styles/typography';
import utils from '../../../styles/utils';

const headerClass = css`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 5px 10px 0 10px;
`;

const headerTextClass = css`
  ${controllerHeaderText};
  text-align: center;
`;

const timerClass = css`
  width: 50px;
  height: 50px;

  ${utils.breakpoints.mobileHeightSmaller} {
    width: 40px;
    height: 40px;
  }
`;

export default {
  headerClass,
  headerTextClass,
  timerClass,
};
