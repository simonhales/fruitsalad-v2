// @flow
import * as React from 'react';
import styles from './styles';

type Props = {
  text: string,
  timer?: React.Node,
};

const ControllerHeader = ({ text, timer }: Props) => (
  <header className={styles.headerClass}>
    <div className={styles.timerClass}>{timer}</div>
    <div className={styles.headerTextClass}>{text}</div>
    <div className={styles.timerClass} />
  </header>
);

ControllerHeader.defaultProps = {
  timer: undefined,
};

export default ControllerHeader;
