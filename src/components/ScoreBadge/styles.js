// @flow

import { css } from 'emotion';
import { getDesktopResponsiveWidthValue } from '../../styles/responsive';
import { getResponsiveDesktopFontSize, getResponsiveFont } from '../../styles/typography';

const containerClass = css`
  display: flex;
  justify-content: center;
  width: 100%;
  transform: translateY(50%);
`;

const scoreClass = css`
  background: #ffea16;
  box-shadow: 3px 3px 0 0 rgba(183, 143, 16, 0.39);
  padding: 5px 35px;
  border-radius: 12px;
`;

const scoreNumberClass = css`
  background-color: #e7c80c;
  border-radius: 50%;
  width: ${getDesktopResponsiveWidthValue(50)};
  height: ${getDesktopResponsiveWidthValue(50)};
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  font-family: Sniglet-Regular;
  ${getResponsiveFont(getResponsiveDesktopFontSize(34.5), '14px')};
  color: #ffffff;
  letter-spacing: 1.27px;
  text-shadow: 0 2px 0 rgba(0, 0, 0, 0.19);

  span {
    position: relative;
    top: 2px;
  }
`;

export default {
  containerClass,
  scoreClass,
  scoreNumberClass,
};
