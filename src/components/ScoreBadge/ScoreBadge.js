// @flow
import React from 'react';
import styles from './styles';

type Props = {
  score: number,
};

const ScoreBadge = ({ score }: Props) => (
  <div className={styles.containerClass}>
    <div className={styles.scoreClass}>
      <div className={styles.scoreNumberClass}>
        <span>{score}</span>
      </div>
    </div>
  </div>
);

export default ScoreBadge;
