// @flow
// eslint-disable-next-line import/no-duplicates
import { Component } from 'react';
// eslint-disable-next-line import/no-duplicates
import * as React from 'react';
import { connect } from 'react-redux';
import { firebaseConnect, isEmpty, isLoaded } from 'react-redux-firebase';
import { withRouter } from 'react-router-dom';
import LoadingView from '../../views/LoadingView/LoadingView';
import type { ReduxState } from '../../state/redux/shared/models';
import { getSessionFromState, getSessionFromStateRaw } from '../../state/redux/rawFirebase/state';
import { getReduxSessionKey } from '../../state/redux/firebase/state';
import NotFoundView from '../../views/NotFoundView/NotFoundView';
import { isDevMode } from '../../utils/env';
import ControllerButton, { controllerButtonProps } from '../controller/ControllerButton/ControllerButton';
import type { RouterHistory } from '../../utils/routes';
import { goToDevCreateScreen } from '../../utils/routes';
import analytics from '../../analytics';

type Props = {
  loaded: boolean,
  invalid: boolean,
  sessionKey: string,
  history: RouterHistory,
  children: React.Node,
};

class ControllerSessionFirebaseConnector extends Component<Props> {
  triggeredNotFoundAnalytics = false;

  goToDevScreen = () => {
    const { history, sessionKey } = this.props;
    goToDevCreateScreen(sessionKey, history);
  };

  renderNotFoundButt() {
    if (!isDevMode()) return null;
    return (
      <ControllerButton onClick={this.goToDevScreen} type={controllerButtonProps.types.vibrant}>
        CREATE GAME
      </ControllerButton>
    );
  }

  renderNotFound() {
    const { sessionKey } = this.props;

    if (!this.triggeredNotFoundAnalytics) {
      this.triggeredNotFoundAnalytics = true;
      analytics.events.controller.sessionNotFound(sessionKey);
    }

    return (
      <NotFoundView
        message={
          <div>
            <span>{`"${sessionKey}"`}</span> not found
          </div>
        }
        subtitle="check the URL or ask the host"
        bottomContent={this.renderNotFoundButt()}
      />
    );
  }

  render() {
    const { loaded, invalid, children } = this.props;

    if (!loaded) {
      return <LoadingView />;
    }

    if (loaded && invalid) {
      return this.renderNotFound();
    }

    return children;
  }
}

const mapStateToProps = (state: ReduxState) => {
  const rawState = getSessionFromStateRaw(state.rawFirebase);
  const loaded = isLoaded(rawState);
  const empty = isEmpty(rawState);
  const invalid = loaded && empty;
  const session = getSessionFromState(state.rawFirebase);
  const sessionKey = getReduxSessionKey(state.firebase);
  return {
    loaded,
    invalid,
    session,
    sessionKey,
  };
};

export default firebaseConnect((props, store) => {
  const state: ReduxState = store.getState();
  const sessionKey = getReduxSessionKey(state.firebase);
  const queries = [
    {
      path: `/sessions/${sessionKey}/session`,
      storeAs: 'session',
    },
  ];
  return queries;
})(connect(mapStateToProps)(withRouter(ControllerSessionFirebaseConnector)));
