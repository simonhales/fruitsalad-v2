import styled from 'react-emotion';
import {
  controllerInstructionText,
  fancyText,
  getResponsiveDesktopFontSize,
  getResponsiveFont,
  instructionsTitle,
  mediumHeading,
  smallHeading,
  tinyHeading,
} from '../styles/typography';
import utils from '../styles/utils';
import { absoluteFullView, flexFullCentered } from '../styles/utils/layout';

export const MediumHeading = styled('h3')`
  ${mediumHeading};
  ${({ centered }) => (centered ? 'text-align: center;' : '')};
`;

export const SmallHeading = styled('h4')`
  ${smallHeading};
  ${({ centered }) => (centered ? 'text-align: center;' : '')};
`;

export const TinyHeading = styled('h4')`
  ${tinyHeading};
  ${({ centered }) => (centered ? 'text-align: center;' : '')};
`;

export const FlexCentered = styled('div')`
  ${utils.layout.flexFullCentered};
  width: 100%;
  height: 100%;
  position: relative;
`;

export const InstructionText = styled('p')`
  ${controllerInstructionText};
`;

export const ControllerInstructionsTitle = styled('h3')`
  ${instructionsTitle};
  text-align: center;
`;

export const AbsoluteFullCentered = styled('div')`
  ${absoluteFullView};
  ${flexFullCentered};
`;

export const FancyHeading = styled('h3')`
  ${fancyText};
  ${getResponsiveFont(getResponsiveDesktopFontSize(84.5), '14px')};
`;
