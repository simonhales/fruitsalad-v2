// @flow
import React, { Component } from 'react';
import ContainerDimensions from 'react-container-dimensions';
import CanvasDraw from 'react-canvas-draw';
import utils from 'styles/utils';
import styles from './styles';
import { getCompressedDrawing } from '../../utils/drawing';

type Props = {
  onMouseUp?: (drawing: string) => void,
  onClear?: () => void,
};

class DrawingCanvas extends Component<Props> {
  static defaultProps = {
    onMouseUp: undefined,
    onClear: undefined,
  };

  canvasContainerRef: ?{
    current: null | HTMLDivElement,
  };

  canvasRef: ?{
    current: null | CanvasDraw,
  };

  constructor(props: Props) {
    super(props);
    this.canvasContainerRef = React.createRef();
    this.canvasRef = React.createRef();
  }

  clearDrawing = () => {
    if (this.canvasRef && this.canvasRef.current) {
      this.canvasRef.current.clear();
      const { onClear } = this.props;
      if (onClear) {
        onClear();
      }
    }
  };

  handleOnMouseUp = () => {
    if (this.canvasRef && this.canvasRef.current) {
      const { onMouseUp } = this.props;
      if (onMouseUp) {
        const data = this.canvasRef.current.getSaveData();
        const compressedData = getCompressedDrawing(data);
        onMouseUp(compressedData);
      }
    }
  };

  getSaveData = () => {
    if (this.canvasRef && this.canvasRef.current) {
      const data = this.canvasRef.current.getSaveData();
      const compressedData = getCompressedDrawing(data);
      return compressedData;
    }
    return null;
  };

  render() {
    return (
      <div
        className={styles.containerClass}
        ref={this.canvasContainerRef}
        onTouchEnd={this.handleOnMouseUp}
        onMouseUp={this.handleOnMouseUp}
      >
        <ContainerDimensions>
          {({ width, height }) => (
            <CanvasDraw
              brushColor={utils.colors.brushColor}
              canvasWidth={width}
              canvasHeight={height}
              brushSize={5}
              ref={this.canvasRef}
            />
          )}
        </ContainerDimensions>
      </div>
    );
  }
}

export default DrawingCanvas;
