// @flow
import { css } from 'emotion';
import utils from '../../styles/utils';

const containerClass = css`
  width: 100%;
  height: 100%;

  canvas {
    background-color: ${utils.colors.lightBackground};
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  }
`;

export default {
  containerClass,
};
