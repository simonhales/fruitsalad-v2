// @flow
import React from 'react';
import { cx } from 'emotion';
import styles from './styles';

const SIZES = {
  default: 'default',
  smaller: 'smaller',
};

export const DisplaySiteURLProps = {
  sizes: SIZES,
};

type Props = {
  size?: $Keys<typeof SIZES>,
};

const DisplaySiteURL = ({ size }: Props) => (
  <h2
    className={cx(styles.textClass, {
      [styles.textDefaultSizeClass]: size === SIZES.default,
      [styles.textSmallerSizeClass]: size === SIZES.smaller,
    })}
  >
    <span style={{ animationDelay: '200ms' }}>fruitsalad</span>
    <span style={{ animationDelay: '300ms' }}>.</span>
    <span style={{ animationDelay: '400ms' }}>party</span>
  </h2>
);

DisplaySiteURL.defaultProps = {
  size: SIZES.default,
};

export default DisplaySiteURL;
