import { css } from 'emotion';
import utils from 'styles/utils';
import {
  fancyText,
  getResponsiveDesktopFontSize,
  getResponsiveFont,
} from '../../../styles/typography';
import animations from '../../../styles/animations';

const textClass = css`
  ${fancyText};
  span {
    ${animations.bounceInTextAnimation};
    display: inline-block;
  }
`;

const textDefaultSizeClass = css`
  ${getResponsiveFont(getResponsiveDesktopFontSize(94.5), '60px')};
`;

const textSmallerSizeClass = css`
  ${getResponsiveFont(getResponsiveDesktopFontSize(60), '40px')};
`;

export default {
  textClass,
  textDefaultSizeClass,
  textSmallerSizeClass,
};
