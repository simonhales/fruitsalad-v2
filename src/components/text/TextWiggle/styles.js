import { css } from 'emotion';
import utils from 'styles/utils';
import animations from 'styles/animations';

const containerClass = css`
  .${utils.classNames.splitTextCharacter} {
    display: inline-block;
    transform-origin: center bottom;
  }
`;

const defaultAnimationClass = css`
  .${utils.classNames.splitTextCharacter} {
    ${animations.includeAnimation(animations.animations.textWiggle, {
      duration: '1s',
      timing: 'infinite',
    })};
  }
`;

const reducedAnimationClass = css`
  .${utils.classNames.splitTextCharacter} {
    ${animations.includeAnimation(animations.animations.textWiggleReduced, {
      duration: '1s',
      timing: 'infinite',
    })};
  }
`;

export default {
  containerClass,
  defaultAnimationClass,
  reducedAnimationClass,
};
