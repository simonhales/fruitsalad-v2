// @flow
import * as React from 'react';
import { cx } from 'emotion';
import styles from './styles';

const AMOUNTS = {
  default: 'default',
  reduced: 'reduced',
};

export const TextWiggleProps = {
  amount: AMOUNTS,
};

type Props = {
  children: React.Node,
  amount?: $Keys<typeof AMOUNTS>,
};

const TextWiggle = ({ amount, children }: Props) => (
  <span
    className={cx(styles.containerClass, {
      [styles.defaultAnimationClass]: amount === AMOUNTS.default,
      [styles.reducedAnimationClass]: amount === AMOUNTS.reduced,
    })}
  >
    {children}
  </span>
);

TextWiggle.defaultProps = {
  amount: AMOUNTS.default,
};

export default TextWiggle;
