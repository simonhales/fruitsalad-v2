import { css } from 'emotion';

const wordClass = css`
  &:not(:last-child) {
    &::after {
      content: ' ';
    }
  }
`;

export default {
  wordClass,
};
