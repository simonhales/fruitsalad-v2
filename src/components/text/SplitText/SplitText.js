// @flow
import React, { Component } from 'react';
import { cx } from 'emotion';
import utils from 'styles/utils';
import styles from './styles';

type Props = {
  text: string,
};

function getDelay(index: number, wordIndex: number): string {
  let delay = 0;
  delay += wordIndex * 75;
  delay += index * 50 * Math.random();
  return `${delay}ms`;
}

function splitWord(word: string, wordIndex: number, random: number) {
  const characters = [];
  for (let i = 0, len = word.length; i < len; i += 1) {
    characters.push(
      <span
        className={utils.classNames.splitTextCharacter}
        key={i.toString()}
        style={{
          animationDelay: getDelay(i, wordIndex),
        }}
      >
        {word.substring(i, i + 1)}
      </span>
    );
  }
  return characters;
}

class SplitText extends Component<Props> {
  random: number;

  constructor(props: Props) {
    super(props);
    // this.random = Math.random();
    this.random = 0.8618925087120071;
    // this.random = 0.2;
  }

  render() {
    const { text } = this.props;
    const words = text.split(' ');
    return (
      <React.Fragment>
        {words.map((word: string, index: number) => (
          <span
            className={cx(utils.classNames.splitTextWord, styles.wordClass)}
            key={index.toString()}
          >
            {splitWord(word, index, this.random)}
          </span>
        ))}
      </React.Fragment>
    );
  }
}

export default SplitText;
