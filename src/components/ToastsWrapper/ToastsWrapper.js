// @flow
import { createPortal } from 'react-dom';
import * as React from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { DomError } from '../../errors';

const toastsDomNode = document.getElementById('toasts-root');

if (!toastsDomNode) {
  throw new DomError({
    code: 'INVALID_TOASTS_DOM_NODE',
    message: 'toastsDomNode is invalid',
  });
}

type Props = {
  children: React.Node,
};

const ToastsPortal = ({ children }: Props) => createPortal(children, toastsDomNode);

const ToastsWrapper = () => (
  <ToastsPortal>
    <ToastContainer pauseOnFocusLoss={false} />
  </ToastsPortal>
);

export default ToastsWrapper;
