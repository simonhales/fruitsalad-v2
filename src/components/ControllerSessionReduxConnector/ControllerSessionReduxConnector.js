// @flow
import React from 'react';
import { connect } from 'react-redux';
import type { ReduxState } from '../../state/redux/shared/models';
import type { SessionModel } from '../../firebase/bananart/session/models';
import ControllerView from '../../views/controller/ControllerView/ControllerView';

type Props = {
  session: SessionModel | null,
};

const ControllerSessionReduxConnector = ({ session }: Props) => (
  <ControllerView session={session} />
);

const mapStateToProps = (state: ReduxState) => ({
  session: state.firebase.mockSession,
});

export default connect(mapStateToProps)(ControllerSessionReduxConnector);
