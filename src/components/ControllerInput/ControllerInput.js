// @flow
import React from 'react';
import styles from './styles';

type Props = {
  placeholder?: string,
  value: string,
  handleChange: (event: SyntheticInputEvent<HTMLInputElement>) => void,
};

const ControllerInput = ({ value, handleChange, placeholder }: Props) => (
  <input
    className={styles.inputClass}
    type="text"
    placeholder={placeholder}
    value={value}
    onChange={handleChange}
  />
);

ControllerInput.defaultProps = {
  placeholder: '',
};

export default ControllerInput;
