import { css } from 'emotion';
import { solidInput } from '../../styles/input';

const inputClass = css`
  ${solidInput};
`;

export default {
  inputClass,
};
