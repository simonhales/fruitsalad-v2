// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import 'styles/global';
// eslint-disable-next-line no-unused-vars
import analytics from './analytics/index';
import App from './App';
import withTracker from './components/AnalyticsTracker/AnalyticsTracker';
import { DomError } from './errors';
import { store } from './state/redux/store';

const appDomNode = document.getElementById('root');

if (!appDomNode) {
  throw new DomError({
    code: 'INVALID_APPL_DOM_NODE',
    message: 'appDomNode is invalid',
  });
}

const render = Component => {
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <Route component={withTracker(Component, {})} />
      </Router>
    </Provider>,
    appDomNode
  );
};

render(App);

declare var module: any;

if (module.hot) {
  module.hot.accept('./App', () => {
    // eslint-disable-next-line global-require
    const NextApp = require('./App').default;
    render(NextApp);
  });
}
