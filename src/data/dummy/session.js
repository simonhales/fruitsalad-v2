// @flow

import type { SessionModel } from '../../firebase/bananart/session/models';
import { sessionStates } from '../../firebase/bananart/session/models';
import type { SessionUserModel } from '../../firebase/bananart/session/user/models';

export function generateDummySessionUser(name: string, online: boolean = true): SessionUserModel {
  return {
    key: name,
    name,
    online,
    timestamps: {
      joined: 0,
    },
  };
}

export const DUMMY_SESSION_USERS: Array<SessionUserModel> = [
  generateDummySessionUser('simon'),
  generateDummySessionUser('chiao', false),
  generateDummySessionUser('aya'),
  generateDummySessionUser('pusheen'),
];

export const DUMMY_SESSION_USER = generateDummySessionUser('simon');

export function generateDummySessionUsers(users: Array<SessionUserModel>) {
  const sessionUsers = {};
  users.forEach(user => {
    sessionUsers[user.key] = user;
  });
  return sessionUsers;
}

export const DUMMY_SESSION: SessionModel = {
  key: 'SESSION_KEY',
  users: generateDummySessionUsers(DUMMY_SESSION_USERS),
  secured: {
    state: sessionStates.pending,
    timestamps: {
      created: 0,
    },
    host: 'chiao',
  },
};

export const DUMMY_HUB_SLOTS = [
  {
    key: 'Simon',
    empty: false,
    user: {
      disabled: false,
      name: 'Simon',
    },
  },
  {
    key: 'Chiao',
    empty: false,
    user: {
      disabled: false,
      name: 'Chiao',
    },
  },
  {
    key: 'Aya',
    empty: false,
    user: {
      name: 'Aya',
      disabled: true,
    },
  },
  {
    key: 'Lea',
    empty: false,
    user: {
      disabled: false,
      name: 'Lea',
    },
  },
  {
    key: 'Nathan',
    empty: false,
    user: {
      disabled: false,
      name: 'Nathan',
    },
  },
  {
    key: 'empty1',
    empty: true,
  },
];
