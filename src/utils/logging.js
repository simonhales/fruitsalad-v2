// @flow
import loglevel from 'loglevel';
import prefix from 'loglevel-plugin-prefix';

prefix.reg(loglevel);

prefix.apply(loglevel, {
  format(level, name) {
    return `${level} ${name}:`;
  },
});

export const AUDIO_LOG_NAME = 'AUDIO';
export const CONTROLLER_LOG_NAME = 'CONTROLLER';
export const DISPLAY_LOG_NAME = 'DISPLAY';
export const GAME_LOG_NAME = 'GAME';
export const NARRATOR_LOG_NAME = 'NARRATOR';
export const SOUNDS_LOG_NAME = 'SOUNDS';
export const MUSIC_LOG_NAME = 'MUSIC';
export const HOST_LOG_NAME = 'HOST';

export const audioLog = loglevel.getLogger(AUDIO_LOG_NAME);
export const controllerLog = loglevel.getLogger(CONTROLLER_LOG_NAME);
export const displayLog = loglevel.getLogger(DISPLAY_LOG_NAME);
export const gameLog = loglevel.getLogger(GAME_LOG_NAME);
export const narratorLog = loglevel.getLogger(NARRATOR_LOG_NAME);
export const soundsLog = loglevel.getLogger(SOUNDS_LOG_NAME);
export const musicLog = loglevel.getLogger(MUSIC_LOG_NAME);
export const hostLog = loglevel.getLogger(HOST_LOG_NAME);

audioLog.setLevel('TRACE');
controllerLog.setLevel('TRACE');
displayLog.setLevel('TRACE');
gameLog.setLevel('TRACE');
narratorLog.setLevel('TRACE');
soundsLog.setLevel('TRACE');
musicLog.setLevel('TRACE');
hostLog.setLevel('TRACE');
