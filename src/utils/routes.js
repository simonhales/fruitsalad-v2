// @flow

export type RouterHistory = {
  push: (path: string) => void,
};

export function goToDevCreateScreen(sessionKey: string, history: RouterHistory) {
  history.push(`/${sessionKey}/dev`);
}
