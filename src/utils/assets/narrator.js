import welcomeToFruitSalad from 'assets/audio/narration/welcome_to_fruitsalad.mp3';
import needFourPlayers from 'assets/audio/narration/need_four_players.mp3';
import nomnomnom from 'assets/audio/narration/nomnomnom.mp3';

export default {
  welcomeToFruitSalad,
  needFourPlayers,
  nomnomnom,
};
