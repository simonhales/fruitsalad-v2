import animations from './animations';
import drawings from './drawings';
import narrator from './narrator';
import patterns from './patterns';
import music from './music';
import sounds from './sounds';

export default {
  animations,
  drawings,
  patterns,
  narrator,
  music,
  sounds,
};
