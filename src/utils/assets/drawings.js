import notFound from 'assets/images/drawings/404_small-min.png';

export default {
  notFound,
};
