import fruitPattern from 'assets/images/patterns/fruit_small.svg';
import fruitLightPattern from 'assets/images/patterns/fruitLight_small.svg';
import darkFruitPattern from 'assets/images/patterns/fruitDark_small.svg';
import noise from 'assets/images/patterns/noise.png';

export default {
  fruitPattern,
  fruitLightPattern,
  darkFruitPattern,
  noise,
};
