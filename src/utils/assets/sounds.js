import randomPlayerSound from 'assets/audio/sounds/player/random.mp3';
import celebrationSound from 'assets/audio/sounds/player/celebration.mp3';
import gongSound from 'assets/audio/sounds/player/gong.mp3';
import sneakyLaughSound from 'assets/audio/sounds/player/pirate_sneaky_laugh.wav';
import sadTromboneSound from 'assets/audio/sounds/player/sad_trombone.wav';

export default {
  randomPlayerSound,
  celebrationSound,
  gongSound,
  sneakyLaughSound,
  sadTromboneSound,
};
