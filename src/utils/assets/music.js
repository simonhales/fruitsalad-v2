import flamingoLoucoFull from 'assets/audio/music/full/o-flamingo-louco.mp3';
import fullyChargedFull from 'assets/audio/music/full/fully_charged.mp3';
import shortBandito from 'assets/audio/music/short/160_short_bandito_0061_ext.mp3';
import shortDjangoRun from 'assets/audio/music/short/85_short_django-run_0031.mp3';
import fullyChargedLoop6 from 'assets/audio/music/loops/12_loop6_fully-charged_0011.wav';
import fullyChargedLoop8 from 'assets/audio/music/loops/12_loop8_fully-charged_0011.wav';

export default {
  flamingoLoucoFull,
  fullyChargedFull,
  shortBandito,
  shortDjangoRun,
  fullyChargedLoop6,
  fullyChargedLoop8,
};
