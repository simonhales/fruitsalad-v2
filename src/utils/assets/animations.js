import bananaCatGif from 'assets/images/animations/banana-running.gif';
import tvCatGif from 'assets/images/animations/tv-cat_small.gif';
import phoneCatOutlineGif from 'assets/images/animations/phone-cat-outline_large.gif';

export default {
  bananaCatGif,
  tvCatGif,
  phoneCatOutlineGif,
};
