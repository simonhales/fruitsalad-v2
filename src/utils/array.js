// @flow

export function shuffleArray(array: Array<any>): Array<any> {
  return array
    .map(a => [Math.random(), a])
    .sort((a, b) => a[0] - b[0])
    .map(a => a[1]);
}
