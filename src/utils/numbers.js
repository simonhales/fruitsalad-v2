// @flow

export function getNumberFromString(numberString: string): number {
  const num: string = numberString.replace(/[^.0-9]/g, '');
  return parseFloat(num);
}

export function randomIntFromInterval(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export function getOffsettedIndex(index: number, range: number): number {
  return index - Math.floor(index / range) * range;
}
