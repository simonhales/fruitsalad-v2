// @flow
import { toast } from 'react-toastify';

export function userActionFailedToast(message: string, toastId?: string): string {
  const config = {
    position: toast.POSITION.TOP_CENTER,
    autoClose: 3000,
  };
  if (toastId) {
    toast.dismiss(toastId);
    config[toastId] = toastId;
  }
  return toast.warn(message, config);
}
