// @flow

export function convertSecondsToMilis(seconds: number): number {
  return seconds * 1000;
}

export function getFutureTimestamp(time: number): number {
  return Date.now() + time;
}
