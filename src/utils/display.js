// @flow

import { css } from 'emotion';
import { tiledBackgroundImage } from '../styles/images';
import utils from '../styles/utils';
import assets from './assets';

export const displayViewContent = css`
  position: absolute;
  top: 20px;
  left: 20px;
  right: 20px;
  bottom: 20px;
  background-color: ${utils.colors.displayBackground};
  ${tiledBackgroundImage(assets.patterns.noise)};
  border-radius: 20px;
  box-shadow: inset 0 0 5px rgba(8, 81, 117, 0.3);
  border: 1px solid rgba(255, 255, 255, 0.05);
`;
