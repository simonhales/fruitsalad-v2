// @flow
import toBoolean from 'to-boolean';

export type EnvProcess = {
  env: {
    REACT_APP_DEV_MODE: string,
    REACT_APP_MUSIC_MUTED: string,
    REACT_APP_NARRATOR_MUTED: string,
    REACT_APP_SOUND_EFFECTS_MUTED: string,
    REACT_APP_AUDIO_DISABLED: string,
    REACT_APP_MUSIC_DISABLED: string,
    REACT_APP_DEFAULT_USER_KEY: string,
    REACT_APP_GOOGLE_ANALYTICS_ID: string,
    REACT_APP_FIREBASE_API_KEY: string,
    REACT_APP_FIREBASE_AUTH_DOMAIN: string,
    REACT_APP_FIREBASE_DATABASE_URL: string,
    REACT_APP_FIREBASE_PROJECT_ID: string,
    REACT_APP_FIREBASE_STORAGE_BUCKET: string,
    REACT_APP_FIREBASE_MESSAGING_SENDER_ID: string,
    REACT_APP_DEV_FIREBASE?: string,
    REACT_APP_DEV_EMAIL?: string,
    REACT_APP_DEV_PASSWORD?: string,
  },
};

declare var process: EnvProcess;

export function getMusicDisabled(): boolean {
  return process.env.REACT_APP_MUSIC_DISABLED
    ? toBoolean(process.env.REACT_APP_MUSIC_DISABLED)
    : false;
}

export function getAudioDisabled(): boolean {
  return process.env.REACT_APP_AUDIO_DISABLED
    ? toBoolean(process.env.REACT_APP_AUDIO_DISABLED)
    : false;
}

export function getSoundEffectsMuted(): boolean {
  return process.env.REACT_APP_SOUND_EFFECTS_MUTED
    ? toBoolean(process.env.REACT_APP_SOUND_EFFECTS_MUTED)
    : false;
}

export function getMusicMuted(): boolean {
  return process.env.REACT_APP_MUSIC_MUTED ? toBoolean(process.env.REACT_APP_MUSIC_MUTED) : false;
}

export function getNarratorMuted(): boolean {
  return process.env.REACT_APP_NARRATOR_MUTED
    ? toBoolean(process.env.REACT_APP_NARRATOR_MUTED)
    : false;
}

export function isDevFirebaseMode(): boolean {
  return process.env.REACT_APP_DEV_FIREBASE ? toBoolean(process.env.REACT_APP_DEV_FIREBASE) : false;
}

export function getDevFirebaseEmail(): string | null {
  return process.env.REACT_APP_DEV_EMAIL ? process.env.REACT_APP_DEV_EMAIL : null;
}

export function getDevFirebasePassword(): string | null {
  return process.env.REACT_APP_DEV_PASSWORD ? process.env.REACT_APP_DEV_PASSWORD : null;
}

export function isDevFirebaseModeWithVars(): boolean {
  const email = getDevFirebaseEmail();
  const password = getDevFirebasePassword();
  return isDevFirebaseMode() && !!email && !!password;
}

export function isDevMode(): boolean {
  return process.env.REACT_APP_DEV_MODE ? toBoolean(process.env.REACT_APP_DEV_MODE) : false;
}

export function getEnvGoogleAnalyticsId(): string {
  return process.env.REACT_APP_GOOGLE_ANALYTICS_ID;
}

export function useDefaultUserKey(): boolean {
  return process.env.REACT_APP_DEFAULT_USER_KEY
    ? toBoolean(process.env.REACT_APP_DEFAULT_USER_KEY)
    : false;
}
