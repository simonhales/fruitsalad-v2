// @flow
export function stripNumberFromString(numberString: string): string {
  return numberString.replace(/[0-9]/g, '');
}
