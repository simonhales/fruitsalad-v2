// @flow
import lzjs from 'lzjs';

export function getCompressedDrawing(drawing: string): string {
  return lzjs.compress(drawing);
}

export function decompressDrawing(drawing: string): string {
  const decompressed = lzjs.decompress(drawing);
  return decompressed;
}
