// @flow
import { Subject } from 'rxjs';
import { distinct, filter, first } from 'rxjs/operators';
import { narratorLog } from 'utils/logging';
import { audioNarrator } from 'audio/narrator/AudioNarrator';
import type { SessionModel } from '../../firebase/bananart/session/models';
import type { AudioPlayConditions, AudioTrack } from '../../audio/narrator/AudioNarrator';
import { loadAudioTrack, prependBase64Code } from '../../audio/utils';
import type { NarratorAsset } from '../../audio/narrator/assets';
import { generateCloudMessage } from '../../audio/cloud';
import type { GoogleTextCloudResponse } from '../../audio/cloud';

function getAudioTrack(
  key: string,
  playConditions: AudioPlayConditions,
  uniqueKey: string
): AudioTrack {
  return {
    key,
    uniqueKey,
    ...playConditions,
  };
}

type ToBeGeneratedAudio = {
  key: string,
  message: string,
};

type GeneratedAudio = {
  key: string,
  url: string,
  generated?: boolean,
};

type LoadedAudio = {
  key: string,
  audioHowler: any,
};

const delay = (time, promise) =>
  Promise.all([promise, new Promise(resolve => setTimeout(resolve, time))]).then(
    ([response]) => response
  );

export class Narrator {
  session: SessionModel;

  toBeGeneratedAudio$: Subject<ToBeGeneratedAudio> = new Subject().pipe(
    distinct((audio: ToBeGeneratedAudio) => audio.key)
  );

  generatedAudio$: Subject<GeneratedAudio> = new Subject().pipe(
    distinct((audio: GeneratedAudio) => audio.key)
  );

  generatedAudio: {
    [string]: GeneratedAudio,
  } = {};

  toBeLoadedAudio$: Subject<GeneratedAudio> = new Subject().pipe(
    distinct((audio: GeneratedAudio) => audio.key)
  );

  loadedAudio$: Subject<LoadedAudio> = new Subject().pipe(
    distinct((audio: LoadedAudio) => audio.key)
  );

  loadedAudio: {
    [string]: LoadedAudio,
  } = {};

  queuedAudio$: Subject<AudioTrack> = new Subject().pipe(
    distinct((audio: AudioTrack) => audio.uniqueKey)
  );

  observeQueue() {
    this.queuedAudio$.subscribe(this.handleQueuedAudio);
  }

  observeToBeGeneratedAudio() {
    this.toBeGeneratedAudio$.subscribe(this.handleToBeGeneratedAudio, error => {
      console.error('error', error);
    });
  }

  observeGeneratedAudio() {
    this.generatedAudio$.subscribe(this.handleGeneratedAudio, error => {
      console.error('error', error);
    });
  }

  observeToBeLoadedAudio() {
    this.toBeLoadedAudio$.subscribe(this.handleToBeLoadedAudio, error => {
      console.error('error', error);
    });
  }

  observeLoadedAudio() {
    this.loadedAudio$.subscribe(this.handleLoadedAudio, error => {
      console.error('error', error);
    });
  }

  handleQueuedAudio = (audioTrack: AudioTrack) => {
    narratorLog.log(`Add audio track "${audioTrack.key}" to queue.`);
    if (this.loadedAudio.hasOwnProperty(audioTrack.key)) {
      narratorLog.log(`Audio "${audioTrack.key}" is loaded already, so continue immediately.`);
      this.handleTryToPlayAudio(audioTrack);
    } else {
      narratorLog.log(
        `Audio "${audioTrack.key}" has not been loaded, so need to wait for it to load.`
      );
      this.handleSubscribeForAudioLoading(audioTrack);
    }
  };

  handleToBeGeneratedAudio = (audio: ToBeGeneratedAudio) => {
    narratorLog.log(`Need to generate audio "${audio.key}".`);
    generateCloudMessage(audio.message).then((response: GoogleTextCloudResponse) => {
      this.generatedAudio$.next({
        key: audio.key,
        url: prependBase64Code(response.audioContent),
        generated: true,
      });
    });
  };

  handleGeneratedAudio = (audio: GeneratedAudio) => {
    narratorLog.log(`Generated audio "${audio.key}".`);
    this.generatedAudio[audio.key] = audio;
    this.toBeLoadedAudio$.next(audio);
  };

  handleToBeLoadedAudio = (audio: GeneratedAudio) => {
    narratorLog.log(`Need to load audio "${audio.key}".`);
    const generated = !!audio.generated;
    loadAudioTrack(audio.url, generated).then(audioHowler => {
      narratorLog.log(`Loaded audio howler for "${audio.key}".`);
      this.loadedAudio$.next({
        key: audio.key,
        audioHowler,
      });
    });
  };

  handleLoadedAudio = (audio: LoadedAudio) => {
    narratorLog.log(`Loaded audio "${audio.key}".`);
    this.loadedAudio[audio.key] = audio;
  };

  handleTryToPlayAudio = (audioTrack: AudioTrack) => {
    narratorLog.log(`Add audio "${audioTrack.key}" to audioNarrator.`);
    const audioHowler = this.getLoadedAudioHowler(audioTrack.key);
    audioNarrator.addTrackToQueue(audioTrack, audioHowler);
  };

  handleSubscribeForAudioLoading = (audioTrack: AudioTrack) => {
    narratorLog.log(`Observe audio loading for "${audioTrack.key}".`);
    this.loadedAudio$
      .pipe(filter((audio: GeneratedAudio) => audio.key === audioTrack.key))
      .pipe(first())
      .subscribe((audio: LoadedAudio) => {
        narratorLog.log(`Audio has loaded for "${audioTrack.key}".`);
        this.handleTryToPlayAudio(audioTrack);
      });
  };

  getLoadedAudioHowler(key: string): any {
    return this.loadedAudio[key].audioHowler;
  }

  generateAndAddToQueue(
    key: string,
    playConditions: AudioPlayConditions,
    message: string,
    uniqueKey: string
  ) {
    const audioTrack = getAudioTrack(key, playConditions, uniqueKey);
    this.addToQueue(audioTrack);
    this.generateTrack(key, message);
  }

  generateTrack(key: string, message: string) {
    this.addToBeGenerated(key, message);
  }

  addToBeGenerated(key: string, message: string) {
    this.toBeGeneratedAudio$.next({
      key,
      message,
    });
  }

  loadAndAddToQueue(
    { key, url }: NarratorAsset,
    playConditions: AudioPlayConditions,
    uniqueKey: string
  ) {
    const audioTrack = getAudioTrack(key, playConditions, uniqueKey);
    this.addToQueue(audioTrack);
    this.loadTrack(key, url);
  }

  loadTrack(key: string, url: string) {
    this.addToBeLoaded(key, url);
  }

  loadAssetTrack({ key, url }: NarratorAsset) {
    this.loadTrack(key, url);
  }

  addToBeLoaded(key: string, url: string) {
    this.toBeLoadedAudio$.next({
      key,
      url,
    });
  }

  addToQueue(audioTrack: AudioTrack) {
    this.queuedAudio$.next(audioTrack);
  }

  startNarrator() {
    this.observeLoadedAudio();
    this.observeToBeLoadedAudio();
    this.observeGeneratedAudio();
    this.observeToBeGeneratedAudio();
    this.observeQueue();
    // todo add observables
  }
}
