// @flow

export function getResolvedTimestamp(duration: number): number {
  return Date.now() + duration;
}
