// @flow

export type TerminateFn = (timerKey: string, entryPoint: string) => void;
