// @flow
import { hostLog } from 'utils/logging';
import type { SessionModel } from '../../firebase/bananart/session/models';
import { getSessionKey, getSessionState } from '../../firebase/bananart/session/state';
import { sessionStates } from '../../firebase/bananart/session/models';
import {
  getCurrentGameFromSession,
  getGameKey,
  getGameState,
  isTimerStillActiveTimer,
} from '../../firebase/bananart/game/state';
import { gameStates } from '../../firebase/bananart/game/models';
import { HostGameListeners } from './listeners';
import type { GameModel } from '../../firebase/bananart/game/models';
import { getGameCurrentRound, getRoundState } from '../../firebase/bananart/game/round/state';
import { getEntryState, getGameCurrentEntry } from '../../firebase/bananart/game/entry/state';
import type { RoundModel } from '../../firebase/bananart/game/round/models';
import type { EntryModel } from '../../firebase/bananart/game/entry/models';
import { roundStates } from '../../firebase/bananart/game/round/models';
import { entryStates } from '../../firebase/bananart/game/entry/models';
import {
  handleGameStateCompleted,
  handleGameStateIntro,
  handleGameStatePending,
  handleGameStateResults,
} from './steps/game';
import {
  handleRoundStateDrawingCompleted,
  handleRoundStateDrawing,
  handleRoundStateDrawingIntro,
  handleRoundStatePending,
  handleRoundStateResults,
  handleRoundStateCompleted,
} from './steps/round';
import {
  handleEntryStateGuessingCompleted,
  handleEntryStateGuessing,
  handleEntryStateGuessingIntro,
  handleEntryStateIntro,
  handleEntryStateNoDrawings,
  handleEntryStatePending,
  handleEntryStateVoting,
  handleEntryStateVotingIntro,
  handleEntryStateVotingCompleted,
  handleEntryStateResults,
  handleEntryStateCompleted,
} from './steps/entry';
import { clearGameTimer } from '../../firebase/bananart/game/actions';

export class GameHandler {
  terminateAndContinueGameLogic = (timerKey: string, entryPoint: string) => {
    hostLog.log(`Terminate and continue game logic. Entry point: ${entryPoint}`);
    if (timerKey) {
      const session = this.getSession();
      const game = this.getCurrentGame();
      if (isTimerStillActiveTimer(timerKey, game)) {
        const sessionKey = getSessionKey(session);
        const gameKey = getGameKey(game);
        clearGameTimer(sessionKey, gameKey);
        this.listeners.stopAllListeners(); // not sure if this causes a circular reference loop?
      } else {
        hostLog.log(`Timer "${timerKey}" has become redundant. Entry point "${entryPoint}"`);
        return;
      }
    }
    this.nextSessionStep();
  };

  logicInProgress: boolean = false;

  listeners: HostGameListeners = new HostGameListeners(this.terminateAndContinueGameLogic);

  session: SessionModel;

  updateSession(session: SessionModel) {
    this.session = session;
    this.tryToStartLogic();
  }

  tryToStartLogic() {
    if (this.logicInProgress) return;
    const session = this.getSession();
    const state = getSessionState(session);
    if (state === sessionStates.playing) {
      hostLog.log(`Starting logic loop.`);
      this.logicInProgress = true;
      this.nextSessionStep();
    }
  }

  getSession(): SessionModel {
    return this.session;
  }

  getCurrentGame(): GameModel {
    const session = this.getSession();
    const game = getCurrentGameFromSession(session);
    if (!game) {
      throw new Error(`No current game in session.`);
    }
    return game;
  }

  getCurrentRound(): RoundModel {
    const game = this.getCurrentGame();
    const round = getGameCurrentRound(game);
    if (!round) {
      throw new Error(`No current round in game.`);
    }
    return round;
  }

  getCurrentEntry(): EntryModel {
    const game = this.getCurrentGame();
    const entry = getGameCurrentEntry(game);
    if (!entry) {
      throw new Error(`No current entry in game.`);
    }
    return entry;
  }

  nextEntryStep() {
    const entry = this.getCurrentEntry();
    const state = getEntryState(entry);
    hostLog.log(`Next entry step. Entry state  is "${state}".`);
    const session = this.getSession();
    const game = this.getCurrentGame();
    const round = this.getCurrentRound();
    switch (state) {
      case entryStates.pending:
        handleEntryStatePending(this.terminateAndContinueGameLogic, session, game, entry);
        break;
      case entryStates.noDrawings:
        handleEntryStateNoDrawings(this.terminateAndContinueGameLogic, session, game, entry);
        break;
      case entryStates.intro:
        handleEntryStateIntro(this.terminateAndContinueGameLogic, session, game, entry);
        break;
      case entryStates.guessingIntro:
        handleEntryStateGuessingIntro(
          this.terminateAndContinueGameLogic,
          session,
          game,
          entry,
          this.listeners
        );
        break;
      case entryStates.guessing:
        handleEntryStateGuessing(this.terminateAndContinueGameLogic, session, game, entry);
        break;
      case entryStates.guessingSubmitted:
      case entryStates.guessingTimesUp:
        handleEntryStateGuessingCompleted(this.terminateAndContinueGameLogic, session, game, entry);
        break;
      case entryStates.votingIntro:
        handleEntryStateVotingIntro(
          this.terminateAndContinueGameLogic,
          session,
          game,
          entry,
          this.listeners
        );
        break;
      case entryStates.voting:
        handleEntryStateVoting(this.terminateAndContinueGameLogic, session, game, entry);
        break;
      case entryStates.votingSubmitted:
      case entryStates.votingTimesUp:
        handleEntryStateVotingCompleted(this.terminateAndContinueGameLogic, session, game, entry);
        break;
      case entryStates.results:
        handleEntryStateResults(this.terminateAndContinueGameLogic, session, game, entry);
        break;
      case entryStates.completed:
        handleEntryStateCompleted(this.terminateAndContinueGameLogic, session, game, entry, round);
        break;
      default:
        hostLog.log(`No match for entry state "${state}", so do nothing...`);
        break;
    }
  }

  nextRoundStep() {
    const round = this.getCurrentRound();
    const state = getRoundState(round);
    hostLog.log(`Next round step. Round state  is "${state}".`);
    const session = this.getSession();
    const game = this.getCurrentGame();
    switch (state) {
      case roundStates.pending:
        handleRoundStatePending(this.terminateAndContinueGameLogic, session, game, round);
        break;
      case roundStates.drawingIntro:
        handleRoundStateDrawingIntro(
          this.terminateAndContinueGameLogic,
          session,
          game,
          round,
          this.listeners
        );
        break;
      case roundStates.drawing:
        handleRoundStateDrawing(this.terminateAndContinueGameLogic, session, game, round);
        break;
      case roundStates.drawingSubmitted:
      case roundStates.drawingTimesUp:
        handleRoundStateDrawingCompleted(this.terminateAndContinueGameLogic, session, game, round);
        break;
      case roundStates.voting:
        this.nextEntryStep();
        break;
      case roundStates.results:
        handleRoundStateResults(this.terminateAndContinueGameLogic, session, game, round);
        break;
      case roundStates.completed:
        handleRoundStateCompleted(this.terminateAndContinueGameLogic, session, game, round);
        break;
      default:
        hostLog.log(`No match for round state "${state}", so do nothing...`);
        break;
    }
  }

  nextGameStep() {
    const game = this.getCurrentGame();
    const state = getGameState(game);

    const session = this.getSession();
    hostLog.log(`Next game step. Game state  is "${state}".`);

    switch (state) {
      case gameStates.pending:
        handleGameStatePending(this.terminateAndContinueGameLogic, session, game, this.listeners);
        break;
      case gameStates.intro:
        handleGameStateIntro(this.terminateAndContinueGameLogic, session, game);
        break;
      case gameStates.playing:
        this.nextRoundStep();
        break;
      case gameStates.results:
        handleGameStateResults(this.terminateAndContinueGameLogic, session, game);
        break;
      case gameStates.completed:
        handleGameStateCompleted(this.terminateAndContinueGameLogic, session);
        break;
      default:
        hostLog.log(`No match for game state "${state}", so do nothing...`);
        break;
    }
  }

  completeLogic() {
    hostLog.log(`Session state is completed, so the loop is terminating.`);
    this.logicInProgress = false;
  }

  nextSessionStep() {
    const session = this.getSession();

    const state = getSessionState(session);
    hostLog.log(`Next session step. Session state  is "${state}".`);

    switch (state) {
      case sessionStates.playing:
        this.nextGameStep();
        break;
      case sessionStates.completed:
        this.completeLogic();
        break;
      default:
        hostLog.log(`Session state "${state}" is not playing, so do nothing...`);
        break;
    }
  }
}
