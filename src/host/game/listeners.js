// @flow

import { getGameIntroSkippedRef, getGameRef } from '../../firebase/bananart/game/refs';
import type { TerminateFn } from './types';
import type { GameModel } from '../../firebase/bananart/game/models';
import {
  areAllRoundDrawingsSubmitted,
  getGameCurrentRound,
  getRoundKey,
} from '../../firebase/bananart/game/round/state';
import { getEntryRef } from '../../firebase/bananart/game/entry/refs';
import type { EntryModel } from '../../firebase/bananart/game/entry/models';
import {
  areAllEntryGuessesSubmitted,
  areAllEntryVotesSubmitted,
} from '../../firebase/bananart/game/entry/state';
import { hostLog } from '../../utils/logging';

export class HostGameListeners {
  terminate: TerminateFn;

  constructor(terminate: TerminateFn) {
    this.terminate = terminate;
  }

  introListenerRef: any;

  drawingsListenerRef: any;

  guessesListenerRef: any;

  votesListenerRef: any;

  listenForIntro(listeningKey: string, sessionKey: string, gameKey: string) {
    hostLog.log(`Listen for intro.`);
    this.stopListeningForIntro();
    this.introListenerRef = getGameIntroSkippedRef(sessionKey, gameKey);
    this.introListenerRef.on('value', snapshot => {
      this.handleIntroValue(snapshot, listeningKey);
    });
  }

  handleIntroValue(snapshot: any, listeningKey: string) {
    const data: boolean = snapshot.val();
    this.checkIntro(listeningKey, data);
  }

  checkIntro(listeningKey: string, skipped: boolean) {
    hostLog.log(`Intro listener check.`);
    if (skipped) {
      hostLog.log(`Intro listener passed state.`);
      this.terminateAndContinue(listeningKey);
    }
  }

  stopListeningForIntro() {
    hostLog.log(`Stop listening for intro.`);
    if (this.introListenerRef) {
      this.introListenerRef.off();
    }
  }

  listenForDrawings(listeningKey: string, sessionKey: string, gameKey: string) {
    hostLog.log(`Listen for drawings.`);
    this.drawingsListenerRef = getGameRef(sessionKey, gameKey);
    this.drawingsListenerRef.on('value', snapshot => {
      this.handleDrawingsValue(snapshot, listeningKey);
    });
  }

  handleDrawingsValue(snapshot: any, listeningKey: string) {
    const data: GameModel = snapshot.val();
    this.checkDrawings(data, listeningKey);
  }

  checkDrawings(game: GameModel, listeningKey: string) {
    hostLog.log(`Drawings listener check.`);
    const round = getGameCurrentRound(game);
    if (!round) {
      throw new Error(`No round.`);
    }
    const roundKey = getRoundKey(round);
    if (areAllRoundDrawingsSubmitted(game, roundKey)) {
      hostLog.log(`Drawings listener passed state.`);
      this.terminateAndContinue(listeningKey);
    }
  }

  stopListeningForDrawings() {
    hostLog.log(`Stop drawings listener.`);
    if (this.drawingsListenerRef) {
      this.drawingsListenerRef.off();
    }
  }

  listenForGuesses(listeningKey: string, sessionKey: string, gameKey: string, entryKey: string) {
    hostLog.log(`Listen for guesses.`);
    this.guessesListenerRef = getEntryRef(entryKey, sessionKey, gameKey);
    this.guessesListenerRef.on('value', snapshot => {
      this.handleGuessesValue(snapshot, listeningKey);
    });
  }

  handleGuessesValue(snapshot: any, listeningKey: string) {
    const data: EntryModel = snapshot.val();
    this.checkGuesses(data, listeningKey);
  }

  checkGuesses(entry: EntryModel, listeningKey: string) {
    hostLog.log(`Guesses listener check.`);
    if (areAllEntryGuessesSubmitted(entry)) {
      hostLog.log(`Guesses listener passed state.`);
      this.terminateAndContinue(listeningKey);
    }
  }

  stopListeningForGuesses() {
    hostLog.log(`Stop guesses listener.`);
    if (this.guessesListenerRef) {
      this.guessesListenerRef.off();
    }
  }

  listenForVotes(listeningKey: string, sessionKey: string, gameKey: string, entryKey: string) {
    hostLog.log(`Listen for votes.`);
    this.votesListenerRef = getEntryRef(entryKey, sessionKey, gameKey);
    this.votesListenerRef.on('value', snapshot => {
      this.handleVotesValue(snapshot, listeningKey);
    });
  }

  handleVotesValue(snapshot: any, listeningKey: string) {
    const data: EntryModel = snapshot.val();
    this.checkVotes(data, listeningKey);
  }

  checkVotes(entry: EntryModel, listeningKey: string) {
    hostLog.log(`Votes listener check.`);
    if (areAllEntryVotesSubmitted(entry)) {
      hostLog.log(`Votes listener passed state.`);
      this.terminateAndContinue(listeningKey);
    }
  }

  stopListeningForVotes() {
    hostLog.log(`Stop votes listener.`);
    if (this.votesListenerRef) {
      this.votesListenerRef.off();
    }
  }

  stopAllListeners() {
    hostLog.log(`Stopping all listeners.`);
    this.stopListeningForIntro();
    this.stopListeningForDrawings();
    this.stopListeningForGuesses();
    this.stopListeningForVotes();
  }

  terminateAndContinue(listeningKey: string) {
    hostLog.log(`Terminating and continuing from listener.`);
    this.stopAllListeners();
    this.terminate(listeningKey, 'terminateAndContinue');
  }
}
