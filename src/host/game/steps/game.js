// @flow
import { hostLog } from 'utils/logging';
import { timeout } from 'utils/async';
import type { TerminateFn } from '../types';
import {
  setGameCurrentRound,
  setGameState,
  setGameTimerKey,
} from '../../../firebase/bananart/game/actions';
import { gameStates } from '../../../firebase/bananart/game/models';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import type { GameModel } from '../../../firebase/bananart/game/models';
import { getSessionKey } from '../../../firebase/bananart/session/state';
import { getGameKey } from '../../../firebase/bananart/game/state';
import { getResolvedTimestamp } from '../utils';
import { GAME_INTRO_TIMER, GAME_RESULTS_TIMER } from '../../../constants/timers';
import { getGameNextRoundKey } from '../../../firebase/bananart/game/round/state';
import { setSessionState } from '../../../firebase/bananart/session/actions';
import { sessionStates } from '../../../firebase/bananart/session/models';
import { HostGameListeners } from '../listeners';

async function startGameIntro(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  listeners: HostGameListeners
) {
  hostLog.log(`Starting game intro.`);

  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const state = gameStates.intro;

  setGameState(sessionKey, gameKey, state);

  const waitDuration = GAME_INTRO_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  listeners.listenForIntro(timerKey, sessionKey, gameKey);

  await timeout(waitDuration);
  hostLog.log(`Starting game intro timer has completed, terminating.`);
  terminate(timerKey, 'startGameIntro');
}

function startGamePlaying(terminate: TerminateFn, session: SessionModel, game: GameModel) {
  hostLog.log(`Starting game playing.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const state = gameStates.playing;
  const startingRoundKey = getGameNextRoundKey(game);
  if (!startingRoundKey) {
    throw new Error(`No starting round found.`);
  }
  setGameCurrentRound(sessionKey, gameKey, startingRoundKey);
  setGameState(sessionKey, gameKey, state);
  terminate('', 'startGamePlaying');
}

export function handleGameStatePending(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  listeners: HostGameListeners
) {
  hostLog.log(`Handle game state pending.`);
  startGameIntro(terminate, session, game, listeners);
}

export function handleGameStateIntro(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel
) {
  hostLog.log(`Handle game state intro.`);
  startGamePlaying(terminate, session, game);
}

export async function startGameResults(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string
) {
  hostLog.log(`Starting game results.`);
  const state = gameStates.results;
  setGameState(sessionKey, gameKey, state);
  const waitDuration = GAME_RESULTS_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);
  hostLog.log(`Starting game results timer has completed, terminating.`);
  terminate(timerKey, 'startGameResults');
}

function setGameCompleted(terminate: TerminateFn, sessionKey: string, gameKey: string) {
  hostLog.log(`Set game completed.`);
  const state = gameStates.completed;
  setGameState(sessionKey, gameKey, state);
  terminate('', 'setGameCompleted');
}

export function handleGameStateResults(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel
) {
  hostLog.log(`Handle game state results.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  setGameCompleted(terminate, sessionKey, gameKey);
}

function setSessionCompleted(terminate: TerminateFn, sessionKey: string) {
  hostLog.log(`Set session completed.`);
  const state = sessionStates.completed;
  setSessionState(sessionKey, state);
  terminate('', 'setSessionCompleted');
}

export function handleGameStateCompleted(terminate: TerminateFn, session: SessionModel) {
  hostLog.log(`Handle game state completed.`);
  const sessionKey = getSessionKey(session);
  setSessionCompleted(terminate, sessionKey);
}
