// @flow

import type { TerminateFn } from '../types';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import type { RoundModel } from '../../../firebase/bananart/game/round/models';
import type { GameModel } from '../../../firebase/bananart/game/models';
import {
  areAllRoundDrawingsSubmitted,
  getGameNextRoundKey,
  getRoundKey,
  getRoundNextEntryKey,
  isRoundFinalRoundInGame,
} from '../../../firebase/bananart/game/round/state';
import { getGameKey } from '../../../firebase/bananart/game/state';
import { getSessionKey } from '../../../firebase/bananart/session/state';
import { setRoundState, submitBotDrawings } from '../../../firebase/bananart/game/round/actions';
import { roundStates } from '../../../firebase/bananart/game/round/models';
import {
  DRAWING_COMPLETED,
  DRAWING_INTRO_TIMER,
  DRAWING_TIMER,
  ROUND_RESULTS_TIMER,
} from '../../../constants/timers';
import { getResolvedTimestamp } from '../utils';
import {
  setGameCurrentEntry,
  setGameCurrentRound,
  setGameTimerKey,
} from '../../../firebase/bananart/game/actions';
import { timeout } from '../../../utils/async';
import { hostLog } from '../../../utils/logging';
import { startGameResults } from './game';
import { HostGameListeners } from '../listeners';

export async function startRoundResults(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  roundKey: string
) {
  hostLog.log(`Starting round results.`);
  const state = roundStates.results;
  setRoundState(state, sessionKey, gameKey, roundKey);
  const waitDuration = ROUND_RESULTS_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);
  hostLog.log(`Starting round results timer has completed, terminating.`);
  terminate(timerKey, 'startRoundResults');
}

export function startNextRound(terminate: TerminateFn, session: SessionModel, game: GameModel) {
  hostLog.log(`Starting next round.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const nextRoundKey = getGameNextRoundKey(game);
  if (!nextRoundKey) {
    throw new Error(`No next round key found.`);
  }
  setGameCurrentRound(sessionKey, gameKey, nextRoundKey);
  terminate('', 'startNextRound');
}

async function startRoundDrawingIntro(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  roundKey: string
) {
  hostLog.log(`Starting round drawing intro.`);
  const state = roundStates.drawingIntro;
  setRoundState(state, sessionKey, gameKey, roundKey);

  const waitDuration = DRAWING_INTRO_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);
  hostLog.log(`Starting round drawing intro timer has completed, terminating.`);
  terminate(timerKey, 'startRoundDrawingIntro');
}

async function startRoundDrawing(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  roundKey: string,
  listeners: HostGameListeners,
  game: GameModel
) {
  hostLog.log(`Starting round drawing.`);
  const state = roundStates.drawing;
  setRoundState(state, sessionKey, gameKey, roundKey);

  const waitDuration = DRAWING_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  submitBotDrawings(sessionKey, gameKey, roundKey, game);

  listeners.listenForDrawings(timerKey, sessionKey, gameKey);

  await timeout(waitDuration);
  hostLog.log(`Starting round drawing timer has completed, terminating.`);
  terminate(timerKey, 'startRoundDrawing');
}

async function setRoundDrawingCompleted(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  roundKey: string,
  game: GameModel
) {
  hostLog.log(`Setting round drawing completed.`);
  const state = areAllRoundDrawingsSubmitted(game, roundKey)
    ? roundStates.drawingSubmitted
    : roundStates.drawingTimesUp;
  setRoundState(state, sessionKey, gameKey, roundKey);

  const waitDuration = DRAWING_COMPLETED;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);
  hostLog.log(`Set round drawing completed timer has completed, terminating.`);
  terminate(timerKey, 'setRoundDrawingCompleted');
}

function setRoundVoting(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  roundKey: string,
  game: GameModel,
  round: RoundModel
) {
  hostLog.log(`Setting round voting.`);
  const startingEntryKey = getRoundNextEntryKey(game, round);
  if (!startingEntryKey) {
    throw new Error(`No starting entry matched.`);
  }
  setGameCurrentEntry(sessionKey, gameKey, startingEntryKey);
  const state = roundStates.voting;
  setRoundState(state, sessionKey, gameKey, roundKey);
  terminate('', 'setRoundVoting');
}

export function handleRoundStatePending(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  round: RoundModel
) {
  hostLog.log(`Handle round state pending.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const roundKey = getRoundKey(round);
  startRoundDrawingIntro(terminate, sessionKey, gameKey, roundKey);
}

export function handleRoundStateDrawingIntro(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  round: RoundModel,
  listeners: HostGameListeners
) {
  hostLog.log(`Handle round state drawing intro.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const roundKey = getRoundKey(round);
  startRoundDrawing(terminate, sessionKey, gameKey, roundKey, listeners, game);
}

export function handleRoundStateDrawing(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  round: RoundModel
) {
  hostLog.log(`Handle round state drawing.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const roundKey = getRoundKey(round);
  setRoundDrawingCompleted(terminate, sessionKey, gameKey, roundKey, game);
}

export function handleRoundStateDrawingCompleted(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  round: RoundModel
) {
  hostLog.log(`Handle round state drawing completed.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const roundKey = getRoundKey(round);
  setRoundVoting(terminate, sessionKey, gameKey, roundKey, game, round);
}

export function setRoundCompleted(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  roundKey: string
) {
  hostLog.log(`Setting round completed.`);
  const state = roundStates.completed;
  setRoundState(state, sessionKey, gameKey, roundKey);
  terminate('', 'setRoundCompleted');
}

export function handleRoundStateResults(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  round: RoundModel
) {
  hostLog.log(`Handle round state results.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const roundKey = getRoundKey(round);
  setRoundCompleted(terminate, sessionKey, gameKey, roundKey);
}

export function handleRoundStateCompleted(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  round: RoundModel
) {
  hostLog.log(`Handle round state completed.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const roundKey = getRoundKey(round);
  if (isRoundFinalRoundInGame(game, roundKey)) {
    startGameResults(terminate, sessionKey, gameKey);
  } else {
    startNextRound(terminate, session, game);
  }
}
