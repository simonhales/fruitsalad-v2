// @flow

import type { TerminateFn } from '../types';
import type { SessionModel } from '../../../firebase/bananart/session/models';
import type { GameModel } from '../../../firebase/bananart/game/models';
import type { EntryModel } from '../../../firebase/bananart/game/entry/models';
import {
  allEntryAnswersRevealed,
  areAllEntryGuessesSubmitted,
  areAllEntryVotesSubmitted,
  calculateEntryAnswerRevealTimerDuration,
  doesEntryHaveDrawingsSubmitted,
  getAnswerKey,
  getEntryAnswerByRevealIndex,
  getEntryKey,
  getNextEntryAnswerIndex,
  hasEntryScoresBeenCalculated,
  isEntryFinalEntryInGame,
  isFinalEntryInRound,
} from '../../../firebase/bananart/game/entry/state';
import { entryStates } from '../../../firebase/bananart/game/entry/models';
import {
  generateAndSetEntryAnswers,
  generateAndSetEntryAnswersRevealOrder,
  setEntryAnswerRevealIndex,
  setEntryMilestoneScoresCalculated,
  setEntryState,
  submitBotsGuesses,
  submitBotsVotes,
  updateUserScores,
} from '../../../firebase/bananart/game/entry/actions';
import { getSessionKey } from '../../../firebase/bananart/session/state';
import { getGameKey } from '../../../firebase/bananart/game/state';
import { hostLog } from '../../../utils/logging';
import {
  ENTRY_COMPLETED_POST_SCORE_TIMER,
  ENTRY_COMPLETED_TIMER,
  ENTRY_GUESSING_COMPLETED_TIMER,
  ENTRY_GUESSING_INTRO_TIMER,
  ENTRY_GUESSING_TIMER,
  ENTRY_INTRO_TIMER,
  ENTRY_NO_DRAWINGS_TIMER,
  ENTRY_RESULTS_TIMER,
  ENTRY_VOTING_COMPLETED_TIMER,
  ENTRY_VOTING_INTRO_TIMER,
  ENTRY_VOTING_TIMER,
} from '../../../constants/timers';
import { getResolvedTimestamp } from '../utils';
import { setGameCurrentEntry, setGameTimerKey } from '../../../firebase/bananart/game/actions';
import { timeout } from '../../../utils/async';
import type { RoundModel } from '../../../firebase/bananart/game/round/models';
import { setRoundCompleted, startRoundResults } from './round';
import { getRoundKey, getRoundNextEntryKey } from '../../../firebase/bananart/game/round/state';
import { HostGameListeners } from '../listeners';

async function setEntryIntro(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string
) {
  hostLog.log(`Set entry to intro.`);
  const state = entryStates.intro;
  setEntryState(state, entryKey, sessionKey, gameKey);

  const waitDuration = ENTRY_INTRO_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);

  hostLog.log(`Set entry to intro timer has completed, terminating.`);
  terminate(timerKey, 'setEntryIntro');
}

async function setEntryNoDrawings(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string
) {
  hostLog.log(`Set entry to no drawings.`);
  const state = entryStates.noDrawings;
  setEntryState(state, entryKey, sessionKey, gameKey);

  const waitDuration = ENTRY_NO_DRAWINGS_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);

  hostLog.log(`Set entry to no drawings timer has completed, terminating.`);
  terminate(timerKey, 'setEntryNoDrawings');
}

async function setEntryGuessingIntro(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string
) {
  hostLog.log(`Set entry to guessing intro.`);
  const state = entryStates.guessingIntro;
  setEntryState(state, entryKey, sessionKey, gameKey);

  const waitDuration = ENTRY_GUESSING_INTRO_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);

  hostLog.log(`Set entry to guessing intro timer has completed, terminating.`);
  terminate(timerKey, 'setEntryGuessingIntro');
}

async function setEntryGuessing(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  listeners: HostGameListeners,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Set entry to guessing.`);
  const state = entryStates.guessing;
  setEntryState(state, entryKey, sessionKey, gameKey);

  const waitDuration = ENTRY_GUESSING_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  submitBotsGuesses(sessionKey, gameKey, entryKey, game, entry);

  listeners.listenForGuesses(timerKey, sessionKey, gameKey, entryKey);

  await timeout(waitDuration);

  hostLog.log(`Set entry to guessing timer has completed, terminating.`);
  terminate(timerKey, 'setEntryGuessing');
}

async function setEntryGuessingCompleted(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  entry: EntryModel
) {
  hostLog.log(`Set entry to guessing completed.`);
  const state = areAllEntryGuessesSubmitted(entry)
    ? entryStates.guessingSubmitted
    : entryStates.guessingTimesUp;
  setEntryState(state, entryKey, sessionKey, gameKey);

  generateAndSetEntryAnswers(sessionKey, gameKey, entryKey, entry);

  const waitDuration = ENTRY_GUESSING_COMPLETED_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);

  hostLog.log(`Set entry to guessing completed timer has completed, terminating.`);

  terminate(timerKey, 'setEntryGuessingCompleted');
}

async function setEntryVotingIntro(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string
) {
  hostLog.log(`Set entry to voting intro.`);
  const state = entryStates.votingIntro;
  setEntryState(state, entryKey, sessionKey, gameKey);

  const waitDuration = ENTRY_VOTING_INTRO_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);

  hostLog.log(`Set entry to voting intro timer has completed, terminating.`);
  terminate(timerKey, 'setEntryVotingIntro');
}

async function setEntryVoting(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  listeners: HostGameListeners,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Set entry to voting.`);
  const state = entryStates.voting;
  setEntryState(state, entryKey, sessionKey, gameKey);

  const waitDuration = ENTRY_VOTING_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  submitBotsVotes(sessionKey, gameKey, entryKey, game, entry);

  listeners.listenForVotes(timerKey, sessionKey, gameKey, entryKey);

  await timeout(waitDuration);

  hostLog.log(`Set entry to voting timer has completed, terminating.`);
  terminate(timerKey, 'setEntryVoting');
}

async function setEntryVotingCompleted(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  entry: EntryModel
) {
  hostLog.log(`Set entry to voting completed.`);
  const state = areAllEntryVotesSubmitted(entry)
    ? entryStates.votingSubmitted
    : entryStates.votingTimesUp;
  setEntryState(state, entryKey, sessionKey, gameKey);

  const waitDuration = ENTRY_VOTING_COMPLETED_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  generateAndSetEntryAnswersRevealOrder(sessionKey, gameKey, entryKey, entry);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);

  hostLog.log(`Set entry to voting completed timer has completed, terminating.`);

  terminate(timerKey, 'setEntryVotingCompleted');
}

async function setEntryResults(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string
) {
  hostLog.log(`Set entry to results.`);
  const state = entryStates.results;
  setEntryState(state, entryKey, sessionKey, gameKey);

  const waitDuration = ENTRY_RESULTS_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);

  hostLog.log(`Set entry to results timer has completed, terminating.`);

  terminate(timerKey, 'setEntryResults');
}

export function handleEntryStatePending(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Handle entry state pending.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  if (doesEntryHaveDrawingsSubmitted(entry)) {
    setEntryIntro(terminate, sessionKey, gameKey, entryKey);
  } else {
    setEntryNoDrawings(terminate, sessionKey, gameKey, entryKey);
  }
}

async function setEntryCompleted(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  immediateTermination: boolean
) {
  hostLog.log(`Set entry to completed.`);
  const state = entryStates.completed;
  setEntryState(state, entryKey, sessionKey, gameKey);

  let timerKey = '';

  if (!immediateTermination) {
    const waitDuration = ENTRY_COMPLETED_TIMER;

    const resolvedTimestamp = getResolvedTimestamp(waitDuration);

    timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

    await timeout(waitDuration);

    hostLog.log(`Set entry to completed timer has completed, terminating.`);
  }
  terminate(timerKey, 'setEntryCompleted');
}

export function handleEntryStateNoDrawings(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Handle entry state no drawings.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  setEntryCompleted(terminate, sessionKey, gameKey, entryKey, true);
}

export function handleEntryStateIntro(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Handle entry state intro.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  setEntryGuessingIntro(terminate, sessionKey, gameKey, entryKey);
}

export function handleEntryStateGuessingIntro(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel,
  listeners: HostGameListeners
) {
  hostLog.log(`Handle entry state guessing intro.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  setEntryGuessing(terminate, sessionKey, gameKey, entryKey, listeners, game, entry);
}

export function handleEntryStateGuessing(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Handle entry state guessing.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  setEntryGuessingCompleted(terminate, sessionKey, gameKey, entryKey, entry);
}

export function handleEntryStateGuessingCompleted(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Handle entry state guessing completed.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  setEntryVotingIntro(terminate, sessionKey, gameKey, entryKey);
}

export function handleEntryStateVotingIntro(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel,
  listeners: HostGameListeners
) {
  hostLog.log(`Handle entry state voting intro.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  setEntryVoting(terminate, sessionKey, gameKey, entryKey, listeners, game, entry);
}

export function handleEntryStateVoting(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Handle entry state voting.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  setEntryVotingCompleted(terminate, sessionKey, gameKey, entryKey, entry);
}

export function handleEntryStateVotingCompleted(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Handle entry state voting completed.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  setEntryResults(terminate, sessionKey, gameKey, entryKey);
}

async function setEntryToRevealNextAnswer(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  entry: EntryModel
) {
  hostLog.log(`Set entry to reveal next answer.`);

  const nextRevealAnswerIndex = getNextEntryAnswerIndex(entry);

  setEntryAnswerRevealIndex(nextRevealAnswerIndex, sessionKey, gameKey, entryKey);

  const answer = getEntryAnswerByRevealIndex(entry, nextRevealAnswerIndex);

  const answerKey = getAnswerKey(answer);

  const waitDuration = calculateEntryAnswerRevealTimerDuration(answerKey, entry);

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);

  hostLog.log(`Set entry to reveal next answer timer has completed, terminating.`);

  terminate(timerKey, 'setEntryToRevealNextAnswer');
}

export function handleEntryStateResults(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel
) {
  hostLog.log(`Handle entry state results.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);

  if (allEntryAnswersRevealed(entry)) {
    const immediateTermination = isEntryFinalEntryInGame(game, entryKey);
    setEntryCompleted(terminate, sessionKey, gameKey, entryKey, immediateTermination);
  } else {
    setEntryToRevealNextAnswer(terminate, sessionKey, gameKey, entryKey, entry);
  }
}

function startNextEntry(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  game: GameModel,
  round: RoundModel
) {
  hostLog.log(`Starting next entry.`);
  const nextEntryKey = getRoundNextEntryKey(game, round);
  if (!nextEntryKey) {
    throw new Error(`No next entry key found.`);
  }
  setGameCurrentEntry(sessionKey, gameKey, nextEntryKey);
  terminate('', 'startNextEntry');
}

async function calculateEntryScores(
  terminate: TerminateFn,
  sessionKey: string,
  gameKey: string,
  entryKey: string,
  entry: EntryModel,
  game: GameModel
) {
  hostLog.log(`Calculate entry scores.`);

  updateUserScores(sessionKey, gameKey, entryKey, entry, game);

  setEntryMilestoneScoresCalculated(sessionKey, gameKey, entryKey);

  const waitDuration = ENTRY_COMPLETED_POST_SCORE_TIMER;

  const resolvedTimestamp = getResolvedTimestamp(waitDuration);

  const timerKey = setGameTimerKey(sessionKey, gameKey, resolvedTimestamp);

  await timeout(waitDuration);

  hostLog.log(`Calculate entry scores timer has completed, terminating.`);

  terminate(timerKey, 'calculateEntryScores');
}

export function handleEntryStateCompleted(
  terminate: TerminateFn,
  session: SessionModel,
  game: GameModel,
  entry: EntryModel,
  round: RoundModel
) {
  hostLog.log(`Handle entry state completed.`);
  const sessionKey = getSessionKey(session);
  const gameKey = getGameKey(game);
  const entryKey = getEntryKey(entry);
  const roundKey = getRoundKey(round);

  if (!hasEntryScoresBeenCalculated(entry)) {
    calculateEntryScores(terminate, sessionKey, gameKey, entryKey, entry, game);
    return;
  }

  if (isEntryFinalEntryInGame(game, entryKey)) {
    setRoundCompleted(terminate, sessionKey, gameKey, roundKey);
  } else if (isFinalEntryInRound(game, round, entryKey)) {
    startRoundResults(terminate, sessionKey, gameKey, roundKey);
  } else {
    startNextEntry(terminate, sessionKey, gameKey, game, round);
  }
}
